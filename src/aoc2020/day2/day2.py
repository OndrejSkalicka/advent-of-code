import re
from typing import Sequence, Tuple


def load_data(fn: str) -> Sequence[Tuple[int, int, str, str]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            parsed = re.match(r'(\d+)-(\d+) (\w): (\w+)$', line.strip())
            if not parsed:
                raise Exception('Cannot parse line "%s"' % line)

            result.append(
                (
                    int(parsed.group(1)),
                    int(parsed.group(2)),
                    parsed.group(3),
                    parsed.group(4),
                )
            )

    return result


def star1(data: Sequence[Tuple[int, int, str, str]]) -> int:
    """
    >>> star1([(1,3,'a','abcde'), (1,3,'b','cdefg'), (2,9,'c','ccccccccc')])
    2
    """

    valid = 0
    for (min, max, char, password) in data:
        cnt = sum(1 for c in password if c == char)

        if min <= cnt <= max:
            valid += 1

    return valid


def star2(data: Sequence[Tuple[int, int, str, str]]) -> int:
    """
    >>> star2([(1,3,'a','abcde'), (1,3,'b','cdefg'), (2,9,'c','ccccccccc')])
    1
    """

    valid = 0
    for (min, max, char, password) in data:
        matches = 0
        if password[min - 1] == char:
            matches += 1

        if password[max - 1] == char:
            matches += 1

        if matches == 1:
            valid += 1

    return valid


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 2 Star 1: %d' % star1(data))
    print('Day 2 Star 2: %d' % star2(data))
