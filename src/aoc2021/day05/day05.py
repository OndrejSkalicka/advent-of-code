import os
from typing import List, Tuple


def load_data(fn: str) -> List[Tuple[int, int, int, int]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            pairs = line.split(" -> ")
            x1, y1 = list(int(i) for i in pairs[0].split(","))
            x2, y2 = list(int(i) for i in pairs[1].split(","))

            result.append((
                x1, y1, x2, y2
            ))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[Tuple[int, int, int, int]]) -> int:
    """
    >>> star1(test_data())
    5
    """

    grid = dict()

    for (x1, y1, x2, y2) in data:
        if x1 == x2:
            for y in range(min(y1, y2), max(y1, y2) + 1):
                grid[(x1, y)] = grid.get((x1, y), 0) + 1

        elif y1 == y2:
            for x in range(min(x1, x2), max(x1, x2) + 1):
                grid[(x, y1)] = grid.get((x, y1), 0) + 1

    return len(list(x for x in grid.values() if x >= 2))


def star2(data: List[Tuple[int, int, int, int]]) -> int:
    """
    >>> star2(test_data())
    12
    """

    grid = dict()

    for (x1, y1, x2, y2) in data:

        dx = (x1 - x2) / abs(x1 - x2) if x1 != x2 else 0
        dy = (y1 - y2) / abs(y1 - y2) if y1 != y2 else 0

        x = x1
        y = y1
        grid[(x, y)] = grid.get((x, y), 0) + 1

        while x != x2 or y != y2:
            x -= dx
            y -= dy
            grid[(x, y)] = grid.get((x, y), 0) + 1

    return len(list(x for x in grid.values() if x >= 2))


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 5 Star 1: %d' % star1(data))
    print('Day 5 Star 2: %d' % star2(data))
