import numpy as np
import numpy.typing as npt

np_array_2d_str = npt.NDArray[np.str_]
np_array_2d_int = npt.NDArray[np.int_]
