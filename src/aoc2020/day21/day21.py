import os
from typing import Dict, Set
from typing import List


class Food:

    def __init__(self, ingredients: Set[str], allergens: Set[str]) -> None:
        self.ingredients = ingredients
        self.allergens = allergens


def load_data(fn: str) -> List[Food]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            (ingredients, allergens) = line[:-1].split(" (contains ")

            result.append(Food(set(ingredients.split(" ")), set(allergens.split(", "))))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def solve_allergens(data: List[Food]) -> Dict[str, str]:
    # reduction algorithm, part 1. For every ingredient, keep list of 'possible options', eg. 'at most those'

    knowledge = {}
    for food in data:
        for allergen in food.allergens:
            # first time seeing this allergen, no reduction, just put all options
            if allergen not in knowledge:
                knowledge[allergen] = food.ingredients.copy()
                continue

            # already got it, reduce
            knowledge[allergen] = knowledge.get(allergen).intersection(food.ingredients)

    # reduction algorithm, part 2. For every alergen, if we KNOW what ingredient it is in, remove that option from other
    # ingredients.

    dirty = True

    while dirty:
        dirty = False
        for allergen, ingredients in knowledge.items():
            # fixed hit, remove ingredients from others
            if len(ingredients) == 1:
                single_ingredient = list(ingredients)[0]
                for other_allergen, other_ingredients in knowledge.items():
                    if allergen == other_allergen:
                        # do not modify self :)
                        continue

                    if single_ingredient in other_ingredients:
                        dirty = True
                        other_ingredients.remove(single_ingredient)

    return {k: list(v)[0] for k, v in knowledge.items()}


def star1(data: List[Food]) -> int:
    """
    >>> star1(test_data())
    5
    """

    allergens = solve_allergens(data)
    ingredients_with_allergens = allergens.values()

    result = 0
    for food in data:
        for ingredient in food.ingredients:
            if ingredient not in ingredients_with_allergens:
                result += 1

    return result


def star2(data: List[Food]) -> str:
    """
    >>> star2(test_data())
    'mxmxvkd,sqjhc,fvjkl'
    """

    allergens_pairs = list((k, v) for k, v in solve_allergens(data).items())
    allergens_pairs.sort(key=lambda x: x[0])
    return ','.join(i[1] for i in allergens_pairs)


if __name__ == "__main__":
    print('Day 21 Star 1: %d' % star1(load_data('data.txt')))
    print('Day 21 Star 2: %s' % star2(load_data('data.txt')))
