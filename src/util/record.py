from collections.abc import Sequence
from dataclasses import dataclass, field
from functools import total_ordering
from typing import Generator, Any, Generic, TypeVar, Union, Iterator, Callable

T = TypeVar('T')


@total_ordering
@dataclass(frozen=True)
class Xy:
    x: int
    y: int

    def __getitem__(self, axis: int) -> int:
        """
        >>> Xy(3, 1)[1]
        1
        """
        if axis == 0:
            return self.x
        elif axis == 1:
            return self.y
        else:
            raise Exception(f"Illegal access {axis}")

    def __add__(self, other: 'Xy') -> 'Xy':
        """
        >>> Xy(3, 5) + Xy(4, 7)
        Xy(x=7, y=12)

        >>> a = Xy(3, 5); a += Xy(4, 7); a
        Xy(x=7, y=12)
        """

        return Xy(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'Xy') -> 'Xy':
        """
        >>> Xy(3, 5) - Xy(4, 7)
        Xy(x=-1, y=-2)
        """

        return Xy(self.x - other.x, self.y - other.y)

    def __mul__(self, other: int) -> 'Xy':
        """
        >>> Xy(3, -7) * 3
        Xy(x=9, y=-21)
        """
        return Xy(self.x * other, self.y * other)

    def __rmul__(self, other: int) -> 'Xy':
        """
        >>> 3 * Xy(3, -7)
        Xy(x=9, y=-21)
        """
        return Xy(self.x * other, self.y * other)

    def __lt__(self, other: 'Xy') -> bool:
        return (self.x, self.y) < (other.x, other.y)

    def np_yx(self) -> tuple[int, int]:
        """
        Shorthand for numpy-index
        """
        return self.y, self.x

    def in_np_shape(self, shape: tuple[int, ...]) -> bool:
        assert len(shape) == 2

        return 0 <= self.x < shape[1] and 0 <= self.y < shape[0]

    def adjacent_np(self, shape: tuple[int, ...]) -> Generator['Xy', None, None]:
        assert len(shape) == 2

        for dxy in Xy.cardinal_directions():
            if (self + dxy).in_np_shape(shape):
                yield self + dxy

    def adjacent(self, data: Sequence[Sequence[Any]]) -> Generator['Xy', None, None]:
        shape_np = len(data), len(data[0])
        for dxy in Xy.cardinal_directions():
            if (self + dxy).in_np_shape(shape_np):
                yield self + dxy

    def adjacent_diagonal(self, data: Sequence[Sequence[Any]]) -> Generator['Xy', None, None]:
        shape_np = len(data), len(data[0])
        for dxy in Xy.cardinal_directions_diagonal():
            if (self + dxy).in_np_shape(shape_np):
                yield self + dxy

    def manhattan(self, other: 'Xy') -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)

    def rot_cw(self) -> 'Xy':
        """
        >>> NORTH.rot_cw() == EAST
        True
        >>> EAST.rot_cw() == SOUTH
        True
        >>> SOUTH.rot_cw() == WEST
        True
        >>> WEST.rot_cw() == NORTH
        True
        >>> NORTH_EAST.rot_cw() == SOUTH_EAST
        True
        """
        return Xy(-self.y, self.x)

    def rot_ccw(self) -> 'Xy':
        """
        >>> NORTH.rot_ccw() == WEST
        True
        >>> EAST.rot_ccw() == NORTH
        True
        >>> SOUTH.rot_ccw() == EAST
        True
        >>> WEST.rot_ccw() == SOUTH
        True
        >>> NORTH_EAST.rot_ccw() == NORTH_WEST
        True
        """
        return Xy(self.y, -self.x)

    @staticmethod
    def iter(data: Sequence[Sequence[Any]]) -> Generator['Xy', None, None]:
        for y in range(len(data)):
            for x in range(len(data[0])):
                yield Xy(x, y)

    @staticmethod
    def iter_np_shape(shape: tuple[int, ...]) -> Generator['Xy', None, None]:
        assert len(shape) == 2

        for x in range(shape[1]):
            for y in range(shape[0]):
                yield Xy(x, y)

    @staticmethod
    def cardinal_directions() -> list['Xy']:
        return [
            Xy(0, -1),
            Xy(1, 0),
            Xy(0, 1),
            Xy(-1, 0),
        ]

    @staticmethod
    def cardinal_directions_diagonal() -> list['Xy']:
        return [
            Xy(0, -1),
            Xy(1, 0),
            Xy(0, 1),
            Xy(-1, 0),
            Xy(1, 1),
            Xy(1, -1),
            Xy(-1, 1),
            Xy(-1, -1),
        ]


NORTH = Xy(0, -1)
SOUTH = Xy(0, 1)
EAST = Xy(1, 0)
WEST = Xy(-1, 0)
NORTH_EAST = Xy(1, -1)
SOUTH_EAST = Xy(1, 1)
NORTH_WEST = Xy(-1, -1)
SOUTH_WEST = Xy(-1, 1)


@dataclass
class XyMutable:
    x: int = 0
    y: int = 0

    def __getitem__(self, axis: int) -> int:
        """
        >>> XyMutable(3, 1)[1]
        1
        """
        if axis == 0:
            return self.x
        elif axis == 1:
            return self.y
        else:
            raise Exception(f"Illegal access {axis}")

    def __setitem__(self, axis: int, data: int) -> None:
        """
        >>> a = XyMutable(3, 1);a[0] = 5; a[0]
        5
        """
        if axis == 0:
            self.x = data
        elif axis == 1:
            self.y = data
        else:
            raise Exception(f"Illegal access {axis}")

    def __add__(self, other: 'XyMutable') -> 'XyMutable':
        """
        >>> XyMutable(3, 5) + XyMutable(4, 7)
        XyMutable(x=7, y=12)

        >>> a = XyMutable(3, 5); a += XyMutable(4, 7); a
        XyMutable(x=7, y=12)
        """
        assert isinstance(other, XyMutable)

        return XyMutable(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'XyMutable') -> 'XyMutable':
        """
        >>> XyMutable(3, 5) - XyMutable(4, 7)
        XyMutable(x=-1, y=-2)
        """
        assert isinstance(other, XyMutable)

        return XyMutable(self.x - other.x, self.y - other.y)


@dataclass
class Cell(Generic[T]):
    xy: Xy
    value: T


@dataclass
class Grid(Generic[T]):
    data: list[list[T]]
    width: int = field(init=False)
    height: int = field(init=False)

    def __post_init__(self) -> None:
        self.width = len(self.data[0])
        self.height = len(self.data)

        for row in self.data:
            assert len(row) == self.width

    def __getitem__(self, index: Union[Xy, tuple[int, int]]) -> T:
        if isinstance(index, Xy):
            return self.data[index.y][index.x]

        assert isinstance(index, tuple)
        assert len(index) == 2

        return self.data[index[1]][index[0]]

    def __setitem__(self, index: Union[Xy, tuple[int, int]], value: T) -> None:
        if isinstance(index, Xy):
            self.data[index.y][index.x] = value

        else:
            assert isinstance(index, tuple)
            assert len(index) == 2

            self.data[index[1]][index[0]] = value

    def rows(self) -> Iterator[list[T]]:
        for row in self.data:
            yield row

    def adjacent(self, xy: Xy) -> Generator[Xy, None, None]:
        for dxy in Xy.cardinal_directions():
            adjacent = xy + dxy
            if adjacent in self:
                yield adjacent

    def adjacent_diagonal(self, xy: Xy) -> Generator[Xy, None, None]:
        for dxy in Xy.cardinal_directions_diagonal():
            adjacent = xy + dxy
            if adjacent in self:
                yield adjacent

    def contains_xy(self, xy: Xy) -> bool:
        return xy in self

    def __iter__(self) -> Iterator[Cell[T]]:
        for y, row in enumerate(self.data):
            for x, value in enumerate(row):
                yield Cell(Xy(x, y), value)

    def find_all_xy(self, condition: Union[Callable[[T], bool], T]) -> list[Cell[T]]:
        result = []
        for y, row in enumerate(self.data):
            for x, value in enumerate(row):
                if isinstance(condition, Callable):  # type: ignore
                    if condition(value):  # type: ignore
                        result.append(Cell(Xy(x, y), value))
                elif condition == value:
                    result.append(Cell(Xy(x, y), value))

        return result

    def __contains__(self, xy: Xy) -> bool:
        """
        >>> Xy(0, 0) in Grid.from_list_str(["..", ".."])
        True

        >>> Xy(2, 0) in Grid.from_list_str(["..", ".."])
        False

        >>> Xy(0, -1) in Grid.from_list_str(["..", ".."])
        False
        """

        return 0 <= xy.x < self.width and 0 <= xy.y < self.height

    def __repr__(self) -> str:
        return f'Grid {self.width} * {self.height} {type(self.data[0][0])}\n' \
               '+' + '-' * self.width + '+\n' + \
            '\n'.join('|' + ''.join(str(element) for element in row) + '|' for row in self.data) + \
            '\n+' + '-' * self.width + '+'

    @staticmethod
    def from_list_str(data: list[str]) -> 'Grid[str]':
        return Grid(list(list(row) for row in data))

    @staticmethod
    def from_list_str_to_int(data: list[str]) -> 'Grid[int]':
        return Grid(list(list(int(e) for e in row) for row in data))

    @staticmethod
    def constant(value: T, width: int, height: int) -> 'Grid[T]':
        return Grid(list(list(value for _ in range(width)) for _ in range(height)))

    def get_or_default(self, xy: Xy, default: T) -> T:
        if xy in self:
            return self[xy]

        return default


@dataclass
class FastGrid(Generic[T]):
    data: list[list[T]]
    width: int = field(init=False)
    height: int = field(init=False)

    def __post_init__(self) -> None:
        self.width = len(self.data[0])
        self.height = len(self.data)

        for row in self.data:
            assert len(row) == self.width

    def __contains__(self, xy: tuple[int, int]) -> bool:
        assert len(xy) == 2

        return 0 <= xy[0] < self.width and 0 <= xy[1] < self.height

    def __getitem__(self, index: tuple[int, int]) -> T:
        assert len(index) == 2

        return self.data[index[1]][index[0]]

    @staticmethod
    def from_list_str(data: list[str]) -> 'FastGrid[str]':
        return FastGrid(list(list(row) for row in data))

    @staticmethod
    def from_list_str_to_int(data: list[str]) -> 'FastGrid[int]':
        return FastGrid(list(list(int(e) for e in row) for row in data))

    @staticmethod
    def constant(value: T, width: int, height: int) -> 'FastGrid[T]':
        return FastGrid(list(list(value for _ in range(width)) for _ in range(height)))

    def __repr__(self) -> str:
        return f'FastGrid {self.width} * {self.height} {type(self.data[0][0])}\n' \
               '+' + '-' * self.width + '+\n' + \
            '\n'.join('|' + ''.join(str(element) for element in row) + '|' for row in self.data) + \
            '\n+' + '-' * self.width + '+'
