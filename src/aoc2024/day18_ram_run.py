from typing import Generator

from util.data import load_strings_newline, txt_fn
from util.dijkstra import dijkstra
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str], width: int = 71, height: int = 71, steps: int = 1024) -> int:
    """
    >>> star1([
    ...        "5,4",
    ...        "4,2",
    ...        "4,5",
    ...        "3,0",
    ...        "2,1",
    ...        "6,3",
    ...        "2,4",
    ...        "1,5",
    ...        "0,6",
    ...        "3,3",
    ...        "2,6",
    ...        "5,1",
    ...        "1,2",
    ...        "5,5",
    ...        "2,5",
    ...        "6,5",
    ...        "1,4",
    ...        "0,4",
    ...        "6,4",
    ...        "1,1",
    ...        "6,1",
    ...        "1,0",
    ...        "0,5",
    ...        "1,6",
    ...        "2,0",
    ...       ], 7, 7, 12)
    22
    """

    grid = Grid.constant('.', width, height)
    input_bytes = list(Xy(int(row.split(',')[0]), int(row.split(',')[1])) for row in data)
    destination = Xy(width - 1, height - 1)

    for i in range(steps):
        grid[input_bytes[i]] = '#'

    distances, _ = dijkstra(Xy(0, 0), lambda node: _adj(node, grid), lambda node: node == destination)

    return distances[destination]


def star2(data: list[str], width: int = 71, height: int = 71) -> str:
    """
    >>> star2([
    ...        "5,4",
    ...        "4,2",
    ...        "4,5",
    ...        "3,0",
    ...        "2,1",
    ...        "6,3",
    ...        "2,4",
    ...        "1,5",
    ...        "0,6",
    ...        "3,3",
    ...        "2,6",
    ...        "5,1",
    ...        "1,2",
    ...        "5,5",
    ...        "2,5",
    ...        "6,5",
    ...        "1,4",
    ...        "0,4",
    ...        "6,4",
    ...        "1,1",
    ...        "6,1",
    ...        "1,0",
    ...        "0,5",
    ...        "1,6",
    ...        "2,0",
    ...       ], 7, 7)
    '6,1'
    """

    input_bytes = list(Xy(int(row.split(',')[0]), int(row.split(',')[1])) for row in data)
    destination = Xy(width - 1, height - 1)

    left = 0
    right = len(input_bytes) - 1
    while right - left > 1:
        middle = (left + right) // 2
        grid = Grid.constant('.', width, height)

        for i in range(middle):
            grid[input_bytes[i]] = '#'

        distances, _ = dijkstra(Xy(0, 0), lambda node: _adj(node, grid), lambda node: node == destination)

        if destination in distances:
            left = middle
        else:
            right = middle

    return f'{input_bytes[left].x},{input_bytes[left].y}'


def _adj(node: Xy, grid: Grid[str]) -> Generator[tuple[Xy, int], None, None]:
    for xy in grid.adjacent(node):
        if grid[xy] == '.':
            yield xy, 1


if __name__ == "__main__":
    run_day(18,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
