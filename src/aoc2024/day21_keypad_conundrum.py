from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Xy

kb_arrows = {
    'PANIC': Xy(0, 0),
    '^': Xy(1, 0),
    'A': Xy(2, 0),
    '<': Xy(0, 1),
    'v': Xy(1, 1),
    '>': Xy(2, 1),
}

kb_numbers = {
    '7': Xy(0, 0),
    '8': Xy(1, 0),
    '9': Xy(2, 0),

    '4': Xy(0, 1),
    '5': Xy(1, 1),
    '6': Xy(2, 1),

    '1': Xy(0, 2),
    '2': Xy(1, 2),
    '3': Xy(2, 2),

    'PANIC': Xy(0, 3),
    '0': Xy(1, 3),
    'A': Xy(2, 3),
}


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "029A",
    ...        "980A",
    ...        "179A",
    ...        "456A",
    ...        "379A",
    ...       ])
    126384
    """

    result = 0
    for code in data:
        result += int(code[:-1]) * path_cost(code, 0, 3, {})

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "029A",
    ...        "980A",
    ...        "179A",
    ...        "456A",
    ...        "379A",
    ...       ])
    154115708116294
    """

    result = 0
    for code in data:
        result += int(code[:-1]) * path_cost(code, 0, 26, {})

    return result


def path_cost(path: str, level: int, max_level: int, cache: dict[tuple[str, int], int]) -> int:
    if level == max_level:
        return len(path)

    cache_key = (path, level)
    if cache_key in cache:
        return cache[cache_key]

    best_cost = 0
    for parent_path in path_to_parent_paths(path):
        parent_path_cost = sum(path_cost(path_fragment, level + 1, max_level, cache) for path_fragment in fragments(parent_path))

        if best_cost == 0 or best_cost > parent_path_cost:
            best_cost = parent_path_cost

    cache[cache_key] = best_cost
    return best_cost


def fragments(path: str) -> list[str]:
    """
    >>> fragments('<A^A>^^AvvvA')
    ['<A', '^A', '>^^A', 'vvvA']
    """
    return list(c + 'A' for c in path.split('A')[:-1])


def path_to_parent_paths(code: str) -> list[str]:
    """
    >>> path_to_parent_paths('029A')
    ['<A^A>^^AvvvA', '<A^A^^>AvvvA']
    """
    for c in '<>^v':
        if c in code:
            arrows = True
            break
    else:
        arrows = False

    current = 'A'
    results = ['']
    for c in code:
        sequences = path_from_to(current, c, arrows)

        new_results = []
        for s in sequences:
            for r in results:
                new_results.append(r + s)
        results = new_results
        current = c

    return results


def path_from_to(current: str, request: str, arrows: bool) -> list[str]:
    """
    >>> path_from_to('A', '0', False)
    ['<A']
    >>> path_from_to('0', '2', False)
    ['^A']
    >>> path_from_to('2', '9', False)
    ['>^^A', '^^>A']
    >>> path_from_to('9', 'A', False)
    ['vvvA']
    >>> path_from_to('A', '1', False)
    ['^<<A']
    >>> path_from_to('1', 'A', False)
    ['>>vA']
    """

    if arrows:
        assert request in kb_arrows
        assert current not in kb_numbers or current == 'A'
        assert request not in kb_numbers or request == 'A'
        keyboard = kb_arrows

    else:
        assert request in kb_numbers
        assert current not in kb_arrows or current == 'A'
        assert request not in kb_arrows or request == 'A'

        keyboard = kb_numbers

    current_xy = keyboard[current]
    request_xy = keyboard[request]

    h_path = '>' * (request_xy.x - current_xy.x) + '<' * (current_xy.x - request_xy.x)
    v_path = 'v' * (request_xy.y - current_xy.y) + '^' * (current_xy.y - request_xy.y)

    hfs_path = h_path + v_path + 'A'
    vfs_path = v_path + h_path + 'A'

    # hfs panic
    if keyboard['PANIC'] == Xy(request_xy.x, current_xy.y):
        return [vfs_path]

    # vfs panic
    if keyboard['PANIC'] == Xy(current_xy.x, request_xy.y):
        return [hfs_path]

    if hfs_path == vfs_path:
        return [hfs_path]

    return [hfs_path, vfs_path]


if __name__ == "__main__":
    run_day(21,
            lambda: star1(load_strings_newline(txt_fn(__file__))),  # 128962
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
