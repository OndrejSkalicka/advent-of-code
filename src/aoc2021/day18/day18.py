import ast
import os
from collections import deque
from typing import Deque, Tuple, List
from typing import Optional


class Snail:

    def __init__(self, left=None, right=None, literal=None) -> None:
        self.left = left  # type: Optional[Snail]
        self.right = right  # type: Optional[Snail]
        self.literal = literal  # type: Optional[int]

    def is_literal(self):
        return self.literal is not None

    def magnitude(self):
        """
        >>> bs([[1,2],[[3,4],5]]).magnitude()
        143
        >>> bs([[[[0,7],4],[[7,8],[6,0]]],[8,1]]).magnitude()
        1384
        >>> bs([[[[1,1],[2,2]],[3,3]],[4,4]]).magnitude()
        445
        >>> bs([[[[3,0],[5,3]],[4,4]],[5,5]]).magnitude()
        791
        >>> bs([[[[5,0],[7,4]],[5,5]],[6,6]]).magnitude()
        1137
        >>> bs([[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]).magnitude()
        3488
        """
        if self.is_literal():
            return self.literal

        return self.left.magnitude() * 3 + self.right.magnitude() * 2

    def __repr__(self) -> str:
        if self.is_literal():
            return '%d' % self.literal

        return '[%s,%s]' % (self.left, self.right)


def load_data(fn: str = 'data.txt'):
    result = []

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            as_list = ast.literal_eval(line)
            result.append(as_list)

    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(values) -> int:
    snails = list(bs(x) for x in values)
    last = add_many(snails)
    return last.magnitude()


def star2(values) -> int:
    """
    >>> star2(test_data())
    3993
    """
    best = 0
    for a in values:
        for b in values:
            if a == b:
                continue

            magnitude = add_many([bs(a), bs(b)]).magnitude()
            if magnitude > best:
                best = magnitude

    return best


def add_many(snails: List[Snail]) -> Snail:
    """
    >>> add_many([bs([1,1]), bs([2,2]), bs([3,3]), bs([4,4])])
    [[[[1,1],[2,2]],[3,3]],[4,4]]
    >>> add_many([bs([1,1]), bs([2,2]), bs([3,3]), bs([4,4]), bs([5,5])])
    [[[[3,0],[5,3]],[4,4]],[5,5]]
    >>> add_many([bs([1,1]), bs([2,2]), bs([3,3]), bs([4,4]), bs([5,5]), bs([6,6])])
    [[[[5,0],[7,4]],[5,5]],[6,6]]
    >>> add_many([bs([[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]),
    ...           bs([7,[[[3,7],[4,3]],[[6,3],[8,8]]]]),
    ...           bs([[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]),
    ...           bs([[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]),
    ...           bs([7,[5,[[3,8],[1,4]]]]),
    ...           bs([[2,[2,2]],[8,[8,1]]]),
    ...           bs([2,9]),
    ...           bs([1,[[[9,3],9],[[9,0],[0,7]]]]),
    ...           bs([[[5,[7,4]],7],1]),
    ...           bs([[[[4,2],2],6],[8,7]])])
    [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
    """

    active = snails[0]
    for i in range(1, len(snails)):
        active = add(active, snails[i])

    return active


def add(left: Snail, right: Snail):
    """
    >>> add(bs([[[[4,3],4],4],[7,[[8,4],9]]]), bs([1,1]))
    [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
    >>> add(bs([[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]), bs([7,[[[3,7],[4,3]],[[6,3],[8,8]]]]))
    [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
    """
    root = Snail(left=left, right=right)
    reduce(root)
    return root


def reduce(root: Snail):
    """
    >>> x=bs([[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]);reduce(x);print(x)
    [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
    """
    while True:
        split = False
        exploded = try_explode(root)

        if not exploded:
            split = try_split(root)

        if not split and not exploded:
            break


def bs(input) -> Snail:
    if type(input) is list:
        return Snail(
            left=bs(input[0]),
            right=bs(input[1]),
        )

    return Snail(literal=input)


def try_explode(root):
    """
    >>> x = bs([[[[[9, 8], 1], 2], 3], 4]); try_explode(x); print(x)
    True
    [[[[0,9],2],3],4]
    >>> x = bs([7, [6, [5, [4, [3, 2]]]]]); try_explode(x); print(x)
    True
    [7,[6,[5,[7,0]]]]
    >>> x = bs([[6, [5, [4, [3, 2]]]], 1]); try_explode(x); print(x)
    True
    [[6,[5,[7,0]]],3]
    >>> x = bs([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]); try_explode(x); print(x)
    True
    [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
    >>> x = bs([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]); try_explode(x); print(x)
    True
    [[3,[2,[8,0]]],[9,[5,[7,0]]]]
    """
    buffer = deque()  # type: Deque[Tuple[Snail, int]]
    buffer.appendleft((root, 0))

    prev = None
    add_right = None

    while buffer:
        snail, level = buffer.popleft()

        if level >= 4 and not snail.is_literal() and snail.left.is_literal() and snail.right.is_literal():
            if prev is not None:
                prev.literal += snail.left.literal

            add_right = snail.right.literal

            snail.literal = 0
            snail.left = None  # type: Optional[Snail]
            snail.right = None  # type: Optional[Snail]
            break

        if snail.is_literal():
            prev = snail

        else:
            buffer.appendleft((snail.right, level + 1))
            buffer.appendleft((snail.left, level + 1))

    if add_right is not None:
        while buffer:
            snail, level = buffer.popleft()
            if snail.is_literal():
                snail.literal += add_right
                break

            buffer.appendleft((snail.right, level + 1))
            buffer.appendleft((snail.left, level + 1))

    return add_right is not None


def try_split(root: Snail):
    """
    >>> x = bs([[[[0,7],4],[15,[0,13]]],[1,1]]); try_split(x); print(x)
    True
    [[[[0,7],4],[[7,8],[0,13]]],[1,1]]
    >>> x = bs([[[[0,7],4],[[7,8],[0,13]]],[1,1]]); try_split(x); print(x)
    True
    [[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]
    """

    buffer = deque()  # type: Deque[Snail]
    buffer.appendleft(root)

    while buffer:
        snail = buffer.popleft()

        if snail.is_literal() and snail.literal >= 10:
            snail.left = Snail(literal=snail.literal // 2)
            snail.right = Snail(literal=snail.literal - snail.left.literal)
            snail.literal = None

            return True

        if not snail.is_literal():
            buffer.appendleft(snail.right)
            buffer.appendleft(snail.left)

    return False


if __name__ == "__main__":
    print('Day 18 Star 1: %d' % star1(load_data()))
    print('Day 18 Star 2: %d' % star2(load_data()))
