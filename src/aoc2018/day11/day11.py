from typing import List, Sequence


def star_1(serial: int):
    """
    >>> star_1(18)
    '33,45'

    >>> star_1(42)
    '21,61'
    """

    best = find_best(serial, (3,))
    return '%d,%d' % (best[0] + 1, best[1] + 1)  # +1 for stupid 'indexed by 1'


def star_2(serial: int):
    """
    >>> star_2(18)
    '90,269,16'

    >>> star_2(42)
    '232,251,12'
    """
    best = find_best(serial, range(1, 301))
    return '%d,%d,%d' % (best[0] + 1, best[1] + 1, best[2])  # +1 for stupid 'indexed by 1'


def find_best(serial: int, sizes: Sequence[int] = (3,)):
    grid = create_grid(serial)
    sat = summed_area_table(grid)
    best = None
    result = None

    for size in sizes:
        for x in range(0, 301 - size):
            for y in range(0, 301 - size):
                total = sat[x + size - 1][y + size - 1]

                if y > 0:
                    total -= sat[x + size - 1][y - 1]
                if x > 0:
                    total -= sat[x - 1][y + size - 1]
                if x > 0 and y > 0:
                    total += sat[x - 1][y - 1]

                if best is None or total > best:
                    best = total
                    result = (x, y, size)

    return result


def summed_area_table(grid: List[List[int]]) -> List[List[int]]:
    """
    >>> summed_area_table([[31, 12, 13, 24, 30, 1], [2, 26, 17, 23, 8, 35], [4, 9, 21, 15, 28, 34], \
[33, 10, 22, 16, 27, 3], [5, 29, 20, 14, 11, 32], [36, 25, 18, 19, 7, 6]])
    [[31, 43, 56, 80, 110, 111], [33, 71, 101, 148, 186, 222], [37, 84, 135, 197, 263, 333], \
[70, 127, 200, 278, 371, 444], [75, 161, 254, 346, 450, 555], [111, 222, 333, 444, 555, 666]]

    See https://en.wikipedia.org/wiki/Summed-area_table
    """
    w = len(grid)
    result = [[0 for y in range(w)] for x in range(w)]

    for y in range(0, w):
        for x in range(0, w):
            result[x][y] = grid[x][y]
            if y > 0:
                result[x][y] += result[x][y - 1]
            if x > 0:
                result[x][y] += result[x - 1][y]
            if x > 0 and y > 0:
                result[x][y] -= result[x - 1][y - 1]

    return result


def create_grid(serial) -> List[List[int]]:
    grid = [[0 for y in range(300)] for x in range(300)]

    for x in range(300):
        for y in range(300):
            grid[x][y] = power_level(x + 1, y + 1, serial)

    return grid


def power_level(x, y, serial):
    """
    >>> power_level(3, 5, 8)
    4

    >>> power_level(122, 79, 57)
    -5

    >>> power_level(217, 196, 39)
    0

    >>> power_level(101, 153, 71)
    4

    """
    rack_id = x + 10
    power_level = rack_id * y + serial
    power_level *= rack_id
    power_level = int(power_level / 100) % 10
    return power_level - 5


if __name__ == "__main__":
    print(star_1(1718))
    print(star_2(1718))
