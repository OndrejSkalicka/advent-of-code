import os
from typing import List, Optional


def load_data(fn: str) -> List[str]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            if not line:
                continue
            result.append(line)

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[str]) -> int:
    """
    >>> star1(test_data())
    198
    """

    gamma_bin = ''
    epsilon_bin = ''
    for i in range(len(data[0])):
        if most_common_at(data, i) == '0':
            gamma_bin += '0'
            epsilon_bin += '1'
        else:
            gamma_bin += '1'
            epsilon_bin += '0'

    return int(gamma_bin, 2) * int(epsilon_bin, 2)


def most_common_at(data: List[str], bit: int) -> Optional[str]:
    zeros = 0
    ones = 0
    for row in data:
        if row[bit] == '0':
            zeros += 1
        elif row[bit] == '1':
            ones += 1
        else:
            raise Exception('Illegal string %s' % row)
    if zeros > ones:
        return '0'
    elif ones > zeros:
        return '1'
    else:
        return None


def star2(data: List[str]) -> int:
    """
    >>> star2(test_data())
    230
    """

    o2_list = data[:]
    co2_list = data[:]

    o2_result = None
    co2_result = None
    for i in range(len(data[0])):
        most_common_o2 = most_common_at(o2_list, i)
        most_common_co2 = most_common_at(co2_list, i)

        if most_common_o2 is None:
            most_common_o2 = '1'

        if most_common_co2 is None:
            most_common_co2 = '1'

        new_o2_list = []
        for row in o2_list:
            if row[i] == most_common_o2:
                new_o2_list.append(row)

        o2_list = new_o2_list
        if len(o2_list) == 1:
            o2_result = o2_list[0]

        new_co2_list = []
        for row in co2_list:
            if row[i] != most_common_co2:
                new_co2_list.append(row)

        co2_list = new_co2_list
        if len(co2_list) == 1:
            co2_result = co2_list[0]

    return int(o2_result, 2) * int(co2_result, 2)


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 2 Star 1: %d' % star1(data))
    print('Day 2 Star 2: %d' % star2(data))
