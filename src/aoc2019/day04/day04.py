from collections import defaultdict


def star1(range_low: int, range_high: int) -> int:
    result = 0
    for pwd in range(100_000, 1_000_000):
        if is_valid(pwd, range_low, range_high):
            result += 1

    return result


def star2(range_low: int, range_high: int) -> int:
    result = 0
    for pwd in range(100_000, 1_000_000):
        if is_valid_2(pwd, range_low, range_high):
            result += 1

    return result


def is_valid(pwd: int, range_low: int = 100_000, range_high: int = 999_999) -> bool:
    """
    >>> is_valid(111111)
    True

    >>> is_valid(223450)
    False

    >>> is_valid(123789)
    False

    >>> is_valid(111111, 111111, 111112)
    True

    >>> is_valid(111111, 111112, 111112)
    False
    """
    pwd_str = str(pwd)
    if not range_low <= pwd <= range_high:
        return False

    for i in range(1, 6):
        if pwd_str[i] < pwd_str[i - 1]:
            return False

    for i in range(1, 6):
        if pwd_str[i] == pwd_str[i - 1]:
            return True

    return False


def is_valid_2(pwd: int, range_low: int = 100_000, range_high: int = 999_999) -> bool:
    """
    >>> is_valid_2(111111)
    False

    >>> is_valid_2(112233)
    True

    >>> is_valid_2(123444)
    False

    >>> is_valid_2(111122)
    True
    """
    pwd_str = str(pwd)
    if not range_low <= pwd <= range_high:
        return False

    for i in range(1, 6):
        if pwd_str[i] < pwd_str[i - 1]:
            return False

    digit_counts: dict[str, int] = defaultdict(int)
    for d in pwd_str:
        digit_counts[d] += 1

    for value in digit_counts.values():
        if value == 2:
            return True

    return False


if __name__ == "__main__":
    print('Day 4 Star 1: %d' % star1(240298, 784956))
    print('Day 4 Star 2: %d' % star2(240298, 784956))
