from util.data import txt_fn, load_strings_newline
from util.perf import run_day
from util.record import Grid, Xy, SOUTH_WEST, SOUTH_EAST, NORTH_WEST, NORTH_EAST


def star1(data: list[str]) -> int:
    """
    >>> star1(["MMMSXXMASM",
    ...        "MSAMXMSMSA",
    ...        "AMXSXMAAMM",
    ...        "MSAMASMSMX",
    ...        "XMASAMXAMM",
    ...        "XXAMMXXAMA",
    ...        "SMSMSASXSS",
    ...        "SAXAMASAAA",
    ...        "MAMMMXMMMM",
    ...        "MXMXAXMASX"])
    18
    """

    grid = Grid.from_list_str(data)
    result = 0

    for cell in grid.find_all_xy(lambda e: e == 'X'):
        for dxy in Xy.cardinal_directions_diagonal():
            if (
                    grid.get_or_default(cell.xy + dxy, '') == 'M' and
                    grid.get_or_default(cell.xy + dxy + dxy, '') == 'A' and
                    grid.get_or_default(cell.xy + dxy + dxy + dxy, '') == 'S'
            ):
                result += 1

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["MMMSXXMASM",
    ...        "MSAMXMSMSA",
    ...        "AMXSXMAAMM",
    ...        "MSAMASMSMX",
    ...        "XMASAMXAMM",
    ...        "XXAMMXXAMA",
    ...        "SMSMSASXSS",
    ...        "SAXAMASAAA",
    ...        "MAMMMXMMMM",
    ...        "MXMXAXMASX"])
    9
    """

    grid = Grid.from_list_str(data)
    result = 0

    for cell in grid.find_all_xy(lambda e: e == 'A'):
        ne = grid.get_or_default(cell.xy + NORTH_EAST, '-')
        se = grid.get_or_default(cell.xy + SOUTH_EAST, '-')
        nw = grid.get_or_default(cell.xy + NORTH_WEST, '-')
        sw = grid.get_or_default(cell.xy + SOUTH_WEST, '-')

        if ne in 'SM' and se in 'SM' and nw in 'SM' and sw in 'SM' and ne != sw and se != nw:
            result += 1

    return result


if __name__ == "__main__":
    run_day(4,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
