from collections import defaultdict
from dataclasses import dataclass, field
from functools import total_ordering

from util.data import load_strings_newline, txt_fn
from util.perf import run_day

CARDS = 'AKQJT98765432'
CARDS_STAR_2 = 'AKQT98765432J'


@total_ordering
@dataclass(frozen=True)
class Hand:
    hand: str
    star2: bool = field(default=False)
    rank: int = field(init=False)

    def __post_init__(self) -> None:
        if self.star2:

            strongest = Hand._hand_rank(self.hand.replace('J', CARDS[0]))
            if 'J' in self.hand:
                for c in CARDS[1:]:
                    candidate = Hand._hand_rank(self.hand.replace('J', c))
                    if candidate < strongest:
                        strongest = candidate

            rank = strongest

        else:
            rank = Hand._hand_rank(self.hand)

        object.__setattr__(self, 'rank', rank)

    @staticmethod
    def _hand_rank(hand: str) -> int:
        assert len(hand) == 5
        sizes: dict[str, int] = defaultdict(int)
        for c in hand:
            sizes[c] += 1

        sizes_values = sorted(sizes.values(), reverse=True)

        if sizes_values[0] == 5:
            return 1  # five of a kind

        elif sizes_values[0] == 4:
            return 2  # four of a kind

        elif sizes_values[0] == 3 and sizes_values[1] == 2:
            return 3  # full house

        elif sizes_values[0] == 3 and sizes_values[1] == 1:
            return 4  # three of a kind

        elif sizes_values[0] == 2 and sizes_values[1] == 2:
            return 5  # two pairs

        elif sizes_values[0] == 2 and sizes_values[1] == 1:
            return 6  # one pair

        elif sizes_values[0] == 1:
            return 7  # high card

        assert False, f'Wrong hand {hand}'

    def __gt__(self, other: 'Hand') -> bool:
        """
        >>> Hand('22222') > Hand('AAAA2')
        True

        >>> Hand('33333') > Hand('22222')
        True

        >>> Hand('33332') > Hand('2AAAA')
        True

        >>> Hand('77888') > Hand('77788')
        True

        >>> Hand('QQQQ2', star2=True) > Hand('JKKK2', star2=True)
        True

        >>> Hand('33332', star2=True) > Hand('JKKK2', star2=True)
        True

        >>> Hand('AJAAA', star2=True) > Hand('A5AAA', star2=True)
        True
        """
        if self.rank != other.rank:
            return self.rank < other.rank

        for i in range(5):
            if self.hand[i] != other.hand[i]:
                if self.star2:
                    return CARDS_STAR_2.index(self.hand[i]) < CARDS_STAR_2.index(other.hand[i])
                return CARDS.index(self.hand[i]) < CARDS.index(other.hand[i])

        assert False

    def __repr__(self) -> str:
        return f'{self.hand} ({self.rank})'


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "32T3K 765",
    ...     "T55J5 684",
    ...     "KK677 28",
    ...     "KTJJT 220",
    ...     "QQQJA 483",
    ... ])
    6440
    """

    parsed: list[tuple[Hand, int]] = []

    for row in data:
        hand = Hand(row.split(' ')[0])
        winning = int(row.split(' ')[1])

        parsed.append((hand, winning))

    parsed.sort()

    result = 0
    for i in range(len(parsed)):
        result += parsed[i][1] * (i + 1)

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "32T3K 765",
    ...     "T55J5 684",
    ...     "KK677 28",
    ...     "KTJJT 220",
    ...     "QQQJA 483",
    ... ])
    5905
    """

    parsed: list[tuple[Hand, int]] = []

    for row in data:
        hand = Hand(row.split(' ')[0], star2=True)
        winning = int(row.split(' ')[1])

        parsed.append((hand, winning))

    parsed.sort()

    result = 0
    for i in range(len(parsed)):
        result += parsed[i][1] * (i + 1)

    return result


if __name__ == "__main__":
    run_day(7,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
