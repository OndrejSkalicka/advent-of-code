import logging

from util.data import load_strings_newline, txt_fn
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


def star1(data: list[str]) -> int:
    """
    >>> star1(["noop",
    ...        "addx 3",
    ...        "addx -5",
    ...        ])
    0

    >>> star1(load_strings_newline(txt_fn(__file__, '.test.txt')))
    13140
    """

    cpu_cycle = 0
    instruction_index = 0
    """
    which instruction to execute
    """
    instruction_step = 0
    """
    how many steps of an execution where finished
    """
    register_x = 1

    signal_strength_sum = 0

    while True:
        cpu_cycle += 1
        if (cpu_cycle + 20) % 40 == 0:
            signal_strength_sum += cpu_cycle * register_x
            logger.debug(f'During CPU cycle {cpu_cycle}, register X = {register_x}')

        instruction = data[instruction_index]

        instruction_finished = True

        if instruction.startswith('addx '):
            instruction_step += 1

            if instruction_step < 2:
                instruction_finished = False
            else:
                register_x += int(instruction.split(' ')[1])
        else:
            assert instruction == 'noop'

        if instruction_finished:
            instruction_index += 1
            instruction_step = 0

        if instruction_index >= len(data):
            break

    return signal_strength_sum


def star2(data: list[str]) -> str:
    """
    >>> star2(load_strings_newline(txt_fn(__file__, '.test.txt')))
    '\
##..##..##..##..##..##..##..##..##..##..\\n\
###...###...###...###...###...###...###.\\n\
####....####....####....####....####....\\n\
#####.....#####.....#####.....#####.....\\n\
######......######......######......####\\n\
#######.......#######.......#######.....\\n'
    """

    cpu_cycle = 0
    instruction_index = 0
    """
    which instruction to execute
    """
    instruction_step = 0
    """
    how many steps of an execution where finished
    """
    register_x = 1

    crt = [[False for x in range(40)] for y in range(6)]

    while True:
        if abs(cpu_cycle % 40 - register_x) < 2:
            crt[cpu_cycle // 40][cpu_cycle % 40] = True

        cpu_cycle += 1

        instruction = data[instruction_index]

        instruction_finished = True

        if instruction.startswith('addx '):
            instruction_step += 1

            if instruction_step < 2:
                instruction_finished = False
            else:
                register_x += int(instruction.split(' ')[1])
        else:
            assert instruction == 'noop'

        if instruction_finished:
            instruction_index += 1
            instruction_step = 0

        if instruction_index >= len(data):
            break

    result = ""
    for row in crt:
        for col in row:
            result += '#' if col else '.'
        result += "\n"

    return result


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(10,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
