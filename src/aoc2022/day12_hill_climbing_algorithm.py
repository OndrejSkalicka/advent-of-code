import logging
from typing import Iterable

from util.data import txt_fn, load_strings_newline
from util.dijkstra import dijkstra
from util.logger_config import init_logger
from util.perf import run_day
from util.record import Xy


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "Sabqponm",
    ...        "abcryxxl",
    ...        "accszExk",
    ...        "acctuvwj",
    ...        "abdefghi",
    ...        ])
    31
    """

    return _distances(data)[_find('S', data)]


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "Sabqponm",
    ...        "abcryxxl",
    ...        "accszExk",
    ...        "acctuvwj",
    ...        "abdefghi",
    ...        ])
    29
    """

    distances = _distances(data)

    return min([distances[xy] for xy in Xy.iter(data) if xy in distances and data[xy.y][xy.x] in 'Sa'])


def _distances(data: list[str]) -> dict[Xy, int]:
    end = _find('E', data)

    return dijkstra(end, lambda node: _adjacent(node, data))[0]


def _adjacent(node: Xy, data: list[str]) -> Iterable[tuple[Xy, int]]:
    for new_node in node.adjacent(data):
        if _elevation(node, data) <= _elevation(new_node, data) + 1:
            yield new_node, 1


def _elevation(node: Xy, data: list[str]) -> int:
    return ord(data[node.y][node.x].replace('E', 'z').replace('S', 'a'))


def _find(value: str, data: list[str]) -> Xy:
    for y, row in enumerate(data):
        for x, c in enumerate(row):
            if c == value:
                return Xy(x, y)
    raise Exception(f"{value} not found in {data}")


if __name__ == "__main__":
    init_logger(logging.INFO)
    run_day(12,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
