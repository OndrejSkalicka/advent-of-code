from typing import List, Tuple


def load_data(fn: str) -> List[Tuple[str, int]]:
    with open(fn, 'r') as fh:
        return list((line.split(' ')[0], int(line.split(' ')[1])) for line in fh.readlines())


def star1(data: List[Tuple[str, int]]) -> int:
    """
    >>> star1([
    ...     ('nop', 0),
    ...     ('acc', 1),
    ...     ('jmp', 4),
    ...     ('acc', 3),
    ...     ('jmp', -3),
    ...     ('acc', -99),
    ...     ('acc', 1),
    ...     ('jmp', -4),
    ...     ('acc', 6),
    ... ])
    5
    """

    return run_program(data)[1]


def star2(data: List[Tuple[str, int]]) -> int:
    """
    >>> star2([
    ...     ('nop', 0),
    ...     ('acc', 1),
    ...     ('jmp', 4),
    ...     ('acc', 3),
    ...     ('jmp', -3),
    ...     ('acc', -99),
    ...     ('acc', 1),
    ...     ('jmp', -4),
    ...     ('acc', 6),
    ... ])
    8
    """

    for i in range(len(data)):
        inst = data[i][0]
        if inst == 'acc':
            # cannot switch acc
            continue

        if inst == 'jmp':
            switch = 'nop'
        else:
            switch = 'jmp'

        modified = data[:]
        modified[i] = (switch, modified[i][1])

        result = run_program(modified)
        if result[0]:
            return result[1]


def run_program(data: List[Tuple[str, int]]) -> Tuple[bool, int]:
    acc = 0
    op = 0
    ops = set()
    while True:
        # already processed! Fail + acct
        if op in ops:
            return False, acc

        # succeeded! OK + acct
        if op == len(data):
            return True, acc

        ops.add(op)

        inst, value = data[op]

        if inst == 'jmp':
            op += value
            continue

        # non-jmp jumps by +1
        op += 1
        if inst == 'nop':
            continue

        if inst == 'acc':
            acc += value
            continue


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 8 Star 1: %d' % star1(my_data))
    print('Day 8 Star 2: %d' % star2(my_data))
