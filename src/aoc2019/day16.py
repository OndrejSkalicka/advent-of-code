import logging

from util.data import load_as_str
from util.logger_config import init_logger

logger = logging.getLogger(__name__)


def star1(str_input: str) -> str:
    """
    >>> star1('80871224585914546619083218645595')
    '24176176'
    >>> star1('19617804207202209144916044189917')
    '73745418'
    >>> star1('69317163492948606335995924319873')
    '52432133'
    """

    current_input = list(int(x) for x in str_input)
    logger.info(f"Working on a pattern with length {len(current_input)}")

    for phase in range(100):
        new_input: list[int] = []
        for row in range(len(current_input)):
            row_sum = 0
            for col in range(len(current_input)):
                row_sum += current_input[col] * pattern_at(col, row)

            new_input.append(int(str(row_sum)[-1]))

        current_input = new_input

        if phase % 10 == 0:
            logger.debug(f"Passed {phase} phases")

    return ''.join(str(x) for x in current_input[:8])


def pattern_at(index_in_pattern: int, pattern_level: int) -> int:
    """
    >>> pattern_at(0, 0)
    1
    >>> pattern_at(1, 0)
    0
    >>> pattern_at(2, 0)
    -1
    >>> pattern_at(3, 0)
    0
    >>> pattern_at(4, 0)
    1
    >>> pattern_at(0, 1)
    0
    >>> pattern_at(1, 1)
    1
    >>> pattern_at(2, 1)
    1
    >>> pattern_at(3, 1)
    0
    >>> pattern_at(4, 1)
    0
    >>> pattern_at(5, 1)
    -1
    >>> pattern_at(6, 1)
    -1
    >>> pattern_at(7, 1)
    0
    >>> pattern_at(8, 1)
    0
    >>> pattern_at(9, 1)
    1
    >>> pattern_at(10, 1)
    1
    >>> pattern_at(0, 3)
    0
    >>> pattern_at(1, 3)
    0
    >>> pattern_at(2, 3)
    0
    >>> pattern_at(3, 3)
    1
    >>> pattern_at(4, 3)
    1
    >>> pattern_at(5, 3)
    1
    >>> pattern_at(6, 3)
    1
    >>> pattern_at(7, 3)
    0
    >>> pattern_at(8, 3)
    0
    >>> pattern_at(9, 3)
    0
    >>> pattern_at(10, 3)
    0
    >>> pattern_at(11, 3)
    -1
    """

    base: tuple[int, int, int, int] = 0, 1, 0, -1
    return base[((index_in_pattern + 1) // (pattern_level + 1)) % 4]


if __name__ == "__main__":
    init_logger()

    print('Day 16 Star 1: %s' % star1(load_as_str('day16.txt')))
