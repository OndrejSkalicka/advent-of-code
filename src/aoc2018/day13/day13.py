import os  # noqa
from typing import Tuple, List, Mapping


class Direction:
    UP = (0, -1)
    DOWN = (0, 1)
    RIGHT = (1, 0)
    LEFT = (-1, 0)

    @staticmethod
    def as_str(dxy: Tuple[int, int]):
        if dxy == Direction.UP:
            return '^'

        if dxy == Direction.DOWN:
            return 'v'

        if dxy == Direction.RIGHT:
            return '>'

        if dxy == Direction.LEFT:
            return '<'

        raise Exception('Illegal direction %s' % (dxy, ))


class Cart:
    def __init__(self, x, y, dxy) -> None:
        self.x = x
        self.y = y
        self.dxy = dxy
        self.intersection = 0
        self.broken = False

    def pos(self):
        return self.x, self.y

    def turn_right(self):
        if self.dxy == Direction.RIGHT:
            self.dxy = Direction.DOWN
        elif self.dxy == Direction.DOWN:
            self.dxy = Direction.LEFT
        elif self.dxy == Direction.LEFT:
            self.dxy = Direction.UP
        elif self.dxy == Direction.UP:
            self.dxy = Direction.RIGHT
        else:
            raise Exception('Illegal direction %s' % (self.dxy,))

    def turn_left(self):
        if self.dxy == Direction.RIGHT:
            self.dxy = Direction.UP
        elif self.dxy == Direction.UP:
            self.dxy = Direction.LEFT
        elif self.dxy == Direction.LEFT:
            self.dxy = Direction.DOWN
        elif self.dxy == Direction.DOWN:
            self.dxy = Direction.RIGHT
        else:
            raise Exception('Illegal direction %s' % (self.dxy,))

    def __str__(self) -> str:
        return 'Cart [%d,%d] heading [%d,%d] i %d' % (self.x, self.y, self.dxy[0], self.dxy[1], self.intersection)


def star_1(tracks: Mapping[Tuple[int, int], str], carts: List[Cart]) -> Tuple[int, int]:
    """
    >>> star_1(*load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test.txt')))
    (7, 3)
    """
    tick = 0
    while True:
        carts.sort(key=lambda cart: (cart.y, cart.x))
        positions = set(cart.pos() for cart in carts)

        for cart in carts:
            positions.remove(cart.pos())
            cart.x += cart.dxy[0]
            cart.y += cart.dxy[1]

            pos = cart.pos()
            track = tracks.get(pos)

            if track == ' ' or track is None:
                raise Exception('Derailed at %s' % (pos, ))

            if track == '/':
                if cart.dxy in (Direction.UP, Direction.DOWN):
                    cart.turn_right()
                elif cart.dxy in (Direction.LEFT, Direction.RIGHT):
                    cart.turn_left()
                else:
                    raise Exception('Approached interception %s %s at wrong angle, %s' % (track, pos, cart))

            if track == '\\':
                if cart.dxy in (Direction.UP, Direction.DOWN):
                    cart.turn_left()
                elif cart.dxy in (Direction.LEFT, Direction.RIGHT):
                    cart.turn_right()
                else:
                    raise Exception('Approached interception %s %s at wrong angle, %s' % (track, pos, cart))

            if track == '+':
                if cart.intersection % 3 == 0:
                    cart.turn_left()
                elif cart.intersection % 3 == 2:
                    cart.turn_right()

                cart.intersection += 1

            if pos in positions:
                return pos

            positions.add(pos)

        tick += 1


def star_2(tracks: Mapping[Tuple[int, int], str], carts: List[Cart]) -> Tuple[int, int]:
    """
    >>> star_2(*load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test-2.txt')))
    (6, 4)
    """
    tick = 0
    while True:
        # speed things up a touch
        carts = list(cart for cart in carts if not cart.broken)
        carts.sort(key=lambda cart: (cart.y, cart.x))

        for cart in carts:
            if cart.broken:
                continue

            cart.x += cart.dxy[0]
            cart.y += cart.dxy[1]

            pos = cart.pos()
            track = tracks.get(pos)

            if track == ' ' or track is None:
                raise Exception('Derailed at %s' % (pos, ))

            if track == '/':
                if cart.dxy in (Direction.UP, Direction.DOWN):
                    cart.turn_right()
                elif cart.dxy in (Direction.LEFT, Direction.RIGHT):
                    cart.turn_left()
                else:
                    raise Exception('Approached interception %s %s at wrong angle, %s' % (track, pos, cart))

            if track == '\\':
                if cart.dxy in (Direction.UP, Direction.DOWN):
                    cart.turn_left()
                elif cart.dxy in (Direction.LEFT, Direction.RIGHT):
                    cart.turn_right()
                else:
                    raise Exception('Approached interception %s %s at wrong angle, %s' % (track, pos, cart))

            if track == '+':
                if cart.intersection % 3 == 0:
                    cart.turn_left()
                elif cart.intersection % 3 == 2:
                    cart.turn_right()

                cart.intersection += 1

            for other in carts:
                if other.pos() == pos and not cart == other and not other.broken:
                    other.broken = True
                    cart.broken = True

        tick += 1
        alive = sum(1 for cart in carts if not cart.broken)
        if alive < 1:
            raise Exception('No carts remaining')

        if alive == 1:
            return next(cart.pos() for cart in carts if not cart.broken)


def load_data(fn) -> Tuple[Mapping[Tuple[int, int], str], List[Cart]]:
    tracks = {}
    carts = []
    with open(fn, 'r') as fh:
        lines = fh.readlines()

        for y, line in enumerate(lines):

            for x, c in enumerate(line.rstrip()):
                # carts
                if c == '<':
                    carts.append(Cart(x, y, Direction.LEFT))

                elif c == '>':
                    carts.append(Cart(x, y, Direction.RIGHT))

                elif c == 'v':
                    carts.append(Cart(x, y, Direction.DOWN))

                elif c == '^':
                    carts.append(Cart(x, y, Direction.UP))

                # paths
                if c == ' ':
                    track = None

                elif c in ('<', '>', '-'):
                    track = '-'

                elif c in ('v', '^', '|'):
                    track = '|'

                elif c in ('/', '\\', '+'):
                    track = c

                else:
                    raise Exception('Unexpected character %s at [%d,%d]' % (c, x, y))

                if track is not None:
                    tracks[x, y] = track

        return tracks, carts


if __name__ == "__main__":
    print(star_1(*load_data('data.txt')))
    print(star_2(*load_data('data.txt')))
