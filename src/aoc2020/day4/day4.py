import re
from typing import Dict, List


def load_data(fn: str) -> List[Dict[str, str]]:
    result = []
    passport = {}
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            if not line:
                result.append(passport)
                passport = {}
                continue

            for part in line.split(' '):
                key, value = part.split(':')
                passport[key] = value

    if passport:
        result.append(passport)

    return result


def star1(data: List[Dict[str, str]]) -> int:
    """
    >>> star1([{'ecl': 'gry', 'pid': '860033327', 'eyr': '2020', 'hcl': '#fffffd', 'byr': '1937', 'iyr': '2017', 'cid': '147', 'hgt': '183cm'},
    ...         {'iyr': '2013', 'ecl': 'amb', 'cid': '350', 'eyr': '2023', 'pid': '028048884;', 'hcl': '#cfa07d', 'byr': '1929'},
    ...         {'hcl': '#ae17e1', 'iyr': '2013', 'eyr': '2024', 'ecl': 'brn', 'pid': '760753108', 'byr': '1931', 'hgt': '179cm'},
    ...         {'hcl': '#cfa07d', 'eyr': '2025', 'pid': '166559648', 'iyr': '2011', 'ecl': 'brn', 'hgt': '59in'}])
    2
    """

    required = {'ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt'}
    result = 0

    for passport in data:
        valid = True
        for required_field in required:
            if required_field not in passport:
                valid = False
                break

        if valid:
            result += 1

    return result


def star2(data: List[Dict[str, str]]) -> int:
    """
    >>> star2([{'ecl': 'gry', 'pid': '860033327', 'eyr': '2020', 'hcl': '#fffffd', 'byr': '1937', 'iyr': '2017', 'cid': '147', 'hgt': '183cm'},
    ...         {'iyr': '2013', 'ecl': 'amb', 'cid': '350', 'eyr': '2023', 'pid': '028048884;', 'hcl': '#cfa07d', 'byr': '1929'},
    ...         {'hcl': '#ae17e1', 'iyr': '2013', 'eyr': '2024', 'ecl': 'brn', 'pid': '760753108', 'byr': '1931', 'hgt': '179cm'},
    ...         {'hcl': '#cfa07d', 'eyr': '2025', 'pid': '166559648', 'iyr': '2011', 'ecl': 'brn', 'hgt': '59in'}])
    2
    """
    return sum(1 for passport in data if valid_passport(passport))


def valid_passport(passport: Dict[str, str]) -> bool:
    """
    >>> valid_passport({'eyr': '1972', 'cid': '100', 'hcl': '#18171d', 'ecl': 'amb', 'hgt': '170', 'pid': '186cm', 'iyr': '2018', 'byr': '1926'})
    False
    >>> valid_passport({'iyr': '2019', 'hcl': '#602927', 'eyr': '1967', 'hgt': '170cm', 'ecl': 'grn', 'pid': '012533040', 'byr': '1946'})
    False
    >>> valid_passport({'hcl': 'dab227', 'iyr': '2012', 'ecl': 'brn', 'hgt': '182cm', 'pid': '021572410', 'eyr': '2020', 'byr': '1992', 'cid': '277'})
    False
    >>> valid_passport({'hgt': '59cm', 'ecl': 'zzz', 'eyr': '2038', 'hcl': '74454a', 'iyr': '2023', 'pid': '3556412378', 'byr': '2007'})
    False
    >>> valid_passport({'pid': '087499704', 'hgt': '74in', 'ecl': 'grn', 'iyr': '2012', 'eyr': '2030', 'byr': '1980', 'hcl': '#623a2f'})
    True
    >>> valid_passport({'eyr': '2029', 'ecl': 'blu', 'cid': '129', 'byr': '1989', 'iyr': '2014', 'pid': '896056539', 'hcl': '#a97842', 'hgt': '165cm'})
    True
    >>> valid_passport({'hcl': '#888785', 'hgt': '164cm', 'byr': '2001', 'iyr': '2015', 'cid': '88', 'pid': '545766238', 'ecl': 'hzl', 'eyr': '2022'})
    True
    >>> valid_passport({'iyr': '2010', 'hgt': '158cm', 'hcl': '#b6652a', 'ecl': 'blu', 'byr': '1944', 'eyr': '2021', 'pid': '093154719'})
    True
    """
    required = {'ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt'}
    for required_field in required:
        if not valid_passport_field(passport, required_field):
            return False

    return True


def valid_passport_field(passport: Dict[str, str], field: str) -> bool:
    if field not in passport:
        return False

    value = passport[field]

    if field == 'byr':
        return 1920 <= int(value) <= 2002

    if field == 'iyr':
        return 2010 <= int(value) <= 2020

    if field == 'eyr':
        return 2020 <= int(value) <= 2030

    if field == 'hgt':
        if value.endswith('cm'):
            return 150 <= int(value[:-2]) <= 193
        if value.endswith('in'):
            return 59 <= int(value[:-2]) <= 76

        return False

    if field == 'hcl':
        return bool(re.match(r'^#[a-f0-9]{6}$', value))

    if field == 'ecl':
        return value in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}

    if field == 'pid':
        return bool(re.match(r'^\d{9}$', value))

    raise Exception('Illegal key %s' % field)


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 4 Star 1: %d' % star1(my_data))
    print('Day 4 Star 2: %d' % star2(my_data))
