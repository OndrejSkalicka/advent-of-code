import logging
import os
from typing import List, Tuple, Optional, Iterable

from util.logger_config import init_logger

logger = logging.getLogger(__name__)


class Point:

    def __init__(self, x: int, y: int, z: int) -> None:
        self.xyz = (x, y, z)

    def __getitem__(self, item) -> int:
        return self.xyz[item]

    def __repr__(self) -> str:
        return '[%d;%d;%d]' % self.xyz


class Cube:

    def __init__(self, start: Point, end_excl: Point) -> None:
        self.start = start
        self.end_excl = end_excl

    def __repr__(self) -> str:
        return '#x=<%d..%d), y=<%d..%d), z=<%d..%d)#' % (
            self.start[0], self.end_excl[0],
            self.start[1], self.end_excl[1],
            self.start[2], self.end_excl[2],
        )

    def volume(self) -> int:
        """
        >>> Cube(Point(10, 10, 10), Point(12, 12, 12)).volume()
        8
        """
        return (
                (self.end_excl[0] - self.start[0]) *
                (self.end_excl[1] - self.start[1]) *
                (self.end_excl[2] - self.start[2])
        )

    def intersection(self, other: 'Cube') -> Tuple[List['Cube'], List['Cube']]:
        """
        >>> Cube(Point(10, 3, 6), Point(15, 5, 9)).intersection(Cube(Point(12, 4, 6), Point(17, 5, 9)))
        ([#x=<12..15), y=<3..4), z=<6..9)#, #x=<10..12), y=<3..5), z=<6..9)#], [#x=<12..15), y=<4..5), z=<6..9)#])
        """

        if (
                self.end_excl[0] <= other.start[0] and self.start[0] <= other.end_excl[0]
                or
                self.end_excl[1] <= other.start[1] and self.start[1] <= other.end_excl[1]
                or
                self.end_excl[2] <= other.start[2] and self.start[2] <= other.end_excl[2]
        ):
            return [self], []

        splits = [
            [],
            [],
            [],
        ]

        for dimension in range(3):
            split = split_line(self.start[dimension], self.end_excl[dimension], other.start[dimension], other.end_excl[dimension])
            splits[dimension] = split
            logger.debug("Splitting dimension %s, base %s-%s off %s-%s, into %s" % (
                dimension, self.start[dimension], self.end_excl[dimension], other.start[dimension], other.end_excl[dimension], split))

        positives = []
        negatives = []

        for sx in splits[0]:
            for sy in splits[1]:
                for sz in splits[2]:
                    cube = Cube(Point(sx[0], sy[0], sz[0]),
                                Point(sx[1], sy[1], sz[1]))

                    if sx[2] and sy[2] and sz[2]:
                        negatives.append(cube)
                    else:
                        positives.append(cube)

        logger.debug("Splitting %s by %s", (self, other))
        logger.debug("Split faces: %s", [splits])
        logger.debug("Split positives: %s", positives)
        logger.debug("Split negatives: %s", negatives)

        return combine_cubes(positives), negatives

    def try_combine(self, other: 'Cube') -> Optional['Cube']:
        """
        >>> Cube(Point(-44, -27, -14), Point(-5, 22, 34)).try_combine(Cube(Point(-44, -27, 34), Point(-5, 22, 36)))
        #x=<-44..-5), y=<-27..22), z=<-14..36)#
        """
        # try different dimensions
        for a, b in ((self, other), (other, self)):
            for ext_dimension in range(3):
                valid_dimension = True
                for dimension in range(3):
                    if dimension == ext_dimension:
                        if a.end_excl[dimension] != b.start[dimension]:
                            valid_dimension = False

                        continue

                    # other dimensions except for the main one must match
                    if a.start[dimension] != b.start[dimension]:
                        valid_dimension = False

                    if a.end_excl[dimension] != b.end_excl[dimension]:
                        valid_dimension = False

                if valid_dimension:
                    return Cube(a.start, b.end_excl)

        return None


class CubeSwitch(Cube):

    def __init__(self, start: Point, end_excl: Point, on: bool) -> None:
        super().__init__(start, end_excl)
        self.on = on

    def __repr__(self) -> str:
        return '%s#x=<%d..%d), y=<%d..%d), z=<%d..%d)#' % (
            '[+]' if self.on else '[-]',
            self.start[0], self.end_excl[0],
            self.start[1], self.end_excl[1],
            self.start[2], self.end_excl[2],
        )


def combine_cubes(cubes: List[Cube]) -> List[Cube]:
    if len(cubes) < 1:
        return cubes

    result = cubes[:]
    dirty = True

    while dirty:

        dirty = False

        for a, b in combinations(result):
            try_combine = a.try_combine(b)
            if try_combine:
                result.remove(a)
                result.remove(b)
                result.append(try_combine)
                dirty = True
                break

    return list(result)


def combinations(items: Iterable[Cube]) -> Tuple[Cube, Cube]:
    for a in items:
        for b in items:
            if a == b:
                continue

            yield a, b


def split_line(a_start: int, a_end_excl: int, b_start: int, b_end_excl: int) -> List[Tuple[int, int, bool]]:
    """
    Third parameter True iff part of second line

    >>> split_line(5, 10, 8, 10) # <5..10), <8..10)
    [(5, 8, False), (8, 10, True)]

    >>> split_line(5, 10, 8, 11) # <5..10), <8..11)
    [(5, 8, False), (8, 10, True)]

    >>> split_line(5, 10, 10, 13) # <5..10), <10..13)
    [(5, 10, False)]

    >>> split_line(5, 10, 1, 5) # <5..10), <1..5)
    [(5, 10, False)]

    >>> split_line(5, 10, 1, 6) # <5..10), <1..6)
    [(5, 6, True), (6, 10, False)]

    >>> split_line(5, 10, 8, 9) # <5..10), <8..9)
    [(5, 8, False), (8, 9, True), (9, 10, False)]
    """

    segments = [
        [a_start, b_start, False],
        [b_start, b_end_excl, True],
        [b_end_excl, a_end_excl, False]
    ]

    for segment in segments:
        segment[0] = max(segment[0], a_start)
        segment[1] = min(segment[1], a_end_excl)

    return list((seg[0], seg[1], seg[2]) for seg in segments if seg[0] < seg[1])


def load_data(fn: str = 'data.txt') -> List[CubeSwitch]:
    result = []

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            on, rest = line.split(' ')

            x, y, z = rest.split(',')
            x1 = int(x[2:].split('..')[0])
            x2 = int(x[2:].split('..')[1])
            y1 = int(y[2:].split('..')[0])
            y2 = int(y[2:].split('..')[1])
            z1 = int(z[2:].split('..')[0])
            z2 = int(z[2:].split('..')[1])

            result.append(
                CubeSwitch(Point(x1, y1, z1), Point(x2 + 1, y2 + 1, z2 + 1), on == 'on')
            )

    return result


def test_data(fn: str = 'data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(switches: List[CubeSwitch]) -> int:
    """
    >>> star1(test_data('data-test-pico.txt'))
    39
    >>> star1(test_data())
    590784
    """
    boundary = Cube(Point(-50, -50, -50), Point(51, 51, 51))
    galaxy = create_galaxy(switches)

    size = 0
    for cluster in galaxy:
        size += sum(fragment.volume() for fragment in cluster.intersection(boundary)[1])
    return size


def star2(switches: List[CubeSwitch]) -> int:
    """
    >>> star2(test_data('data-test-pico.txt'))
    39

    >>> star2(test_data('data-test-star2.txt'))
    2758514936282235
    """

    return sum(cube.volume() for cube in create_galaxy(switches))


def create_galaxy(switches: List[CubeSwitch]) -> List[Cube]:
    galaxy = []  # type: List[Cube]

    i = 0
    for switch in switches:
        logger.info("Processing switch %s", switch)
        if switch.on:
            remainder = [switch]

            for cube in galaxy:
                new_remainder = []
                for r in remainder:
                    new_remainder += r.intersection(cube)[0]

                remainder = new_remainder

            galaxy += remainder

        else:
            new_galaxy = []

            for cube in galaxy:
                new_galaxy += cube.intersection(switch)[0]

            galaxy = new_galaxy

        i += 1
        logger.info("Processed switch %d/%d: %s", i, len(switches), switch)
        logger.info("Items in galaxy: %d", len(galaxy))
        logger.debug("Current galaxy after step %d: %s", i, galaxy)
        logger.info("Sum of galaxy: %d", sum(cube.volume() for cube in galaxy))

    return galaxy


if __name__ == "__main__":
    init_logger()

    print('Day 22 Star 1: %d' % star1(load_data()))
    print('Day 22 Star 2: %d' % star2(load_data()))
