from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["0123",
    ...        "1234",
    ...        "8765",
    ...        "9876",])
    1
    >>> star1(["...0...",
    ...        "...1...",
    ...        "...2...",
    ...        "6543456",
    ...        "7.....7",
    ...        "8.....8",
    ...        "9.....9",])
    2
    >>> star1(["..90..9",
    ...        "...1.98",
    ...        "...2..7",
    ...        "6543456",
    ...        "765.987",
    ...        "876....",
    ...        "987....",])
    4
    >>> star1(["10..9..",
    ...        "2...8..",
    ...        "3...7..",
    ...        "4567654",
    ...        "...8..3",
    ...        "...9..2",
    ...        ".....01",])
    3
    >>> star1(["89010123",
    ...        "78121874",
    ...        "87430965",
    ...        "96549874",
    ...        "45678903",
    ...        "32019012",
    ...        "01329801",
    ...        "10456732",])
    36
    """

    grid = Grid.from_list_str(data)

    result = 0
    for start in grid.find_all_xy('0'):
        reachable = {start.xy}

        for next_step in '123456789':
            next_reachable = set()

            for current in reachable:
                for adj in grid.adjacent(current):
                    if grid[adj] == next_step:
                        next_reachable.add(adj)

            reachable = next_reachable

        result += len(reachable)

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([".....0.",
    ...        "..4321.",
    ...        "..5..2.",
    ...        "..6543.",
    ...        "..7..4.",
    ...        "..8765.",
    ...        "..9....",])
    3
    >>> star2(["..90..9",
    ...        "...1.98",
    ...        "...2..7",
    ...        "6543456",
    ...        "765.987",
    ...        "876....",
    ...        "987....",])
    13
    >>> star2(["012345",
    ...        "123456",
    ...        "234567",
    ...        "345678",
    ...        "4.6789",
    ...        "56789.",])
    227
    >>> star2(["89010123",
    ...        "78121874",
    ...        "87430965",
    ...        "96549874",
    ...        "45678903",
    ...        "32019012",
    ...        "01329801",
    ...        "10456732",])
    81
    """

    grid = Grid.from_list_str(data)

    result = 0
    for start in grid.find_all_xy('0'):
        reachable: set[tuple[Xy, int]] = {(start.xy, 1)}

        for next_step in '123456789':
            next_reachable: dict[Xy, int] = defaultdict(int)

            for current, rank in reachable:
                for adj in grid.adjacent(current):
                    if grid[adj] == next_step:
                        next_reachable[adj] += rank

            reachable = set(next_reachable.items())

        result += sum(r[1] for r in reachable)

    return result


if __name__ == "__main__":
    run_day(10,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
