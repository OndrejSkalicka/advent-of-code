from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, NORTH, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["....#.....",
    ...        ".........#",
    ...        "..........",
    ...        "..#.......",
    ...        ".......#..",
    ...        "..........",
    ...        ".#..^.....",
    ...        "........#.",
    ...        "#.........",
    ...        "......#..."])
    41
    """

    grid = Grid.from_list_str(data)
    walk_grid(grid)

    return len(grid.find_all_xy('X'))


def star2(data: list[str]) -> int:
    """
    >>> star2(["....#.....",
    ...        ".........#",
    ...        "..........",
    ...        "..#.......",
    ...        ".......#..",
    ...        "..........",
    ...        ".#..^.....",
    ...        "........#.",
    ...        "#.........",
    ...        "......#..."])
    6
    """

    grid = Grid.from_list_str(data)
    guard = grid.find_all_xy('^')[0].xy
    walk_grid(grid)

    result = 0

    for c in grid.find_all_xy('X'):
        if c.xy == guard:
            continue

        cycle_grid = Grid.from_list_str(data)
        cycle_grid[c.xy] = '#'
        if walk_grid(cycle_grid):
            result += 1

    return result


def walk_grid(grid: Grid[str]) -> bool:
    cycle: dict[Xy, set[Xy]] = defaultdict(set)
    guard = grid.find_all_xy('^')[0].xy
    step = NORTH

    while True:
        grid[guard] = 'X'

        if step in cycle[guard]:
            return True

        cycle[guard].add(step)

        new_xy = guard + step
        if new_xy not in grid:
            break

        if grid[new_xy] == '#':
            step = step.rot_cw()
            continue

        guard = new_xy

    return False


if __name__ == "__main__":
    run_day(6,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
