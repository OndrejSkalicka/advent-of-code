from collections import defaultdict
from typing import Set

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Xy, Grid

NUM = '0123456789'


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...       "467..114..",
    ...       "...*......",
    ...       "..35..633.",
    ...       "......#...",
    ...       "617*......",
    ...       ".....+.58.",
    ...       "..592.....",
    ...       "......755.",
    ...       "...$.*....",
    ...       ".664.598..",
    ...       ])
    4361
    """

    grid = Grid.from_list_str(data)
    result = 0
    acc = ''
    num_adj = False

    for y, row in enumerate(grid.rows()):
        for x, c in enumerate(row + ['.']):
            if c in NUM:
                acc += c
                if _adj(Xy(x, y), grid):
                    num_adj = True

            elif acc:
                if num_adj:
                    result += int(acc)

                num_adj = False
                acc = ''

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...       "467..114..",
    ...       "...*......",
    ...       "..35..633.",
    ...       "......#...",
    ...       "617*......",
    ...       ".....+.58.",
    ...       "..592.....",
    ...       "......755.",
    ...       "...$.*....",
    ...       ".664.598..",
    ...       ])
    467835
    """

    grid = Grid.from_list_str(data)
    result = 0
    acc = ''
    adjacent: Set[Xy] = set()
    gears = defaultdict(list)

    for y, row in enumerate(grid.rows()):
        for x, c in enumerate(row + ['.']):
            if c in NUM:
                acc += c
                for xy in _adj_gears(Xy(x, y), grid):
                    adjacent.add(xy)

            elif acc:
                number = int(acc)

                for xy in adjacent:
                    gears[xy].append(number)

                acc = ''
                adjacent = set()

    for num_list in gears.values():
        if len(num_list) == 2:
            result += num_list[0] * num_list[1]

    return result


def _adj(xy: Xy, grid: Grid[str]) -> bool:
    for adj in grid.adjacent_diagonal(xy):
        if grid[adj] not in '.' + NUM:
            return True

    return False


def _adj_gears(xy: Xy, grid: Grid[str]) -> list[Xy]:
    result = []
    for adj in grid.adjacent_diagonal(xy):
        if grid[adj] == '*':
            result.append(adj)

    return result


if __name__ == "__main__":
    run_day(3,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
