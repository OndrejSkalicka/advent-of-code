import heapq
import logging
from collections import defaultdict
from copy import deepcopy
from typing import Optional

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger
from util.record import Xy

logger = logging.getLogger(__name__)

DIRECTIONS: dict[int, Xy] = {
    1: Xy(0, -1),  # north
    2: Xy(0, 1),  # south
    3: Xy(-1, 0),  # west
    4: Xy(1, 0),  # east
}


def star1(memory: list[int]) -> int:
    start_program = Program(memory)
    start_program.run()

    start_pos: Xy = Xy(0, 0)
    target_pos: Optional[Xy] = None
    program_at_pos: dict[Xy, Program] = {start_pos: deepcopy(start_program)}

    dungeon_map: dict[Xy, str] = defaultdict(lambda: ' ')
    dungeon_map[start_pos] = '@'

    # dijkstra
    pq: list[tuple[int, Xy]] = []
    heapq.heappush(pq, (0, start_pos))
    visited: set[Xy] = set()
    distances: dict[Xy, int] = {start_pos: 0}
    parents_map = {}

    while pq and target_pos is None:
        _, active = heapq.heappop(pq)

        if active in visited:
            continue

        visited.add(active)
        distance = distances[active]

        for command, dxy in DIRECTIONS.items():
            program = deepcopy(program_at_pos[active])
            new_xy = active + dxy
            if new_xy in visited:
                continue

            program.write_input(command)
            program.run_or_resume()
            out = program.read_output()
            program_at_pos[new_xy] = deepcopy(program)

            if new_xy not in dungeon_map:
                dungeon_map[new_xy] = '#.O'[out]

            parents_map[new_xy] = active
            distances[new_xy] = distance + 1

            # only push non-walls
            if out > 0:
                heapq.heappush(pq, (distance + 1, new_xy))

            if out == 2:
                target_pos = new_xy

        if len(visited) % 100 == 0:
            logger.info(f"Visited {len(visited)} nodes")

    logger.debug(f"Final map: \n{map_as_string(dungeon_map)}")

    assert target_pos is not None
    return distances[target_pos]


def star2(memory: list[int]) -> int:
    start_program = Program(memory)
    start_program.run()

    start_pos: Xy = Xy(0, 0)
    target_pos: Optional[Xy] = None
    program_at_pos: dict[Xy, Program] = {start_pos: deepcopy(start_program)}

    dungeon_map: dict[Xy, str] = defaultdict(lambda: ' ')
    dungeon_map[start_pos] = '@'

    # dijkstra
    pq: list[tuple[int, Xy]] = []
    heapq.heappush(pq, (0, start_pos))
    visited: set[Xy] = set()
    distances = {start_pos: 0}
    parents_map = {}

    while pq and target_pos is None:
        _, active = heapq.heappop(pq)

        if active in visited:
            continue

        visited.add(active)
        distance = distances[active]

        for command, dxy in DIRECTIONS.items():
            program = deepcopy(program_at_pos[active])
            new_xy = active + dxy
            if new_xy in visited:
                continue

            program.write_input(command)
            program.run_or_resume()
            out = program.read_output()
            program_at_pos[new_xy] = deepcopy(program)

            if new_xy not in dungeon_map:
                dungeon_map[new_xy] = '#.X'[out]

            parents_map[new_xy] = active
            distances[new_xy] = distance + 1

            # only push non-walls
            if out > 0:
                heapq.heappush(pq, (distance + 1, new_xy))

            if out == 2:
                target_pos = new_xy

        if len(visited) % 100 == 0:
            logger.info(f"Visited {len(visited)} nodes")

    logger.debug(f"Final map: \n{map_as_string(dungeon_map)}")

    assert target_pos is not None
    return star2phase2(program_at_pos[target_pos])


def star2phase2(oxygen_program: Program) -> int:
    start_pos: Xy = Xy(0, 0)
    program_at_pos: dict[Xy, Program] = {start_pos: deepcopy(oxygen_program)}

    dungeon_map: dict[Xy, str] = defaultdict(lambda: ' ')
    dungeon_map[start_pos] = 'X'

    # dijkstra
    pq: list[tuple[int, Xy]] = []
    heapq.heappush(pq, (0, start_pos))
    visited: set[Xy] = set()
    distances = {start_pos: 0}
    parents_map = {}

    while pq:
        _, active = heapq.heappop(pq)

        if active in visited:
            continue

        visited.add(active)
        distance = distances[active]

        for command, dxy in DIRECTIONS.items():
            program = deepcopy(program_at_pos[active])
            new_xy = active + dxy
            if new_xy in visited:
                continue

            program.write_input(command)
            program.run_or_resume()
            out = program.read_output()
            program_at_pos[new_xy] = deepcopy(program)

            if new_xy not in dungeon_map:
                dungeon_map[new_xy] = '#.O'[out]

            parents_map[new_xy] = active

            # only push non-walls
            if out > 0:
                distances[new_xy] = distance + 1
                heapq.heappush(pq, (distance + 1, new_xy))

        if len(visited) % 100 == 0:
            logger.info(f"Visited {len(visited)} nodes")

    logger.debug(f"Final map from oxygen: \n{map_as_string(dungeon_map)}")
    return max(distances.values())


def map_as_string(dungeon_map: dict[Xy, str]) -> str:
    min_y = min(xy.y for xy in dungeon_map.keys())
    max_y = max(xy.y for xy in dungeon_map.keys())
    min_x = min(xy.x for xy in dungeon_map.keys())
    max_x = max(xy.x for xy in dungeon_map.keys())
    result = ""
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            result += dungeon_map[Xy(x, y)]
        result += "\n"

    return result


if __name__ == "__main__":
    init_logger()

    print('Day 15 Star 1: %d' % star1(load_numbers_csv('day15.txt')))
    print('Day 15 Star 2: %d' % star2(load_numbers_csv('day15.txt')))
