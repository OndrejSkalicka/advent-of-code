from typing import List, Union


def load_data(fn: str) -> List[str]:
    with open(fn, 'r') as fh:
        return list(line.strip() for line in fh.readlines())


def star1(data: List[str]) -> int:
    return sum(eval_line(line) for line in data)


def eval_line(line: str) -> int:
    """
    Only evaluates until return parenthesis is found

    >>> eval_line('1 + 2 * 3 + 4 * 5 + 6')
    71

    >>> eval_line('2 * 3 + (4 * 5)')
    26

    >>> eval_line('5 + (8 * 3 + 9 + 3 * 4 * 3)')
    437

    >>> eval_line('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))')
    12240

    >>> eval_line('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2')
    13632
    """

    buffer = []
    tokens = list(reversed(tokenize(line)))
    while tokens:
        token = tokens.pop()

        if token in ('*', '+', '('):
            buffer.append(token)
            continue

        if token == ')':
            prev = buffer.pop()
            buffer.pop()  # '('
            tokens.append(prev)
            continue

        num = token

        if len(buffer) == 0 or buffer[-1] == '(':
            buffer.append(num)
            continue

        if buffer[-1] == '*':
            buffer.pop()
            prev = buffer.pop()
            buffer.append(prev * num)
            continue

        if buffer[-1] == '+':
            buffer.pop()
            prev = buffer.pop()
            buffer.append(prev + num)
            continue

        raise Exception('Illegal operation')

    return buffer[0]


def star2(data: List[str]) -> int:
    return sum(star2_line(line) for line in data)


def star2_line(line: str) -> int:
    """
    >>> star2_line('1 + 2 * 3 + 4 * 5 + 6')
    231

    >>> star2_line('1 + (2 * 3) + (4 * (5 + 6))')
    51

    >>> star2_line('2 * 3 + (4 * 5)')
    46

    >>> star2_line('5 + (8 * 3 + 9 + 3 * 4 * 3)')
    1445

    >>> star2_line('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))')
    669060

    >>> star2_line('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2')
    23340
    """

    return star2_segment(tokenize(line))


def star2_segment(tokens: List[Union[str, int]]) -> int:
    pure = []

    # eval all parentheses
    tokens_reversed = list(reversed(tokens))
    while tokens_reversed:
        token = tokens_reversed.pop()

        if token != '(':
            pure.append(token)
            continue

        opened = 1
        sub = []
        while opened > 0:
            token = tokens_reversed.pop()

            if token == ')':
                opened -= 1

            if token == '(':
                opened += 1

            if opened:
                sub.append(token)

        sub_value = star2_segment(sub)
        pure.append(sub_value)

    # adds
    while '+' in pure:
        index = pure.index('+')

        pure = pure[:index - 1] + [pure[index - 1] + pure[index + 1]] + pure[index + 2:]

    # mults
    while '*' in pure:
        index = pure.index('*')

        pure = pure[:index - 1] + [pure[index - 1] * pure[index + 1]] + pure[index + 2:]

    return pure[0]


def tokenize(line: str) -> List[Union[str, int]]:
    result = []
    for token in line.replace(' ', ''):
        if token in '()*+':
            result.append(token)
        else:
            result.append(int(token))

    return result


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 18 Star 1: %d' % star1(my_data))
    print('Day 18 Star 2: %d' % star2(my_data))
