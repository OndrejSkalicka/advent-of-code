import heapq
import logging
from dataclasses import dataclass, field
from functools import total_ordering
from typing import Optional, Generator

from util.data import load_as_str
from util.logger_config import init_logger
from util.record import Xy

FLOOR = '.'

logger = logging.getLogger(__name__)

KEYS = 'abcdefghijklmnopqrstuvwxyz'
DOORS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


@total_ordering
@dataclass(frozen=True)
class CrawlState:
    pos: Xy
    keys: list[str]
    all_keys: list[str]
    steps: int
    map: list[list[str]] = field(repr=False)

    def all_keys_found(self) -> bool:
        return len(self.keys) == len(self.all_keys)

    def __lt__(self, other: 'CrawlState') -> bool:
        if self.steps == other.steps:
            return len(self.keys) < len(other.keys)
        return self.steps < other.steps


def star1(input_str: str) -> int:
    """
    >>> star1("#########\\n"
    ...       "#b.A.@.a#\\n"
    ...       "#########")
    8

    >>> star1("########################\\n"
    ...       "#f.D.E.e.C.b.A.@.a.B.c.#\\n"
    ...       "######################.#\\n"
    ...       "#d.....................#\\n"
    ...       "########################")
    86

    >>> star1("########################\\n"
    ...       "#...............b.C.D.f#\\n"
    ...       "#.######################\\n"
    ...       "#.....@.a.B.c.d.A.e.F.g#\\n"
    ...       "########################")
    132

    # >>> star1("#################\\n"
    # ...       "#i.G..c...e..H.p#\\n"
    # ...       "########.########\\n"
    # ...       "#j.A..b...f..D.o#\\n"
    # ...       "########@########\\n"
    # ...       "#k.E..a...g..B.n#\\n"
    # ...       "########.########\\n"
    # ...       "#l.F..d...h..C.m#\\n"
    # ...       "#################")
    # 136

    >>> star1("########################\\n"
    ...       "#@..............ac.GI.b#\\n"
    ...       "###d#e#f################\\n"
    ...       "###A#B#C################\\n"
    ...       "###g#h#i################\\n"
    ...       "########################")
    81
    """

    initial_map = list(list(row) for row in input_str.replace('@', FLOOR).split("\n"))

    start_xy: Optional[Xy] = None
    all_keys = []
    for y, row in enumerate(input_str.split("\n")):
        for x, c in enumerate(row):
            if c == '@':
                start_xy = Xy(x, y)
            elif c in KEYS:
                all_keys.append(c)

    assert start_xy is not None

    start_state = CrawlState(start_xy, [], all_keys, 0, initial_map)
    best: Optional[CrawlState] = None

    pq_states: list[CrawlState] = []
    heapq.heappush(pq_states, start_state)

    dijkstras: dict[tuple[str, Xy], list[tuple[Xy, int]]] = {}

    counter = 0
    while pq_states:
        state = heapq.heappop(pq_states)

        # kill a state, can never be best
        if best is not None and best.steps <= state.steps:
            continue

        if state.all_keys_found():
            if best is None or state.steps < best.steps:
                best = state

            continue

        dijkstra_key = ''.join(sorted(state.keys)), state.pos
        if dijkstra_key in dijkstras:
            keys_and_distances = dijkstras[dijkstra_key]
        else:
            found_keys: list[tuple[Xy, str]] = []

            # single dijkstra
            pq: list[tuple[int, Xy]] = []
            active: Xy
            heapq.heappush(pq, (0, state.pos))
            visited = set()
            distances: dict[Xy, int] = {state.pos: 0}
            parents_map = {}

            while pq:
                _, active = heapq.heappop(pq)

                if active in visited:
                    continue

                visited.add(active)
                distance = distances[active]

                if state.map[active.y][active.x] in KEYS and state.map[active.y][active.x] not in state.keys:
                    found_keys.append((active, state.map[active.y][active.x]))
                    # do not go over newly found keys
                    continue

                for node in _adj(active, state):
                    if node in visited:
                        continue

                    parents_map[node] = active
                    distances[node] = distance + 1

                    heapq.heappush(pq, (distance + 1, node))

            # end of dijkstra

            keys_and_distances = list((key_xy, distances[key_xy]) for key_xy, _ in found_keys)
            dijkstras[dijkstra_key] = keys_and_distances

        for key_xy, distance in keys_and_distances:
            key = state.map[key_xy.y][key_xy.x]

            new_state = CrawlState(key_xy, state.keys + [key], state.all_keys, state.steps + distance, state.map)

            heapq.heappush(pq_states, new_state)

        counter += 1
        if counter % 10_000 == 0:
            logger.info(f"Passed {counter:,} iterations, steps counter: {state.steps}, buffer {len(pq_states):,}, Dijkstras calculated: {len(dijkstras)}, "
                        f"state keys {'-'.join(state.keys)}.")

    assert best is not None

    logger.info(f"Best solution: {best}")
    return best.steps


def _adj(pos: Xy, state: CrawlState) -> Generator[Xy, None, None]:
    for dxy in (Xy(1, 0), Xy(-1, 0), Xy(0, 1), Xy(0, -1)):
        new_xy = pos + dxy
        new_point: str = state.map[new_xy.y][new_xy.x]
        if new_point == '.' or (new_point in DOORS and new_point.lower() in state.keys) or (new_point in KEYS):
            yield new_xy


if __name__ == "__main__":
    init_logger()

    print('Day 18 Star 1: %d' % star1(load_as_str('day18.txt')))
