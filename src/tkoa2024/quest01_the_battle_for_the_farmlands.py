from util.data import txt_fn, load_as_str, split_chunks
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1('ABBAC')
    5
    """

    creatures = {'A': 0, 'B': 1, 'C': 3}
    return sum(creatures[c] for c in data)


def star2(data: str) -> int:
    """
    >>> star2('AxBCDDCAxD')
    28
    """

    result = 0
    creatures = {'A': 0, 'B': 1, 'C': 3, 'D': 5, 'x': 0}
    extras = {2: 2 * 1, 1: 0, 0: 0}
    for battle in split_chunks(data, 2):
        result += sum(creatures[c] for c in battle)

        result += extras[sum(1 for c in battle if c != 'x')]

    return result


def star3(data: str) -> int:
    """
    >>> star3('xBxAAABCDxCC')
    30
    """

    result = 0
    creatures = {'A': 0, 'B': 1, 'C': 3, 'D': 5, 'x': 0}
    extras = {3: 3 * 2, 2: 2, 1: 0, 0: 0}
    for battle in split_chunks(data, 3):
        result += sum(creatures[c] for c in battle)

        result += extras[sum(1 for c in battle if c != 'x')]

    return result


if __name__ == "__main__":
    run_day(1,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__, '-part-2.txt'))),
            lambda: star3(load_as_str(txt_fn(__file__, '-part-3.txt'))),
            )
