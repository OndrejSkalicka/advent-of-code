import os
from typing import List, Tuple


def load_data(fn: str) -> List[Tuple[str, int]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            if not line:
                continue
            (key, value) = line.split(' ')
            result.append((key, int(value)))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[Tuple[str, int]]) -> int:
    """
    >>> star1(test_data())
    150
    """

    horizontal = 0
    depth = 0
    for (key, value) in data:
        if key == 'forward':
            horizontal += value

        elif key == 'down':
            depth += value

        elif key == 'up':
            depth -= value

        if depth < 0:
            raise Exception("Flying ship")

    return depth * horizontal


def star2(data: List[Tuple[str, int]]) -> int:
    """
    >>> star2(test_data())
    900
    """

    horizontal = 0
    depth = 0
    aim = 0
    for (key, value) in data:
        if key == 'forward':
            horizontal += value
            depth += aim * value

        elif key == 'down':
            aim += value

        elif key == 'up':
            aim -= value

        if depth < 0:
            raise Exception("Flying ship")

    return depth * horizontal


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 2 Star 1: %d' % star1(data))
    print('Day 2 Star 2: %d' % star2(data))
