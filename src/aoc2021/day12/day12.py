import os
from typing import List, Tuple, Dict


def load_data(fn: str = 'data.txt') -> List[Tuple[str, str]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            a, b = line.split('-')
            result.append((a, b))

    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(data: List[Tuple[str, str]]) -> int:
    """
    >>> star1(test_data())
    10

    >>> star1(test_data('data-test2.txt'))
    19

    >>> star1(test_data('data-test3.txt'))
    226
    """

    # paths map
    directions = {}
    for x, y in data:
        if x not in directions:
            directions[x] = []
        if y not in directions:
            directions[y] = []
        directions[x].append(y)
        directions[y].append(x)

    result = []
    star_1_rec(directions, ['start'], result)

    return len(result)


def star_1_rec(data: Dict[str, List[str]], path: List[str], result: List[List[str]]) -> None:
    for direction in data[path[-1]]:
        if direction == 'end':
            new_path = path[:]
            result.append(new_path)
            continue

        if direction == direction.upper() or direction not in path:
            new_path = path[:]
            new_path.append(direction)
            star_1_rec(data, new_path, result)


def star2(data: List[Tuple[str, str]]) -> int:
    """
    >>> star2(test_data())
    36
    """

    # paths map
    directions = {}
    for x, y in data:
        if x not in directions:
            directions[x] = []
        if y not in directions:
            directions[y] = []
        directions[x].append(y)
        directions[y].append(x)

    result = []
    star_2_rec(directions, ['start'], result)

    return len(result)


def star_2_rec(data: Dict[str, List[str]], path: List[str], result: List[List[str]]) -> None:
    smalls = {}
    for small in (x for x in path if x.lower() == x and x != 'start'):
        smalls[small] = smalls.get(small, 0) + 1

    for direction in data[path[-1]]:
        if direction == 'end':
            new_path = path[:]
            new_path.append(direction)
            result.append(new_path)
            continue

        if direction == 'start':
            continue

        if direction == direction.lower():
            if direction in smalls and max(smalls.values()) > 1:
                continue

        new_path = path[:]
        new_path.append(direction)
        star_2_rec(data, new_path, result)


if __name__ == "__main__":
    print('Day 12 Star 1: %d' % star1(load_data()))
    print('Day 12 Star 2: %d' % star2(load_data()))
