from dataclasses import dataclass

from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1("2333133121414131402")
    1928
    """

    disk: list[int] = []

    file = True
    file_id = 0
    # embrace the naive
    for c in data:
        if file:
            disk += [file_id] * int(c)

            file_id += 1
        else:
            disk += [-1] * int(c)

        file = not file

    start = 0
    end = len(disk) - 1
    while True:
        while disk[start] >= 0 and start < len(disk):
            start += 1

        while disk[end] == -1 and end > 0:
            end -= 1

        if start >= end:
            break

        assert disk[start] == -1
        assert disk[end] != -1
        disk[start] = disk[end]
        disk[end] = -1

    result = 0
    for i, d in enumerate(disk):
        if d >= 0:
            result += i * d

    return result


@dataclass
class File:
    pos: int
    size: int
    id: int


@dataclass
class Hole:
    pos: int
    size: int


def star2(data: str) -> int:
    """
    >>> star2("2333133121414131402")
    2858
    """

    is_file = True
    file_id = 0
    files: list[File] = []
    holes: list[Hole] = []
    pos = 0

    for c in data:
        if is_file:
            files.append(File(pos, int(c), file_id))
            pos += int(c)
            file_id += 1
        else:
            holes.append(Hole(pos, int(c)))
            pos += int(c)

        is_file = not is_file

    for file in reversed(files):
        for hole in holes:
            if hole.pos >= file.pos:
                break
            if hole.size >= file.size:
                file.pos = hole.pos
                hole.pos += file.size
                hole.size -= file.size

                if hole.size == 0:
                    holes.remove(hole)

                break
                # we do not crete new free space as we only move LTR

    result = 0
    for file in sorted(files, key=lambda e: e.pos):
        for i in range(file.size):
            result += file.id * (file.pos + i)

    return result


if __name__ == "__main__":
    run_day(9,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
