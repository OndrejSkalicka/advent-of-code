import logging
import os
from typing import List, Tuple, Optional

from util.logger_config import init_logger

logger = logging.getLogger(__name__)

VECTORS = {
    '>': (1, 0),
    'v': (0, 1),
}


class Cucu:

    def __init__(self, x: int, y: int, type: str) -> None:
        self.x = x
        self.y = y
        self.type = type

    def __repr__(self) -> str:
        return '%s [%d;%d]' % (self.type, self.x, self.y)


def load_data(fn: str = 'data.txt') -> Tuple[List[Cucu], int, int]:
    cucus = []
    y = 0
    width = 0

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            width = len(line)

            for x in range(len(line)):
                if line[x] == '.':
                    continue

                cucus.append(Cucu(x, y, line[x]))

            y += 1

    return cucus, width, y


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(cucus: List[Cucu], width: int, height: int) -> int:
    """
    >>> star1(*test_data())
    58
    """

    grid = [[None] * height for _ in range(width)]  # type: List[List[Optional[Cucu]]]

    for cucu in cucus:
        grid[cucu.x][cucu.y] = cucu

    # dump(grid, width, height)
    changed = True
    step = 0

    while changed:
        changed = perform_step(cucus, grid, width, height)
        step += 1

        # print("After %d steps:" % step)
        # dump(grid, width, height)

    return step


def perform_step(cucus: List[Cucu], grid: List[List[Optional[Cucu]]], width: int, height: int) -> bool:
    change = False
    for direction in ('>', 'v'):
        dx = VECTORS[direction][0]
        dy = VECTORS[direction][1]
        movers = []

        for cucu in cucus:
            if cucu.type != direction:
                continue

            nx = (cucu.x + dx) % width
            ny = (cucu.y + dy) % height

            if grid[nx][ny] is None:
                change = True
                movers.append(cucu)

        for cucu in movers:
            nx = (cucu.x + dx) % width
            ny = (cucu.y + dy) % height

            assert grid[cucu.x][cucu.y] is not None
            grid[cucu.x][cucu.y] = None

            cucu.x = nx
            cucu.y = ny
            assert grid[cucu.x][cucu.y] is None
            grid[cucu.x][cucu.y] = cucu

    return change


def dump(grid: List[List[Optional[Cucu]]], width: int, height: int) -> None:
    for y in range(height):
        for x in range(width):
            print(grid[x][y].type if grid[x][y] is not None else '.', end='')

        print()
    print()


if __name__ == "__main__":
    init_logger()

    print('Day 25 Star 1: %d' % star1(*load_data()))
