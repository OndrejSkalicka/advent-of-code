from collections import defaultdict
from dataclasses import dataclass
from itertools import count
from typing import Optional

from util.data import txt_fn, load_strings_newline
from util.perf import run_day
from util.record import Xy

DIRECTIONS = [
    (Xy(0, -1), [Xy(-1, -1), Xy(0, -1), Xy(1, -1)]),  # N
    (Xy(0, 1), [Xy(-1, 1), Xy(0, 1), Xy(1, 1)]),  # S
    (Xy(-1, 0), [Xy(-1, -1), Xy(-1, 0), Xy(-1, 1)]),  # W
    (Xy(1, 0), [Xy(1, -1), Xy(1, 0), Xy(1, 1)]),  # E
]

ADJACENT = [
    Xy(1, 1),
    Xy(1, 0),
    Xy(1, -1),
    Xy(0, 1),
    Xy(0, -1),
    Xy(-1, 1),
    Xy(-1, 0),
    Xy(-1, -1),
]


@dataclass
class Elf:
    pos: Xy
    proposal: Optional[Xy] = None


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "..............",
    ...        "..............",
    ...        ".......#......",
    ...        ".....###.#....",
    ...        "...#...#.#....",
    ...        "....#...##....",
    ...        "...#.###......",
    ...        "...##.#.##....",
    ...        "....#..#......",
    ...        "..............",
    ...        "..............",
    ...        "..............",
    ...        ])
    110
    """

    return _simulate(data, limit=10)[0]


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        ".....",
    ...        "..##.",
    ...        "..#..",
    ...        ".....",
    ...        "..##.",
    ...        ".....",
    ...        ])
    4

    >>> star2([
    ...        "..............",
    ...        "..............",
    ...        ".......#......",
    ...        ".....###.#....",
    ...        "...#...#.#....",
    ...        "....#...##....",
    ...        "...#.###......",
    ...        "...##.#.##....",
    ...        "....#..#......",
    ...        "..............",
    ...        "..............",
    ...        "..............",
    ...        ])
    20
    """

    return _simulate(data)[1]


def _simulate(data: list[str], limit: Optional[int] = None) -> tuple[int, int]:
    elves: dict[Xy, Elf] = dict()
    for y, row in enumerate(data):
        for x, c in enumerate(row):
            if c == '#':
                elf = Elf(Xy(x, y))
                elves[elf.pos] = elf

    step = 0
    for step in count():
        if limit is not None and step == limit:
            break

        proposals: dict[Xy, int] = defaultdict(int)
        for elf in elves.values():
            elf.proposal = None

            for adjacent in ADJACENT:
                if elf.pos + adjacent in elves:
                    break
            else:
                continue

            for i in range(4):
                assert elf.proposal is None
                direction, checks = DIRECTIONS[(step + i) % 4]
                for check in checks:
                    if elf.pos + check in elves:
                        break
                else:
                    elf.proposal = elf.pos + direction
                    proposals[elf.proposal] += 1
                    break

        new_elves: dict[Xy, Elf] = dict()
        moves = False
        for elf in elves.values():
            if elf.proposal is not None and proposals[elf.proposal] == 1:
                moves = True
                elf.pos = elf.proposal
            assert elf.pos not in new_elves
            new_elves[elf.pos] = elf

        if not moves:
            break
        elves = new_elves

    min_x = min(pos.x for pos in elves.keys())
    max_x = max(pos.x for pos in elves.keys())
    min_y = min(pos.y for pos in elves.keys())
    max_y = max(pos.y for pos in elves.keys())

    result = 0
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if Xy(x, y) not in elves:
                result += 1

    return result, step + 1


if __name__ == "__main__":
    run_day(23,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
