from dataclasses import dataclass
from operator import add, sub, mul, truediv
from typing import Union, Callable, Any

from sympy import symbols, solve

from util.data import txt_fn, load_strings_newline
from util.perf import run_day

OPS = {
    '+': add,
    '-': sub,
    '*': mul,
    '/': truediv,
}


@dataclass
class Monkey:
    name: str
    left: Union[str, int]
    right: Union[str, int]
    operation: Callable[[int, int], int]


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "root: pppw + sjmn",
    ...        "dbpl: 5",
    ...        "cczh: sllz + lgvd",
    ...        "zczc: 2",
    ...        "ptdq: humn - dvpt",
    ...        "dvpt: 3",
    ...        "lfqf: 4",
    ...        "humn: 5",
    ...        "ljgn: 2",
    ...        "sjmn: drzm * dbpl",
    ...        "sllz: 4",
    ...        "pppw: cczh / lfqf",
    ...        "lgvd: ljgn * ptdq",
    ...        "drzm: hmdt - zczc",
    ...        "hmdt: 32",
    ...        ])
    152
    """

    math_monkeys = []
    number_monkeys: dict[str, int] = {}
    for row in data:
        left, right = row.split(': ')
        if ' ' in right:
            parts = right.split(' ')
            math_monkeys.append((
                left,
                OPS[parts[1]],
                parts[0],
                parts[2],
            ))
        else:
            number_monkeys[left] = int(right)

    buffer = math_monkeys[:]

    while 'root' not in number_monkeys:
        unsolved = []
        for monkey in buffer:
            if monkey[2] in number_monkeys and monkey[3] in number_monkeys:
                number_monkeys[monkey[0]] = int(monkey[1](number_monkeys[monkey[2]], number_monkeys[monkey[3]]))
            else:
                unsolved.append(monkey)
        buffer = unsolved

    return number_monkeys['root']


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "root: pppw + sjmn",
    ...        "dbpl: 5",
    ...        "cczh: sllz + lgvd",
    ...        "zczc: 2",
    ...        "ptdq: humn - dvpt",
    ...        "dvpt: 3",
    ...        "lfqf: 4",
    ...        "humn: 5",
    ...        "ljgn: 2",
    ...        "sjmn: drzm * dbpl",
    ...        "sllz: 4",
    ...        "pppw: cczh / lfqf",
    ...        "lgvd: ljgn * ptdq",
    ...        "drzm: hmdt - zczc",
    ...        "hmdt: 32",
    ...        ])
    301
    """

    math_monkeys: list[Monkey] = []
    number_monkeys: dict[str, int] = {}
    for row in data:
        left, right = row.split(': ')
        if ' ' in right:
            parts = right.split(' ')
            math_monkeys.append(
                Monkey(
                    name=left,
                    left=parts[0],
                    right=parts[2],
                    operation=OPS[parts[1]],
                ))
        else:
            number_monkeys[left] = int(right)

    del number_monkeys['humn']
    buffer: list[Monkey] = math_monkeys[:]
    unsolved: list[Monkey]

    while True:
        unsolved = []
        for monkey in buffer:
            if monkey.name == 'root':
                assert isinstance(monkey.left, str)
                assert isinstance(monkey.right, str)

                if monkey.left in number_monkeys:
                    number_monkeys[monkey.right] = number_monkeys[monkey.left]
                elif monkey.right in number_monkeys:
                    number_monkeys[monkey.left] = number_monkeys[monkey.right]
                else:
                    unsolved.append(monkey)
                continue

            if monkey.left in number_monkeys and monkey.right in number_monkeys:
                assert isinstance(monkey.left, str)
                assert isinstance(monkey.right, str)
                number_monkeys[monkey.name] = int(monkey.operation(number_monkeys[monkey.left], number_monkeys[monkey.right]))
            else:
                unsolved.append(monkey)

        if len(buffer) == len(unsolved):
            break
        buffer = unsolved

    assert unsolved

    buffer = unsolved[:]

    # preprocess
    for monkey in buffer:
        if monkey.right in number_monkeys:
            assert isinstance(monkey.right, str)
            monkey.right = number_monkeys[monkey.right]
        if monkey.left in number_monkeys:
            assert isinstance(monkey.left, str)
            monkey.left = number_monkeys[monkey.left]
        assert not (isinstance(monkey.left, int) and isinstance(monkey.right, int))

    equation_monkeys: dict[str, Any] = {
        'humn': symbols('humn'),
    }

    while True:
        unsolved = []
        for monkey in buffer:
            if (isinstance(monkey.left, int) or monkey.left in equation_monkeys) and (isinstance(monkey.right, int) or monkey.right in equation_monkeys):
                # noinspection PyTypeChecker
                equation_monkeys[monkey.name] = monkey.operation(
                    monkey.left if isinstance(monkey.left, int) else equation_monkeys[monkey.left],
                    monkey.right if isinstance(monkey.right, int) else equation_monkeys[monkey.right],
                )
            else:
                unsolved.append(monkey)

        if not unsolved:
            break
        buffer = unsolved

    for name, formula in equation_monkeys.items():
        if name in number_monkeys:
            solution = solve(formula - number_monkeys[name])  # type: ignore
            assert len(solution) == 1
            return int(solution[0])

    assert False


if __name__ == "__main__":
    run_day(21,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
