import os
from typing import List

D_STEPS = {
    'e': (1, 0),
    'se': (0, 1),
    'sw': (-1, 1),
    'w': (-1, 0),
    'nw': (0, -1),
    'ne': (1, -1),
}

BLACK = True
WHITE = False


def load_data(fn: str) -> List[List[str]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            steps = []
            while line:
                for step in ('e', 'se', 'sw', 'w', 'nw', 'ne'):
                    if line.startswith(step):
                        steps.append(step)
                        line = line[len(step):]
                        break

            result.append(steps)

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(step_lists: List[List[str]]) -> int:
    """
    >>> star1(test_data())
    10
    """

    floor = {}
    """
    default = false = white
    """
    for steps in step_lists:
        x = 0
        y = 0

        for step in steps:
            dx, dy = D_STEPS[step]
            x += dx
            y += dy

        floor[(x, y)] = not floor.get((x, y), False)

    return sum(1 for x in floor.values() if x == BLACK)


def star2(step_lists: List[List[str]], debug=False) -> int:
    """
    >>> star2(test_data())
    2208
    """

    floor = {}

    for steps in step_lists:
        x = 0
        y = 0

        for step in steps:
            dx, dy = D_STEPS[step]
            x += dx
            y += dy

        set_tile(floor, x, y, not floor.get((x, y), False))

    for day in range(100):
        changes = []

        for x, y in floor.keys():
            if floor.get((x, y)) == BLACK:
                # count number of blacks around
                blacks = 0
                for dx, dy in D_STEPS.values():
                    adj_x = x + dx
                    adj_y = y + dy

                    if floor.get((adj_x, adj_y), WHITE) == BLACK:
                        blacks += 1

                if blacks == 0 or blacks > 2:
                    changes.append(((x, y), WHITE))

            else:
                # count number of blacks around
                blacks = 0
                for dx, dy in D_STEPS.values():
                    adj_x = x + dx
                    adj_y = y + dy

                    if floor.get((adj_x, adj_y), WHITE) == BLACK:
                        blacks += 1

                if blacks == 2:
                    changes.append(((x, y), BLACK))

        for (x, y), color in changes:
            set_tile(floor, x, y, color)

        if debug:
            print('Day %d: %d' % (day + 1, sum(1 for x in floor.values() if x == BLACK)))

    return sum(1 for x in floor.values() if x)


def set_tile(floor, x, y, color):
    if color == WHITE:
        floor[(x, y)] = WHITE
        return

    floor[(x, y)] = BLACK
    for dx, dy in D_STEPS.values():
        adj_x = x + dx
        adj_y = y + dy

        if floor.get((adj_x, adj_y)) is None:
            floor[(adj_x, adj_y)] = WHITE


if __name__ == "__main__":
    print('Day 24 Star 1: %d' % star1(load_data('data.txt')))
    print('Day 24 Star 2: %s' % star2(load_data('data.txt')))
