import logging
import math
from dataclasses import dataclass

from util.data import txt_fn, load_as_str
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


@dataclass
class Monkey:
    name: int
    items: list[int]

    operator: str
    operation_value: str

    test_divisible: int
    test_true: int
    test_false: int

    inspections: int = 0

    def operation(self, old: int) -> int:
        self.inspections += 1
        value = old if self.operation_value == 'old' else int(self.operation_value)
        return old * value if self.operator == '*' else old + value

    def test(self, item: int) -> int:
        return self.test_true if item % self.test_divisible == 0 else self.test_false


def star1(data: str) -> int:
    """
    >>> star1("Monkey 0:\\n"
    ...        "  Starting items: 79, 98\\n"
    ...        "  Operation: new = old * 19\\n"
    ...        "  Test: divisible by 23\\n"
    ...        "    If true: throw to monkey 2\\n"
    ...        "    If false: throw to monkey 3\\n"
    ...        "\\n"
    ...        "Monkey 1:\\n"
    ...        "  Starting items: 54, 65, 75, 74\\n"
    ...        "  Operation: new = old + 6\\n"
    ...        "  Test: divisible by 19\\n"
    ...        "    If true: throw to monkey 2\\n"
    ...        "    If false: throw to monkey 0\\n"
    ...        "\\n"
    ...        "Monkey 2:\\n"
    ...        "  Starting items: 79, 60, 97\\n"
    ...        "  Operation: new = old * old\\n"
    ...        "  Test: divisible by 13\\n"
    ...        "    If true: throw to monkey 1\\n"
    ...        "    If false: throw to monkey 3\\n"
    ...        "\\n"
    ...        "Monkey 3:\\n"
    ...        "  Starting items: 74\\n"
    ...        "  Operation: new = old + 3\\n"
    ...        "  Test: divisible by 17\\n"
    ...        "    If true: throw to monkey 0\\n"
    ...        "    If false: throw to monkey 1"
    ...        )
    10605
    """

    monkeys = _parse_monkeys(data)

    for r in range(20):
        for monkey in monkeys:
            for item in monkey.items:
                item = monkey.operation(item) // 3
                monkeys[monkey.test(item)].items.append(item)

            monkey.items = []

        logger.debug(f"After round {r + 1}")
        for monkey in monkeys:
            logger.debug(monkey)

    inspections = sorted((m.inspections for m in monkeys), reverse=True)

    return inspections[0] * inspections[1]


def star2(data: str) -> int:
    """
    >>> star2("Monkey 0:\\n"
    ...        "  Starting items: 79, 98\\n"
    ...        "  Operation: new = old * 19\\n"
    ...        "  Test: divisible by 23\\n"
    ...        "    If true: throw to monkey 2\\n"
    ...        "    If false: throw to monkey 3\\n"
    ...        "\\n"
    ...        "Monkey 1:\\n"
    ...        "  Starting items: 54, 65, 75, 74\\n"
    ...        "  Operation: new = old + 6\\n"
    ...        "  Test: divisible by 19\\n"
    ...        "    If true: throw to monkey 2\\n"
    ...        "    If false: throw to monkey 0\\n"
    ...        "\\n"
    ...        "Monkey 2:\\n"
    ...        "  Starting items: 79, 60, 97\\n"
    ...        "  Operation: new = old * old\\n"
    ...        "  Test: divisible by 13\\n"
    ...        "    If true: throw to monkey 1\\n"
    ...        "    If false: throw to monkey 3\\n"
    ...        "\\n"
    ...        "Monkey 3:\\n"
    ...        "  Starting items: 74\\n"
    ...        "  Operation: new = old + 3\\n"
    ...        "  Test: divisible by 17\\n"
    ...        "    If true: throw to monkey 0\\n"
    ...        "    If false: throw to monkey 1"
    ...        )
    2713310158
    """

    monkeys = _parse_monkeys(data)

    modulo = math.prod(m.test_divisible for m in monkeys)

    for _ in range(10000):
        for monkey in monkeys:
            for item in monkey.items:
                item = monkey.operation(item) % modulo
                monkeys[monkey.test(item)].items.append(item)

            monkey.items = []

    inspections = sorted((m.inspections for m in monkeys), reverse=True)

    return inspections[0] * inspections[1]


def _parse_monkeys(data: str) -> list[Monkey]:
    monkeys: list[Monkey] = []
    for row in data.split("\n\n"):
        name_line, items_line, operation_line, test_line_condition, test_line_true, test_line_false = row.split("\n")

        assert int(name_line.split(" ")[1][:-1]) == len(monkeys)
        assert items_line.startswith('  Starting items: ')
        assert test_line_condition.startswith('  Test: divisible by ')
        assert test_line_true.startswith('    If true: throw to monkey ')
        assert test_line_false.startswith('    If false: throw to monkey ')

        starting_items = [int(i) for i in items_line[len('  Starting items: '):].split(", ")]

        operator, value = operation_line[len('  Operation: new = old '):].split(' ')

        monkeys.append(Monkey(
            int(name_line.split(" ")[1][:-1]),
            starting_items,
            operator,
            value,
            int(test_line_condition[len('  Test: divisible by '):]),
            int(test_line_true[len('    If true: throw to monkey '):]),
            int(test_line_false[len('    If false: throw to monkey '):])
        ))
    return monkeys


if __name__ == "__main__":
    init_logger(logging.INFO)
    run_day(11,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
