import re
from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "r, wr, b, g, bwu, rb, gb, br",
    ...        "",
    ...        "brwrr",
    ...        "bggr",
    ...        "gbbr",
    ...        "rrbgbr",
    ...        "ubwu",
    ...        "bwurrg",
    ...        "brgr",
    ...        "bbrgwb",
    ...       ])
    6
    """

    patterns: list[str] = data[0].split(', ')
    designs = data[2:]
    result = 0

    for design in designs:
        if re.match(r'^(' + '|'.join(patterns) + ')*$', design):
            result += 1

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "r, wr, b, g, bwu, rb, gb, br",
    ...        "",
    ...        "brwrr",
    ...        "bggr",
    ...        "gbbr",
    ...        "rrbgbr",
    ...        "ubwu",
    ...        "bwurrg",
    ...        "brgr",
    ...        "bbrgwb",
    ...       ])
    16
    """

    patterns: list[str] = data[0].split(', ')
    designs = data[2:]
    cache: dict[str, int] = defaultdict(int)
    result = 0

    for design in designs:
        result += _rec_check(design, patterns, cache)

    return result


def _rec_check(design: str, patterns: list[str], cache: dict[str, int]) -> int:
    if design == '':
        return 1

    if design in cache:
        return cache[design]

    matches = 0

    for pattern in patterns:
        if design[:len(pattern)] == pattern:
            matches += _rec_check(design[len(pattern):], patterns, cache)

    cache[design] = matches

    return matches


if __name__ == "__main__":
    run_day(19,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
