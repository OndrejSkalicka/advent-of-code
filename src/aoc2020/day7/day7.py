import re
from typing import Dict


def load_data(fn: str) -> Dict[str, Dict[str, int]]:
    result = {}
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            parsed = re.match(r'(\w+ \w+) bags contain (.+ bags?)\.', line)
            if not parsed:
                raise Exception('Cannot parse line "%s"' % line)

            color = parsed[1]
            bag = {}
            for part in parsed[2].split(', '):
                if part == 'no other bags':
                    continue

                parsed_part = re.match(r'(\d+) (\w+ \w+) bags?', part)
                if not parsed_part:
                    raise Exception('Cannot parse part %s' % part)

                bag[parsed_part[2]] = int(parsed_part[1])

            result[color] = bag

    return result


def star1(data: Dict[str, Dict[str, int]]) -> int:
    """
    >>> star1({
    ...     'light red': {'bright white': 1, 'muted yellow': 2},
    ...     'dark orange': {'bright white': 3, 'muted yellow': 4},
    ...     'bright white': {'shiny gold': 1},
    ...     'muted yellow': {'shiny gold': 2, 'faded blue': 9},
    ...     'shiny gold': {'dark olive': 1, 'vibrant plum': 2},
    ...     'dark olive': {'faded blue': 3, 'dotted black': 4},
    ...     'vibrant plum': {'faded blue': 5, 'dotted black': 6},
    ...     'faded blue': {},
    ...     'dotted black': {}
    ... })
    4
    """

    return sum(1 for key in data.keys() if star1_can_contain_gold(key, data))


def star1_can_contain_gold(key: str, data: Dict[str, Dict[str, int]]) -> bool:
    for bag_key in data[key].keys():
        # can contain directly
        if bag_key == 'shiny gold':
            return True

        # can contain indirectly
        if star1_can_contain_gold(bag_key, data):
            return True

    # cannot contain at all
    return False


def star2(data: Dict[str, Dict[str, int]]) -> int:
    """
    >>> star2({
    ...     'light red': {'bright white': 1, 'muted yellow': 2},
    ...     'dark orange': {'bright white': 3, 'muted yellow': 4},
    ...     'bright white': {'shiny gold': 1},
    ...     'muted yellow': {'shiny gold': 2, 'faded blue': 9},
    ...     'shiny gold': {'dark olive': 1, 'vibrant plum': 2},
    ...     'dark olive': {'faded blue': 3, 'dotted black': 4},
    ...     'vibrant plum': {'faded blue': 5, 'dotted black': 6},
    ...     'faded blue': {},
    ...     'dotted black': {}
    ... })
    32

    >>> star2({
    ...     'shiny gold': {'dark red': 2},
    ...     'dark red': {'dark orange': 2},
    ...     'dark orange': {'dark yellow': 2},
    ...     'dark yellow': {'dark green': 2},
    ...     'dark green': {'dark blue': 2},
    ...     'dark blue': {'dark violet': 2},
    ...     'dark violet': {}
    ... })
    126
    """

    return star2_bags_in_bag('shiny gold', data)


def star2_bags_in_bag(key: str, data: Dict[str, Dict[str, int]]) -> int:
    contents = 0
    for bag_key, count in data[key].items():
        contents += (1 + star2_bags_in_bag(bag_key, data)) * count

    return contents


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 7 Star 1: %d' % star1(my_data))
    print('Day 7 Star 2: %d' % star2(my_data))
