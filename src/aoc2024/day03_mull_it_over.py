import re

from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1("xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))")
    161
    """

    result = 0
    for m in re.findall(r'mul\((\d+),(\d+)\)', data):
        result += int(m[0]) * int(m[1])

    return result


def star2(data: str) -> int:
    """
    >>> star2("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))")
    48
    """

    result = 0
    enabled = True
    for m in re.findall(r'(mul\((\d+),(\d+)\)|do\(\)|don\'t\(\))', data):
        if m[0] == 'do()':
            enabled = True
            continue

        if m[0] == "don't()":
            enabled = False
            continue

        if enabled:
            result += int(m[1]) * int(m[2])

    return result


def is_safe(levels: list[int]) -> bool:
    report_inc = levels[0] < levels[1]
    for i in range(len(levels) - 1):
        inc = levels[i] < levels[i + 1]

        if report_inc != inc:
            return False

        if not 1 <= abs(levels[i] - levels[i + 1]) <= 3:
            return False

    return True


if __name__ == "__main__":
    run_day(3,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
