from typing import List


def load_data(fn: str) -> List[int]:
    with open(fn, 'r') as fh:
        return list(int(line.strip()) for line in fh.readlines())


def star1(preamble_size: int, data: List[int]) -> int:
    """
    >>> star1(5, [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576])
    127
    """

    for i in range(preamble_size, len(data)):
        value = data[i]
        preamble = data[i - preamble_size:i]

        if not in_preamble(value, preamble):
            return value


def star2(preamble_size: int, data: List[int]) -> int:
    """
    >>> star2(5, [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576])
    62
    """

    invalid_number = star1(preamble_size, data)

    for range_start in range(len(data)):
        range_length = 2
        overload = False

        while not overload:
            range_values = data[range_start:range_length]
            if sum(range_values) > invalid_number:
                overload = True

            elif sum(range_values) == invalid_number:
                return min(range_values) + max(range_values)

            else:
                range_length += 1


def in_preamble(value: int, preamble: List[int]) -> bool:
    for x in preamble:
        for y in preamble:
            if x != y and x + y == value:
                return True

    return False


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 9 Star 1: %d' % star1(25, my_data))
    print('Day 9 Star 2: %d' % star2(25, my_data))
