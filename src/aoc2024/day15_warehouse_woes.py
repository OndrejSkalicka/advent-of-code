import itertools

from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day
from util.record import Grid, EAST, NORTH, SOUTH, WEST, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["########",
    ...        "#..O.O.#",
    ...        "##@.O..#",
    ...        "#...O..#",
    ...        "#.#.O..#",
    ...        "#...O..#",
    ...        "#......#",
    ...        "########",
    ...        "",
    ...        "<^^>>>vv<v>>v<<",
    ...       ])
    2028
    >>> star1(["##########",
    ...        "#..O..O.O#",
    ...        "#......O.#",
    ...        "#.OO..O.O#",
    ...        "#..O@..O.#",
    ...        "#O#..O...#",
    ...        "#O..O..O.#",
    ...        "#.OO.O.OO#",
    ...        "#....O...#",
    ...        "##########",
    ...        "",
    ...        "<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^",
    ...        "vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v",
    ...        "><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<",
    ...        "<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^",
    ...        "^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><",
    ...        "^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^",
    ...        ">^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^",
    ...        "<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>",
    ...        "^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>",
    ...        "v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^",
    ...       ])
    10092
    """

    grid = Grid.from_list_str(l_split(data, '')[0])
    moves = ''.join(l_split(data, '')[1])

    robot = grid.find_all_xy('@')[0].xy
    grid[robot] = '.'

    for move in moves:
        direction = {'<': WEST, '^': NORTH, '>': EAST, 'v': SOUTH}[move]

        steps = 1
        while True:
            xy = robot + steps * direction
            if grid[xy] in '#.':
                break
            steps += 1

        if grid[xy] == '.':
            grid[xy] = grid[robot + direction]
            grid[robot + direction] = '.'
            robot = robot + direction

    result = 0
    for cell in grid.find_all_xy('O'):
        result += cell.xy.x + 100 * cell.xy.y

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["##########",
    ...        "#..O..O.O#",
    ...        "#......O.#",
    ...        "#.OO..O.O#",
    ...        "#..O@..O.#",
    ...        "#O#..O...#",
    ...        "#O..O..O.#",
    ...        "#.OO.O.OO#",
    ...        "#....O...#",
    ...        "##########",
    ...        "",
    ...        "<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^",
    ...        "vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v",
    ...        "><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<",
    ...        "<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^",
    ...        "^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><",
    ...        "^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^",
    ...        ">^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^",
    ...        "<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>",
    ...        "^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>",
    ...        "v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^",
    ...       ])
    9021
    """

    grid_data = "\n".join(l_split(data, '')[0])
    grid_data = grid_data.replace('.', '..')
    grid_data = grid_data.replace('O', '[]')
    grid_data = grid_data.replace('#', '##')
    grid_data = grid_data.replace('@', '@.')

    grid = Grid.from_list_str(grid_data.split('\n'))
    moves = ''.join(l_split(data, '')[1])
    robot = grid.find_all_xy('@')[0].xy

    for move in moves:

        direction = {'<': WEST, '^': NORTH, '>': EAST, 'v': SOUTH}[move]

        if move in '<>':
            what_to_move = _what_to_move_lr(grid, robot, direction)
        else:
            what_to_move = _what_to_move_up(grid, robot, direction)

        for to_move in reversed(what_to_move):
            assert grid[to_move + direction] == '.'
            if grid[to_move] == '@':
                robot = to_move + direction
            grid[to_move + direction] = grid[to_move]
            grid[to_move] = '.'

    result = 0
    for cell in grid.find_all_xy('['):
        result += cell.xy.x + 100 * cell.xy.y

    return result


def _what_to_move_lr(grid: Grid[str], robot: Xy, direction: Xy) -> list[Xy]:
    has_to_move = [robot]
    while True:
        piece_xy = has_to_move[-1] + direction

        if grid[piece_xy] == '#':
            return []

        if grid[piece_xy] in '[]':
            has_to_move.append(piece_xy)

        if grid[piece_xy] == '.':
            return has_to_move


def _what_to_move_up(grid: Grid[str], robot: Xy, direction: Xy) -> list[Xy]:
    has_to_move = [{robot}]

    while True:
        new_has_to_move = set()

        # check HTM line
        for xy in has_to_move[-1]:

            piece_xy = xy + direction
            if grid[piece_xy] == '#':
                return []

            if grid[piece_xy] == '[':
                new_has_to_move.add(piece_xy)
                new_has_to_move.add(piece_xy + Xy(1, 0))

            if grid[piece_xy] == ']':
                new_has_to_move.add(piece_xy)
                new_has_to_move.add(piece_xy - Xy(1, 0))

        if not new_has_to_move:
            return list(itertools.chain.from_iterable(has_to_move))

        has_to_move.append(new_has_to_move)


if __name__ == "__main__":
    run_day(15,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
