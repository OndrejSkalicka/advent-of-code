def load_data(fn: str) -> tuple[list[str], list[str]]:
    with open(fn, 'r') as fh:
        return (
            list(fh.readline().split(',')),
            list(fh.readline().split(',')),
        )


def star1(first: list[str], second: list[str]) -> int:
    """
    >>> star1('R8,U5,L5,D3'.split(','), 'U7,R6,D4,L4'.split(','))
    6

    >>> star1('R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','), 'U62,R66,U55,R34,D71,R55,D58,R83'.split(','))
    159

    >>> star1('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','), 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(','))
    135
    """

    first_expanded = expand(first)

    crossings_distance: list[int] = []
    for point in expand(second).keys():
        if point in first_expanded:
            crossings_distance.append(abs(point[0]) + abs(point[1]))

    return min(crossings_distance)


def star2(first: list[str], second: list[str]) -> int:
    """
    >>> star2('R8,U5,L5,D3'.split(','), 'U7,R6,D4,L4'.split(','))
    30

    >>> star2('R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','), 'U62,R66,U55,R34,D71,R55,D58,R83'.split(','))
    610

    >>> star2('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','), 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(','))
    410
    """

    first_expanded = expand(first)

    crossings_steps: list[int] = []
    for point, steps in expand(second).items():
        if point in first_expanded:
            crossings_steps.append(
                steps + first_expanded[point]
            )

    return min(crossings_steps)


def expand(wire: list[str]) -> dict[tuple[int, int], int]:
    result = dict()

    x = 0
    y = 0
    step = 0

    for coord in wire:
        direction = coord[0]
        steps = int(coord[1:])

        match direction:
            case 'R':
                vector = (1, 0)
            case 'L':
                vector = (-1, 0)
            case 'U':
                vector = (0, 1)
            case 'D':
                vector = (0, -1)
            case _:
                raise Exception(f"Illegal direction {direction}")

        for i in range(steps):
            x += vector[0]
            y += vector[1]
            step += 1
            if (x, y) not in result:
                result[(x, y)] = step

    return result


if __name__ == "__main__":
    print('Day 3 Star 1: %d' % star1(*load_data('data.txt')))
    print('Day 3 Star 2: %d' % star2(*load_data('data.txt')))
