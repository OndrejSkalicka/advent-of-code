import os
from typing import List, Tuple, Optional, Set


def load_data(fn: str = 'data.txt') -> Tuple[List[Tuple[int, int]], List[Tuple[str, int]]]:
    result = (
        [],
        []
    )

    with open(fn, 'r') as fh:
        phase = 'dots'
        for line in fh.readlines():
            line = line.strip()

            if not line:
                phase = 'folds'
                continue

            if phase == 'dots':
                a, b = line.split(',')
                result[0].append((int(a), int(b)))

            else:
                line = line[len('fold along '):]
                a, b = line.split('=')
                result[1].append((a, int(b)))

    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(data: Tuple[List[Tuple[int, int]], List[Tuple[str, int]]]) -> int:
    """
    >>> star1(test_data())
    17
    """

    paper = fold_paper(data, max_iter=1)

    return len(paper)


def star2(data: Tuple[List[Tuple[int, int]], List[Tuple[str, int]]]) -> str:
    """
    >>> print(star2(test_data()))
    #####
    #...#
    #...#
    #...#
    #####
    """

    paper = fold_paper(data)

    return dump(paper)


def fold_paper(data: Tuple[List[Tuple[int, int]], List[Tuple[str, int]]], max_iter: Optional[int] = None) -> Set[Tuple[int, int]]:
    paper = set()

    dots = data[0]
    folds = data[1]
    for x, y in dots:
        paper.add((x, y))

    cnt = 0
    for fold_along, fold_value in folds:
        new_paper = set()

        for x, y in paper:
            if fold_along == 'x' and x < fold_value or fold_along == 'y' and y < fold_value:
                new_paper.add((x, y))

            elif fold_along == 'x':
                new_paper.add((2 * fold_value - x, y))

            elif fold_along == 'y':
                new_paper.add((x, 2 * fold_value - y))

            else:
                raise Exception('Exploded')

        paper = new_paper
        cnt += 1
        if max_iter is not None and cnt >= max_iter:
            break

    return paper


def dump(paper: Set[Tuple[int, int]]) -> str:
    res = ''
    max_x = max(xy[0] for xy in paper)
    max_y = max(xy[1] for xy in paper)

    for y in range(max_y + 1):
        for x in range(max_x + 1):
            res += '#' if (x, y) in paper else '.'

        if y < max_y:
            res += "\n"

    return res


if __name__ == "__main__":
    print('Day 13 Star 1: %d' % star1(load_data()))
    print('Day 13 Star 2: \n%s' % star2(load_data()))
