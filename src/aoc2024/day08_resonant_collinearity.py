import itertools
from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["..........",
    ...        "..........",
    ...        "..........",
    ...        "....a.....",
    ...        "..........",
    ...        ".....a....",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",])
    2
    >>> star1(["..........",
    ...        "..........",
    ...        "..........",
    ...        "....a.....",
    ...        "........a.",
    ...        ".....a....",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",])
    4
    >>> star1(["............",
    ...        "........0...",
    ...        ".....0......",
    ...        ".......0....",
    ...        "....0.......",
    ...        "......A.....",
    ...        "............",
    ...        "............",
    ...        "........A...",
    ...        ".........A..",
    ...        "............",
    ...        "............",])
    14
    """

    grid = Grid.from_list_str(data)

    groups: dict[str, list[Xy]] = defaultdict(list)

    for cell in grid.find_all_xy(lambda e: e != '.'):
        groups[cell.value].append(cell.xy)

    antinodes = set()

    for antenna in groups.values():
        for a, b in itertools.combinations(antenna, 2):
            antinodes.add(2 * b - a)
            antinodes.add(2 * a - b)

    return len(list(xy for xy in antinodes if xy in grid))


def star2(data: list[str]) -> int:
    """
    >>> star2(["T.........",
    ...        "...T......",
    ...        ".T........",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",
    ...        "..........",])
    9
    >>> star2(["............",
    ...        "........0...",
    ...        ".....0......",
    ...        ".......0....",
    ...        "....0.......",
    ...        "......A.....",
    ...        "............",
    ...        "............",
    ...        "........A...",
    ...        ".........A..",
    ...        "............",
    ...        "............",])
    34
    """

    grid = Grid.from_list_str(data)

    groups: dict[str, list[Xy]] = defaultdict(list)

    for cell in grid.find_all_xy(lambda e: e != '.'):
        groups[cell.value].append(cell.xy)

    antinodes = set()

    for antenna in groups.values():
        for a, b in itertools.combinations(antenna, 2):
            di = 0
            while True:
                added = False
                xy = b + di * (b - a)
                if xy in grid:
                    antinodes.add(xy)
                    added = True
                xy = a + di * (a - b)
                if xy in grid:
                    antinodes.add(xy)
                    added = True

                di += 1
                if not added:
                    break

    return len(antinodes)


if __name__ == "__main__":
    run_day(8,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
