import os
from collections import deque
from typing import List


def load_data(fn: str = 'data.txt') -> List[List[str]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            result.append(list(line))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


PAIRS = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}
SCORE = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
    '(': 1,
    '[': 2,
    '{': 3,
    '<': 4,
}


def star1(data: List[List[str]]) -> int:
    """
    >>> star1(test_data())
    26397
    """

    score = 0
    for line in data:
        buffer = deque()

        for c in line:
            if c in PAIRS:
                buffer.append(c)
                continue

            if c in PAIRS.values():
                top = buffer.pop()
                if PAIRS[top] != c:
                    score += SCORE[c]
                    break

    return score


def star2(data: List[List[str]]) -> int:
    """
    >>> star2(test_data())
    288957
    """

    scores = []
    for line in data:
        buffer = deque()
        incorrect = False

        for c in line:
            if c in PAIRS:
                buffer.append(c)
                continue

            if c in PAIRS.values():
                top = buffer.pop()
                if PAIRS[top] != c:
                    incorrect = True
                    break

        if incorrect:
            continue

        line_score = 0
        while buffer:
            line_score *= 5
            line_score += SCORE[buffer.pop()]

        scores.append(line_score)

    scores.sort()
    return scores[int(len(scores) / 2)]


if __name__ == "__main__":
    print('Day 10 Star 1: %d' % star1(load_data()))
    print('Day 10 Star 2: %d' % star2(load_data()))
