from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["vJrwpWtwJgWrhcsFMMfFFhFp",
    ...        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
    ...        "PmmdzqPrVvPwwTWBwg",
    ...        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
    ...        "ttgJtRGJQctTZtZT",
    ...        "CrZsJsPPZsGzwwsLwLmpwMDw"])
    157
    """

    priorities = []

    for row in data:
        shared = set(row[:(len(row) // 2)]).intersection(set(row[(len(row) // 2):]))
        assert len(shared) == 1
        priorities.append(priority(max(shared)))

    return sum(priorities)


def star2(data: list[str]) -> int:
    """
    >>> star2(["vJrwpWtwJgWrhcsFMMfFFhFp",
    ...        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
    ...        "PmmdzqPrVvPwwTWBwg",
    ...        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
    ...        "ttgJtRGJQctTZtZT",
    ...        "CrZsJsPPZsGzwwsLwLmpwMDw"])
    70
    """

    priorities = []
    for i in range(0, len(data), 3):
        badge = set(data[i]).intersection(set(data[i + 1])).intersection(set(data[i + 2]))
        assert len(badge) == 1
        priorities.append(priority(max(badge)))

    return sum(priorities)


def priority(c: str) -> int:
    if c.islower():
        priority = ord(c) - ord('a') + 1
    else:
        priority = ord(c) - ord('A') + 27
    return priority


if __name__ == "__main__":
    run_day(3,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
