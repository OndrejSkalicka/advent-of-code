def reduce(polymer: str) -> str:
    """
    >>> reduce('dabAcCaCBAcCcaDA')
    'dabCBAcaDA'
    """

    i = 0
    result = []
    while i < len(polymer):
        if len(result) == 0:
            # empty stack, no possible match
            result.append(polymer[i])
        elif abs(ord(result[len(result) - 1]) - ord(polymer[i])) == 32:
            # match both stacks, delete both
            result.pop()
        else:
            # no match, move from one stack to another
            result.append(polymer[i])

        i += 1

    return ''.join(result)


def reduce_2(polymer: str) -> str:
    """
    >>> reduce_2('dabAcCaCBAcCcaDA')
    'daDA'
    """

    best = None
    for i in range(ord('a'), ord('z') + 1):
        char = chr(i)
        subpolymer = polymer.replace(char, '').replace(char.upper(), '')
        reduced = reduce(subpolymer)
        if best is None or len(best) > len(reduced):
            best = reduced

    return best


def load_data():
    with open('data.txt', 'r') as fh:
        return fh.read().strip()


if __name__ == "__main__":
    data = load_data()
    print('Day 5 Star 1: %d' % len(reduce(data)))
    print('Day 5 Star 2: %d' % len(reduce_2(data)))
