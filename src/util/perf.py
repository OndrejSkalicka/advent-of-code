from time import perf_counter
from typing import Callable, Union, Optional


def run_day(day: int, callback_1: Callable[[], Union[str, int]], callback_2: Optional[Callable[[], Union[str, int]]] = None,
            callback_3: Optional[Callable[[], Union[str, int]]] = None) -> None:
    perf_start = perf_counter()
    result = callback_1()
    duration = perf_counter() - perf_start
    print(f'Day {day} Star 1, Duration {format_s_ms_ns(duration)} >> \n{result}')

    if callback_2 is not None:
        perf_start = perf_counter()
        result = callback_2()
        duration = perf_counter() - perf_start
        print(f'Day {day} Star 2, Duration {format_s_ms_ns(duration)} >> \n{result}')

    if callback_3 is not None:
        perf_start = perf_counter()
        result = callback_3()
        duration = perf_counter() - perf_start
        print(f'Day {day} Star 3, Duration {format_s_ms_ns(duration)} >> \n{result}')


def format_s_ms_ns(duration: float) -> str:
    """
    >>> format_s_ms_ns(1.23456789)
    '1.2s'

    >>> format_s_ms_ns(12.3456789)
    '12.3s'

    >>> format_s_ms_ns(123456.789)
    '123,456.8s'

    >>> format_s_ms_ns(0.1234)
    '123.4ms'

    >>> format_s_ms_ns(0.0001234)
    '123.4µs'

    >>> format_s_ms_ns(0.0000001234)
    '123.4ns'

    >>> format_s_ms_ns(0.0000000001234)
    '0.1ns'
    """
    suffix = ['s', 'ms', 'µs', 'ns']
    order = 0

    while order < len(suffix):
        if duration > 1.0:
            return f'{duration:,.1f}{suffix[order]}'

        duration *= 1000
        order += 1

    return f'{duration / 1000:,.1f}{suffix[-1]}'
