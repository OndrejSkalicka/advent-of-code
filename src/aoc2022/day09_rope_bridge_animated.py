from dataclasses import dataclass, field, InitVar

import tcod
from tcod.event import KeySym

from util.data import load_strings_newline
from util.record import Xy, XyMutable

WIDTH, HEIGHT = 80, 40
VIEWPORT_BORDER = 10
DIRECTION_TO_DX = {
    'R': Xy(1, 0),
    'L': Xy(-1, 0),
    'U': Xy(0, -1),
    'D': Xy(0, 1),
}


@dataclass
class Bridge:
    data: InitVar[list[str]]

    length: int

    instructions: list[tuple[Xy, int]] = field(init=False)
    active_instruction: int = 0
    active_step: int = 0
    visited: set[Xy] = field(default_factory=set)

    rope: list[Xy] = field(default_factory=list, init=False)

    def __post_init__(self, data: list[str]) -> None:
        self.instructions = []
        for row in data:
            c, d = row.split(' ')
            self.instructions.append((DIRECTION_TO_DX[c], int(d)))
        self.reset()

    def reset(self) -> None:
        self.active_instruction = 0
        self.active_step = 0
        self.rope = [Xy(0, 0)] * self.length
        self.visited = set()

    def tick(self) -> None:
        instruction = self.instructions[self.active_instruction]
        dx = instruction[0]
        distance = instruction[1]

        self.rope[0] += dx

        for i in range(self.length - 1):
            self.rope[i + 1] = _move_tail(self.rope[i], self.rope[i + 1])
        self.visited.add(self.rope[-1])

        self.active_step += 1
        if self.active_step >= distance:
            self.active_instruction += 1
            self.active_step = 0

        if self.active_instruction >= len(self.instructions):
            self.reset()


def _simulate(data: list[str], length: int = 2) -> int:
    rope = [Xy(0, 0)] * length

    visited: set[Xy] = set()

    for direction, distance in (row.split(' ') for row in data):
        dx = DIRECTION_TO_DX[direction]
        for _ in range(int(distance)):
            rope[0] += dx

            for i in range(length - 1):
                rope[i + 1] = _move_tail(rope[i], rope[i + 1])
            visited.add(rope[-1])

    print("X: %d / %d" % (min(v.x for v in visited), max(v.x for v in visited)))
    print("Y: %d / %d" % (min(v.y for v in visited), max(v.y for v in visited)))

    return len(visited)


def _move_tail(head: Xy, tail: Xy) -> Xy:
    """
    >>> _move_tail(Xy(2, 1), Xy(1, 3))
    Xy(x=2, y=2)

    >>> _move_tail(Xy(3, 2), Xy(1, 3))
    Xy(x=2, y=2)
    """
    dx = head - tail
    assert abs(dx.x) + abs(dx.y) <= 4

    if abs(dx.x) <= 1 and abs(dx.y) <= 1:
        return tail

    if abs(dx.x) == 2:
        dx = Xy(dx.x // 2, dx.y)

    if abs(dx.y) == 2:
        dx = Xy(dx.x, dx.y // 2)
    return tail + dx


def main(bridge: Bridge) -> None:
    with tcod.context.new(
            columns=WIDTH, rows=HEIGHT,
            title="Day 9: Rope Bridge",
    ) as context:
        root_console = tcod.Console(WIDTH, HEIGHT, order="F")

        viewport = XyMutable(- WIDTH // 2, - HEIGHT // 2)
        paused = True
        energy = 0

        while True:
            head = bridge.rope[0]

            if head.x > viewport.x + (WIDTH - VIEWPORT_BORDER):
                viewport.x = head.x - (WIDTH - VIEWPORT_BORDER)
                print(f"Change viewport X+ to {viewport}")
            if head.x < viewport.x + VIEWPORT_BORDER:
                viewport.x = head.x - VIEWPORT_BORDER
                print(f"Change viewport X- to {viewport}")
            if head.y > viewport.y + (HEIGHT - VIEWPORT_BORDER):
                viewport.y = head.y - (HEIGHT - VIEWPORT_BORDER)
                print(f"Change viewport Y+ to {viewport}")
            if head.y < viewport.y + VIEWPORT_BORDER:
                viewport.y = head.y - VIEWPORT_BORDER
                print(f"Change viewport Y- to {viewport}")

            root_console.clear()

            for v in bridge.visited:
                root_console.print(x=v.x - viewport.x, y=v.y - viewport.y, string='.')

            for i, knot in enumerate(bridge.rope[::-1]):
                i = bridge.length - i - 1
                c = 'H' if i == 0 else f'{i}'

                root_console.print(x=knot.x - viewport.x, y=knot.y - viewport.y, string=c)

            if not paused:
                energy += 1
                if energy == 2:
                    bridge.tick()
                    energy = 0
            context.present(root_console)

            for event in tcod.event.get():
                context.convert_event(event)

                if event.type == 'KEYDOWN' and event.sym == KeySym.q:
                    raise SystemExit()

                if event.type == 'KEYDOWN' and event.sym == KeySym.p:
                    paused = not paused

                if event.type == 'KEYDOWN' and event.sym == KeySym.r:
                    bridge.reset()
                    viewport = XyMutable(- WIDTH // 2, - HEIGHT // 2)

                if isinstance(event, tcod.event.Quit):
                    raise SystemExit()


if __name__ == "__main__":
    bridge = Bridge(data=load_strings_newline('day09_rope_bridge.txt'), length=10)
    main(bridge)
