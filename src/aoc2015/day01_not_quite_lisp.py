from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1('(())')
    0
    >>> star1('()()')
    0
    >>> star1('(((')
    3
    >>> star1('(()(()(')
    3
    >>> star1('))(((((')
    3
    >>> star1('())')
    -1
    >>> star1('))(')
    -1
    >>> star1(')))')
    -3
    >>> star1(')())())')
    -3
    """

    return sum(1 for c in data if c == '(') - sum(1 for c in data if c == ')')


def star2(data: str) -> int:
    """
    >>> star2(')')
    1
    >>> star2('()())')
    5
    """

    floor = 0

    for idx, c in enumerate(data):
        if c == '(':
            floor += 1
        else:
            floor -= 1
            if floor < 0:
                return idx + 1
    return -1


if __name__ == "__main__":
    run_day(1,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
