import math
from collections import deque
from typing import Deque, Optional, List


class Packet:

    def __init__(self,
                 version: int,
                 type_id: int,
                 literal_value: Optional[int] = None,
                 subpackets: List['Packet'] = None):
        self.version = version
        self.type_id = type_id
        self.literal_value = literal_value
        self.subpackets = subpackets

    def packet_value(self) -> int:
        if self.type_id == 4:
            return self.literal_value

        if self.type_id == 0:
            return sum(packet.packet_value() for packet in self.subpackets)

        if self.type_id == 1:
            return math.prod(packet.packet_value() for packet in self.subpackets)

        if self.type_id == 2:
            return min(packet.packet_value() for packet in self.subpackets)

        if self.type_id == 3:
            return max(packet.packet_value() for packet in self.subpackets)

        if self.type_id == 5:
            return 1 if self.subpackets[0].packet_value() > self.subpackets[1].packet_value() else 0

        if self.type_id == 6:
            return 1 if self.subpackets[0].packet_value() < self.subpackets[1].packet_value() else 0

        if self.type_id == 7:
            return 1 if self.subpackets[0].packet_value() == self.subpackets[1].packet_value() else 0

        raise Exception("Wrong type id %d" % self.type_id)

    def __str__(self) -> str:
        return '[V=%d T=%d val=%s]' % (self.version, self.type_id, self.literal_value)


def load_data(fn: str = 'data.txt') -> Deque[int]:
    with open(fn, 'r') as fh:
        return hex_to_bits(fh.readline().strip())


HEX = {
    '0': '0000',
    '1': '0001',
    '2': '0010',
    '3': '0011',
    '4': '0100',
    '5': '0101',
    '6': '0110',
    '7': '0111',
    '8': '1000',
    '9': '1001',
    'A': '1010',
    'B': '1011',
    'C': '1100',
    'D': '1101',
    'E': '1110',
    'F': '1111',
}

LOG = False


def log(str: str) -> None:
    if LOG:
        print(str)


def star1(bits: Deque[int]) -> int:
    """
    >>> star1(hex_to_bits("8A004A801A8002F478"))
    16
    >>> star1(hex_to_bits("620080001611562C8802118E34"))
    12
    >>> star1(hex_to_bits("C0015000016115A2E0802F182340"))
    23
    >>> star1(hex_to_bits("A0016C880162017C3686B18A3D4780"))
    31
    """

    root = parse_packet(bits)

    sum_version = 0
    buffer = [root]

    while buffer:
        packet = buffer.pop()
        sum_version += packet.version

        if packet.type_id != 4:
            buffer += packet.subpackets

    return sum_version


def star2(bits: Deque[int]) -> int:
    """
    >>> star2(hex_to_bits("C200B40A82"))
    3
    >>> star2(hex_to_bits("04005AC33890"))
    54
    >>> star2(hex_to_bits("880086C3E88112"))
    7
    >>> star2(hex_to_bits("CE00C43D881120"))
    9
    >>> star2(hex_to_bits("D8005AC2A8F0"))
    1
    >>> star2(hex_to_bits("F600BC2D8F"))
    0
    >>> star2(hex_to_bits("9C005AC2F8F0"))
    0
    >>> star2(hex_to_bits("9C0141080250320F1802104A08"))
    1
    """
    root = parse_packet(bits)

    return root.packet_value()


def parse_packet(bits: Deque[int]) -> Packet:
    log("Parsing %s" % ''.join(str(x) for x in reversed(bits)))

    version = pop_int(bits, 3)
    type_id = pop_int(bits, 3)

    log("Version: %d" % version)
    log("Type_id: %d" % type_id)

    if type_id == 4:
        result_b = ''

        while True:

            part = pop_str(bits, 5)
            result_b += part[1:]
            log("T4 packet partial result: %s" % result_b)

            if part[0] == '0':
                break

        literal = int(result_b, 2)
        log("T4 packet: %d" % literal)

        return Packet(version, type_id, literal_value=literal)

    else:
        length_type_id_b = pop_str(bits, 1)

        subpackets = []

        if length_type_id_b == '0':
            total_length_in_bits_of_subpackets = pop_int(bits, 15)
            log("total_length_in_bits_of_subpackets: %d" % total_length_in_bits_of_subpackets)

            next_bits = pop_bits(bits, total_length_in_bits_of_subpackets)
            while next_bits:
                sub = parse_packet(next_bits)
                subpackets.append(sub)
                log("Parsed subpacket %s" % sub)

        else:
            number_of_subpackets = pop_int(bits, 11)
            log("number_of_subpackets: %d" % number_of_subpackets)

            for _ in range(number_of_subpackets):
                sub = parse_packet(bits)
                subpackets.append(sub)
                log("Parsed subpacket %s" % sub)

        return Packet(version, type_id, subpackets=subpackets)


def pop_str(bits: Deque[int], count: int) -> str:
    result = ''
    for _ in range(count):
        result += str(bits.popleft())

    return result


def pop_int(bits: Deque[int], count: int) -> int:
    return int(pop_str(bits, count), 2)


def pop_bits(bits: Deque[int], count: int) -> Deque[int]:
    result = deque()
    for _ in range(count):
        result.append(bits.popleft())

    return result


def hex_to_bits(input: str) -> Deque[int]:
    result = deque()
    for c in input:
        result += list(int(x) for x in HEX[c])

    return result


if __name__ == "__main__":
    LOG = True
    print('Day 16 Star 1: %d' % star1(load_data()))
    print('Day 16 Star 2: %d' % star2(load_data()))
