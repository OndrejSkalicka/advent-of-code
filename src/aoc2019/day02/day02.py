from aoc2019.intcode import Program
from util.data import load_numbers_csv


def star1(memory: list[int]) -> int:
    memory[1] = 12
    memory[2] = 2

    program = Program(memory)
    program.run()
    return program.memory[0]


def star2(memory: list[int]) -> int:
    for noun in range(100):
        for verb in range(100):
            new_memory = memory[:]

            new_memory[1] = noun
            new_memory[2] = verb

            program = Program(new_memory)
            program.run()

            if program.memory[0] == 19690720:
                return 100 * noun + verb

    raise Exception("Solution not found")


if __name__ == "__main__":
    print('Day 2 Star 1: %d' % star1(load_numbers_csv('data.txt')))
    print('Day 2 Star 2: %d' % star2(load_numbers_csv('data.txt')))
