from collections import defaultdict

from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1("125 17")
    55312
    """
    return stone_it(data, 25)


def star2(data: str) -> int:
    return stone_it(data, 75)


def stone_it(data: str, blinks: int) -> int:
    stones: dict[int, int] = defaultdict(int)
    for c in data.split(' '):
        stones[int(c)] += 1

    for i in range(blinks):
        new_stones: dict[int, int] = defaultdict(int)
        for stone, count in stones.items():
            if stone == 0:
                new_stones[1] += count
            elif len(str(stone)) % 2 == 0:
                s = str(stone)

                new_stones[int(s[:len(s) // 2])] += count
                new_stones[int(s[len(s) // 2:])] += count
            else:
                new_stones[stone * 2024] += count
        stones = new_stones

    return sum(stones.values())


if __name__ == "__main__":
    run_day(11,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
