package aoc2020.day06;

import java.time.Instant;

public class App {
    public static void main(String... args) {
        System.out.println(new Day23().star1("853192647", 100));
        System.out.println(new Day23().star2("853192647"));
    }
}
