import logging

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


def star1(memory: list[int]) -> int:
    program = Program(memory)

    program.write_ascii_command("NOT C J")
    program.write_ascii_command("AND D J")
    program.write_ascii_command("NOT A T")
    program.write_ascii_command("OR T J")

    program.write_ascii_command("WALK")
    program.run()

    while program.output:

        o = program.read_output()
        if o > 1000:
            return o

        print(chr(o), end='')
    return 0


def star2(memory: list[int]) -> int:
    program = Program(memory)

    # hole ahead
    program.write_ascii_command("NOT A J")

    program.write_ascii_command("NOT C T")
    program.write_ascii_command("AND D T")
    program.write_ascii_command("OR T J")

    # reset T=False
    program.write_ascii_command("NOT A T")
    program.write_ascii_command("AND A T")

    # JUMP if !B AND !E (move 1 forward will force jump next turn)
    program.write_ascii_command("OR B T")
    program.write_ascii_command("OR E T")
    program.write_ascii_command("NOT T T")
    program.write_ascii_command("OR T J")

    # reset T=False
    program.write_ascii_command("NOT A T")
    program.write_ascii_command("AND A T")

    # NEVER jump if !E AND !H
    program.write_ascii_command("OR E T")
    program.write_ascii_command("OR H T")
    program.write_ascii_command("AND T J")

    program.write_ascii_command("RUN")
    program.run()

    while program.output:

        o = program.read_output()
        if o > 1000:
            return o

        print(chr(o), end='')
    return 0


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(19,
            lambda: star1(load_numbers_csv('day21.txt')),
            lambda: star2(load_numbers_csv('day21.txt')),
            )
