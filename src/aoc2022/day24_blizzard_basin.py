from dataclasses import dataclass, field
from typing import Iterable

from util.data import load_strings_newline, txt_fn
from util.dijkstra import dijkstra
from util.perf import run_day
from util.record import Xy


@dataclass
class Map:
    initial: list[list[str]]
    cache: dict[int, list[list[str]]] = field(init=False, default_factory=dict)

    def at(self, step: int) -> list[list[str]]:
        if step in self.cache:
            return self.cache[step]
        map_at = [['.'] * len(self.initial[0]) for _ in range(len(self.initial))]

        for y, row in enumerate(self.initial):
            for x, c in enumerate(row):
                if c == '#':
                    map_at[y][x] = c
                    continue

                if c in '<>v^':
                    dx, dy = {
                        '>': (1, 0),
                        '<': (-1, 0),
                        '^': (0, -1),
                        'v': (0, 1),
                    }[c]

                    new_x = (x - 1 + step * dx) % (len(row) - 2) + 1
                    new_y = (y - 1 + step * dy) % (len(self.initial) - 2) + 1

                    if map_at[new_y][new_x] == '.':
                        map_at[new_y][new_x] = c
                    elif map_at[new_y][new_x] in '<>v^':
                        map_at[new_y][new_x] = '2'
                    else:
                        map_at[new_y][new_x] = str(int(map_at[new_y][new_x]) + 1)
        self.cache[step] = map_at
        return map_at


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "#.######",
    ...        "#>>.<^<#",
    ...        "#.<..<<#",
    ...        "#>v.><>#",
    ...        "#<^v^^>#",
    ...        "######.#",
    ...        ])
    18
    """

    start_xy = Xy(data[0].index('.'), 0)
    goal_xy = Xy(data[-1].index('.'), len(data) - 1)
    initial: Map = Map(list(list(r) for r in data))

    return _distance_to(start_xy, goal_xy, initial)


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "#.######",
    ...        "#>>.<^<#",
    ...        "#.<..<<#",
    ...        "#>v.><>#",
    ...        "#<^v^^>#",
    ...        "######.#",
    ...        ])
    54
    """

    start_xy = Xy(data[0].index('.'), 0)
    goal_xy = Xy(data[-1].index('.'), len(data) - 1)
    initial: Map = Map(list(list(r) for r in data))

    time_there = _distance_to(start_xy, goal_xy, initial)
    time_back = _distance_to(goal_xy, start_xy, initial, start_step=time_there)
    return time_there + time_back + _distance_to(start_xy, goal_xy, initial, start_step=time_there + time_back)


def _distance_to(start: Xy, goal: Xy, initial: Map, start_step: int = 0) -> int:
    distances, _ = dijkstra((start, start_step), adjacent=lambda node: _adjacent(node[0], node[1], initial), destination=lambda node: node[0] == goal)
    for k, v in distances.items():
        if k[0] == goal:
            return v

    assert False


def _adjacent(pos: Xy, step: int, initial: Map) -> Iterable[tuple[tuple[Xy, int], int]]:
    map_at = initial.at(step + 1)
    for new_pos in list(pos.adjacent(map_at)) + [pos]:
        if map_at[new_pos.y][new_pos.x] == '.':
            yield (new_pos, step + 1), 1


if __name__ == "__main__":
    run_day(24,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
