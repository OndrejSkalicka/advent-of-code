#include <iostream>
#include <map>
#include <set>
#include <chrono>

using std::string;
using std::map;
using std::set;

class Cup {
public:
    int value;
    Cup *nextCup;

    void insertAfter(Cup *target) {
        Cup *currentNext = nextCup;
        nextCup = target;

        Cup *active = target;
        while (active->nextCup) {
            active = active->nextCup;
        }
        active->nextCup = currentNext;
    }

    Cup *pluck_next(int count) {
        Cup *pluckRoot = nextCup;
        Cup *active = nextCup;

        for (int i = 0; i < count - 1; ++i) {
            active = active->nextCup;
        }

        nextCup = active->nextCup;
        active->nextCup = nullptr;

        return pluckRoot;
    }
};

Cup *parseCups(const string &cupsDefinition) {
    Cup *previous = nullptr;
    Cup *first = nullptr;
    Cup *current;

    for (char i: cupsDefinition) {
        int cupId = i - '0';

        current = new Cup();
        current->value = cupId;

        if (previous) {
            previous->nextCup = current;
        }

        previous = current;

        if (!first) {
            first = current;
        }
    }

    previous->nextCup = first;

    return first;
}


void fillCupMap(Cup *rootCup, Cup **target) {
    for (int i = 0; i <= sizeof(target); ++i) {
        target[i] = nullptr;
    }

    Cup *currentCup = rootCup;
    while (!target[currentCup->value]) {
        target[currentCup->value] = currentCup;
        currentCup = currentCup->nextCup;
    }
}

int dec(int val, int cupCount) {
    int result = val - 1;
    if (result > 0) {
        return result;
    }

    return result + cupCount;
}


Cup *crabGame(Cup *rootCup, int iterations, int maxValue) {
    Cup **cupMap = new Cup *[maxValue + 1];
    fillCupMap(rootCup, cupMap);

    Cup *currentCup = rootCup;

    for (int i = 0; i < iterations; ++i) {
        Cup *pluck3 = currentCup->pluck_next(3);

        int value1 = pluck3->value;
        int value2 = pluck3->nextCup->value;
        int value3 = pluck3->nextCup->nextCup->value;

        int destination = dec(currentCup->value, maxValue);
        while (destination == value1 || destination == value2 || destination == value3) {
            destination = dec(destination, maxValue);
        }

        cupMap[destination]->insertAfter(pluck3);
        currentCup = currentCup->nextCup;
    }

    return currentCup;
}

long long star2(const string &cupsDefinition, int iterations) {
    const int MAX_VALUE = 1 * 1000 * 1000;

    Cup *rootCup = parseCups(cupsDefinition);

    set<int> initialValues;
    Cup *lastCup;
    Cup *currentCup = rootCup;

    do {
        initialValues.insert(currentCup->value);
        lastCup = currentCup;
        currentCup = currentCup->nextCup;
    } while (!initialValues.contains(currentCup->value));

    int maxInitialValue = *(initialValues.rbegin());
    Cup **initialCupMap = new Cup *[maxInitialValue + 1];
    fillCupMap(rootCup, initialCupMap);
    int extrasCounter = maxInitialValue + 1;
    Cup *extrasRoot = nullptr;
    Cup *extrasLast = nullptr;

    while (extrasCounter <= MAX_VALUE) {
        Cup *newCup = new Cup();
        newCup->value = extrasCounter;
        extrasCounter++;

        if (!extrasRoot) {
            extrasRoot = newCup;
        }

        if (extrasLast) {
            extrasLast->nextCup = newCup;
        }

        extrasLast = newCup;
    }

    lastCup->insertAfter(extrasRoot);
    crabGame(rootCup, iterations, MAX_VALUE);

    Cup *cup1 = initialCupMap[1];

    return (long long) cup1->nextCup->value * cup1->nextCup->nextCup->value;
}

int main() {
    for (int i: {1, 5, 10, 20, 30, 50, 75, 100}) {
        auto start = std::chrono::system_clock::now();

        long long result = star2("853192647", i * 1000 * 1000);

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> duration = end - start;

        std::cout << "Finished " << i << "M in " << duration.count() << "s [result=" << result << "]" << std::endl;
    }

    return 0;
}
