import re
from typing import List, Union, Tuple, Dict


def load_data(fn: str) -> Tuple[Dict[int, Union[str, List[List[int]]]], List[str]]:
    rules_map = {}
    messages = []

    with open(fn, 'r') as fh:
        phase = 0
        for line in fh.readlines():
            line = line.strip()

            if not line:
                phase = 1

            if phase == 0:
                key, segments = line.split(': ')
                parsed_rule = []

                if segments[0] == '"' and segments[-1] == '"':
                    parsed_rule = segments[1]
                else:
                    for segment in segments.split(' | '):
                        parsed_rule.append(list(int(s) for s in segment.split(' ')))

                rules_map[int(key)] = parsed_rule
                continue

            if phase == 1:
                messages.append(line)

    return rules_map, messages


def star1(rules: Dict[int, Union[str, List[List[int]]]], messages: List[str]) -> int:
    """
    >>> star1({0: [[4, 1, 5]], 1: [[2, 3], [3, 2]], 2: [[4, 4], [5, 5]], 3: [[4, 5], [5, 4]], 4: 'a', 5: 'b'},
    ...       ['ababbb', 'bababa', 'abbbab', 'aaabbb', 'aaaabbb'])
    2
    """
    rex = re.compile('^%s$' % rule_as_regexp(rules, 0))

    return sum(1 for message in messages if rex.match(message))


def rule_as_regexp(rules: Dict[int, Union[str, List[List[int]]]], index: int) -> str:
    rule = rules[index]

    if isinstance(rule, str):
        return rule

    segments_regex = []
    for segment in rule:
        segment_regex = ''
        for sub_rule_id in segment:
            segment_regex += rule_as_regexp(rules, sub_rule_id)

        segments_regex.append(segment_regex)

    if len(segments_regex) == 1:
        return segments_regex[0]

    return '(%s)' % ('|'.join(segments_regex))


def star2(rules: Dict[int, Union[str, List[List[int]]]], messages: List[str]) -> int:
    """
    >>> star2({42: [[9, 14], [10, 1]], 9: [[14, 27], [1, 26]], 10: [[23, 14], [28, 1]], 1: 'a', 11: [[42, 31]], 5: [[1, 14], [15, 1]], 19: [[14, 1], [14, 14]],
    ...   12: [[24, 14], [19, 1]], 16: [[15, 1], [14, 14]], 31: [[14, 17], [1, 13]], 6: [[14, 14], [1, 14]], 2: [[1, 24], [14, 4]], 0: [[8, 11]],
    ...   13: [[14, 3], [1, 12]], 15: [[1], [14]], 17: [[14, 2], [1, 7]], 23: [[25, 1], [22, 14]], 28: [[16, 1]], 4: [[1, 1]], 20: [[14, 14], [1, 15]],
    ...   3: [[5, 14], [16, 1]], 27: [[1, 6], [14, 18]], 14: 'b', 21: [[14, 1], [1, 14]], 25: [[1, 1], [1, 14]], 22: [[14, 14]], 8: [[42]],
    ...   26: [[14, 22], [1, 20]], 18: [[15, 15]], 7: [[14, 5], [1, 21]], 24: [[14, 1]]},
    ...  ['abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa', 'bbabbbbaabaabba', 'babbbbaabbbbbabbbbbbaabaaabaaa', 'aaabbbbbbaaaabaababaabababbabaaabbababababaaa',
    ...   'bbbbbbbaaaabbbbaaabbabaaa', 'bbbababbbbaaaaaaaabbababaaababaabab', 'ababaaaaaabaaab', 'ababaaaaabbbaba', 'baabbaaaabbaaaababbaababb',
    ...   'abbbbabbbbaaaababbbbbbaaaababb', 'aaaaabbaabaaaaababaa', 'aaaabbaaaabbaaa', 'aaaabbaabbaaaaaaabbbabbbaaabbaabaaa', 'babaaabbbaaabaababbaabababaaab',
    ...   'aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba'])
    12
    """
    rules[8] = [[42], [42, 8]]
    rules[11] = [[42, 31], [42, 11, 31]]

    return 12


if __name__ == "__main__":
    my_rules, my_messages = load_data('data.txt')
    print('Day 19 Star 1: %d' % star1(my_rules, my_messages))
    # print('Day 19 Star 2: %d' % star2(my_rules, my_messages))
