import re
from dataclasses import dataclass
from typing import Optional

from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day


@dataclass
class Part:
    x: int
    m: int
    a: int
    s: int


@dataclass
class MultiPartAt:
    location: str
    x: tuple[int, int]
    m: tuple[int, int]
    a: tuple[int, int]
    s: tuple[int, int]

    def volume(self) -> int:
        return (self.x[1] - self.x[0] + 1) * (self.m[1] - self.m[0] + 1) * (self.a[1] - self.a[0] + 1) * (self.s[1] - self.s[0] + 1)


@dataclass
class Rule:
    part: str
    gt: bool
    value: int
    destination: str

    def match(self, part: Part) -> bool:
        value = part.__getattribute__(self.part)

        if self.gt and value > self.value:
            return True

        if not self.gt and value < self.value:
            return True

        return False

    def multi_match(self, part: MultiPartAt) -> tuple[Optional[MultiPartAt], Optional[MultiPartAt]]:
        """
        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (1, 4000), (1, 4000)))
        (MultiPartAt(location='R', x=(1, 4000), m=(1, 4000), a=(1717, 4000), s=(1, 4000)), MultiPartAt(location='pv', x=(1, 4000), m=(1, 4000), a=(1, 1716), s=(1, 4000)))

        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (2000, 4000), (1, 4000)))
        (MultiPartAt(location='R', x=(1, 4000), m=(1, 4000), a=(2000, 4000), s=(1, 4000)), None)

        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (1, 1000), (1, 4000)))
        (None, MultiPartAt(location='pv', x=(1, 4000), m=(1, 4000), a=(1, 1000), s=(1, 4000)))

        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (1, 1716), (1, 4000)))
        (None, MultiPartAt(location='pv', x=(1, 4000), m=(1, 4000), a=(1, 1716), s=(1, 4000)))

        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (1, 1717), (1, 4000)))
        (MultiPartAt(location='R', x=(1, 4000), m=(1, 4000), a=(1717, 1717), s=(1, 4000)), MultiPartAt(location='pv', x=(1, 4000), m=(1, 4000), a=(1, 1716), s=(1, 4000)))

        >>> Rule('a', True, 1716, 'R').multi_match(MultiPartAt('pv', (1, 4000), (1, 4000), (1716, 2000), (1, 4000)))
        (MultiPartAt(location='R', x=(1, 4000), m=(1, 4000), a=(1717, 2000), s=(1, 4000)), MultiPartAt(location='pv', x=(1, 4000), m=(1, 4000), a=(1716, 1716), s=(1, 4000)))
        """  # noqa: E501
        xmas = {
            'x': part.x,
            'm': part.m,
            'a': part.a,
            's': part.s
        }

        selected = xmas[self.part]

        if self.gt:
            match_range = max(self.value + 1, selected[0]), selected[1]
            remainder_range = selected[0], min(selected[1], self.value)
        else:
            match_range = selected[0], min(selected[1], self.value - 1)
            remainder_range = max(self.value, selected[0]), selected[1]

        match_part = None
        remainder_part = None

        if match_range[0] <= match_range[1]:
            xmas[self.part] = match_range
            match_part = MultiPartAt(self.destination, **xmas)

        if remainder_range[0] <= remainder_range[1]:
            xmas[self.part] = remainder_range
            remainder_part = MultiPartAt(part.location, **xmas)

        return match_part, remainder_part


@dataclass
class Workflow:
    rules: list[Rule]
    last: str

    def send(self, part: Part) -> str:
        for rule in self.rules:
            if rule.match(part):
                return rule.destination

        return self.last

    def send_multipart(self, part: MultiPartAt) -> list[MultiPartAt]:
        """
        >>> Workflow(rules=[Rule(part='a', gt=False, value=2006, destination='qkq'), Rule(part='m', gt=True, value=2090, destination='A')], last='rfg').send_multipart(MultiPartAt('px', (1, 4000), (1, 4000), (1, 4000), (1, 4000)))
        [MultiPartAt(location='qkq', x=(1, 4000), m=(1, 4000), a=(1, 2005), s=(1, 4000)), MultiPartAt(location='A', x=(1, 4000), m=(2091, 4000), a=(2006, 4000), s=(1, 4000)), MultiPartAt(location='rfg', x=(1, 4000), m=(1, 2090), a=(2006, 4000), s=(1, 4000))]
        """  # noqa: E501
        result = []

        active = part
        for rule in self.rules:
            active_volume = active.volume()
            matching, active = rule.multi_match(active)  # type: ignore

            assert active_volume == sum(e.volume() for e in [matching, active] if e)

            if matching:
                result.append(matching)

            if not active:
                break

        if active:
            result.append(MultiPartAt(self.last, active.x, active.m, active.a, active.s))

        assert part.volume() == sum(p.volume() for p in result)

        return result


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "px{a<2006:qkq,m>2090:A,rfg}",
    ...     "pv{a>1716:R,A}",
    ...     "lnx{m>1548:A,A}",
    ...     "rfg{s<537:gd,x>2440:R,A}",
    ...     "qs{s>3448:A,lnx}",
    ...     "qkq{x<1416:A,crn}",
    ...     "crn{x>2662:A,R}",
    ...     "in{s<1351:px,qqz}",
    ...     "qqz{s>2770:qs,m<1801:hdj,R}",
    ...     "gd{a>3333:R,R}",
    ...     "hdj{m>838:A,pv}",
    ...     "",
    ...     "{x=787,m=2655,a=1222,s=2876}",
    ...     "{x=1679,m=44,a=2067,s=496}",
    ...     "{x=2036,m=264,a=79,s=2244}",
    ...     "{x=2461,m=1339,a=466,s=291}",
    ...     "{x=2127,m=1623,a=2188,s=1013}",
    ... ])
    19114
    """

    workflows, parts = _parse(data)
    result = 0

    for part in parts:
        location = 'in'
        while location not in ('A', 'R'):
            location = workflows[location].send(part)

        if location == 'A':
            result += part.x + part.m + part.a + part.s

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "px{a<2006:qkq,m>2090:A,rfg}",
    ...     "pv{a>1716:R,A}",
    ...     "lnx{m>1548:A,A}",
    ...     "rfg{s<537:gd,x>2440:R,A}",
    ...     "qs{s>3448:A,lnx}",
    ...     "qkq{x<1416:A,crn}",
    ...     "crn{x>2662:A,R}",
    ...     "in{s<1351:px,qqz}",
    ...     "qqz{s>2770:qs,m<1801:hdj,R}",
    ...     "gd{a>3333:R,R}",
    ...     "hdj{m>838:A,pv}",
    ...     "",
    ...     "{x=787,m=2655,a=1222,s=2876}",
    ...     "{x=1679,m=44,a=2067,s=496}",
    ...     "{x=2036,m=264,a=79,s=2244}",
    ...     "{x=2461,m=1339,a=466,s=291}",
    ...     "{x=2127,m=1623,a=2188,s=1013}",
    ... ])
    167409079868000
    """

    workflows, _ = _parse(data)
    active = [MultiPartAt('in', (1, 4000), (1, 4000), (1, 4000), (1, 4000))]
    accepted = []
    rejected = []

    while active:
        part = active.pop()

        parts = workflows[part.location].send_multipart(part)

        for p in parts:
            match p.location:
                case 'A':
                    accepted.append(p)
                case 'R':
                    rejected.append(p)
                case _:
                    active.append(p)

    assert sum(e.volume() for e in accepted) + sum(e.volume() for e in rejected) == MultiPartAt('px', (1, 4000), (1, 4000), (1, 4000), (1, 4000)).volume()

    return sum(a.volume() for a in accepted)


def _parse(data: list[str]) -> tuple[dict[str, Workflow], list[Part]]:
    ws, ps = l_split(data, '')

    workflows = {}

    for row in ws:
        m = re.match(r'(\w+)\{(.*)}', row)
        assert m

        rules = []
        last = None
        for r in m.group(2).split(','):
            for part in r.split(','):
                m2 = re.match(r'(\w+)([<>])(\d+):(\w+)', part)

                if m2 is None:
                    last = part
                    break

                else:
                    rules.append(Rule(m2.group(1), m2.group(2) == '>', int(m2.group(3)), m2.group(4)))

        assert last is not None
        workflows[m.group(1)] = Workflow(rules, last)

    parts = []

    for row in ps:
        m = re.match(r'\{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}', row)
        assert m
        parts.append(Part(
            int(m.group(1)),
            int(m.group(2)),
            int(m.group(3)),
            int(m.group(4)),
        ))

    return workflows, parts


if __name__ == "__main__":
    run_day(19,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),  # 26601120000000 too low
            )
