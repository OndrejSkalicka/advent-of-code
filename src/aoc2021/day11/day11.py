import os
from collections import deque
from typing import List, Tuple


def load_data(fn: str = 'data.txt') -> List[List[int]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            result.append(list(int(c) for c in line))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[List[int]]) -> int:
    """
    >>> star1(test_data())
    1656
    """
    data = data[:]
    len_x = len(data)
    len_y = len(data[0])
    total = 0

    for _ in range(100):
        flashed = set()
        buffer = deque()
        for x in range(len_x):
            for y in range(len_y):
                data[x][y] += 1

                if data[x][y] > 9:
                    flashed.add((x, y))
                    buffer.append((x, y))

        while buffer:
            x, y = buffer.pop()

            for nx, ny in adj_coord(x, y, len_x, len_y):
                data[nx][ny] += 1
                if data[nx][ny] > 9 and (nx, ny) not in flashed:
                    flashed.add((nx, ny))
                    buffer.append((nx, ny))

        for x, y in flashed:
            data[x][y] = 0

        total += len(flashed)

    return total


def star2(data: List[List[int]]) -> int:
    """
    >>> star2(test_data())
    195
    """
    data = data[:]
    len_x = len(data)
    len_y = len(data[0])
    day = 0

    while True:
        day += 1
        flashed = set()
        buffer = deque()
        for x in range(len_x):
            for y in range(len_y):
                data[x][y] += 1

                if data[x][y] > 9:
                    flashed.add((x, y))
                    buffer.append((x, y))

        while buffer:
            x, y = buffer.pop()

            for nx, ny in adj_coord(x, y, len_x, len_y):
                data[nx][ny] += 1
                if data[nx][ny] > 9 and (nx, ny) not in flashed:
                    flashed.add((nx, ny))
                    buffer.append((nx, ny))

        for x, y in flashed:
            data[x][y] = 0

        if len(flashed) == len_x * len_y:
            return day


def adj_coord(x: int, y: int, len_x: int, len_y: int) -> List[Tuple[int, int]]:
    result = []
    for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0), (1, 1), (-1, 1), (1, -1), (-1, -1)):
        nx = x + dx
        ny = y + dy

        if 0 <= nx < len_x and 0 <= ny < len_y:
            result.append((nx, ny))

    return result


if __name__ == "__main__":
    print('Day 11 Star 1: %d' % star1(load_data()))
    print('Day 11 Star 2: %d' % star2(load_data()))
