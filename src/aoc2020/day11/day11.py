from typing import List


def load_data(fn: str) -> List[str]:
    with open(fn, 'r') as fh:
        return list(line.strip() for line in fh.readlines())


def star1(data: List[str]) -> int:
    """
    >>> star1(['L.LL.LL.LL','LLLLLLL.LL','L.L.L..L..','LLLL.LL.LL','L.LL.LL.LL','L.LLLLL.LL','..L.L.....','LLLLLLLLLL','L.LLLLLL.L','L.LLLLL.LL'])
    37
    """

    while True:
        updated = change_seats(data)
        # no change, we got a winner!
        if updated == data:
            return sum(sum(1 for seat in row if seat == '#') for row in data)

        data = updated


def change_seats(data: List[str]) -> List[str]:
    new_seating = []

    for y, row in enumerate(data):
        new_row = ''
        for x, cell in enumerate(row):

            occupied = adjacent_occupied(x, y, data)

            if cell == 'L' and occupied == 0:
                new_cell = '#'
            elif cell == '#' and occupied >= 4:
                new_cell = 'L'
            else:
                new_cell = cell

            new_row += new_cell

        new_seating.append(new_row)

    return new_seating


def adjacent_occupied(x: int, y: int, data) -> int:
    seats = 0
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            if dx == 0 and dy == 0:
                continue

            if 0 <= y + dy < len(data):
                row = data[y + dy]

                if 0 <= x + dx < len(row):
                    cell = row[x + dx]

                    if cell == '#':
                        seats += 1

    return seats


def star2(data: List[str]) -> int:
    """
    >>> star2(['L.LL.LL.LL','LLLLLLL.LL','L.L.L..L..','LLLL.LL.LL','L.LL.LL.LL','L.LLLLL.LL','..L.L.....','LLLLLLLLLL','L.LLLLLL.L','L.LLLLL.LL'])
    26
    """

    while True:
        updated = change_seats_s2(data)
        # no change, we got a winner!
        if updated == data:
            return sum(sum(1 for seat in row if seat == '#') for row in data)

        data = updated


def change_seats_s2(data: List[str]) -> List[str]:
    new_seating = []

    for y, row in enumerate(data):
        new_row = ''
        for x, cell in enumerate(row):

            occupied = adjacent_occupied_s2(x, y, data)

            if cell == 'L' and occupied == 0:
                new_cell = '#'
            elif cell == '#' and occupied >= 5:
                new_cell = 'L'
            else:
                new_cell = cell

            new_row += new_cell

        new_seating.append(new_row)

    return new_seating


def adjacent_occupied_s2(x: int, y: int, data) -> int:
    seats = 0
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            if dx == 0 and dy == 0:
                continue

            # cursor x/y
            cx = x
            cy = y
            while True:
                cx += dx
                cy += dy

                if cy < 0 or cy >= len(data):
                    break
                row = data[cy]

                if cx < 0 or cx >= len(row):
                    break

                cell = row[cx]
                if cell == '#':
                    seats += 1
                    break

                if cell == 'L':
                    break

                # pass for floor
                pass

    return seats


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 11 Star 1: %d' % star1(my_data))
    print('Day 11 Star 2: %d' % star2(my_data))
