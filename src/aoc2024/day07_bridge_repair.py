import itertools

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["190: 10 19",
    ...        "3267: 81 40 27",
    ...        "83: 17 5",
    ...        "156: 15 6",
    ...        "7290: 6 8 6 15",
    ...        "161011: 16 10 13",
    ...        "192: 17 8 14",
    ...        "21037: 9 7 18 13",
    ...        "292: 11 6 16 20",])
    3749
    """

    result = 0
    for row in data:
        target = int(row.split(':')[0])
        parts = [int(p) for p in row.split(': ')[1].split(' ')]

        for ops in itertools.product(['+', '*'], repeat=len(parts) - 1):
            eq = parts[0]
            for i, op in enumerate(ops):
                if op == '+':
                    eq += parts[i + 1]
                else:
                    eq *= parts[i + 1]

            if eq == target:
                result += target
                break

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["190: 10 19",
    ...        "3267: 81 40 27",
    ...        "83: 17 5",
    ...        "156: 15 6",
    ...        "7290: 6 8 6 15",
    ...        "161011: 16 10 13",
    ...        "192: 17 8 14",
    ...        "21037: 9 7 18 13",
    ...        "292: 11 6 16 20",])
    11387
    """

    result = 0
    for row in data:
        target = int(row.split(':')[0])
        parts = [int(p) for p in row.split(': ')[1].split(' ')]

        for ops in itertools.product(['+', '*', '||'], repeat=len(parts) - 1):
            eq = parts[0]
            for i, op in enumerate(ops):
                num = parts[i + 1]
                if op == '+':
                    eq += num
                elif op == '*':
                    eq *= num
                else:
                    eq = int(str(eq) + str(num))

            if eq == target:
                result += target
                break

    return result


if __name__ == "__main__":
    run_day(7,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
