import json
from functools import cmp_to_key
from typing import Any

from util.data import txt_fn, load_strings_newline, l_split
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "[1,1,3,1,1]",
    ...        "[1,1,5,1,1]",
    ...        "",
    ...        "[[1],[2,3,4]]",
    ...        "[[1],4]",
    ...        "",
    ...        "[9]",
    ...        "[[8,7,6]]",
    ...        "",
    ...        "[[4,4],4,4]",
    ...        "[[4,4],4,4,4]",
    ...        "",
    ...        "[7,7,7,7]",
    ...        "[7,7,7]",
    ...        "",
    ...        "[]",
    ...        "[3]",
    ...        "",
    ...        "[[[]]]",
    ...        "[[]]",
    ...        "",
    ...        "[1,[2,[3,[4,[5,6,7]]]],8,9]",
    ...        "[1,[2,[3,[4,[5,6,0]]]],8,9]",
    ...        ])
    13
    """

    pairs = []

    for rows in l_split(data):
        pairs.append(
            (
                json.loads(rows[0]),
                json.loads(rows[1]),
            )
        )

    result = 0
    for i, pair in enumerate(pairs):
        if _compare(*pair) > 0:
            result += i + 1

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "[1,1,3,1,1]",
    ...        "[1,1,5,1,1]",
    ...        "",
    ...        "[[1],[2,3,4]]",
    ...        "[[1],4]",
    ...        "",
    ...        "[9]",
    ...        "[[8,7,6]]",
    ...        "",
    ...        "[[4,4],4,4]",
    ...        "[[4,4],4,4,4]",
    ...        "",
    ...        "[7,7,7,7]",
    ...        "[7,7,7]",
    ...        "",
    ...        "[]",
    ...        "[3]",
    ...        "",
    ...        "[[[]]]",
    ...        "[[]]",
    ...        "",
    ...        "[1,[2,[3,[4,[5,6,7]]]],8,9]",
    ...        "[1,[2,[3,[4,[5,6,0]]]],8,9]",
    ...        ])
    140
    """

    packets = []
    for row in data:
        if row == '':
            continue
        packets.append(json.loads(row))

    packets.append([[2]])
    packets.append([[6]])

    packets.sort(key=cmp_to_key(_compare), reverse=True)

    return (packets.index([[2]]) + 1) * (packets.index([[6]]) + 1)


def _compare(left: Any, right: Any) -> int:
    if isinstance(left, int) and isinstance(right, int):
        if left == right:
            return 0

        return 1 if left < right else -1

    if isinstance(left, list) and isinstance(right, list):

        for i in range(min(len(left), len(right))):
            cmp = _compare(left[i], right[i])
            if cmp == 0:
                continue

            return cmp

        if len(left) < len(right):
            return 1

        if len(left) > len(right):
            return -1

        return 0

    if isinstance(left, int):
        return _compare([left], right)

    return _compare(left, [right])


if __name__ == "__main__":
    run_day(13,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
