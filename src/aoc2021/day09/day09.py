import os
from typing import List, Tuple


def load_data(fn: str = 'data.txt') -> List[List[int]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            result.append(list(int(c) for c in line))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[List[int]]) -> int:
    """
    >>> star1(test_data())
    15
    """

    len_x = len(data)
    len_y = len(data[0])

    result = 0

    for x in range(len_x):
        for y in range(len_y):
            val = data[x][y]
            low_point = True

            for nx, ny in adj_coord(x, y, len_x, len_y):
                if data[nx][ny] <= val:
                    low_point = False
                    break

            if low_point:
                result += val + 1

    return result


def star2(data: List[List[int]]) -> int:
    """
    >>> star2(test_data())
    1134
    """

    len_x = len(data)
    len_y = len(data[0])

    low_points = []

    for x in range(len_x):
        for y in range(len_y):
            val = data[x][y]
            low_point = True

            for nx, ny in adj_coord(x, y, len_x, len_y):
                if data[nx][ny] <= val:
                    low_point = False
                    break

            if low_point:
                low_points.append((x, y))

    sizes = []
    for low_point in low_points:
        buffer = [low_point]
        processed = set()
        processed.add(low_point)

        while buffer:
            x, y = buffer.pop()

            for nx, ny in adj_coord(x, y, len_x, len_y):

                if (nx, ny) in processed:
                    continue

                if data[nx][ny] == 9:
                    continue

                buffer.append((nx, ny))
                processed.add((nx, ny))

        sizes.append(len(processed))

    result = 1
    for x in sorted(sizes, reverse=True)[:3]:
        result *= x
    return result


def adj_coord(x: int, y: int, len_x: int, len_y: int) -> List[Tuple[int, int]]:
    result = []
    for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0)):
        nx = x + dx
        ny = y + dy

        if 0 <= nx < len_x and 0 <= ny < len_y:
            result.append((nx, ny))

    return result


if __name__ == "__main__":
    print('Day 9 Star 1: %d' % star1(load_data()))
    print('Day 9 Star 2: %d' % star2(load_data()))
