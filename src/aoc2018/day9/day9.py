def star_1(players: int, last_marble: int) -> int:
    """
    >>> star_1(9, 25)
    32

    >>> star_1(10, 1618)
    8317

    >>> star_1(13, 7999)
    146373

    >>> star_1(17, 1104)
    2764

    >>> star_1(21, 6111)
    54718

    >>> star_1(30, 5807)
    37305
    """

    elves = {i: 0 for i in range(players)}
    active = DoubleLinkedNode(0)

    for i in range(last_marble):
        value = i + 1
        if value % 23 == 0:
            left6 = active.left.left.left.left.left.left
            left7 = left6.left
            left7.remove()
            active = left6
            elves[i % players] += left7.value + value

        else:
            node = DoubleLinkedNode(value)
            active.right.add_right(node)
            active = node

    return max(elves.values())


class DoubleLinkedNode:
    def __init__(self, value) -> None:
        self.value = value
        self.left = self
        self.right = self

    def add_right(self, node: 'DoubleLinkedNode') -> None:
        right = self.right
        self.right = node
        right.left = node
        node.left = self
        node.right = right

    def remove(self) -> None:
        left = self.left
        right = self.right
        left.right = right
        right.left = left
        self.left = self
        self.right = right


if __name__ == "__main__":
    print('Day 9 Star 1: %d' % star_1(465, 71940))
    print('Day 9 Star 2: %d' % star_1(465, 71940 * 100))
