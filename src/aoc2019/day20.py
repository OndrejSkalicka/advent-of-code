import logging
from dataclasses import dataclass
from functools import total_ordering
from typing import Optional, Generator

import numpy as np

from util.data import to_2d_np_char, load_as_str
from util.dijkstra import dijkstra
from util.dtype import np_array_2d_str
from util.logger_config import init_logger
from util.perf import run_day
from util.record import Xy

logger = logging.getLogger(__name__)

FLOOR = '.'
DIRECTIONS: list[Xy] = [
    Xy(0, -1),
    Xy(1, 0),
    Xy(0, 1),
    Xy(-1, 0),
]
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def star1(input_str: str) -> int:
    """
    >>> star1("         A           \\n"
    ...       "         A           \\n"
    ...       "  #######.#########  \\n"
    ...       "  #######.........#  \\n"
    ...       "  #######.#######.#  \\n"
    ...       "  #######.#######.#  \\n"
    ...       "  #######.#######.#  \\n"
    ...       "  #####  B    ###.#  \\n"
    ...       "BC...##  C    ###.#  \\n"
    ...       "  ##.##       ###.#  \\n"
    ...       "  ##...DE  F  ###.#  \\n"
    ...       "  #####    G  ###.#  \\n"
    ...       "  #########.#####.#  \\n"
    ...       "DE..#######...###.#  \\n"
    ...       "  #.#########.###.#  \\n"
    ...       "FG..#########.....#  \\n"
    ...       "  ###########.#####  \\n"
    ...       "             Z       \\n"
    ...       "             Z       ")
    23

    >>> star1("                   A               \\n"
    ...       "                   A               \\n"
    ...       "  #################.#############  \\n"
    ...       "  #.#...#...................#.#.#  \\n"
    ...       "  #.#.#.###.###.###.#########.#.#  \\n"
    ...       "  #.#.#.......#...#.....#.#.#...#  \\n"
    ...       "  #.#########.###.#####.#.#.###.#  \\n"
    ...       "  #.............#.#.....#.......#  \\n"
    ...       "  ###.###########.###.#####.#.#.#  \\n"
    ...       "  #.....#        A   C    #.#.#.#  \\n"
    ...       "  #######        S   P    #####.#  \\n"
    ...       "  #.#...#                 #......VT\\n"
    ...       "  #.#.#.#                 #.#####  \\n"
    ...       "  #...#.#               YN....#.#  \\n"
    ...       "  #.###.#                 #####.#  \\n"
    ...       "DI....#.#                 #.....#  \\n"
    ...       "  #####.#                 #.###.#  \\n"
    ...       "ZZ......#               QG....#..AS\\n"
    ...       "  ###.###                 #######  \\n"
    ...       "JO..#.#.#                 #.....#  \\n"
    ...       "  #.#.#.#                 ###.#.#  \\n"
    ...       "  #...#..DI             BU....#..LF\\n"
    ...       "  #####.#                 #.#####  \\n"
    ...       "YN......#               VT..#....QG\\n"
    ...       "  #.###.#                 #.###.#  \\n"
    ...       "  #.#...#                 #.....#  \\n"
    ...       "  ###.###    J L     J    #.#.###  \\n"
    ...       "  #.....#    O F     P    #.#...#  \\n"
    ...       "  #.###.#####.#.#####.#####.###.#  \\n"
    ...       "  #...#.#.#...#.....#.....#.#...#  \\n"
    ...       "  #.#####.###.###.#.#.#########.#  \\n"
    ...       "  #...#.#.....#...#.#.#.#.....#.#  \\n"
    ...       "  #.###.#####.###.###.#.#.#######  \\n"
    ...       "  #.#.........#...#.............#  \\n"
    ...       "  #########.###.###.#############  \\n"
    ...       "           B   J   C               \\n"
    ...       "           U   P   P               ")
    58
    """
    map: np_array_2d_str = to_2d_np_char(input_str, rpad=' ')

    portals_by_xy: dict[Xy, str] = {}
    portals_by_name: dict[str, Xy] = {}
    portals: dict[Xy, Xy] = {}
    start_pos: Optional[Xy] = None
    finish_pos: Optional[Xy] = None

    x: int
    y: int
    # noinspection PyTypeChecker
    for y, x in np.ndindex(map.shape):
        if map[y, x] == FLOOR:
            portal_xy = Xy(x, y)
            for dxy in DIRECTIONS:
                if map[(portal_xy + dxy).np_yx()] in LETTERS:
                    portal_name = map[(portal_xy + dxy).np_yx()] + map[(portal_xy + 2 * dxy).np_yx()]
                    if dxy in (Xy(0, -1), Xy(-1, 0)):
                        portal_name = portal_name[::-1]

                    portals_by_xy[portal_xy] = portal_name
                    if portal_name == 'AA':
                        start_pos = portal_xy
                    elif portal_name == 'ZZ':
                        finish_pos = portal_xy
                    elif portal_name in portals_by_name:
                        target_xy = portals_by_name[portal_name]
                        del portals_by_name[portal_name]

                        portals[target_xy] = portal_xy
                        portals[portal_xy] = target_xy
                    else:
                        portals_by_name[portal_name] = portal_xy

    logger.debug(f"Portals: {portals}")

    assert start_pos is not None
    assert finish_pos is not None
    assert len(portals_by_name) == 0

    distances, _ = dijkstra(start_pos, lambda node: _map_adj_full(node, map, portals))

    return distances[finish_pos]


@total_ordering
@dataclass(frozen=True)
class XyLevel:
    xy: Xy
    level: int

    def __lt__(self, other: 'XyLevel') -> bool:
        return (self.xy, self.level) < (other.xy, other.level)


def star2(input_str: str) -> int:
    """
    >>> star2("             Z L X W       C                 \\n"
    ...       "             Z P Q B       K                 \\n"
    ...       "  ###########.#.#.#.#######.###############  \\n"
    ...       "  #...#.......#.#.......#.#.......#.#.#...#  \\n"
    ...       "  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  \\n"
    ...       "  #.#...#.#.#...#.#.#...#...#...#.#.......#  \\n"
    ...       "  #.###.#######.###.###.#.###.###.#.#######  \\n"
    ...       "  #...#.......#.#...#...#.............#...#  \\n"
    ...       "  #.#########.#######.#.#######.#######.###  \\n"
    ...       "  #...#.#    F       R I       Z    #.#.#.#  \\n"
    ...       "  #.###.#    D       E C       H    #.#.#.#  \\n"
    ...       "  #.#...#                           #...#.#  \\n"
    ...       "  #.###.#                           #.###.#  \\n"
    ...       "  #.#....OA                       WB..#.#..ZH\\n"
    ...       "  #.###.#                           #.#.#.#  \\n"
    ...       "CJ......#                           #.....#  \\n"
    ...       "  #######                           #######  \\n"
    ...       "  #.#....CK                         #......IC\\n"
    ...       "  #.###.#                           #.###.#  \\n"
    ...       "  #.....#                           #...#.#  \\n"
    ...       "  ###.###                           #.#.#.#  \\n"
    ...       "XF....#.#                         RF..#.#.#  \\n"
    ...       "  #####.#                           #######  \\n"
    ...       "  #......CJ                       NM..#...#  \\n"
    ...       "  ###.#.#                           #.###.#  \\n"
    ...       "RE....#.#                           #......RF\\n"
    ...       "  ###.###        X   X       L      #.#.#.#  \\n"
    ...       "  #.....#        F   Q       P      #.#.#.#  \\n"
    ...       "  ###.###########.###.#######.#########.###  \\n"
    ...       "  #.....#...#.....#.......#...#.....#.#...#  \\n"
    ...       "  #####.#.###.#######.#######.###.###.#.#.#  \\n"
    ...       "  #.......#.......#.#.#.#.#...#...#...#.#.#  \\n"
    ...       "  #####.###.#####.#.#.#.#.###.###.#.###.###  \\n"
    ...       "  #.......#.....#.#...#...............#...#  \\n"
    ...       "  #############.#.#.###.###################  \\n"
    ...       "               A O F   N                     \\n"
    ...       "               A A D   M                     ")
    396
    """
    map: np_array_2d_str = to_2d_np_char(input_str, rpad=' ')

    portals_by_xy: dict[Xy, str] = {}
    portals_by_name: dict[str, Xy] = {}
    portals: dict[Xy, Xy] = {}
    start_pos: Optional[Xy] = None
    finish_pos: Optional[Xy] = None

    x: int
    y: int
    # noinspection PyTypeChecker
    for y, x in np.ndindex(map.shape):
        if map[y, x] == FLOOR:
            portal_xy = Xy(x, y)
            for dxy in DIRECTIONS:
                if map[(portal_xy + dxy).np_yx()] in LETTERS:
                    portal_name = map[(portal_xy + dxy).np_yx()] + map[(portal_xy + 2 * dxy).np_yx()]
                    if dxy in (Xy(0, -1), Xy(-1, 0)):
                        portal_name = portal_name[::-1]

                    portals_by_xy[portal_xy] = portal_name
                    if portal_name == 'AA':
                        start_pos = portal_xy
                    elif portal_name == 'ZZ':
                        finish_pos = portal_xy
                    elif portal_name in portals_by_name:
                        target_xy = portals_by_name[portal_name]
                        del portals_by_name[portal_name]

                        portals[target_xy] = portal_xy
                        portals[portal_xy] = target_xy
                    else:
                        portals_by_name[portal_name] = portal_xy

    logger.debug(f"Portals: {portals}")

    assert start_pos is not None
    assert finish_pos is not None
    assert len(portals_by_name) == 0

    destination = XyLevel(finish_pos, 0)
    distances, _ = dijkstra(XyLevel(start_pos, 0), lambda node: _map_adj_full_3d(node, map, portals), destination=lambda xy: xy == destination)

    return distances[XyLevel(finish_pos, 0)]


def _map_adj_full(node: Xy, map: np_array_2d_str, portals: dict[Xy, Xy]) -> Generator[tuple[Xy, int], None, None]:
    for dxy in DIRECTIONS:
        adj_xy = node + dxy
        if map[adj_xy.np_yx()] == FLOOR:
            yield adj_xy, 1

    if node in portals:
        yield portals[node], 1


def _map_adj_full_3d(node: XyLevel, map: np_array_2d_str, portals: dict[Xy, Xy]) -> Generator[tuple[XyLevel, int], None, None]:
    for dxy in DIRECTIONS:
        adj_xy = node.xy + dxy
        if map[adj_xy.np_yx()] == FLOOR:
            yield XyLevel(adj_xy, node.level), 1

    if node.xy in portals:
        x_edge = min(node.xy.x, map.shape[1] - node.xy.x - 1)
        y_edge = min(node.xy.y, map.shape[0] - node.xy.y - 1)
        if x_edge == 2 or y_edge == 2:
            d_level = -1
        else:
            d_level = 1

        if node.level + d_level >= 0:
            yield XyLevel(portals[node.xy], node.level + d_level), 1


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(20,
            lambda: star1(load_as_str('day20.txt')),
            lambda: star2(load_as_str('day20.txt')),
            )
