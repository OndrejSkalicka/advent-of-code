from typing import Sequence, List


def load_data(fn: str) -> List[str]:
    with open(fn, 'r') as fh:
        return list(line.strip() for line in fh.readlines())


def star1(data: Sequence[str]) -> int:
    """
    >>> star1(['FBFBBFFRLR', 'BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL'])
    820
    """
    return max(seat_id(seat) for seat in data)


def star2(data: Sequence[str]) -> int:
    seat_ids = set(seat_id(seat) for seat in data)

    for seat in seat_ids:
        if seat + 1 not in seat_ids and seat + 2 in seat_ids:
            return seat + 1


def seat_id(seat: str) -> int:
    """
    >>> seat_id('FBFBBFFRLR')
    357
    >>> seat_id('BFFFBBFRRR')
    567
    >>> seat_id('FFFBBBFRRR')
    119
    >>> seat_id('BBFFBBFRLL')
    820
    """
    return row(seat) * 8 + column(seat)


def row(seat: str) -> int:
    """
    >>> row('FBFBBFFRLR')
    44
    >>> row('BFFFBBFRRR')
    70
    >>> row('FFFBBBFRRR')
    14
    >>> row('BBFFBBFRLL')
    102
    """

    return int(seat[:-3].replace('F', '0').replace('B', '1'), 2)


def column(seat: str) -> int:
    """
    >>> column('FBFBBFFRLR')
    5
    >>> column('BFFFBBFRRR')
    7
    >>> column('FFFBBBFRRR')
    7
    >>> column('BBFFBBFRLL')
    4
    """

    return int(seat[-3:].replace('L', '0').replace('R', '1'), 2)


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 4 Star 1: %d' % star1(my_data))
    print('Day 4 Star 2: %d' % star2(my_data))
