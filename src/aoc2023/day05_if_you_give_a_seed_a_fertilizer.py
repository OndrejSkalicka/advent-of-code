from dataclasses import dataclass
from typing import Optional

from util.data import l_split, load_strings_newline, txt_fn
from util.perf import run_day


@dataclass(frozen=True)
class SeedRange:
    start: int
    range: int

    def end(self) -> int:
        return self.start + self.range - 1

    @staticmethod
    def from_start_end(start: int, end: int) -> 'SeedRange':
        assert end >= start

        return SeedRange(start, end - start + 1)

    def __repr__(self) -> str:
        return f'<{self.start}, {self.end()})|{self.range}|'


@dataclass(frozen=True)
class MappingRange:
    source_start: int
    destination_start: int
    range: int

    def map(self, item: int) -> Optional[int]:
        """
        >>> MappingRange(50, 98, 2).map(50)
        98

        >>> MappingRange(50, 98, 2).map(51)
        99

        >>> MappingRange(50, 98, 2).map(52)

        """
        if item < self.source_start:
            return None

        if item >= self.source_start + self.range:
            return None

        return item + self._offset()

    def overlap(self, seed_range: SeedRange) -> tuple[Optional[SeedRange], list[SeedRange]]:
        """
        >>> MappingRange(6, 16, 3).overlap(SeedRange(1, 10))
        (<16, 18)|3|, [<1, 5)|5|, <9, 10)|2|])

        >>> MappingRange(6, 16, 3).overlap(SeedRange(1, 5))
        (None, [<1, 5)|5|])

        >>> MappingRange(6, 16, 3).overlap(SeedRange(9, 5))
        (None, [<9, 13)|5|])

        >>> MappingRange(6, 16, 3).overlap(SeedRange(8, 5))
        (<18, 18)|1|, [<9, 12)|4|])

        >>> MappingRange(25, 18, 70).overlap(SeedRange(81, 14))
        (<74, 87)|14|, [])

        """
        cut_offs: list[SeedRange] = []
        mapped = None

        if seed_range.start < self.source_start:
            cut_offs.append(SeedRange(seed_range.start, min(seed_range.range, self.source_start - seed_range.start)))

        if seed_range.end() > self.source_end():
            cut_offs.append(SeedRange.from_start_end(max(seed_range.start, self.source_start + self.range), seed_range.end()))

        if seed_range.start <= self.source_end() and seed_range.end() >= self.source_start:
            map_start = max(self.source_start, seed_range.start)
            map_end = min(self.source_end(), seed_range.end())

            mapped = SeedRange.from_start_end(map_start + self._offset(), map_end + self._offset())

        assert seed_range.range == sum(cu.range for cu in cut_offs) + (mapped.range if mapped is not None else 0)

        return mapped, cut_offs

    def source_end(self) -> int:
        return self.source_start + self.range - 1

    def _offset(self) -> int:
        return self.destination_start - self.source_start


@dataclass(frozen=True)
class Mapping:
    source: str
    destination: str
    ranges: list[MappingRange]


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "seeds: 79 14 55 13",
    ...     "",
    ...     "seed-to-soil map:",
    ...     "50 98 2",
    ...     "52 50 48",
    ...     "",
    ...     "soil-to-fertilizer map:",
    ...     "0 15 37",
    ...     "37 52 2",
    ...     "39 0 15",
    ...     "",
    ...     "fertilizer-to-water map:",
    ...     "49 53 8",
    ...     "0 11 42",
    ...     "42 0 7",
    ...     "57 7 4",
    ...     "",
    ...     "water-to-light map:",
    ...     "88 18 7",
    ...     "18 25 70",
    ...     "",
    ...     "light-to-temperature map:",
    ...     "45 77 23",
    ...     "81 45 19",
    ...     "68 64 13",
    ...     "",
    ...     "temperature-to-humidity map:",
    ...     "0 69 1",
    ...     "1 0 69",
    ...     "",
    ...     "humidity-to-location map:",
    ...     "60 56 37",
    ...     "56 93 4",
    ... ])
    35
    """

    seeds, maps = _parse(data)

    locations = []

    for entity_number in seeds:
        entity_type = 'seed'

        while entity_type != 'location':
            map = maps[entity_type]

            entity_type = map.destination

            for mapping_range in map.ranges:
                new_destination = mapping_range.map(entity_number)
                if new_destination is not None:
                    entity_number = new_destination
                    break

        locations.append(entity_number)

    return min(locations)


def _parse(data: list[str]) -> tuple[list[int], dict[str, Mapping]]:
    maps: dict[str, Mapping] = {}

    seeds = [int(e) for e in data[0].split(' ')[1:]]
    raw_maps = l_split(data[2:], "")

    for raw_map in raw_maps:
        source, destination = raw_map[0][:-5].split('-to-')
        mappings = []
        for mapping in raw_map[1:]:
            destination_start, source_start, map_range = [int(e) for e in mapping.split(' ')]
            mappings.append(MappingRange(source_start, destination_start, map_range))

        maps[source] = Mapping(source, destination, mappings)

    return seeds, maps


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "seeds: 79 14 55 13",
    ...     "",
    ...     "seed-to-soil map:",
    ...     "50 98 2",
    ...     "52 50 48",
    ...     "",
    ...     "soil-to-fertilizer map:",
    ...     "0 15 37",
    ...     "37 52 2",
    ...     "39 0 15",
    ...     "",
    ...     "fertilizer-to-water map:",
    ...     "49 53 8",
    ...     "0 11 42",
    ...     "42 0 7",
    ...     "57 7 4",
    ...     "",
    ...     "water-to-light map:",
    ...     "88 18 7",
    ...     "18 25 70",
    ...     "",
    ...     "light-to-temperature map:",
    ...     "45 77 23",
    ...     "81 45 19",
    ...     "68 64 13",
    ...     "",
    ...     "temperature-to-humidity map:",
    ...     "0 69 1",
    ...     "1 0 69",
    ...     "",
    ...     "humidity-to-location map:",
    ...     "60 56 37",
    ...     "56 93 4",
    ... ])
    46
    """

    seeds, maps = _parse(data)

    active_ranges: list[SeedRange] = []
    for i in range(0, len(seeds), 2):
        active_ranges.append(SeedRange(seeds[i], seeds[i + 1]))

    entity_type = 'seed'

    while entity_type != 'location':
        map = maps[entity_type]
        entity_type = map.destination
        new_ranges: list[SeedRange] = []

        for mapping_range in map.ranges:
            unprocessed_ranges: list[SeedRange] = []
            for active_range in active_ranges:
                mapped, cut_offs = mapping_range.overlap(active_range)
                if mapped is not None:
                    new_ranges.append(mapped)

                for cut_off in cut_offs:
                    unprocessed_ranges.append(cut_off)

            active_ranges = unprocessed_ranges

        for unprocessed_range in active_ranges:
            new_ranges.append(unprocessed_range)

        active_ranges = new_ranges

    return min(ar.start for ar in active_ranges)


if __name__ == "__main__":
    run_day(5,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
