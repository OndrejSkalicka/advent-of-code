import os
from typing import List, Tuple, Dict


def load_data(fn: str = 'data.txt') -> Tuple[List[str], Dict[str, str]]:
    input = []
    rules = dict()

    with open(fn, 'r') as fh:
        phase = 0
        for line in fh.readlines():
            line = line.strip()

            if not line:
                phase = 1
                continue

            if phase == 0:
                input = list(line)

            else:
                a, b = line.split(' -> ')
                rules[a] = b

    return input, rules


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(data: Tuple[List[str], Dict[str, str]], steps: int = 10) -> int:
    """
    >>> star1(test_data())
    1588

    >>> star1(test_data(), steps=40)
    2188189693529
    """

    polymer = data[0]
    rules = data[1]

    pairs = dict()
    for i in range(len(polymer) - 1):
        pairs[polymer[i] + polymer[i + 1]] = pairs.get(polymer[i] + polymer[i + 1], 0) + 1

    for step in range(steps):
        new_pairs = dict()
        for k, v in pairs.items():
            if k not in rules:
                new_pairs[k] = new_pairs.get(k, 0) + v

            else:
                mid = rules[k]
                new_pairs[k[0] + mid] = new_pairs.get(k[0] + mid, 0) + v
                new_pairs[mid + k[1]] = new_pairs.get(mid + k[1], 0) + v

        pairs = new_pairs

    q = {}

    for k, v in pairs.items():
        q[k[0]] = q.get(k[0], 0) + v
        q[k[1]] = q.get(k[1], 0) + v

    q[polymer[0]] += 1
    q[polymer[-1]] += 1

    most = None
    least = None
    for k, v in q.items():
        if most is None or most < v:
            most = v
        if least is None or least > v:
            least = v

    return int((most - least) / 2)


if __name__ == "__main__":
    print('Day 14 Star 1: %d' % star1(load_data()))
    print('Day 14 Star 2: %d' % star1(load_data(), steps=40))
