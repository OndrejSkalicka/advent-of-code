import re
from dataclasses import dataclass

from util.data import txt_fn, load_strings_newline
from util.dijkstra import dijkstra
from util.perf import run_day


@dataclass
class Valve:
    name: str
    rate: int
    paths: list[str]


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
    ...        "Valve BB has flow rate=13; tunnels lead to valves CC, AA",
    ...        "Valve CC has flow rate=2; tunnels lead to valves DD, BB",
    ...        "Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE",
    ...        "Valve EE has flow rate=3; tunnels lead to valves FF, DD",
    ...        "Valve FF has flow rate=0; tunnels lead to valves EE, GG",
    ...        "Valve GG has flow rate=0; tunnels lead to valves FF, HH",
    ...        "Valve HH has flow rate=22; tunnel leads to valve GG",
    ...        "Valve II has flow rate=0; tunnels lead to valves AA, JJ",
    ...        "Valve JJ has flow rate=21; tunnel leads to valve II",
    ...        ])
    1651
    """

    all_valves: dict[str, Valve] = _load_data(data)
    i_valves: dict[str, Valve] = {valve.name: valve for valve in all_valves.values() if valve.rate > 0 or valve.name == 'AA'}
    distance_map = _build_distance_map(i_valves, all_valves)

    return _solve_rec(i_valves, distance_map, set(v.name for v in i_valves.values() if not v.name == 'AA'), 'AA', 30, 0)


def _solve_rec(i_valves: dict[str, Valve],
               distance_map: dict[tuple[str, str], int],
               available: set[str],
               active_name: str,
               time_remaining: int,
               value: int) -> int:
    """
    One step = go to a valve and open it + accumulate future score
    """
    active = i_valves[active_name]
    subs = []
    for target_name in available:
        distance_to_value = distance_map[(active.name, target_name)]

        if time_remaining > distance_to_value + 1:
            subs.append(_solve_rec(i_valves,
                                   distance_map,
                                   available - {target_name},
                                   target_name,
                                   time_remaining - distance_to_value - 1,
                                   value + (time_remaining - distance_to_value - 1) * i_valves[target_name].rate))

    return max(subs) if subs else value


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
    ...        "Valve BB has flow rate=13; tunnels lead to valves CC, AA",
    ...        "Valve CC has flow rate=2; tunnels lead to valves DD, BB",
    ...        "Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE",
    ...        "Valve EE has flow rate=3; tunnels lead to valves FF, DD",
    ...        "Valve FF has flow rate=0; tunnels lead to valves EE, GG",
    ...        "Valve GG has flow rate=0; tunnels lead to valves FF, HH",
    ...        "Valve HH has flow rate=22; tunnel leads to valve GG",
    ...        "Valve II has flow rate=0; tunnels lead to valves AA, JJ",
    ...        "Valve JJ has flow rate=21; tunnel leads to valve II",
    ...        ])
    1707
    """

    all_valves: dict[str, Valve] = _load_data(data)

    i_valves: dict[str, Valve] = {valve.name: valve for valve in all_valves.values() if valve.rate > 0 or valve.name == 'AA'}

    distance_map = _build_distance_map(i_valves, all_valves)

    return _solve_rec_2(i_valves, distance_map, set(v.name for v in i_valves.values() if not v.name == 'AA'), ('AA', 'AA'), (26, 26), 0)


def _load_data(data: list[str]) -> dict[str, Valve]:
    all_valves: dict[str, Valve] = dict()
    for row in data:
        m = re.match(r'^Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? ([\w, ]+)$', row)
        assert m

        valve = Valve(m[1], int(m[2]), [p for p in m[3].split(', ')])

        all_valves[valve.name] = valve
    return all_valves


def _build_distance_map(i_valves: dict[str, Valve], all_valves: dict[str, Valve]) -> dict[tuple[str, str], int]:
    distance_map: dict[tuple[str, str], int] = dict()
    for source in i_valves.values():
        distances_from_valve = dijkstra(source.name, adjacent=lambda node: [(code, 1) for code in all_valves[node].paths])[0]

        for destination in i_valves.values():
            if source == destination:
                continue

            distance_map[(source.name, destination.name)] = distances_from_valve[destination.name]
    return distance_map


def _solve_rec_2(i_valves: dict[str, Valve],
                 distance_map: dict[tuple[str, str], int],
                 available: set[str],
                 active_names: tuple[str, str],
                 time_remaining: tuple[int, int],
                 value: int) -> int:
    """
    One step = go to a valve and open it + accumulate future score
    """
    active_explorer = 0 if time_remaining[0] > time_remaining[1] else 1

    active = i_valves[active_names[active_explorer]]
    subs = []
    for target_name in available:
        distance_to_value = distance_map[(active.name, target_name)]

        if time_remaining[active_explorer] > distance_to_value + 1:
            if active_explorer == 0:
                new_active_names = (target_name, active_names[1])
                new_time_remaining = (time_remaining[0] - distance_to_value - 1, time_remaining[1])
            else:
                new_active_names = (active_names[0], target_name)
                new_time_remaining = (time_remaining[0], time_remaining[1] - distance_to_value - 1)

            subs.append(_solve_rec_2(i_valves,
                                     distance_map,
                                     available - {target_name},
                                     new_active_names,
                                     new_time_remaining,
                                     value + (time_remaining[active_explorer] - distance_to_value - 1) * i_valves[target_name].rate))

    return max(subs) if subs else value


if __name__ == "__main__":
    run_day(15,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
