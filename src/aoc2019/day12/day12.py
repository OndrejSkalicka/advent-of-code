import copy
import logging
import math
from dataclasses import dataclass, field
from itertools import combinations

from util.logger_config import init_logger

logger = logging.getLogger(__name__)


@dataclass
class Xyz:
    x: int = 0
    y: int = 0
    z: int = 0

    def __getitem__(self, axis: int) -> int:
        if axis == 0:
            return self.x
        elif axis == 1:
            return self.y
        elif axis == 2:
            return self.z
        else:
            raise Exception(f"Illegal access {axis}")

    def __setitem__(self, axis: int, data: int) -> None:
        if axis == 0:
            self.x = data
        elif axis == 1:
            self.y = data
        elif axis == 2:
            self.z = data
        else:
            raise Exception(f"Illegal access {axis}")


@dataclass
class Moon:
    position: Xyz
    velocity: Xyz = field(default_factory=Xyz, init=False)

    def energy(self) -> int:
        return sum(abs(self.position[axis]) for axis in range(3)) * sum(abs(self.velocity[axis]) for axis in range(3))


def star1(moons: list[Moon], steps: int = 1_000) -> int:
    """
    >>> star1([Moon(Xyz(-1, 0, 2)), Moon(Xyz(2, -10, -7)), Moon(Xyz(4, -8, 8)), Moon(Xyz(3, 5, -1))], steps=10)
    179

    >>> star1([Moon(Xyz(-8, -10, 0)), Moon(Xyz(5, 5, 10)), Moon(Xyz(2, -7, 3)), Moon(Xyz(9, -8, -3))], steps=100)
    1940
    """

    for step in range(steps):

        a: Moon
        b: Moon
        for a, b in combinations(moons, 2):

            for axis in range(3):
                if a.position[axis] == b.position[axis]:
                    continue

                da = -1 if a.position[axis] > b.position[axis] else 1
                a.velocity[axis] += da
                b.velocity[axis] -= da

        for moon in moons:
            for axis in range(3):
                moon.position[axis] += moon.velocity[axis]

    return sum(moon.energy() for moon in moons)


def star2(original: list[Moon]) -> int:
    """
    >>> star2([Moon(Xyz(-1, 0, 2)), Moon(Xyz(2, -10, -7)), Moon(Xyz(4, -8, 8)), Moon(Xyz(3, 5, -1))])
    2772

    >>> star2([Moon(Xyz(-8, -10, 0)), Moon(Xyz(5, 5, 10)), Moon(Xyz(2, -7, 3)), Moon(Xyz(9, -8, -3))])
    4686774924
    """

    steps_by_axis = []

    for axis in range(3):
        moons = copy.deepcopy(original)
        steps = 0

        while True:
            steps += 1
            a: Moon
            b: Moon
            for a, b in combinations(moons, 2):
                if a.position[axis] == b.position[axis]:
                    continue

                da = -1 if a.position[axis] > b.position[axis] else 1
                a.velocity[axis] += da
                b.velocity[axis] -= da

            for moon in moons:
                moon.position[axis] += moon.velocity[axis]

            if moons == original:
                logger.debug(f"Match at step {steps}")
                steps_by_axis.append(steps)
                break
    return math.lcm(*steps_by_axis)


if __name__ == "__main__":
    init_logger()

    print('Day 12 Star 1: %d' % star1([Moon(Xyz(-16, -1, -12)), Moon(Xyz(0, -4, -17)), Moon(Xyz(-11, 11, 0)), Moon(Xyz(2, 2, -6))]))
    print('Day 12 Star 2: %d' % star2([Moon(Xyz(-16, -1, -12)), Moon(Xyz(0, -4, -17)), Moon(Xyz(-11, 11, 0)), Moon(Xyz(2, 2, -6))]))
