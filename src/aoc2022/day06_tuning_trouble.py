from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1("mjqjpqmgbljsphdztnvjfqwrcgsmlb")
    7

    >>> star1("bvwbjplbgvbhsrlpgdmjqwftvncz")
    5

    >>> star1("nppdvjthqldpwncqszvftbrmjlhg")
    6

    >>> star1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
    10

    >>> star1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
    11
    """

    for i in range(3, len(data)):
        if len(set(data[i - 3:i + 1])) == 4:
            return i + 1

    raise Exception("Illegal input")


def star2(data: str) -> int:
    """
    >>> star2("mjqjpqmgbljsphdztnvjfqwrcgsmlb")
    19

    >>> star2("bvwbjplbgvbhsrlpgdmjqwftvncz")
    23

    >>> star2("nppdvjthqldpwncqszvftbrmjlhg")
    23

    >>> star2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
    29

    >>> star2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
    26
    """

    for i in range(13, len(data)):
        if len(set(data[i - 13:i + 1])) == 14:
            return i + 1

    raise Exception("Illegal input")


if __name__ == "__main__":
    run_day(6,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
