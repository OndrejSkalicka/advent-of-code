package aoc2020.day06;

import java.time.Duration;
import java.time.Instant;

import static java.util.Arrays.asList;

public class PerfTest {
    public static void main(String... args) {
        System.out.printf("Java Runtime %s %s (%s)%n", System.getProperty("java.runtime.name"),
                System.getProperty("java.vendor.version"), System.getProperty("java.runtime.version"));
        System.out.printf("Java VM %s %s (%s)%n", System.getProperty("java.vm.name"),
                System.getProperty("java.vendor.version"), System.getProperty("java.vm.version"));
        System.out.println(System.getProperty("java.home"));

        for (int iterations : asList(1, 5, 10, 20, 30, 50, 75, 100)) {
            final Instant start = Instant.now();
            final long result = new Day23().star2("853192647", iterations * 1_000_000);

            System.out.printf("Finished %3dM in %s [result=%d]%n",
                    iterations, Duration.between(start, Instant.now()), result);
        }
    }
}
