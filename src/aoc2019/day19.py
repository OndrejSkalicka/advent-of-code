import logging
from dataclasses import dataclass, field
from typing import Optional

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


@dataclass
class Row:
    y: int
    start: int
    end: int

    def width(self) -> int:
        return self.end - self.start + 1


@dataclass
class Radar:
    memory: list[int]
    invocations: int = field(init=False, default=0)

    def find_row(self, y: int, verify: bool = True) -> Optional[Row]:
        if y == 0:
            return Row(y, 0, 0)

        expected_start = round(0.799 * y)
        expected_end = round(0.886 * y)

        real_start = None
        for x in (expected_start, expected_start + 1, expected_start + 2):
            if self._compute_at_xy(x, y):
                real_start = x
                break

        # early rows
        if not real_start:
            return None

        real_end = None
        for x in (expected_end - 1, expected_end, expected_end + 1):
            if not self._compute_at_xy(x, y):
                real_end = x - 1
                break

        assert real_end is not None

        if verify:
            assert self._compute_at_xy(real_start - 1, y) is False
            assert self._compute_at_xy(real_start, y) is True
            assert self._compute_at_xy(real_end, y) is True
            assert self._compute_at_xy(real_end + 1, y) is False

        return Row(y, real_start, real_end)

    def _compute_at_xy(self, x: int, y: int) -> bool:
        self.invocations += 1
        program = Program(self.memory)
        program.write_input(x, y)
        program.run()
        output = program.read_output()
        return output == 1


def star1(memory: list[int]) -> int:
    rows: list[Optional[Row]] = []
    last_row: Optional[Row] = None
    invocations = 0

    for y in range(50):
        if y == 0:
            last_row = Row(y, 0, 0)
            rows.append(last_row)

            continue

        assert last_row is not None

        start: Optional[int] = None
        width: Optional[int] = None

        for x in range(last_row.start, last_row.start + 5):
            invocations += 1
            program = Program(memory)
            program.write_input(x, y)
            program.run()
            output = program.read_output()

            if output == 1:
                start = x
                break

        if start is None:
            rows.append(None)
            continue

        # begin with last row's end
        for x in range(max(start, last_row.end + 1), max(start, last_row.end + 1) + 5):
            invocations += 1
            program = Program(memory)
            program.write_input(x, y)
            program.run()
            output = program.read_output()

            if output == 0:
                width = x - start
                break

        assert width is not None

        last_row = Row(y, start, start + width - 1)
        rows.append(last_row)

    logger.debug("Map:\n%s", _dump(rows))
    logger.info(f"Invocations: {invocations:,}")
    return sum(row.width() for row in rows if row is not None)


def star2(memory: list[int]) -> int:
    radar = Radar(memory)

    lo_y = 0
    hi_y = 5000

    while True:
        pivot = (hi_y + lo_y) // 2
        square_width = _square_width_at_y(radar, pivot)

        if square_width > 100:
            logger.debug(f"Moving HI range {lo_y}--{hi_y} to {lo_y}--{pivot}")
            hi_y = pivot
            continue

        if square_width < 100:
            logger.debug(f"Moving LO range {lo_y}--{hi_y} to {pivot}--{hi_y}")
            lo_y = pivot
            continue

        break

    last_100 = pivot
    while True:
        pivot -= 1

        square_width = _square_width_at_y(radar, pivot)
        if square_width == 100:
            last_100 = pivot
            continue

        if square_width == 98:
            logger.info(f"Found 98 at Y={pivot}, using Y={last_100} as last known 100!")
            top_row = radar.find_row(last_100)
            assert top_row is not None
            return (top_row.end - 99) * 10_000 + top_row.y


def _square_width_at_y(radar: Radar, y: int) -> int:
    top_row = radar.find_row(y)
    bottom_row = radar.find_row(y + 99)

    assert top_row is not None
    assert bottom_row is not None

    square_width = top_row.end - bottom_row.start + 1
    logger.debug(f"Square width at Y={y}: {square_width}")
    return square_width


def _dump(rows: list[Optional[Row]]) -> str:
    result = ''
    for row in rows:
        if row is None:
            result += f'{"N/A":>5s}> '
            result += 50 * '.'
        else:
            result += f'{row.y: 5d}> '
            result += row.start * '.'
            result += row.width() * '#'
            result += (50 - row.width() - row.start) * '.'
        result += "\n"

    return result


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(19,
            lambda: star1(load_numbers_csv('day19.txt')),
            lambda: star2(load_numbers_csv('day19.txt')),
            )
