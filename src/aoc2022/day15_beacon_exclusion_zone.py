from dataclasses import dataclass

from util.data import txt_fn, load_strings_newline
from util.perf import run_day
from util.record import Xy


@dataclass
class Sensor:
    position: Xy
    beacon: Xy


def star1(data: list[str], y: int = 2_000_000) -> int:
    """
    >>> star1([
    ...        "Sensor at x=2, y=18: closest beacon is at x=-2, y=15",
    ...        "Sensor at x=9, y=16: closest beacon is at x=10, y=16",
    ...        "Sensor at x=13, y=2: closest beacon is at x=15, y=3",
    ...        "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
    ...        "Sensor at x=10, y=20: closest beacon is at x=10, y=16",
    ...        "Sensor at x=14, y=17: closest beacon is at x=10, y=16",
    ...        "Sensor at x=8, y=7: closest beacon is at x=2, y=10",
    ...        "Sensor at x=2, y=0: closest beacon is at x=2, y=10",
    ...        "Sensor at x=0, y=11: closest beacon is at x=2, y=10",
    ...        "Sensor at x=20, y=14: closest beacon is at x=25, y=17",
    ...        "Sensor at x=17, y=20: closest beacon is at x=21, y=22",
    ...        "Sensor at x=16, y=7: closest beacon is at x=15, y=3",
    ...        "Sensor at x=14, y=3: closest beacon is at x=15, y=3",
    ...        "Sensor at x=20, y=1: closest beacon is at x=15, y=3",
    ...        ], y=10)
    26
    """

    sensors = _load_data(data)

    ranges_at_y: list[tuple[int, int]] = []

    for sensor in sensors:
        y_dist = abs(sensor.position.y - y)
        detect_range = sensor.position.manhattan(sensor.beacon)

        detect_range_at_y = detect_range - y_dist

        if detect_range_at_y < 0:
            continue

        ranges_at_y.append(
            (
                sensor.position.x - detect_range_at_y,
                sensor.position.x + detect_range_at_y,
            )
        )

    total = 0
    previous = None
    for current in sorted(ranges_at_y):
        if previous is None:
            previous = current
            continue

        if current[0] > previous[1]:
            total += previous[1] - previous[0] + 1
            previous = current
        else:
            previous = (
                previous[0],
                max(previous[1], current[1])
            )
    if previous is not None:
        total += previous[1] - previous[0] + 1

    beacons_at_y = set()
    for sensor in sensors:
        if sensor.beacon.y == y:
            beacons_at_y.add(sensor.beacon)

    return total - len(beacons_at_y)


def star2(data: list[str], max_x: int = 4_000_000, max_y: int = 4_000_000) -> int:
    """
    >>> star2([
    ...        "Sensor at x=2, y=18: closest beacon is at x=-2, y=15",
    ...        "Sensor at x=9, y=16: closest beacon is at x=10, y=16",
    ...        "Sensor at x=13, y=2: closest beacon is at x=15, y=3",
    ...        "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
    ...        "Sensor at x=10, y=20: closest beacon is at x=10, y=16",
    ...        "Sensor at x=14, y=17: closest beacon is at x=10, y=16",
    ...        "Sensor at x=8, y=7: closest beacon is at x=2, y=10",
    ...        "Sensor at x=2, y=0: closest beacon is at x=2, y=10",
    ...        "Sensor at x=0, y=11: closest beacon is at x=2, y=10",
    ...        "Sensor at x=20, y=14: closest beacon is at x=25, y=17",
    ...        "Sensor at x=17, y=20: closest beacon is at x=21, y=22",
    ...        "Sensor at x=16, y=7: closest beacon is at x=15, y=3",
    ...        "Sensor at x=14, y=3: closest beacon is at x=15, y=3",
    ...        "Sensor at x=20, y=1: closest beacon is at x=15, y=3",
    ...        ], max_x=20, max_y=20)
    56000011
    """

    sensors = _load_data(data)

    for y in range(max_y + 1):
        ranges_at_y: list[tuple[int, int]] = []

        for sensor in sensors:
            y_dist = abs(sensor.position.y - y)
            detect_range = sensor.position.manhattan(sensor.beacon)

            detect_range_at_y = detect_range - y_dist

            if detect_range_at_y < 0:
                continue

            ranges_at_y.append(
                (
                    sensor.position.x - detect_range_at_y,
                    sensor.position.x + detect_range_at_y,
                )
            )

        min_x = 0
        for range_at_x in sorted(ranges_at_y):
            if range_at_x[0] > min_x:
                return min_x * 4_000_000 + y

            if max_x > 4_000_000:
                break

            min_x = max(min_x, range_at_x[1] + 1)

    raise Exception("Solution not found :(")


def _load_data(data: list[str]) -> list[Sensor]:
    sensors = []
    for row in data:
        parts = row[len('Sensor at x='):].split(': closest beacon is at x=')

        position = parts[0].split(', y=')
        closest = parts[1].split(', y=')

        sensors.append(Sensor(
            Xy(int(position[0]), int(position[1])),
            Xy(int(closest[0]), int(closest[1])),
        ))
    return sensors


if __name__ == "__main__":
    run_day(15,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
