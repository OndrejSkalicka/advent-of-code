import os
from typing import List, Set, Dict, Tuple


def load_data(fn: str = 'data.txt') -> List[Tuple[List[str], List[str]]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            (a, b) = line.split(' | ')
            result.append(
                (
                    a.split(' '),
                    b.split(' ')
                )
            )

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def decode(data: List[str]):
    signal_by_len = {k: [] for k in range(2, 8)}  # type: Dict[int, List[Set[str]]]

    for x in data:
        signal_by_len[len(x)].append(set(x))

    fixed = {
        'a': None,
        'b': None,
        'c': None,
        'd': None,
        'e': None,
        'f': None,
        'g': None,
    }

    counts = {
        'a': 0,
        'b': 0,
        'c': 0,
        'd': 0,
        'e': 0,
        'f': 0,
        'g': 0,
    }
    for signal in data:
        for c in signal:
            counts[c] += 1

    r_counts = {
        4: [],
        6: [],
        7: [],
        8: [],
        9: []
    }  # type: Dict[int, List[str]]
    for k, v in counts.items():
        r_counts[v].append(k)

    """
     aaa
    b   c
    b   c
    b   c
     ddd
    e   f
    e   f
    e   f
     ggg

    a 8x
    b 6x
    c 8x
    d 7x
    e 4x
    f 9x
    g 7x
    """

    fixed['b'] = r_counts[6][0]
    fixed['e'] = r_counts[4][0]
    fixed['f'] = r_counts[9][0]

    fixed['a'] = ni(signal_by_len[3][0].difference(signal_by_len[2][0]))
    fixed['f'] = ni(signal_by_len[6][0]
                    .intersection(signal_by_len[6][1])
                    .intersection(signal_by_len[6][2])
                    .intersection(signal_by_len[2][0]))
    fixed['c'] = ni(signal_by_len[2][0].difference(set(fixed['f'])))
    fixed['d'] = ni(signal_by_len[4][0]
                    .intersection(signal_by_len[5][0])
                    .intersection(signal_by_len[5][1])
                    .intersection(signal_by_len[5][2]))
    fixed['g'] = ni(set(r_counts[7]).difference(set(fixed['d'])))

    return {
        ''.join(sorted([fixed['a'], fixed['b'], fixed['c'], fixed['e'], fixed['f'], fixed['g']])): 0,
        ''.join(sorted([fixed['c'], fixed['f']])): 1,
        ''.join(sorted([fixed['a'], fixed['c'], fixed['d'], fixed['e'], fixed['g']])): 2,
        ''.join(sorted([fixed['a'], fixed['c'], fixed['d'], fixed['f'], fixed['g']])): 3,
        ''.join(sorted([fixed['b'], fixed['c'], fixed['d'], fixed['f']])): 4,
        ''.join(sorted([fixed['a'], fixed['b'], fixed['d'], fixed['f'], fixed['g']])): 5,
        ''.join(sorted([fixed['a'], fixed['b'], fixed['d'], fixed['e'], fixed['f'], fixed['g']])): 6,
        ''.join(sorted([fixed['a'], fixed['c'], fixed['f']])): 7,
        ''.join(sorted([fixed['a'], fixed['b'], fixed['c'], fixed['d'], fixed['e'], fixed['f'], fixed['g']])): 8,
        ''.join(sorted([fixed['a'], fixed['b'], fixed['c'], fixed['d'], fixed['f'], fixed['g']])): 9,
    }


def ni(val: Set[str]) -> str:
    if len(val) != 1:
        raise Exception("Too big %s" % val)
    return next(iter(val))


def star1(data: List[Tuple[List[str], List[str]]]) -> int:
    """
    >>> star1(test_data())
    26
    """

    cnt = 0
    for key, value in data:
        dec = decode(key)

        for segment in value:
            seg_key = ''.join(sorted(set(segment)))
            if dec[seg_key] in (1, 4, 7, 8):
                cnt += 1

    return cnt


def star2(data: List[Tuple[List[str], List[str]]]) -> int:
    """
    >>> star2(test_data())
    61229
    """

    total = 0
    for key, value in data:
        dec = decode(key)
        part = ''

        for segment in value:
            seg_key = ''.join(sorted(set(segment)))
            part += str(dec[seg_key])

        total += int(part)

    return total


if __name__ == "__main__":
    print('Day 8 Star 1: %d' % star1(load_data()))
    print('Day 8 Star 2: %d' % star2(load_data()))
