import json
from datetime import datetime, timedelta


def sec_to_str(sec: int) -> str:
    return '+%s' % str(timedelta(seconds=sec))


if __name__ == "__main__":
    with open('leaderboard.json', 'r', encoding="utf-8") as file:
        data = file.read()

    js = json.loads(data)

    members = sorted(list(v for v in js['members'].values()), key=lambda x: -x['local_score'])

    best = {}
    max_day = 0
    for member in members:
        for day, result in member.get('completion_day_level').items():
            day = int(day)
            for star in (1, 2):

                sts = result.get(str(star), {}).get('get_star_ts')
                if sts:
                    max_day = max(day, max_day)
                    key = (day, star)
                    if key not in best or best[key] > sts:
                        best[key] = sts

    for member in members:
        line = "%-20s " % member['name']

        cdl = member.get('completion_day_level')
        for day in reversed(range(1, max_day + 1)):
            day_js = cdl.get(str(day), {})
            day_star_1 = day_js.get('1', {}).get('get_star_ts')
            day_star_2 = day_js.get('2', {}).get('get_star_ts')

            day_star_1_str = "              ---"
            day_star_2_str = "              ---"

            if day_star_1:
                day_star_1_str = '%s %18s ' % (datetime.fromtimestamp(day_star_1),
                                               sec_to_str(day_star_1 - best[(day, 1)]))
            if day_star_2:
                day_star_2_str = '%s %18s ' % (datetime.fromtimestamp(day_star_2),
                                               sec_to_str(day_star_2 - best[(day, 2)]))

            line += '#  %-40s .  %-40s ' % (day_star_1_str, day_star_2_str)
        print(line)
