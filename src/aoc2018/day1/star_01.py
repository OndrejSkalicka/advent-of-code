def day_1_part_1(fs):
    """
    >>> day_1_part_1([1, -2, 3, 1])
    3

    >>> day_1_part_1([1, 1, 1])
    3

    >>> day_1_part_1([1, 1, -2])
    0

    >>> day_1_part_1([-1, -2, -3])
    -6
    """
    return sum(fs)


def day_1_part_2(fs):
    """
    >>> day_1_part_2([1, -2, 3, 1])
    2

    >>> day_1_part_2([1, -1])
    0

    >>> day_1_part_2([3, 3, 4, -2, -4])
    10

    >>> day_1_part_2([-6, 3, 8, 5, -6])
    5

    >>> day_1_part_2([7, 7, -2, -7 -4])
    14
    """
    seen = set()
    current = 0
    index = 0
    while True:
        if current in seen:
            return current

        seen.add(current)
        current += fs[index % len(fs)]
        index += 1


def load_data():
    with open('data.txt', 'r') as fh:
        return list(int(i) for i in fh.read().split("\n"))


if __name__ == "__main__":
    data = load_data()
    print('Day 1 Star 1: %d' % day_1_part_1(data))
    print('Day 1 Star 2: %d' % day_1_part_2(data))
