from typing import List

from util.data import txt_fn, load_strings_newline
from util.perf import run_day


def star1(data: List[str]) -> int:
    """
    >>> star1(['2x3x4'])
    58
    >>> star1(['1x1x10'])
    43
    """

    total = 0
    for row in data:
        l, w, h = list(int(x) for x in row.split('x'))

        total += 2 * l * w + 2 * w * h + 2 * h * l + min(l * w, w * h, h * l)

    return total


def star2(data: List[str]) -> int:
    """
    >>> star2(['2x3x4'])
    34
    >>> star2(['1x1x10'])
    14
    """

    total = 0
    for row in data:
        l, w, h = list(int(x) for x in row.split('x'))

        total += 2 * min(l + w, w + h, h + l) + w * h * l

    return total


if __name__ == "__main__":
    run_day(2,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
