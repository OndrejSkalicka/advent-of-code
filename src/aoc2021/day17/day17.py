import logging
from typing import Optional, Tuple

from util.logger_config import init_logger

logger = logging.getLogger(__name__)


def star1():
    best = None  # type: Optional[int]
    for dx in range(164):
        for dy in range(200):
            res = boom(dx, dy, (117, 164, -140, -89))
            if res is not None and (best is None or best < res):
                best = res

    return best


def star2(hit_zone: Tuple[int, int, int, int]):
    """
    >>> star2((20, 30, -10, -5))
    112
    """

    hits = 0
    for dx in range(max(hit_zone[:2]) + 1):
        for dy in range(min(hit_zone[2:4]) - 1, 200):
            res = boom(dx, dy, hit_zone)
            if res is not None:
                hits += 1

    return hits


def boom(dx: int, dy: int, hit_zone: Tuple[int, int, int, int]) -> Optional[int]:
    """
    >>> boom(7, 2, (20, 30, -10, -5))
    3
    >>> boom(6, 3, (20, 30, -10, -5))
    6
    >>> boom(9, 0, (20, 30, -10, -5))
    0
    >>> boom(17, -4, (20, 30, -10, -5))

    >>> boom(6, 9, (20, 30, -10, -5))
    45
    """
    x = 0
    y = 0
    max_y = 0

    original_dx = dx
    original_dy = dy

    steps = 0
    while True:
        x += dx
        y += dy

        if dx > 0:
            dx -= 1
        elif dx < 0:
            dx += 1
        dy -= 1

        if y > max_y:
            max_y = y
        steps += 1

        logger.debug("New coord [%d; %d], new dx [%d; %d]" % (x, y, dx, dy))

        if hit_zone[0] <= x <= hit_zone[1] and hit_zone[2] <= y <= hit_zone[3]:
            logger.info("HIT! max y %5d, original d[%d;%d], xy [%d;%d]" % (max_y, original_dx, original_dy, x, y))
            return max_y

        if dx >= 0 and x > hit_zone[1] or dx <= 0 and x < hit_zone[0]:
            logger.debug("Cannot hit X zone: [%d;%d] + [%d;%d], zone %d..%d, steps %d" %
                         (x, y, dx, dy, hit_zone[0], hit_zone[1], steps))
            break

        if dy <= 0 and y < hit_zone[2]:
            logger.debug("Cannot hit Y zone: [%d;%d] + [%d;%d], zone %d..%d, steps %d" %
                         (x, y, dx, dy, hit_zone[2], hit_zone[3], steps))
            break

    return None


if __name__ == "__main__":
    init_logger()

    print('Day 17 Star 1: %d' % star1())
    print('Day 17 Star 2: %d' % star2((117, 164, -140, -89)))
