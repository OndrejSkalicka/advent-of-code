import math
import re
from collections import defaultdict
from dataclasses import dataclass

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


@dataclass()
class Robot:
    x: int
    y: int
    dx: int
    dy: int


def star1(data: list[str], width: int = 101, height: int = 103) -> int:
    """
    >>> star1(["p=0,4 v=3,-3",
    ...        "p=6,3 v=-1,-3",
    ...        "p=10,3 v=-1,2",
    ...        "p=2,0 v=2,-1",
    ...        "p=0,0 v=1,3",
    ...        "p=3,0 v=-2,-2",
    ...        "p=7,6 v=-1,-3",
    ...        "p=3,0 v=-1,-2",
    ...        "p=9,3 v=2,3",
    ...        "p=7,3 v=-1,2",
    ...        "p=2,4 v=2,-3",
    ...        "p=9,5 v=-3,-3",], 11, 7)
    12
    """

    robots = list()
    for row in data:
        m = re.match(r'^p=(\d+),(\d+) v=(-?\d+),(-?\d+)$', row)
        assert m

        robots.append(Robot(int(m[1]), int(m[2]), int(m[3]), int(m[4])))

    for _ in range(100):
        for robot in robots:
            robot.x = (robot.x + robot.dx) % width
            robot.y = (robot.y + robot.dy) % height

    quadrants = [0, 0, 0, 0]
    for robot in robots:
        if robot.x < width // 2 and robot.y < height // 2:
            quadrants[0] += 1
        elif robot.x < width // 2 and robot.y > height // 2:
            quadrants[1] += 1
        elif robot.x > width // 2 and robot.y < height // 2:
            quadrants[2] += 1
        elif robot.x > width // 2 and robot.y > height // 2:
            quadrants[3] += 1

    return math.prod(quadrants)


def star2(data: list[str], width: int = 101, height: int = 103) -> int:
    robots = list()
    for row in data:
        m = re.match(r'^p=(\d+),(\d+) v=(-?\d+),(-?\d+)$', row)
        assert m

        robots.append(Robot(int(m[1]), int(m[2]), int(m[3]), int(m[4])))

    step = 0
    while True:
        step += 1

        for robot in robots:
            robot.x = (robot.x + robot.dx) % width
            robot.y = (robot.y + robot.dy) % height

        all_robots = set((r.x, r.y) for r in robots)
        far_away = 0

        for r in all_robots:
            if not _is_nearby(r, all_robots):
                far_away += 1

        if far_away < 150:
            bots_by_xy: dict[tuple[int, int], int] = defaultdict(int)
            for robot in robots:
                bots_by_xy[(robot.x, robot.y)] += 1

            for y in range(height):
                for x in range(width):
                    if bots_by_xy[(x, y)] > 0:
                        print(bots_by_xy[(x, y)], end='')
                    else:
                        print('.', end='')
                print()

            return step


def _is_nearby(xy: tuple[int, int], all_robots: set[tuple[int, int]]) -> bool:
    for dx in range(-1, 2):
        for dy in range(-1, 2):
            if dx == 0 and dy == 0:
                continue

            if (xy[0] + dx, xy[1] + dy) in all_robots:
                return True

    return False


if __name__ == "__main__":
    run_day(14,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
