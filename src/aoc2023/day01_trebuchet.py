import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["1abc2",
    ...       "pqr3stu8vwx",
    ...       "a1b2c3d4e5f",
    ...       "treb7uchet"])
    142
    """

    result = 0

    for row in data:
        m1 = re.match(r'.*?(\d)', row)
        m2 = re.match(r'.*(\d)', row)

        assert m1 is not None
        assert m2 is not None

        result += int(m1.group(1) + m2.group(1))

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["two1nine",
    ...       "eightwothree",
    ...       "abcone2threexyz",
    ...       "xtwone3four",
    ...       "4nineeightseven2",
    ...       "zoneight234",
    ...       "7pqrstsixteen"])
    281
    """

    result = 0

    digits = {
        'one': 1,
        'two': 2,
        'three': 3,
        'four': 4,
        'five': 5,
        'six': 6,
        'seven': 7,
        'eight': 8,
        'nine': 9,
        '0': 0,
        '1': 1,
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
    }

    for row in data:
        m1 = re.match(r'.*?(one|two|three|four|five|six|seven|eight|nine|\d)', row)
        m2 = re.match(r'.*(one|two|three|four|five|six|seven|eight|nine|\d)', row)

        assert m1 is not None
        assert m2 is not None

        result += 10 * digits[m1.group(1)] + digits[m2.group(1)]

    return result


if __name__ == "__main__":
    run_day(1,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
