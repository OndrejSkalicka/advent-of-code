import os
from queue import PriorityQueue
from typing import List, Tuple, Dict


def load_data(fn: str = 'data.txt') -> Dict[Tuple[int, int], int]:
    result = dict()

    with open(fn, 'r') as fh:
        y = 0
        for line in fh.readlines():
            line = line.strip()

            for x in range(len(line)):
                result[(x, y)] = int(line[x])

            y += 1

    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(data: Dict[Tuple[int, int], int]) -> int:
    """
    >>> star1(test_data())
    40
    """

    max_x = max(xy[0] for xy in data.keys())
    max_y = max(xy[1] for xy in data.keys())
    processed = set()
    distances = dict()  # type: Dict[Tuple[int, int], int]
    pq = PriorityQueue()

    pq.put((0, (0, 0)))
    distances[(0, 0)] = 0

    while not pq.empty():
        active = pq.get_nowait()[1]
        if active in processed:
            continue

        for nxy in adj_coord(active, max_x, max_y):
            if nxy in processed:
                continue

            distance = data[nxy] + distances[active]
            if nxy not in distances or distance < distances[nxy]:
                distances[nxy] = distance
                pq.put_nowait((distance, nxy))

        processed.add(active)

    return distances[(max_x, max_y)]


def star2(data: Dict[Tuple[int, int], int]) -> int:
    """
    >>> star2(test_data())
    315
    """

    new_map = dict()
    max_x = max(xy[0] for xy in data.keys())
    max_y = max(xy[1] for xy in data.keys())
    len_x = max_x + 1
    len_y = max_y + 1

    for mx in range(5):
        for my in range(5):
            for xy, value in data.items():
                new_value = (value + mx + my - 1) % 9 + 1
                new_map[(xy[0] + mx * len_x, xy[1] + my * len_y)] = new_value

    return star1(new_map)


def adj_coord(xy: Tuple[int, int], max_x: int, max_y: int) -> List[Tuple[int, int]]:
    result = []
    for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0)):
        nx = xy[0] + dx
        ny = xy[1] + dy

        if 0 <= nx <= max_x and 0 <= ny <= max_y:
            result.append((nx, ny))

    return result


if __name__ == "__main__":
    print('Day 15 Star 1: %d' % star1(load_data()))
    print('Day 15 Star 2: %d' % star2(load_data()))
