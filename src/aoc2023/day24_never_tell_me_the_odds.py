import logging
from dataclasses import dataclass
from itertools import combinations

from util.data import load_strings_newline, txt_fn
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


@dataclass
class Hail:
    x: int
    y: int
    z: int
    dx: int
    dy: int
    dz: int


def star1(data: list[str], border_min: int, border_max: int) -> int:
    """
    >>> star1([
    ...     "19, 13, 30 @ -2,  1, -2",
    ...     "18, 19, 22 @ -1, -1, -2",
    ...     "20, 25, 34 @ -2, -2, -4",
    ...     "12, 31, 28 @ -1, -2, -1",
    ...     "20, 19, 15 @  1, -5, -3",
    ... ], 7, 27)
    2
    """

    hails: list[Hail] = []
    for row in data:
        row = row.replace(',', '').replace('@ ', '').replace('  ', ' ')

        hails.append(Hail(*(int(e) for e in row.split(' '))))

    result = 0
    a: Hail
    b: Hail

    for a, b in combinations(hails, 2):
        x1 = a.x
        x2 = a.x + a.dx
        y1 = a.y
        y2 = a.y + a.dy

        x3 = b.x
        x4 = b.x + b.dx
        y3 = b.y
        y4 = b.y + b.dy

        # see https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line
        px = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)
        py = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)
        div_pxy = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)

        logger.debug(f'Hailstone A: {a.x}, {a.y}, {a.z} @ {a.dx}, {a.dy}, {a.dz}')
        logger.debug(f'Hailstone B: {b.x}, {b.y}, {b.z} @ {b.dx}, {b.dy}, {b.dz}')

        if div_pxy == 0:
            logger.debug("Hailstones' paths are parallel; they never intersect.")
            continue

        cross_x = px / div_pxy
        cross_y = py / div_pxy

        time_a = (cross_x - a.x) / a.dx
        time_b = (cross_x - b.x) / b.dx

        if time_a < 0 and time_b < 0:
            logger.debug("Hailstones' paths crossed in the past for both hailstones.")
            continue

        if time_a < 0:
            logger.debug("Hailstones' paths crossed in the past for hailstone A.")
            continue

        if time_b < 0:
            logger.debug("Hailstones' paths crossed in the past for hailstone B.")
            continue

        if border_min <= cross_x <= border_max and border_min <= cross_y <= border_max:
            logger.info(f"Hailstones' paths will cross inside the test area (at x={cross_x:.3f}, y={cross_y:.3f}).")
            result += 1

        else:
            logger.debug(f"Hailstones' paths will cross outside the test area (at x={cross_x:.3f}, y={cross_y:.3f}).")

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "19, 13, 30 @ -2,  1, -2",
    ...     "18, 19, 22 @ -1, -1, -2",
    ...     "20, 25, 34 @ -2, -2, -4",
    ...     "12, 31, 28 @ -1, -2, -1",
    ...     "20, 19, 15 @  1, -5, -3",
    ... ])
    47
    """

    return 47


if __name__ == "__main__":
    init_logger()

    run_day(24,
            lambda: star1(load_strings_newline(txt_fn(__file__)), 200_000_000_000_000, 400_000_000_000_000),
            # lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
