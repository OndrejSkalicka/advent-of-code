import math
import re
from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
    ...       "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
    ...       "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
    ...       "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
    ...       "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"])
    8
    """

    result = 0
    limit = {
        'red': 12,
        'green': 13,
        'blue': 14,
    }

    for game in data:
        m = re.match(r'Game (\d+): (.*)', game)
        assert m

        game_id = int(m.group(1))
        raw_rounds = m.group(2).split('; ')
        valid = True

        for game_round in raw_rounds:
            cubes = {
                part.split(' ')[1]: int(part.split(' ')[0])
                for part in game_round.split(', ')
            }

            for color, number in cubes.items():
                if limit[color] < number:
                    valid = False

        if valid:
            result += game_id

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
    ...       "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
    ...       "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
    ...       "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
    ...       "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"])
    2286
    """

    result = 0

    for game in data:
        m = re.match(r'Game (\d+): (.*)', game)
        assert m

        raw_rounds = m.group(2).split('; ')
        fewest_bag: dict[str, int] = defaultdict(int)

        for game_round in raw_rounds:
            cubes = {
                part.split(' ')[1]: int(part.split(' ')[0])
                for part in game_round.split(', ')
            }

            for color, number in cubes.items():
                fewest_bag[color] = max(fewest_bag[color], number)

        result += math.prod(fewest_bag.values())

    return result


if __name__ == "__main__":
    run_day(2,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
