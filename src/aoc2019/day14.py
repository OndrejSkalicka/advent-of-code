import logging
import math
from collections import defaultdict
from dataclasses import dataclass

from util.data import load_strings_newline
from util.logger_config import init_logger

logger = logging.getLogger(__name__)


@dataclass
class QuantifiedElement:
    amount: int
    element: str

    @staticmethod
    def from_str(text: str) -> 'QuantifiedElement':
        q, e = text.split(' ')
        return QuantifiedElement(int(q), e)


@dataclass
class Recipe:
    result: QuantifiedElement
    parts: list[QuantifiedElement]


def star1(lines: list[str], fuel: int = 1) -> int:
    """
    >>> star1("10 ORE => 10 A\\n"
    ...       "1 ORE => 1 B\\n"
    ...       "7 A, 1 B => 1 C\\n"
    ...       "7 A, 1 C => 1 D\\n"
    ...       "7 A, 1 D => 1 E\\n"
    ...       "7 A, 1 E => 1 FUEL".split("\\n"))
    31

    >>> star1("9 ORE => 2 A\\n"
    ...       "8 ORE => 3 B\\n"
    ...       "7 ORE => 5 C\\n"
    ...       "3 A, 4 B => 1 AB\\n"
    ...       "5 B, 7 C => 1 BC\\n"
    ...       "4 C, 1 A => 1 CA\\n"
    ...       "2 AB, 3 BC, 4 CA => 1 FUEL".split("\\n"))
    165

    >>> star1("157 ORE => 5 NZVS\\n"
    ...       "165 ORE => 6 DCFZ\\n"
    ...       "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\\n"
    ...       "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\\n"
    ...       "179 ORE => 7 PSHF\\n"
    ...       "177 ORE => 5 HKGWZ\\n"
    ...       "7 DCFZ, 7 PSHF => 2 XJWVT\\n"
    ...       "165 ORE => 2 GPVTF\\n"
    ...       "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT".split("\\n"))
    13312

    >>> star1("2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\\n"
    ...       "17 NVRVD, 3 JNWZP => 8 VPVL\\n"
    ...       "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\\n"
    ...       "22 VJHF, 37 MNCFX => 5 FWMGM\\n"
    ...       "139 ORE => 4 NVRVD\\n"
    ...       "144 ORE => 7 JNWZP\\n"
    ...       "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\\n"
    ...       "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\\n"
    ...       "145 ORE => 6 MNCFX\\n"
    ...       "1 NVRVD => 8 CXFTF\\n"
    ...       "1 VJHF, 6 MNCFX => 4 RFSQX\\n"
    ...       "176 ORE => 6 VJHF".split("\\n"))
    180697

    >>> star1("171 ORE => 8 CNZTR\\n"
    ...       "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\\n"
    ...       "114 ORE => 4 BHXH\\n"
    ...       "14 VRPVC => 6 BMBT\\n"
    ...       "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\\n"
    ...       "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\\n"
    ...       "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\\n"
    ...       "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\\n"
    ...       "5 BMBT => 4 WPTQ\\n"
    ...       "189 ORE => 9 KTJDG\\n"
    ...       "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\\n"
    ...       "12 VRPVC, 27 CNZTR => 2 XDBXC\\n"
    ...       "15 KTJDG, 12 BHXH => 5 XCVML\\n"
    ...       "3 BHXH, 2 VRPVC => 7 MZWV\\n"
    ...       "121 ORE => 7 VRPVC\\n"
    ...       "7 XCVML => 6 RJRHP\\n"
    ...       "5 BHXH, 4 VRPVC => 5 LTCX".split("\\n"))
    2210736
    """

    recipes: dict[str, Recipe] = {}

    for row in lines:
        parts = list(QuantifiedElement.from_str(x) for x in row.split(' => ')[0].split(', '))
        result = QuantifiedElement.from_str(row.split(' => ')[1])

        recipes[result.element] = Recipe(result, parts)

    stock: dict[str, int] = defaultdict(int)
    stock['FUEL'] = -fuel

    while True:
        element = next(k for k in stock.keys() if stock[k] < 0 and k != 'ORE')

        recipe = recipes[element]
        amount = -stock[element]

        invocations = math.ceil(amount / recipe.result.amount)
        logger.debug(f'Converting {invocations} * {recipe.parts} to get at least {amount} * {element}')

        stock[element] += invocations * recipe.result.amount
        for part in recipe.parts:
            stock[part.element] -= invocations * part.amount
            logger.debug(f'Removing {invocations * part.amount} of {part.element} from stock')

        logger.debug(f'Stock: {stock}')

        if not list(k for k in stock.keys() if stock[k] < 0 and k != 'ORE'):
            return -stock['ORE']


def star2(lines: list[str]) -> int:
    """
    >>> star2("157 ORE => 5 NZVS\\n"
    ...       "165 ORE => 6 DCFZ\\n"
    ...       "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\\n"
    ...       "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\\n"
    ...       "179 ORE => 7 PSHF\\n"
    ...       "177 ORE => 5 HKGWZ\\n"
    ...       "7 DCFZ, 7 PSHF => 2 XJWVT\\n"
    ...       "165 ORE => 2 GPVTF\\n"
    ...       "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT".split("\\n"))
    82892753

    >>> star2("2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\\n"
    ...       "17 NVRVD, 3 JNWZP => 8 VPVL\\n"
    ...       "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\\n"
    ...       "22 VJHF, 37 MNCFX => 5 FWMGM\\n"
    ...       "139 ORE => 4 NVRVD\\n"
    ...       "144 ORE => 7 JNWZP\\n"
    ...       "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\\n"
    ...       "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\\n"
    ...       "145 ORE => 6 MNCFX\\n"
    ...       "1 NVRVD => 8 CXFTF\\n"
    ...       "1 VJHF, 6 MNCFX => 4 RFSQX\\n"
    ...       "176 ORE => 6 VJHF".split("\\n"))
    5586022

    >>> star2("171 ORE => 8 CNZTR\\n"
    ...       "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\\n"
    ...       "114 ORE => 4 BHXH\\n"
    ...       "14 VRPVC => 6 BMBT\\n"
    ...       "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\\n"
    ...       "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\\n"
    ...       "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\\n"
    ...       "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\\n"
    ...       "5 BMBT => 4 WPTQ\\n"
    ...       "189 ORE => 9 KTJDG\\n"
    ...       "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\\n"
    ...       "12 VRPVC, 27 CNZTR => 2 XDBXC\\n"
    ...       "15 KTJDG, 12 BHXH => 5 XCVML\\n"
    ...       "3 BHXH, 2 VRPVC => 7 MZWV\\n"
    ...       "121 ORE => 7 VRPVC\\n"
    ...       "7 XCVML => 6 RJRHP\\n"
    ...       "5 BHXH, 4 VRPVC => 5 LTCX".split("\\n"))
    460664
    """
    MAX_ORE = 1_000_000_000_000

    wc_ratio = star1(lines)
    low_fuel = MAX_ORE // wc_ratio
    high_fuel = low_fuel * 2

    while True:
        mid_fuel = (low_fuel + high_fuel) // 2
        mid_ore = star1(lines, mid_fuel)

        logger.info(f"Low {low_fuel:,}, high {high_fuel:,}, mid {mid_fuel:,}, ore {mid_ore:,}")

        if mid_ore > MAX_ORE:
            high_fuel = mid_fuel
        else:
            low_fuel = mid_fuel

        if high_fuel - low_fuel == 1:
            return low_fuel


if __name__ == "__main__":
    init_logger()

    print('Day 14 Star 1: %d' % star1(load_strings_newline('day14.txt')))
    print('Day 14 Star 2: %d' % star2(load_strings_newline('day14.txt')))
