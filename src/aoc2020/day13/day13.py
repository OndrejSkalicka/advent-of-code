import math
from typing import List, Tuple, Optional


def load_data(fn: str) -> Tuple[int, List[Optional[int]]]:
    with open(fn, 'r') as fh:
        e = int(fh.readline().strip())
        d = []
        for x in fh.readline().strip().split(','):
            if x == 'x':
                d.append(None)
            else:
                d.append(int(x))
        return e, d


def star1(earliest: int, data: List[int]) -> int:
    """
    >>> star1(939, [7, 13, None, None, 59, None, 31, 19])
    295
    """

    best_depart = None
    result = None
    for bus in data:
        if bus is None:
            continue

        depart = math.ceil(earliest / bus) * bus

        if best_depart is None or best_depart > depart:
            best_depart = depart
            result = (depart - earliest) * bus

    return result


def star2(data: List[int]) -> int:
    """
    >>> star2([7, 13, None, None, 59, None, 31, 19])
    1068781
    >>> star2([17, None, 13, 19])
    3417
    >>> star2([67, 7, 59, 61])
    754018
    >>> star2([67, None, 7, 59, 61])
    779210
    >>> star2([67, 7, None, 59, 61])
    1261476
    >>> star2([1789, 37, 47, 1889])
    1202161486
    """

    t = 0
    dt = data[0]
    index = 0

    while True:
        while True:
            index += 1
            if index >= len(data):
                return t

            if data[index] is not None:
                break

        bus_id = data[index]

        while True:
            t += dt
            if (t + index) % bus_id == 0:
                dt *= bus_id
                break


def star2_valid(t: int, data: List[int]) -> bool:
    for bus in data:
        if bus is not None and t % bus != 0:
            return False

        t += 1

    return True


if __name__ == "__main__":
    my_earliest, my_data = load_data('data.txt')
    print('Day 13 Star 1: %d' % star1(my_earliest, my_data))
    print('Day 13 Star 2: %d' % star2(my_data))
