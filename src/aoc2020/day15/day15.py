from typing import List, Dict


def star1(data: List[int], iterations: int = 2020) -> int:
    """
    >>> star1([0, 3, 6])
    436
    >>> star1([1, 3, 2])
    1
    >>> star1([2, 1, 3])
    10
    >>> star1([1, 2, 3])
    27
    >>> star1([2, 3, 1])
    78
    >>> star1([3, 2, 1])
    438
    >>> star1([3, 1, 2])
    1836
    """

    numbers = {}  # type: Dict[int, List[int]]
    for k, v in enumerate(data):
        s1_add_num(v, k, numbers)

    while len(data) < iterations:
        index = len(data)
        last = data[-1]
        info = numbers.get(last)

        if len(info) == 1:
            data.append(0)
            s1_add_num(0, index, numbers)
            continue

        last_two = info[-2:]
        next_number = last_two[1] - last_two[0]

        data.append(next_number)
        s1_add_num(next_number, index, numbers)

    return data[-1]


def s1_add_num(value: int, index: int, numbers: Dict[int, List[int]]):
    x = numbers.get(value, [])
    x.append(index)
    numbers[value] = x


if __name__ == "__main__":
    print('Day 15 Star 1: %d' % star1([2, 1, 10, 11, 0, 6]))
    # unoptimized version, takes about a minute
    print('Day 15 Star 2: %d' % star1([2, 1, 10, 11, 0, 6], iterations=30_000_000))
