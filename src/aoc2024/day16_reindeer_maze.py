import heapq
from typing import Callable, Iterable

from util.data import load_strings_newline, txt_fn, l_split
from util.dijkstra import dijkstra
from util.perf import run_day
from util.record import Grid, EAST, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["###############",
    ...        "#.......#....E#",
    ...        "#.#.###.#.###.#",
    ...        "#.....#.#...#.#",
    ...        "#.###.#####.#.#",
    ...        "#.#.#.......#.#",
    ...        "#.#.#####.###.#",
    ...        "#...........#.#",
    ...        "###.#.#####.#.#",
    ...        "#...#.....#.#.#",
    ...        "#.#.#.###.#.#.#",
    ...        "#.....#...#.#.#",
    ...        "#.###.#.#.#.#.#",
    ...        "#S..#.....#...#",
    ...        "###############",
    ...       ])
    7036
    >>> star1(["#################",
    ...        "#...#...#...#..E#",
    ...        "#.#.#.#.#.#.#.#.#",
    ...        "#.#.#.#...#...#.#",
    ...        "#.#.#.#.###.#.#.#",
    ...        "#...#.#.#.....#.#",
    ...        "#.#.#.#.#.#####.#",
    ...        "#.#...#.#.#.....#",
    ...        "#.#.#####.#.###.#",
    ...        "#.#.#.......#...#",
    ...        "#.#.###.#####.###",
    ...        "#.#.#...#.....#.#",
    ...        "#.#.#.#####.###.#",
    ...        "#.#.#.........#.#",
    ...        "#.#.#.#########.#",
    ...        "#S#.............#",
    ...        "#################",
    ...       ])
    11048
    """

    grid = Grid.from_list_str(l_split(data, '')[0])
    reindeer = grid.find_all_xy('S')[0].xy
    end = grid.find_all_xy('E')[0].xy

    distances, _ = dijkstra((reindeer, EAST), lambda node: _adj(node, grid), lambda node: node[0] == end)

    return list(distances[d] for d in distances if d[0] == end)[0]


def _adj(node: tuple[Xy, Xy], grid: Grid[str]) -> list[tuple[tuple[Xy, Xy], int]]:
    result = [
        ((node[0], node[1].rot_ccw()), 1000),
        ((node[0], node[1].rot_cw()), 1000)
    ]

    if grid[node[0] + node[1]] != '#':
        result.append(((node[0] + node[1], node[1]), 1))

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["###############",
    ...        "#.......#....E#",
    ...        "#.#.###.#.###.#",
    ...        "#.....#.#...#.#",
    ...        "#.###.#####.#.#",
    ...        "#.#.#.......#.#",
    ...        "#.#.#####.###.#",
    ...        "#...........#.#",
    ...        "###.#.#####.#.#",
    ...        "#...#.....#.#.#",
    ...        "#.#.#.###.#.#.#",
    ...        "#.....#...#.#.#",
    ...        "#.###.#.#.#.#.#",
    ...        "#S..#.....#...#",
    ...        "###############",
    ...       ])
    45
    >>> star2(["#################",
    ...        "#...#...#...#..E#",
    ...        "#.#.#.#.#.#.#.#.#",
    ...        "#.#.#.#...#...#.#",
    ...        "#.#.#.#.###.#.#.#",
    ...        "#...#.#.#.....#.#",
    ...        "#.#.#.#.#.#####.#",
    ...        "#.#...#.#.#.....#",
    ...        "#.#.#####.#.###.#",
    ...        "#.#.#.......#...#",
    ...        "#.#.###.#####.###",
    ...        "#.#.#...#.....#.#",
    ...        "#.#.#.#####.###.#",
    ...        "#.#.#.........#.#",
    ...        "#.#.#.#########.#",
    ...        "#S#.............#",
    ...        "#################",
    ...       ])
    64
    """

    grid = Grid.from_list_str(l_split(data, '')[0])
    reindeer = grid.find_all_xy('S')[0].xy
    end = grid.find_all_xy('E')[0].xy

    _, parents_map = mod_dijkstra((reindeer, EAST), lambda node: _adj(node, grid))

    shortest = min(parents_map[x][0] for x in parents_map if x[0] == end)
    visited = set()
    buffer = list(parents_map[x][1] for x in parents_map if parents_map[x][0] == shortest)[0]
    while buffer:
        active = buffer.pop()
        visited.add(active[0])
        if active not in parents_map:
            continue
        for parent in parents_map[active][1]:
            buffer.append(parent)

    return len(visited) + 1


def mod_dijkstra(start: tuple[Xy, Xy],
                 adjacent: Callable[[tuple[Xy, Xy]], Iterable[tuple[tuple[Xy, Xy], int]]]
                 ) -> tuple[dict[tuple[Xy, Xy], int], dict[tuple[Xy, Xy], tuple[int, list[tuple[Xy, Xy]]]]]:
    pq: list[tuple[int, tuple[Xy, Xy]]] = []
    active: tuple[Xy, Xy]
    heapq.heappush(pq, (0, start))
    visited = set()
    distances: dict[tuple[Xy, Xy], int] = {start: 0}
    parents_map: dict[tuple[Xy, Xy], tuple[int, list[tuple[Xy, Xy]]]] = {}

    while pq:
        active_distance, active = heapq.heappop(pq)

        if active in visited:
            continue

        visited.add(active)

        for node, node_distance in adjacent(active):
            if node in visited:
                continue

            if node in distances and distances[node] < active_distance + node_distance:
                continue

            if node in distances and distances[node] == active_distance + node_distance:
                parents_map[node][1].append(active)
                continue

            parents_map[node] = (active_distance + node_distance, [active])
            distances[node] = active_distance + node_distance

            heapq.heappush(pq, (active_distance + node_distance, node))

    return distances, parents_map


if __name__ == "__main__":
    run_day(16,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
