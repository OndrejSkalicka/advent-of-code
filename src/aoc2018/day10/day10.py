import os  # noqa
import re
from typing import Sequence, List


def star_1(stars: Sequence[List[int]]) -> str:
    """
    >>> star_1(load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test.txt')))
    '#...#..###\\n#...#...#.\\n#...#...#.\\n#####...#.\\n#...#...#.\\n#...#...#.\\n#...#...#.\\n#...#..###\\nDone in 3s'
    """

    last_w = None
    last_h = None
    last_min_x = None
    last_min_y = None
    sec = 0

    while True:
        min_x = None
        max_x = None
        min_y = None
        max_y = None
        sec += 1

        for star in stars:
            star[0] += star[2]
            star[1] += star[3]
            min_x = min(star[0], min_x) if min_x is not None else star[0]
            max_x = max(star[0], max_x) if max_x is not None else star[0]
            min_y = min(star[1], min_y) if min_y is not None else star[1]
            max_y = max(star[1], max_y) if max_y is not None else star[1]

        w = max_x - min_x
        h = max_y - min_y

        if last_w is None:
            last_w = w
            last_h = h
            last_min_x = min_x
            last_min_y = min_y
        elif w >= last_w and h >= last_h:

            result = [['.' for x in range(last_w + 1)] for y in range(last_h + 1)]
            for star in stars:
                # use last state
                y = star[1] - star[3] - last_min_y
                x = star[0] - star[2] - last_min_x
                result[y][x] = "#"

            return "\n".join(("".join(r) for r in result)) + "\nDone in %ds" % (sec - 1)
        else:
            last_w = w
            last_h = h
            last_min_x = min_x
            last_min_y = min_y


def load_data(fn) -> Sequence[List[int]]:
    result = []

    matcher = re.compile(r'''
                ^
                position=<\s*
                    (?P<x>-?\d+)
                    ,\s*
                    (?P<y>-?\d+)
                >\ velocity=<\s*
                    (?P<dx>-?\d+)
                    ,\s*
                    (?P<dy>-?\d+)
                >$''', re.X)
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            m = matcher.match(line)
            if not m:
                raise Exception('Cannot parse line "%s"' % line)

            result.append([
                int(m.group('x')),
                int(m.group('y')),
                int(m.group('dx')),
                int(m.group('dy'))
            ])

    return result


if __name__ == "__main__":
    print(star_1(load_data('data.txt')))
