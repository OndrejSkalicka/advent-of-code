from typing import List


def load_data(fn: str) -> List[str]:
    with open(fn, 'r') as fh:
        return list(line.strip() for line in fh.readlines())


def star1(data: List[str]) -> int:
    """
    >>> star1(['..##.......', '#...#...#..', '.#....#..#.', '..#.#...#.#', '.#...##..#.',
    ...        '..#.##.....', '.#.#.#....#', '.#........#', '#.##...#...', '#...##....#', '.#..#...#.#'])
    7
    """

    x = 0
    trees = 0
    for row in data:
        if row[x % len(row)] == '#':
            trees += 1

        x += 3

    return trees


def star2(data: List[str]) -> int:
    """
    >>> star2(['..##.......', '#...#...#..', '.#....#..#.', '..#.#...#.#', '.#...##..#.',
    ...        '..#.##.....', '.#.#.#....#', '.#........#', '#.##...#...', '#...##....#', '.#..#...#.#'])
    336
    """

    trees_mult = 1

    for dx, dy in ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2)):

        x = 0
        y = 0
        trees = 0
        while y < len(data):
            row = data[y]

            if row[x % len(row)] == '#':
                trees += 1

            x += dx
            y += dy

        trees_mult *= trees

    return trees_mult


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 3 Star 1: %d' % star1(data))
    print('Day 3 Star 2: %d' % star2(data))
