import os  # noqa
import re
from typing import Tuple, List, Set


def star_1(initial: List[int], rules: Set[Tuple[int, int, int, int, int]]) -> int:
    """
    >>> star_1(*load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test.txt')))
    325
    """

    offset = 50
    current = [0] * 200
    # -50 -> 150

    for key, value in enumerate(initial):
        current[key + offset] = value

    for generation in range(20):
        next_gen = [0] * 200
        for i in range(2, 198):
            if (current[i - 2], current[i - 1], current[i], current[i + 1], current[i + 2]) in rules:
                next_gen[i] = 1

        # print(''.join('#' if x == 1 else '.' for x in current))
        current = next_gen

    tot = 0
    for i in range(200):
        if current[i] == 1:
            tot += i - 50

    return tot


def load_data(fn) -> Tuple[List[int], Set[Tuple[int, int, int, int, int]]]:
    with open(fn, 'r') as fh:
        line = fh.readline()
        m = re.match(r'^initial state: ([#.]+)$', line.strip())
        initial = list(1 if x == '#' else 0 for x in m.group(1))

        matcher = re.compile(r'^(?P<key>[#.]{5}) => (?P<value>[#.])$')
        result = set()  # type: Set[Tuple[int, int, int, int, int]]
        for line in fh.readlines():
            line = line.strip()
            if not line:
                continue

            m = matcher.match(line)
            if not m:
                raise Exception('Cannot parse line "%s"' % line)

            if m.group('value') == '#':
                # noinspection PyTypeChecker
                result.add(tuple(1 if x == '#' else 0 for x in m.group('key')))

        return initial, result


if __name__ == "__main__":
    print(star_1(*load_data('data.txt')))
