import os
from typing import List


class Bingo:

    def __init__(self, numbers: List[int], boards: List[List[List[int]]]) -> None:
        self.numbers = numbers
        self.boards = boards

    def play(self) -> List[int]:
        board_flags = list(
            list(
                list(
                    0 for _ in self.boards[0][0]
                ) for _ in self.boards[0]
            ) for _ in self.boards
        )

        board_win_scores = []
        boards_won = set()

        for number in self.numbers:

            # write numbers
            for board_id, board in enumerate(self.boards):
                if board_id in boards_won:
                    continue

                for row_id, row in enumerate(board):
                    for cell_id, cell in enumerate(row):
                        if cell == number:
                            board_flags[board_id][row_id][cell_id] = 1

            # check for bingos
            for board_id, board in enumerate(board_flags):
                if board_id in boards_won:
                    continue

                if self.check_board_win(board):
                    boards_won.add(board_id)
                    board_win_scores.append(self.calculate_score(self.boards[board_id], board, number))

            if len(boards_won) == len(self.boards):
                break

        return board_win_scores

    @staticmethod
    def check_board_win(board_flags: List[List[int]]) -> bool:
        for row_id, row in enumerate(board_flags):
            if sum(row) == len(row):
                return True

        for col_id, _ in enumerate(board_flags[0]):
            # create column
            column = list(row[col_id] for row in board_flags)
            if sum(column) == len(column):
                return True

        return False

    @staticmethod
    def calculate_score(board_numbers: List[List[int]], board_flags: List[List[int]], number: int) -> int:
        total = 0
        for x, row in enumerate(board_flags):
            for y, value in enumerate(row):
                total += board_numbers[x][y] * (1 - value)

        return total * number


def load_data(fn: str) -> Bingo:
    numbers = None
    active = []  # type: List[List[int]]
    boards = []  # type: List[List[List[int]]]
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            if numbers is None:
                numbers = list(int(x) for x in line.split(","))
                continue

            if not line:
                if active:
                    boards.append(active)
                    active = []
                continue

            active.append(list(int(x) for x in line.split()))

    if active:
        boards.append(active)

    return Bingo(numbers, boards)


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(bingo: Bingo) -> int:
    """
    >>> star1(test_data())
    4512
    """

    return bingo.play()[0]


def star2(bingo: Bingo) -> int:
    """
    >>> star2(test_data())
    1924
    """
    return bingo.play()[-1]


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 4 Star 1: %d' % star1(data))
    print('Day 4 Star 2: %d' % star2(data))
