import copy
from dataclasses import dataclass

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


@dataclass
class Brick:
    x1: int
    x2: int
    y1: int
    y2: int
    z1: int
    z2: int
    id: int

    def __post_init__(self) -> None:
        assert self.x1 <= self.x2, f'{self.x1} <= {self.x2}'
        assert self.y1 <= self.y2, f'{self.y1} <= {self.y2}'
        assert self.z1 <= self.z2, f'{self.z1} <= {self.z2}'

    def xy_intersect(self, other: 'Brick') -> bool:
        if other.x1 > self.x2 or other.x2 < self.x1:
            return False

        if other.y1 > self.y2 or other.y2 < self.y1:
            return False

        return True


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "1,0,1~1,2,1",
    ...     "0,0,2~2,0,2",
    ...     "0,2,3~2,2,3",
    ...     "0,0,4~0,2,4",
    ...     "2,0,5~2,2,5",
    ...     "0,1,6~2,1,6",
    ...     "1,1,8~1,1,9",
    ... ])
    5
    """

    supports = _brick_support(data)
    supported_by: dict[int, set[int]] = {brick_id: set() for brick_id in sorted(supports.keys())}
    for brick_id, support in supports.items():
        for support_brick_id in support:
            supported_by[support_brick_id].add(brick_id)

    result = 0
    for brick_id, support in supports.items():

        for support_brick_id in support:
            if len(supported_by[support_brick_id]) == 1:
                break
        else:
            result += 1

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "1,0,1~1,2,1",
    ...     "0,0,2~2,0,2",
    ...     "0,2,3~2,2,3",
    ...     "0,0,4~0,2,4",
    ...     "2,0,5~2,2,5",
    ...     "0,1,6~2,1,6",
    ...     "1,1,8~1,1,9",
    ... ])
    7
    """

    supports = _brick_support(data)
    bricks = sorted(supports.keys())

    supported_by: dict[int, set[int]] = {brick_id: set() for brick_id in sorted(supports.keys())}
    for brick_id, support in supports.items():
        for support_brick_id in support:
            supported_by[support_brick_id].add(brick_id)

    result = 0
    for brick_id in bricks:
        result += _chain_xr(brick_id, supports, supported_by)

    return result


def _brick_support(data: list[str]) -> dict[int, list[int]]:
    bricks: list[Brick] = []
    for row in data:
        parts = list(int(e) for e in row.replace('~', ',').split(','))
        bricks.append(Brick(parts[0], parts[3], parts[1], parts[4], parts[2], parts[5], len(bricks)))

    bricks.sort(key=lambda b: b.z1, reverse=True)

    fixed: list[Brick] = []

    while bricks:
        brick = bricks.pop()

        for fix in fixed:
            if fix.xy_intersect(brick):
                z_drop = fix.z2 + 1
                break
        else:
            z_drop = 1

        height = brick.z2 - brick.z1
        brick.z1 = z_drop
        brick.z2 = z_drop + height

        fixed.append(brick)
        fixed.sort(key=lambda b: b.z2, reverse=True)

    supports: dict[int, list[int]] = {}
    for brick in fixed:
        supports[brick.id] = []
        for other in fixed:
            if brick == other:
                continue

            if not brick.xy_intersect(other):
                continue

            if brick.z2 + 1 == other.z1:
                supports[brick.id].append(other.id)

    return supports


def _chain_xr(brick_id: int, supports: dict[int, list[int]], supported_by: dict[int, set[int]]) -> int:
    supports = copy.deepcopy(supports)
    supported_by = copy.deepcopy(supported_by)

    disintegrate = [brick_id]
    result = 0

    while disintegrate:
        disintegrated_brick = disintegrate.pop()

        for supported_brick_id in supports[disintegrated_brick]:
            assert disintegrated_brick in supported_by[supported_brick_id]

            supported_by[supported_brick_id].remove(disintegrated_brick)
            if len(supported_by[supported_brick_id]) == 0:
                disintegrate.append(supported_brick_id)
                result += 1

    return result


if __name__ == "__main__":
    run_day(22,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
