import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["2-4,6-8",
    ...        "2-3,4-5",
    ...        "5-7,7-9",
    ...        "2-8,3-7",
    ...        "6-6,4-6",
    ...        "2-6,4-8",
    ...        ])
    2
    """

    result = 0
    for row in data:
        a, b, c, d = [int(p) for p in re.split(r'[-,]', row)]

        result += 1 if (a <= c and b >= d) or (a >= c and b <= d) else 0

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["2-4,6-8",
    ...        "2-3,4-5",
    ...        "5-7,7-9",
    ...        "2-8,3-7",
    ...        "6-6,4-6",
    ...        "2-6,4-8",
    ...        ])
    4
    """

    result = 0
    for row in data:
        a, b, c, d = [int(p) for p in re.split(r'[-,]', row)]

        result += 0 if (b < c or d < a) else 1

    return result


if __name__ == "__main__":
    run_day(4,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
