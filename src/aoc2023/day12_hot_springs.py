from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "???.### 1,1,3",
    ...     ".??..??...?##. 1,1,3",
    ...     "?#?#?#?#?#?#?#? 1,3,1,6",
    ...     "????.#...#... 4,1,1",
    ...     "????.######..#####. 1,6,5",
    ...     "?###???????? 3,2,1",
    ... ])
    21
    """

    result = []
    for row in data:
        pattern = row.split(' ')[0]
        numbers = [int(e) for e in row.split(' ')[1].split(',')]

        space = len(pattern) - sum(numbers) - len(numbers) + 1

        rs = _solve_rec(pattern, numbers, space, 0, 0, {})
        result.append(rs)

    return sum(result)


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "???.### 1,1,3",
    ...     ".??..??...?##. 1,1,3",
    ...     "?#?#?#?#?#?#?#? 1,3,1,6",
    ...     "????.#...#... 4,1,1",
    ...     "????.######..#####. 1,6,5",
    ...     "?###???????? 3,2,1",
    ... ])
    525152
    """

    result = []
    for row in data:
        pattern = '?'.join([row.split(' ')[0]] * 5)
        numbers = [int(e) for e in ','.join([row.split(' ')[1]] * 5).split(',')]

        space = len(pattern) - sum(numbers) - len(numbers) + 1

        rs = _solve_rec(pattern, numbers, space, 0, 0, {})
        result.append(rs)

    return sum(result)


def _solve_rec(pattern: str, numbers: list[int], space: int, index: int, start: int, cache: dict[tuple[int, int], int]) -> int:
    if (index, start) in cache:
        return cache[(index, start)]
    if index == len(numbers):
        return 1

    valid_offsets = []
    for offset in range(space + 1):
        # try to place current block
        for i in range(numbers[index]):
            if pattern[start + offset + i] == '.':
                break
        else:
            if start + offset + numbers[index] == len(pattern) or pattern[start + offset + numbers[index]] != '#':
                valid_offsets.append(offset)

        if pattern[start + offset] == '#':
            break

    result = 0
    for valid_offset in valid_offsets:
        new_start = start + valid_offset + numbers[index] + 1

        if sum(1 if e == '#' else 0 for e in pattern[new_start:]) > sum(numbers[index + 1:]):
            continue

        subs = _solve_rec(pattern, numbers, space - valid_offset, index + 1, new_start, cache)
        result += subs

    cache[(index, start)] = result
    return result


if __name__ == "__main__":
    run_day(12,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
