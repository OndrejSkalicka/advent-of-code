import logging
import os
from collections import deque
from functools import total_ordering
from typing import List, Optional, Tuple, Dict, Set

from util.logger_config import init_logger

logger = logging.getLogger(__name__)

VECTORS = {
    '>': (1, 0),
    'v': (0, 1),
}


@total_ordering
class Point:

    def __init__(self, xyz: Tuple[int, int, int]) -> None:
        self.xyz = xyz

    def __eq__(self, o: 'Point') -> bool:
        return self.xyz.__eq__(o.xyz)

    def __lt__(self, other: 'Point'):
        return self.xyz < other.xyz

    def __hash__(self) -> int:
        return self.xyz.__hash__()

    def __repr__(self) -> str:
        return '[%d;%d;%d]' % self.xyz

    def abs_vector(self, other: 'Point') -> 'Point':
        # noinspection PyTypeChecker
        return Point(tuple(sorted((abs(self.xyz[0] - other.xyz[0]),
                                   abs(self.xyz[1] - other.xyz[1]),
                                   abs(self.xyz[2] - other.xyz[2])))))

    def manhattan(self) -> int:
        return abs(self.xyz[0]) + abs(self.xyz[1]) + abs(self.xyz[2])

    def vector(self, other: 'Point') -> 'Point':
        return Point((self.xyz[0] - other.xyz[0],
                      self.xyz[1] - other.xyz[1],
                      self.xyz[2] - other.xyz[2]))

    def transform(self, t: Tuple[Tuple[int, int, int], Tuple[int, int, int], Tuple[int, int, int]]) -> 'Point':
        """
        >>> Point((1, 2, 3)).transform(TRANSFORMATIONS[0])
        [1;2;3]

        >>> Point((1, 2, 3)).transform(TRANSFORMATIONS[7])
        [-1;-2;-3]
        """
        return Point(
            (
                self.xyz[0] * t[0][0] + self.xyz[1] * t[1][0] + self.xyz[2] * t[2][0],
                self.xyz[0] * t[0][1] + self.xyz[1] * t[1][1] + self.xyz[2] * t[2][1],
                self.xyz[0] * t[0][2] + self.xyz[1] * t[1][2] + self.xyz[2] * t[2][2],
            )
        )

    def plus(self, vector: 'Point') -> 'Point':
        return Point((
            self.xyz[0] + vector.xyz[0],
            self.xyz[1] + vector.xyz[1],
            self.xyz[2] + vector.xyz[2],
        ))


class Scanner:

    def __init__(self, id: int) -> None:
        self.id = id
        self.beacons = []  # type: List[Point]

        self.position = None  # type: Optional[Point]

    def __repr__(self) -> str:
        return 'Scanner %d' % self.id


class Universe:

    def __init__(self) -> None:
        self.beacons = set()  # type: Set[Point]
        self.beacon_vectors = dict()  # type: Dict[Point, List[Tuple[Point, Point]]]
        self.scanners = []  # type: List[Scanner]

    def add_beacon(self, beacon: Point):
        if beacon in self.beacons:
            logger.debug("Beacon already in universe")
            return

        for other_beacon in self.beacons:
            vector = other_beacon.abs_vector(beacon)
            tmp = self.beacon_vectors.get(vector, [])
            tmp.append((beacon, other_beacon))
            self.beacon_vectors[vector] = tmp

        self.beacons.add(beacon)

    def __repr__(self) -> str:
        return "Universe with %d beacons; %d vectors" % (len(self.beacons), len(self.beacon_vectors))


def load_data(fn: str = 'data.txt') -> List[Scanner]:
    result = []
    active = None

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            if not line:
                continue

            if line.startswith('--- scanner '):
                if active:
                    result.append(active)

                id = line[len('--- scanner '):].rstrip(' -')
                active = Scanner(int(id))
                continue

            x, y, z = line.split(',')

            active.beacons.append(Point((int(x), int(y), int(z))))

    result.append(active)
    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


TRANSFORMATIONS = (
    (
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, 1),
    ),
    (
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, -1),
    ),
    (
        (1, 0, 0),
        (0, -1, 0),
        (0, 0, 1),
    ),
    (
        (1, 0, 0),
        (0, -1, 0),
        (0, 0, -1),
    ),
    (
        (-1, 0, 0),
        (0, 1, 0),
        (0, 0, 1),
    ),
    (
        (-1, 0, 0),
        (0, 1, 0),
        (0, 0, -1),
    ),
    (
        (-1, 0, 0),
        (0, -1, 0),
        (0, 0, 1),
    ),
    (
        (-1, 0, 0),
        (0, -1, 0),
        (0, 0, -1),
    ),
    (
        (1, 0, 0),
        (0, 0, 1),
        (0, 1, 0),
    ),
    (
        (1, 0, 0),
        (0, 0, -1),
        (0, 1, 0),
    ),
    (
        (1, 0, 0),
        (0, 0, 1),
        (0, -1, 0),
    ),
    (
        (1, 0, 0),
        (0, 0, -1),
        (0, -1, 0),
    ),
    (
        (-1, 0, 0),
        (0, 0, 1),
        (0, 1, 0),
    ),
    (
        (-1, 0, 0),
        (0, 0, -1),
        (0, 1, 0),
    ),
    (
        (-1, 0, 0),
        (0, 0, 1),
        (0, -1, 0),
    ),
    (
        (-1, 0, 0),
        (0, 0, -1),
        (0, -1, 0),
    ),
    (
        (0, 1, 0),
        (1, 0, 0),
        (0, 0, 1),
    ),
    (
        (0, 1, 0),
        (1, 0, 0),
        (0, 0, -1),
    ),
    (
        (0, -1, 0),
        (1, 0, 0),
        (0, 0, 1),
    ),
    (
        (0, -1, 0),
        (1, 0, 0),
        (0, 0, -1),
    ),
    (
        (0, 1, 0),
        (-1, 0, 0),
        (0, 0, 1),
    ),
    (
        (0, 1, 0),
        (-1, 0, 0),
        (0, 0, -1),
    ),
    (
        (0, -1, 0),
        (-1, 0, 0),
        (0, 0, 1),
    ),
    (
        (0, -1, 0),
        (-1, 0, 0),
        (0, 0, -1),
    ),
    (
        (0, 0, 1),
        (1, 0, 0),
        (0, 1, 0),
    ),
    (
        (0, 0, -1),
        (1, 0, 0),
        (0, 1, 0),
    ),
    (
        (0, 0, 1),
        (1, 0, 0),
        (0, -1, 0),
    ),
    (
        (0, 0, -1),
        (1, 0, 0),
        (0, -1, 0),
    ),
    (
        (0, 0, 1),
        (-1, 0, 0),
        (0, 1, 0),
    ),
    (
        (0, 0, -1),
        (-1, 0, 0),
        (0, 1, 0),
    ),
    (
        (0, 0, 1),
        (-1, 0, 0),
        (0, -1, 0),
    ),
    (
        (0, 0, -1),
        (-1, 0, 0),
        (0, -1, 0),
    ),
    (
        (0, 1, 0),
        (0, 0, 1),
        (1, 0, 0),
    ),
    (
        (0, 1, 0),
        (0, 0, -1),
        (1, 0, 0),
    ),
    (
        (0, -1, 0),
        (0, 0, 1),
        (1, 0, 0),
    ),
    (
        (0, -1, 0),
        (0, 0, -1),
        (1, 0, 0),
    ),
    (
        (0, 1, 0),
        (0, 0, 1),
        (-1, 0, 0),
    ),
    (
        (0, 1, 0),
        (0, 0, -1),
        (-1, 0, 0),
    ),
    (
        (0, -1, 0),
        (0, 0, 1),
        (-1, 0, 0),
    ),
    (
        (0, -1, 0),
        (0, 0, -1),
        (-1, 0, 0),
    ),
    (
        (0, 0, 1),
        (0, 1, 0),
        (1, 0, 0),
    ),
    (
        (0, 0, -1),
        (0, 1, 0),
        (1, 0, 0),
    ),
    (
        (0, 0, 1),
        (0, -1, 0),
        (1, 0, 0),
    ),
    (
        (0, 0, -1),
        (0, -1, 0),
        (1, 0, 0),
    ),
    (
        (0, 0, 1),
        (0, 1, 0),
        (-1, 0, 0),
    ),
    (
        (0, 0, -1),
        (0, 1, 0),
        (-1, 0, 0),
    ),
    (
        (0, 0, 1),
        (0, -1, 0),
        (-1, 0, 0),
    ),
    (
        (0, 0, -1),
        (0, -1, 0),
        (-1, 0, 0),
    ),
)


def create_universe(scanners: List[Scanner]) -> Universe:
    scanners_queue = deque(scanners)

    universe = Universe()

    # add initial scanner as ground-0

    scanner = scanners_queue.popleft()
    for beacon in scanner.beacons:
        universe.add_beacon(beacon)

    scanner.position = Point((0, 0, 0))
    universe.scanners.append(scanner)

    logger.info("Beacons: %d" % len(universe.beacons))
    logger.info("Vectors: %d" % len(universe.beacon_vectors))
    logger.info("Initial universe %s" % universe)

    # pick next (at "random" :D )

    short_circuit = 0
    while scanners_queue:
        short_circuit += 1
        if short_circuit > len(scanners) ** 2:
            break
        scanner = scanners_queue.popleft()

        added = try_to_add_scanner(universe, scanner)
        if not added:
            scanners_queue.append(scanner)

    return universe


def star1(scanners: List[Scanner]) -> int:
    """
    >>> star1(test_data())
    79
    """
    universe = create_universe(scanners)

    logger.info("Universe final: %s" % universe)
    logger.info("Scanners final: %s" % universe.scanners)
    logger.info("RESULT: %d" % len(universe.beacons))
    return len(universe.beacons)


def star2(scanners: List[Scanner]) -> int:
    """
    >>> star2(test_data())
    3621
    """

    universe = create_universe(scanners)

    logger.info("Universe final: %s" % universe)
    logger.info("Scanners final: %s" % universe.scanners)
    logger.info("RESULT: %d" % len(universe.beacons))

    max_manhattan = 0
    for scanner_a in universe.scanners:
        for scanner_b in universe.scanners:
            max_manhattan = max(max_manhattan, scanner_a.position.vector(scanner_b.position).manhattan())

    return max_manhattan


def try_to_add_scanner(universe: Universe, scanner: Scanner) -> bool:
    for match in find_matches(universe, scanner):
        scanner_a, scanner_b, universe_a, universe_b = match

        logger.debug('Found match, scanner %s && %s universe %s && %s' % match)

        scanner_vector = scanner_a.vector(scanner_b)
        universe_vector = universe_a.vector(universe_b)

        logger.debug('Scanner pair vector %s' % [scanner_vector])
        logger.debug('Universe pair vector %s' % [universe_vector])

        for transformation in TRANSFORMATIONS:
            transformed_scanner_vector = scanner_vector.transform(transformation)

            if transformed_scanner_vector != universe_vector:
                logger.debug("Transformation %s did not cut it." % [transformation])
                continue

            logger.info('Found correct transformation: %s' % [transformation])

            # rotate
            scanner_a_t = scanner_a.transform(transformation)
            scanner_b_t = scanner_b.transform(transformation)

            logger.info("scanner_a_t %s" % scanner_a_t)
            logger.info("scanner_b_t %s" % scanner_b_t)

            logger.info("CRC vector scanner_a_t && scanner_b_t %s" % [scanner_a_t.vector(scanner_b_t)])

            # shift vector
            shift_vector = universe_a.vector(scanner_a_t)

            # shift via vector
            matches = 0
            for beacon in scanner.beacons:
                shifted = beacon.transform(transformation).plus(shift_vector)
                if shifted in universe.beacons:
                    matches += 1

            shifted_a = scanner_a_t.plus(shift_vector)
            shifted_b = scanner_b_t.plus(shift_vector)

            logger.info("Shift vector %s" % [shift_vector])
            logger.info("shifted_a %s" % shifted_a)
            logger.info("shifted_b %s" % shifted_b)
            logger.info("Beacon matches %d" % matches)

            if matches < 12:
                continue

            logger.info("YAAAY <3 <3 <3 Adding scanner %s" % scanner)

            for beacon in scanner.beacons:
                universe.add_beacon(beacon.transform(transformation).plus(shift_vector))

            scanner.position = shift_vector
            universe.scanners.append(scanner)

            logger.info("Updated universe %s" % universe)

            return True

        logger.info("Unable to add scanner %s due to incomplete match (fails after transformations)" % scanner)

    logger.info("No more matches.")
    return False


def find_matches(universe: Universe, scanner: Scanner) -> Tuple[Point, Point, Point, Point]:
    # look for a vector
    for id_a in range(len(scanner.beacons)):
        for id_b in range(id_a + 1, len(scanner.beacons)):
            scanner_a = scanner.beacons[id_a]
            scanner_b = scanner.beacons[id_b]

            vector = scanner_a.abs_vector(scanner_b)

            if vector not in universe.beacon_vectors:
                logger.debug("No match for vector %s from scanner %s" % (vector, scanner))
                continue

            matches = universe.beacon_vectors[vector]
            logger.debug("Found %d vector matches with scanner %s, vector %s" % (len(matches), scanner, vector))

            for match in matches:
                yield scanner_a, scanner_b, match[0], match[1]

    logger.debug("No more matches")


if __name__ == "__main__":
    init_logger()

    print('Day 19 Star 1: %d' % star1(load_data()))
    print('Day 19 Star 2: %d' % star2(load_data()))
