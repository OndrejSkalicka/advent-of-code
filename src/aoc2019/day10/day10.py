import math
from collections import defaultdict
from typing import Generator

import numpy as np

from util.data import to_2d_np_char, load_as_str

ASTEROID = '#'

xy = tuple[int, int]


def star1(star_map_str: str) -> int:
    """
    >>> star1(".#..#\\n.....\\n#####\\n....#\\n...##")
    8

    >>> star1("......#.#.\\n#..#.#....\\n..#######.\\n.#.#.###..\\n.#..#.....\\n..#....#.#\\n#..#....#.\\n.##.#..###\\n##...#..#.\\n.#....####")
    33

    >>> star1("#.#...#.#.\\n.###....#.\\n.#....#...\\n##.#.#.#.#\\n....#.#.#.\\n.##..###.#\\n..#...##..\\n..##....##\\n......#...\\n.####.###.")
    35

    >>> star1(".#..#..###\\n####.###.#\\n....###.#.\\n..###.##.#\\n##.##.#.#.\\n....###..#\\n..#.#..#.#\\n#..#.#.###\\n.##...##.#\\n.....#.#..")
    41

    >>> star1(".#..##.###...#######\\n##.############..##.\\n.#.######.########.#\\n.###.#######.####.#.\\n#####.##.#.##.###.##\\n"
    ...       "..#####..#.#########\\n####################\\n#.####....###.#.#.##\\n##.#################\\n#####.##.###..####..\\n"
    ...       "..######..##.#######\\n####.##.####...##..#\\n.#####..#.######.###\\n##...#.##########...\\n#.##########.#######\\n"
    ...       ".####.#.###.###.#.##\\n....##.##.###..#####\\n.#.#.###########.###\\n#.#.#.#####.####.###\\n###.##.####.##.#..##")
    210
    """

    star_map = to_2d_np_char(star_map_str)
    visibilities = np.zeros(star_map.shape, dtype=int)
    asteroids = list(_asteroids(star_map))

    angle_map = _asteroid_angle_graph(asteroids)
    for k, v in angle_map.items():
        visibilities[k] = len(v)

    return np.amax(visibilities)  # type: ignore


def star2(star_map_str: str) -> int:
    """
    >>> star2(".#..##.###...#######\\n##.############..##.\\n.#.######.########.#\\n.###.#######.####.#.\\n#####.##.#.##.###.##\\n"
    ...       "..#####..#.#########\\n####################\\n#.####....###.#.#.##\\n##.#################\\n#####.##.###..####..\\n"
    ...       "..######..##.#######\\n####.##.####...##..#\\n.#####..#.######.###\\n##...#.##########...\\n#.##########.#######\\n"
    ...       ".####.#.###.###.#.##\\n....##.##.###..#####\\n.#.#.###########.###\\n#.#.#.#####.####.###\\n###.##.####.##.#..##")
    802
    """

    star_map = to_2d_np_char(star_map_str)
    visibilities = np.zeros(star_map.shape, dtype=int)
    asteroids = list(_asteroids(star_map))

    angle_map = _asteroid_angle_graph(asteroids)
    for k, v in angle_map.items():
        visibilities[k] = len(v)

    base: xy = np.unravel_index(visibilities.argmax(), visibilities.shape)  # type: ignore

    targets = angle_map[base]

    killed = 0

    for angle in sorted(targets.keys(), key=lambda x: (x < -math.pi / 2, x)):
        beam_targets = targets[angle]
        beam_targets.sort(key=lambda x: (x[0] - base[0]) ** 2 + (x[1] - base[1]) ** 2)

        beam_target = beam_targets[0]
        beam_targets.remove(beam_target)
        if not beam_targets:
            del targets[angle]

        killed += 1

        if killed == 200:
            return beam_target[0] + 100 * beam_target[1]

    raise Exception("Not found")


def _asteroid_angle_graph(asteroids: list[xy]) -> dict[xy, dict[float, list[xy]]]:
    result: dict[xy, dict[float, list[xy]]] = defaultdict(dict)

    for a in asteroids:
        for b in asteroids:
            if a == b:
                continue

            ab_angle = math.atan2(b[0] - a[0], b[1] - a[1])

            if ab_angle not in result[a]:
                result[a][ab_angle] = []

            result[a][ab_angle].append(b)

    return result


def _asteroids(star_map: np.ndarray) -> Generator[xy, None, None]:  # type: ignore
    # noinspection PyTypeChecker
    for target_y, target_x in np.ndindex(star_map.shape):
        # not an asteroid
        if not star_map[target_y, target_x] == ASTEROID:
            continue

        yield target_y, target_x


if __name__ == "__main__":
    print('Day 10 Star 1: %d' % star1(load_as_str('data.txt')))
    print('Day 10 Star 2: %d' % star2(load_as_str('data.txt')))
