from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> str:
    """
    >>> star1(["    [D]    ",
    ...        "[N] [C]    ",
    ...        "[Z] [M] [P]",
    ...        " 1   2   3 ",
    ...        "",
    ...        "move 1 from 2 to 1",
    ...        "move 3 from 1 to 3",
    ...        "move 2 from 2 to 1",
    ...        "move 1 from 1 to 2",
    ...        ])
    'CMZ'
    """

    return _crane(data, False)


def star2(data: list[str]) -> str:
    """
    >>> star2(["    [D]    ",
    ...        "[N] [C]    ",
    ...        "[Z] [M] [P]",
    ...        " 1   2   3 ",
    ...        "",
    ...        "move 1 from 2 to 1",
    ...        "move 3 from 1 to 3",
    ...        "move 2 from 2 to 1",
    ...        "move 1 from 1 to 2",
    ...        ])
    'MCD'
    """

    return _crane(data, True)


def _crane(data: list[str], multi_pickup: bool) -> str:
    columns: dict[int, list[str]] = defaultdict(list)

    for row in reversed(data[:data.index("") - 1]):
        for i in range(len(row) // 4 + 1):
            c = row[4 * i + 1]
            if c != ' ':
                columns[i + 1].append(c)

    for row in data[data.index("") + 1:]:
        parts = row.split(' ')

        amount = int(parts[1])
        source = int(parts[3])
        destination = int(parts[5])

        columns[destination] += columns[source][-amount:] if multi_pickup else reversed(columns[source][-amount:])
        columns[source] = columns[source][:-amount]

    return ''.join(columns[key][-1] for key in sorted(columns.keys()))


if __name__ == "__main__":
    run_day(5,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
