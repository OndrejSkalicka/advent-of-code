from itertools import permutations

from aoc2019.intcode import Program
from util.data import load_numbers_csv


def star1(memory: list[int]) -> int:
    """
    >>> star1([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0])
    43210

    >>> star1([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0])
    54321

    >>> star1([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0])
    65210
    """

    best = None
    for phase_setting_sequence in list(permutations(range(5))):
        # phase_setting_sequence = [4, 3, 2, 1, 0]
        input_signal = 0
        for phase_setting in phase_setting_sequence:
            program = Program(memory, input=[phase_setting, input_signal])
            program.run()
            input_signal = program.output[0]

        if best is None or best < input_signal:
            best = input_signal

    assert best is not None
    return best


def star2(memory: list[int]) -> int:
    """
    >>> star2([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5])
    139629729

    >>> star2([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
    ...        -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
    ...        53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10])
    18216
    """

    best = None
    for phase_setting_sequence in list(permutations(range(5, 10))):
        input_signal = 0

        programs = [
            Program(memory, input=[phase_setting_sequence[0]]),
            Program(memory, input=[phase_setting_sequence[1]]),
            Program(memory, input=[phase_setting_sequence[2]]),
            Program(memory, input=[phase_setting_sequence[3]]),
            Program(memory, input=[phase_setting_sequence[4]]),
        ]

        active_program = 0
        while True:
            program = programs[active_program % 5]
            program.input.append(input_signal)
            program.run_or_resume()
            input_signal = program.output.pop()

            if not program.waiting_for_input and active_program % 5 == 4:
                break

            active_program += 1

        if best is None or input_signal > best:
            best = input_signal

    assert best is not None
    return best


if __name__ == "__main__":
    print('Day 7 Star 1: %d' % star1(load_numbers_csv('data.txt')))
    print('Day 7 Star 2: %d' % star2(load_numbers_csv('data.txt')))
