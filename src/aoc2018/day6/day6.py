from typing import Sequence, Tuple, Optional


def find_largest_area(coordinates: Sequence[Tuple[int, int]]) -> int:
    """
    >>> find_largest_area(((1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)))
    17
    """
    manhattan_map = create_closest_map(coordinates)

    area_sizes = {}
    infinite = set()
    x_min, x_max, y_min, y_max = x_y_min_max(coordinates)
    for c, value in manhattan_map.items():
        if value is None:
            continue

        if value not in area_sizes:
            area_sizes[value] = 0

        area_sizes[value] += 1

        if c[0] == x_min or c[0] == x_max or c[1] == y_min or c[1] == y_max:
            infinite.add(value)

    largest = None
    for value, cnt in area_sizes.items():
        if value in infinite:
            continue

        if largest is None or largest < cnt:
            largest = cnt

    return largest


def create_closest_map(coordinates: Sequence[Tuple[int, int]]):
    # find grid size
    x_min, x_max, y_min, y_max = x_y_min_max(coordinates)

    manhattan_map = {}
    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            manhattan_map[(x, y)] = closest(coordinates, (x, y))

    return manhattan_map


def find_largest_area_2(coordinates: Sequence[Tuple[int, int]], threshold: int) -> int:
    """
    >>> find_largest_area_2(((1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)), 32)
    16
    """

    sum_map = create_sum_map(coordinates)

    cnt = 0
    for val in sum_map.values():
        if val < threshold:
            cnt += 1

    return cnt


def create_sum_map(coordinates: Sequence[Tuple[int, int]]):
    # find grid size
    x_min, x_max, y_min, y_max = x_y_min_max(coordinates)

    manhattan_map = {}
    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            manhattan_map[(x, y)] = sum_distance(coordinates, (x, y))

    return manhattan_map


def x_y_min_max(coordinates: Sequence[Tuple[int, int]]):
    x_min = coordinates[0][0]
    x_max = coordinates[0][0]
    y_min = coordinates[0][1]
    y_max = coordinates[0][1]

    for c in coordinates:
        x_min = min(x_min, c[0])
        x_max = max(x_max, c[0])
        y_min = min(y_min, c[1])
        y_max = max(y_max, c[1])

    return x_min, x_max, y_min, y_max


def closest(coordinates: Sequence[Tuple[int, int]], point: Tuple[int, int]) -> Optional[int]:
    """
    Finds closest coordinate to given point within given boundaries.

    :return: index of closest coordinate or None if none found.
    """

    closest_distance = None
    closest_index = None
    duplicity = False
    for idx, c in enumerate(coordinates):
        distance = dist(c, point)

        if closest_distance is None or distance < closest_distance:
            closest_distance = distance
            closest_index = idx
            duplicity = False

        # tie
        elif closest_distance == distance:
            duplicity = True

    if duplicity:
        return None
    return closest_index


def sum_distance(coordinates: Sequence[Tuple[int, int]], point: Tuple[int, int]) -> Optional[int]:
    """
    Finds total distance from point to all coords

    :return: index of closest coordinate or None if none found.
    """

    total = 0
    for idx, c in enumerate(coordinates):
        total += dist(c, point)

    return total


def dist(first: Tuple[int, int], second: Tuple[int, int]) -> int:
    """
    >>> dist((1, 1), (8, 3))
    9
    """
    return abs(first[0] - second[0]) + abs(first[1] - second[1])


def load_data(fn) -> Sequence[Tuple[int, int]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            x, y = line.split(", ")
            result.append((int(x), int(y)))

    return result


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 6 Star 1: %d' % find_largest_area(data))
    print('Day 6 Star 2: %d' % find_largest_area_2(data, 10000))
