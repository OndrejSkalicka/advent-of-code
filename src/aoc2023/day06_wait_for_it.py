import math
import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "Time:      7  15   30",
    ...     "Distance:  9  40  200",
    ... ])
    288
    """

    times = [int(d) for d in re.findall(r'\d+', data[0])]
    distances = [int(d) for d in re.findall(r'\d+', data[1])]
    results = []

    for i in range(len(times)):
        time = times[i]
        distance = distances[i]
        wins = 0

        for hold_time in range(time):
            if hold_time * (time - hold_time) > distance:
                wins += 1

        results.append(wins)

    return math.prod(results)


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "Time:      7  15   30",
    ...     "Distance:  9  40  200",
    ... ])
    71503
    """

    time = int(re.search(r'\d+', data[0].replace(' ', '')).group(0))  # type: ignore
    distance = int(re.search(r'\d+', data[1].replace(' ', '')).group(0))  # type: ignore

    wins = 0

    for hold_time in range(time):
        if hold_time * (time - hold_time) > distance:
            wins += 1

    return wins


if __name__ == "__main__":
    run_day(6,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
