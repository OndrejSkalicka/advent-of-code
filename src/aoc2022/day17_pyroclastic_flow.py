from util.data import load_as_str, txt_fn
from util.perf import run_day
from util.record import Xy

shapes: list[list[str]] = [
    ['####'],
    [' # ', '###', ' # '],
    ['###', '  #', '  #'],
    ['#', '#', '#', '#'],
    ['##', '##']
]


def star1(data: str) -> int:
    """
    >>> star1(">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>")
    3068
    """

    jet_index = 0
    tetris: set[Xy] = set()
    top = 0

    for shape_index in range(2022):
        rock = Xy(2, top + 3)
        shape = shapes[shape_index % len(shapes)]

        while True:
            pushed_rock = Xy(
                rock.x + (1 if data[jet_index % len(data)] == '>' else -1),
                rock.y
            )
            jet_index += 1

            if _free(pushed_rock, tetris, shape):
                rock = pushed_rock

            falling_rock = Xy(
                rock.x,
                rock.y - 1
            )
            if _free(falling_rock, tetris, shape):
                rock = falling_rock
            else:
                for dy in range(len(shape)):
                    for dx in range(len(shape[0])):
                        if shape[dy][dx] == '#':
                            tetris.add(rock + Xy(dx, dy))

                top = max(top, rock.y + len(shape))
                break

    return top


def _free(rock: Xy, tetris: set[Xy], shape: list[str]) -> bool:
    if rock.x < 0:
        return False
    if rock.x + len(shape[0]) > 7:
        return False

    if rock.y < 0:
        return False

    for dy in range(len(shape)):
        for dx in range(len(shape[0])):
            if shape[dy][dx] == '#' and rock + Xy(dx, dy) in tetris:
                return False

    return True


def star2(data: str) -> int:
    """
    >>> star2(">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>")
    1514285714288
    """

    jet_index = 0
    tetris: set[Xy] = set()
    shape_index = 0
    top = 0
    pattern_data = []

    # do 10k steps
    for _ in range(10000):
        rock = Xy(2, top + 3)
        shape = shapes[shape_index % len(shapes)]
        shape_index += 1

        while True:
            pushed_rock = Xy(
                rock.x + (1 if data[jet_index % len(data)] == '>' else -1),
                rock.y
            )
            jet_index += 1

            if _free(pushed_rock, tetris, shape):
                rock = pushed_rock

            falling_rock = Xy(
                rock.x,
                rock.y - 1
            )
            if _free(falling_rock, tetris, shape):
                rock = falling_rock
            else:
                for dy in range(len(shape)):
                    for dx in range(len(shape[0])):
                        if shape[dy][dx] == '#':
                            tetris.add(rock + Xy(dx, dy))

                new_top = max(top, rock.y + len(shape))
                pattern_data.append((new_top - top))
                top = new_top
                break

    # stringize + reverse for easier lookup
    pattern_line = ''.join('%d' % i for i in reversed(pattern_data))

    pattern_length = pattern_line[1:].index(pattern_line[:5000]) + 1
    pattern = pattern_line[:pattern_length]
    pattern_value = sum(int(c) for c in pattern)
    assert pattern_length > 0

    tail_length = len(pattern_line) % pattern_length
    tail_value = sum(pattern_data[:tail_length])

    steps_remaining = 1_000_000_000_000 - tail_length
    pattern_iterations = steps_remaining // pattern_length
    patterned_value = pattern_iterations * pattern_value
    steps_remaining -= pattern_iterations * pattern_length

    head = pattern[-steps_remaining:]
    head_value = sum(int(c) for c in head)

    return head_value + tail_value + patterned_value


if __name__ == "__main__":
    run_day(17,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
