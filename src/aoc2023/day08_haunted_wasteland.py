import math
import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "RL",
    ...     "",
    ...     "AAA = (BBB, CCC)",
    ...     "BBB = (DDD, EEE)",
    ...     "CCC = (ZZZ, GGG)",
    ...     "DDD = (DDD, DDD)",
    ...     "EEE = (EEE, EEE)",
    ...     "GGG = (GGG, GGG)",
    ...     "ZZZ = (ZZZ, ZZZ)",
    ... ])
    2

    >>> star1([
    ...     "LLR",
    ...     "",
    ...     "AAA = (BBB, BBB)",
    ...     "BBB = (AAA, ZZZ)",
    ...     "ZZZ = (ZZZ, ZZZ)",
    ... ])
    6
    """

    pattern = data[0]
    map: dict[str, dict[str, str]] = {}

    for row in data[2:]:
        m = re.match(r'(\w+) = \((\w+), (\w+)\)$', row)
        assert m is not None

        map[m.group(1)] = {
            'L': m.group(2),
            'R': m.group(3),
        }

    steps = 0
    active = 'AAA'

    while active != 'ZZZ':
        active = map[active][pattern[steps % len(pattern)]]
        steps += 1

    return steps


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "LR",
    ...     "",
    ...     "11A = (11B, XXX)",
    ...     "11B = (XXX, 11Z)",
    ...     "11Z = (11B, XXX)",
    ...     "22A = (22B, XXX)",
    ...     "22B = (22C, 22C)",
    ...     "22C = (22Z, 22Z)",
    ...     "22Z = (22B, 22B)",
    ...     "XXX = (XXX, XXX)",
    ... ])
    6
    """

    pattern = data[0]
    map: dict[str, dict[str, str]] = {}

    for row in data[2:]:
        m = re.match(r'(\w+) = \((\w+), (\w+)\)$', row)
        assert m is not None

        map[m.group(1)] = {
            'L': m.group(2),
            'R': m.group(3),
        }

    ghosts = [c for c in map.keys() if c.endswith('A')]
    ghost_cycles = []

    for ghost in ghosts:
        steps = 0
        active = ghost

        while not active.endswith('Z'):
            active = map[active][pattern[steps % len(pattern)]]
            steps += 1

        ghost_cycles.append(steps)

    return math.lcm(*ghost_cycles)


if __name__ == "__main__":
    run_day(8,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
