package aoc2020.day06;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link Day23}.
 */
class Day23Test {
    private Day23 fixture;

    @BeforeEach
    void setUp() {
        fixture = new Day23();
    }

    @Test
    void star1ten() {

        final String actual = fixture.star1("389125467", 10);

        Assertions.assertEquals(actual, "92658374");
    }

    @Test
    void star1hundred() {

        final String actual = fixture.star1("389125467", 100);

        Assertions.assertEquals(actual, "67384529");
    }

    @Test
    void star2() {

        final long actual = fixture.star2("389125467");

        Assertions.assertEquals(actual, 149245887792L);
    }
}