from util.data import txt_fn, load_strings_newline
from util.perf import run_day

D_XYZ = [
    (1, 0, 0),
    (-1, 0, 0),
    (0, 1, 0),
    (0, -1, 0),
    (0, 0, 1),
    (0, 0, -1),
]


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "1,1,1",
    ...        "2,1,1",
    ...        ])
    10

    >>> star1([
    ...        "2,2,2",
    ...        "1,2,2",
    ...        "3,2,2",
    ...        "2,1,2",
    ...        "2,3,2",
    ...        "2,2,1",
    ...        "2,2,3",
    ...        "2,2,4",
    ...        "2,2,6",
    ...        "1,2,5",
    ...        "3,2,5",
    ...        "2,1,5",
    ...        "2,3,5",
    ...        ])
    64
    """

    points: set[tuple[int, int, int]] = set()

    for row in data:
        parts = row.split(',')
        points.add((
            int(parts[0]),
            int(parts[1]),
            int(parts[2]),
        ))

    area = 0
    for point in points:
        for dx, dy, dz in D_XYZ:
            if (point[0] + dx, point[1] + dy, point[2] + dz) not in points:
                area += 1

    return area


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "2,2,2",
    ...        "1,2,2",
    ...        "3,2,2",
    ...        "2,1,2",
    ...        "2,3,2",
    ...        "2,2,1",
    ...        "2,2,3",
    ...        "2,2,4",
    ...        "2,2,6",
    ...        "1,2,5",
    ...        "3,2,5",
    ...        "2,1,5",
    ...        "2,3,5",
    ...        ])
    58
    """

    points: set[tuple[int, int, int]] = set()

    for row in data:
        parts = row.split(',')
        points.add((
            int(parts[0]),
            int(parts[1]),
            int(parts[2]),
        ))

    borders = (
        (min(p[0] for p in points) - 1, max(p[0] for p in points) + 1),
        (min(p[1] for p in points) - 1, max(p[1] for p in points) + 1),
        (min(p[2] for p in points) - 1, max(p[2] for p in points) + 1),
    )

    buffer: list[tuple[int, int, int]] = [(borders[0][0], borders[1][0], borders[2][0])]
    steam: set[tuple[int, int, int]] = set()

    while buffer:
        point = buffer.pop()
        if point in steam:
            continue

        steam.add(point)

        for dx, dy, dz in D_XYZ:
            adjacent_point = (point[0] + dx, point[1] + dy, point[2] + dz)
            if adjacent_point in points:
                continue

            if adjacent_point in steam:
                continue

            if adjacent_point[0] < borders[0][0] or adjacent_point[0] > borders[0][1]:
                continue

            if adjacent_point[1] < borders[1][0] or adjacent_point[1] > borders[1][1]:
                continue

            if adjacent_point[2] < borders[2][0] or adjacent_point[2] > borders[2][1]:
                continue

            buffer.append(adjacent_point)

    area = 0
    for point in points:
        for dx, dy, dz in D_XYZ:
            if (point[0] + dx, point[1] + dy, point[2] + dz) in steam:
                area += 1

    return area


if __name__ == "__main__":
    run_day(18,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
