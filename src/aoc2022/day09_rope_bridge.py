from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Xy

DIRECTION_TO_DX = {
    'R': Xy(1, 0),
    'L': Xy(-1, 0),
    'U': Xy(0, -1),
    'D': Xy(0, 1),
}


def star1(data: list[str]) -> int:
    """
    >>> star1(["R 4",
    ...        "U 4",
    ...        "L 3",
    ...        "D 1",
    ...        "R 4",
    ...        "D 1",
    ...        "L 5",
    ...        "R 2",
    ...        ])
    13
    """

    return _simulate(data)


def star2(data: list[str]) -> int:
    """
    >>> star2(["R 5",
    ...        "U 8",
    ...        "L 8",
    ...        "D 3",
    ...        "R 17",
    ...        "D 10",
    ...        "L 25",
    ...        "U 20",
    ...        ])
    36
    """

    return _simulate(data, length=10)


def _simulate(data: list[str], length: int = 2) -> int:
    rope = [Xy(0, 0)] * length

    visited: set[Xy] = set()

    for direction, distance in (row.split(' ') for row in data):
        dx = DIRECTION_TO_DX[direction]
        for _ in range(int(distance)):
            rope[0] += dx

            for i in range(length - 1):
                rope[i + 1] = _move_tail(rope[i], rope[i + 1])
            visited.add(rope[-1])

    return len(visited)


def _move_tail(head: Xy, tail: Xy) -> Xy:
    """
    >>> _move_tail(Xy(2, 1), Xy(1, 3))
    Xy(x=2, y=2)

    >>> _move_tail(Xy(3, 2), Xy(1, 3))
    Xy(x=2, y=2)
    """
    dx = head - tail
    assert abs(dx.x) + abs(dx.y) <= 4

    if abs(dx.x) <= 1 and abs(dx.y) <= 1:
        return tail

    if abs(dx.x) == 2:
        dx = Xy(dx.x // 2, dx.y)

    if abs(dx.y) == 2:
        dx = Xy(dx.x, dx.y // 2)
    return tail + dx


if __name__ == "__main__":
    run_day(9,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
