from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy, Cell


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     ".....",
    ...     ".S-7.",
    ...     ".|.|.",
    ...     ".L-J.",
    ...     ".....",
    ... ])
    4

    >>> star1([
    ...     "7-F7-",
    ...     ".FJ|7",
    ...     "SJLL7",
    ...     "|F--J",
    ...     "LJ.LJ",
    ... ])
    8
    """

    grid = Grid.from_list_str(data)
    start = grid.find_all_xy(lambda e: e == 'S')[0].xy

    for candidate in _pipe_adj(start, grid):
        if start in _pipe_adj(candidate, grid):
            active = candidate
            break
    else:
        assert False

    prev = start
    steps = 0
    while True:
        adj = _pipe_adj(active, grid)
        if active == start:
            break

        assert len(adj) == 2
        assert prev in adj

        if adj[0] == prev:
            prev = active
            active = adj[1]
        else:
            prev = active
            active = adj[0]

        steps += 1

    return (steps + 1) // 2


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "...........",
    ...     ".S-------7.",
    ...     ".|F-----7|.",
    ...     ".||.....||.",
    ...     ".||.....||.",
    ...     ".|L-7.F-J|.",
    ...     ".|..|.|..|.",
    ...     ".L--J.L--J.",
    ...     "...........",
    ... ])
    4

    >>> star2([
    ...     "..........",
    ...     ".S------7.",
    ...     ".|F----7|.",
    ...     ".||....||.",
    ...     ".||....||.",
    ...     ".|L-7F-J|.",
    ...     ".|..||..|.",
    ...     ".L--JL--J.",
    ...     "..........",
    ... ])
    4

    >>> star2([
    ...     ".F----7F7F7F7F-7....",
    ...     ".|F--7||||||||FJ....",
    ...     ".||.FJ||||||||L7....",
    ...     "FJL7L7LJLJ||LJ.L-7..",
    ...     "L--J.L7...LJS7F-7L7.",
    ...     "....F-J..F7FJ|L7L7L7",
    ...     "....L7.F7||L7|.L7L7|",
    ...     ".....|FJLJ|FJ|F7|.LJ",
    ...     "....FJL-7.||.||||...",
    ...     "....L---J.LJ.LJLJ...",
    ... ])
    8

    """

    grid = Grid.from_list_str(data)
    start = grid.find_all_xy(lambda e: e == 'S')[0].xy

    for candidate in _pipe_adj(start, grid):
        if start in _pipe_adj(candidate, grid):
            active = candidate
            break
    else:
        assert False

    split = Grid.constant('.', grid.width, grid.height)

    prev = start
    while True:
        split[active] = grid[active]
        adj = _pipe_adj(active, grid)
        if active == start:
            break

        assert len(adj) == 2
        assert prev in adj

        if adj[0] == prev:
            prev = active
            active = adj[1]
        else:
            prev = active
            active = adj[0]

        step = active - prev

        d_a = []
        d_b = []
        match step:
            case Xy(0, -1):
                d_a = [Xy(1, 0), Xy(1, -1)]
                d_b = [Xy(-1, 0), Xy(-1, -1)]
            case Xy(0, 1):
                d_a = [Xy(-1, 0), Xy(-1, 1)]
                d_b = [Xy(1, 0), Xy(1, 1)]
            case Xy(1, 0):
                d_a = [Xy(0, 1), Xy(1, 1)]
                d_b = [Xy(0, -1), Xy(1, -1)]
            case Xy(-1, 0):
                d_a = [Xy(0, -1), Xy(-1, -1)]
                d_b = [Xy(0, 1), Xy(-1, 1)]

        for dxy in d_a:
            xy = prev + dxy
            if xy in split and split[xy] == '.':
                assert split[xy] != 'B'
                split[xy] = 'A'
        for dxy in d_b:
            xy = prev + dxy
            if xy in split and split[xy] == '.':
                assert split[xy] != 'A'
                split[xy] = 'B'

    expandables = split.find_all_xy(lambda cell: cell in ('A', 'B'))

    while expandables:
        expandable = expandables.pop()

        for adj_xy in split.adjacent_diagonal(expandable.xy):
            if split[adj_xy] == '.':
                split[adj_xy] = expandable.value
                expandables.append(Cell(adj_xy, split[adj_xy]))

    if split[0, 0] == 'A':
        return len(split.find_all_xy(lambda cell: cell == 'B'))

    assert split[0, 0] == 'B'
    return len(split.find_all_xy(lambda cell: cell == 'A'))


def _pipe_adj(xy: Xy, grid: Grid[str]) -> list[Xy]:
    cell = grid[xy]
    d_xy_list: list[Xy]
    match cell:
        case '-':
            d_xy_list = [Xy(-1, 0), Xy(1, 0)]

        case '|':
            d_xy_list = [Xy(0, -1), Xy(0, 1)]

        case 'L':
            d_xy_list = [Xy(0, -1), Xy(1, 0)]

        case 'J':
            d_xy_list = [Xy(0, -1), Xy(-1, 0)]

        case 'F':
            d_xy_list = [Xy(0, 1), Xy(1, 0)]

        case '7':
            d_xy_list = [Xy(0, 1), Xy(-1, 0)]

        case '.':
            d_xy_list = []

        case 'S':
            d_xy_list = [Xy(0, 1), Xy(1, 0), Xy(0, -1), Xy(-1, 0)]

        case _:
            assert False

    return list(xy + dxy for dxy in d_xy_list if xy + dxy in grid)


if __name__ == "__main__":
    run_day(10,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),  # 372 = too low, 487 = too low
            )
