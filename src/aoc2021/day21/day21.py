import logging
from typing import Tuple, Dict

from util.logger_config import init_logger

logger = logging.getLogger(__name__)


def star1(player_1: int, player_2: int) -> int:
    """
    >>> star1(4, 8)
    739785
    """

    dice = d100()
    score = [0, 0]
    position = [player_1, player_2]
    rolls = 0
    active = 0

    while True:
        move = next(dice) + next(dice) + next(dice)
        rolls += 3
        position[active] = (position[active] + move - 1) % 10 + 1
        score[active] += position[active]

        logger.info("Player %d moves to %d for a total of %d" % (active + 1, position[active], score[active]))
        if score[active] >= 1000:
            break

        active = 1 - active

    return rolls * score[1 - active]


def d100():
    n = 1
    while True:
        yield n
        n += 1
        if n > 100:
            n = 1


def star2(player_1: int, player_2: int) -> int:
    """
    >>> star2(4, 8)
    444356092776315
    """

    return max(calc_universe((player_1, player_2, 0, 0, 0), {}))


def calc_universe(universe: Tuple[int, int, int, int, int],
                  cache: Dict[Tuple[int, int, int, int, int], Tuple[int, int]]) -> Tuple[int, int]:
    if universe in cache:
        return cache[universe]

    logger.debug("Universe %s not in cache, computing. Cache size: %d" % (universe, len(cache)))

    position = universe[:2]
    score = universe[2:4]
    active = universe[4]

    if score[0] >= 21:
        cache[universe] = (1, 0)
        logger.debug("Finished universe %s, winner 0. Cache size: %d" % (universe, len(cache)))
        return 1, 0

    if score[1] >= 21:
        cache[universe] = (0, 1)
        logger.debug("Finished universe %s, winner 1. Cache size: %d" % (universe, len(cache)))
        return 0, 1

    wins = [0, 0]

    for move, roll_count in d3s():
        new_position = list(position)
        new_position[active] = (new_position[active] + move - 1) % 10 + 1
        new_score = list(score)
        new_score[active] += new_position[active]
        new_universe = (
            new_position[0], new_position[1],
            new_score[0], new_score[1],
            1 - active
        )

        new_uni_wins = calc_universe(new_universe, cache)
        wins[0] += new_uni_wins[0] * roll_count
        wins[1] += new_uni_wins[1] * roll_count

    if universe in cache:
        raise Exception("Ouch")

    wins = (wins[0], wins[1])
    cache[universe] = wins
    return wins


def d3s():
    return [
        (3, 1),
        (4, 3),
        (5, 6),
        (6, 7),
        (7, 6),
        (8, 3),
        (9, 1),
    ]


if __name__ == "__main__":
    init_logger()

    print('Day 21 Star 1: %d' % star1(1, 3))
    print('Day 21 Star 2: %d' % star2(1, 3))
