from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid


def star1(data: list[str], multiplier: int = 2) -> int:
    """
    >>> star1([
    ...     "...#......",
    ...     ".......#..",
    ...     "#.........",
    ...     "..........",
    ...     "......#...",
    ...     ".#........",
    ...     ".........#",
    ...     "..........",
    ...     ".......#..",
    ...     "#...#.....",
    ... ])
    374

    >>> star1([
    ...     "...#......",
    ...     ".......#..",
    ...     "#.........",
    ...     "..........",
    ...     "......#...",
    ...     ".#........",
    ...     ".........#",
    ...     "..........",
    ...     ".......#..",
    ...     "#...#.....",
    ... ], multiplier=10)
    1030

    >>> star1([
    ...     "...#......",
    ...     ".......#..",
    ...     "#.........",
    ...     "..........",
    ...     "......#...",
    ...     ".#........",
    ...     ".........#",
    ...     "..........",
    ...     ".......#..",
    ...     "#...#.....",
    ... ], multiplier=100)
    8410
    """

    expanded_rows = []
    expanded_columns = []
    for y, row in enumerate(data):
        if '#' not in row:
            expanded_rows.append(y)

    for x in range(len(data)):
        for row in data:
            if row[x] == '#':
                break
        else:
            expanded_columns.append(x)

    universe = Grid.from_list_str(data)

    galaxies = universe.find_all_xy(lambda e: e == '#')

    result = 0
    for galaxy_from in galaxies:
        for galaxy_to in galaxies:

            min_x = min(galaxy_from.xy.x, galaxy_to.xy.x)
            max_x = max(galaxy_from.xy.x, galaxy_to.xy.x)
            min_y = min(galaxy_from.xy.y, galaxy_to.xy.y)
            max_y = max(galaxy_from.xy.y, galaxy_to.xy.y)

            dist = max_x - min_x + max_y - min_y
            for y in expanded_rows:
                if min_y < y < max_y:
                    dist += multiplier - 1
            for x in expanded_columns:
                if min_x < x < max_x:
                    dist += multiplier - 1
            result += dist

    return result // 2


if __name__ == "__main__":
    run_day(11,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star1(load_strings_newline(txt_fn(__file__)), multiplier=1_000_000)
            )
