import os
from typing import Tuple, Dict, Iterable, List


def load_data(fn: str = 'data.txt') -> Tuple[Dict[Tuple[int, int], int], List[int]]:
    ieas = []
    image = dict()

    with open(fn, 'r') as fh:
        y = 0
        for line in fh.readlines():
            line = line.strip()

            if not ieas:
                for x in line:
                    ieas.append(1 if x == '#' else 0)
                continue

            if not line:
                continue

            for x in range(len(line)):
                image[(x, y)] = 1 if line[x] == '#' else 0

            y += 1

    return image, ieas


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(image: Dict[Tuple[int, int], int], ieas: List[int], iter=2) -> int:
    """
    >>> star1(*test_data())
    35
    """

    min_x, max_x, min_y, max_y = img_min_max(image)
    min_x -= 2 * iter
    min_y -= 2 * iter
    max_x += 2 * iter
    max_y += 2 * iter

    for _ in range(iter):
        image = expand_once(image, ieas, min_x, max_x, min_y, max_y)

    return sum(v for k, v in image.items()
               if min_x + iter <= k[0] <= max_x - iter and min_y + iter <= k[1] <= max_y - iter)


def expand_once(image: Dict[Tuple[int, int], int], ieas: List[int],
                min_x: int, max_x: int, min_y: int, max_y: int) -> Dict[Tuple[int, int], int]:
    new_image = dict()

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            decode_str = ''
            for ax, ay in nine_around_in_order(x, y):
                decode_str += str(image.get((ax, ay), 0))

            new_image[(x, y)] = ieas[int(decode_str, base=2)]

    return new_image


def nine_around_in_order(x: int, y: int) -> Iterable[Tuple[int, int]]:
    return ((x + dx, y + dy) for dx, dy in [(-1, -1), (0, -1), (1, -1),
                                            (-1, 0), (0, 0), (1, 0),
                                            (-1, 1), (0, 1), (1, 1)])


def dump(image: Dict[Tuple[int, int], int], min_x: int, max_x: int, min_y: int, max_y: int) -> None:
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            print('#' if image.get((x, y), 0) else '.', end='')

        print()
    print()


def img_min_max(image: Dict[Tuple[int, int], int]):
    return (
        min(key[0] for key in image.keys()),
        max(key[0] for key in image.keys()),
        min(key[1] for key in image.keys()),
        max(key[1] for key in image.keys())
    )


if __name__ == "__main__":
    print('Day 20 Star 1: %d' % star1(*load_data()))
    print('Day 20 Star 2: %d' % star1(*load_data(), iter=50))
