import os

from typing import List, Optional, Iterator

WALL = '#'
SPACE = '.'
ELF = 'E'
GOBLIN = 'G'

td = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test-')


class Square:

    def __init__(self, x: int, y: int, type: str) -> None:
        self.x = x
        self.y = y
        self.type = type
        self.unit = None  # type: Optional[Unit]

    def as_chr(self):
        if self.unit is None:
            return self.type

        return self.unit.as_chr()

    def adjacent(self, other: 'Square') -> bool:
        return abs(self.x - other.x) + abs(self.y - other.y) == 1

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return '%s[%d-%d]' % (self.type, self.x, self.y)

    def __lt__(self, other: 'Square'):
        return self.y < other.y or (self.y == other.y and self.x < other.x)


# noinspection PyMethodMayBeStatic
class Unit:
    def __init__(self, type: str, square: Square) -> None:
        self.type = type
        self.square = square
        self.hp = 200
        self.power = 3

    def as_chr(self):
        return self.type

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return '%s[%d-%d] %d hp' % (self.type, self.square.x, self.square.y, self.hp)

    def __lt__(self, other: 'Unit'):
        return self.square.__lt__(other.square)

    def attack(self, target: 'Unit') -> None:
        if target.dead():
            raise Exception('Cannot attack dead unit %s' % target)

        target.hp -= self.power
        if target.dead():
            target.square.unit = None

    def dead(self) -> bool:
        return self.hp < 1


# noinspection PyMethodMayBeStatic
class Game:
    DX_DY = ((0, -1), (-1, 0), (1, 0), (0, 1))

    def __init__(self) -> None:
        self.map = [[]]  # type: List[List[Square]]
        self.units = []  # type: List[Unit]
        self.w = 0
        self.h = 0
        self.verbosity = 0
        self.turn_no = 0

    @staticmethod
    def load(fn, elven_power=None):
        game = Game()
        with open(fn, 'r') as fh:
            lines = list(line.strip() for line in fh.readlines())

            game.w = len(lines[0])
            game.h = len(lines)
            game.map = [[Square(0, 0, '') for y in range(game.h)] for x in range(game.w)]

            for y, line in enumerate(lines):
                for x, c in enumerate(line):
                    if c in (WALL, SPACE):
                        game.map[x][y] = Square(x, y, c)

                    elif c in (GOBLIN, ELF):
                        square = Square(x, y, SPACE)
                        unit = Unit(c, square)
                        square.unit = unit

                        if elven_power is not None and c == ELF:
                            unit.power = elven_power

                        game.units.append(unit)
                        game.map[x][y] = square

                    else:
                        raise Exception('Illegal character %s at %d-%d' % (c, x, y))

        return game

    def battle(self, short_circuit_elves=False):
        """
        >>> g = Game.load(td + 'attack.txt');g.battle()
        (47, 590)

        >>> g = Game.load(td + 'attack-2.txt');g.battle()
        (37, 982)

        >>> g = Game.load(td + 'attack-3.txt');g.battle()
        (46, 859)

        >>> g = Game.load(td + 'attack-4.txt');g.battle()
        (35, 793)

        >>> g = Game.load(td + 'attack-5.txt');g.battle()
        (54, 536)

        >>> g = Game.load(td + 'attack-6.txt');g.battle()
        (20, 937)
        """
        while True:
            self.turn()

            # an elf dieded
            if short_circuit_elves and sum(1 for elf in self.units if elf.type == ELF and elf.dead()) > 0:
                return None

            elf_hp = sum(elf.hp for elf in self.units if elf.type == ELF and not elf.dead())
            goblin_hp = sum(goblin.hp for goblin in self.units if goblin.type == GOBLIN and not goblin.dead())
            # elves won
            if goblin_hp == 0:
                if self.verbosity > 0:
                    self.print()
                return self.turn_no, elf_hp

            # goblins won
            if elf_hp == 0:
                if self.verbosity > 0:
                    self.print()
                return self.turn_no, goblin_hp

            if self.verbosity > 1:
                self.print()
                print()

    def turn(self):
        """
        >>> g = Game.load(td + 'move.txt');g.turn();g.print();g.turn();g.print();g.turn();g.print()
        #########
        #.G...G.#
        #...G...#
        #...E..G#
        #.G.....#
        #.......#
        #G..G..G#
        #.......#
        #########
        #########
        #..G.G..#
        #...G...#
        #.G.E.G.#
        #.......#
        #G..G..G#
        #.......#
        #.......#
        #########
        #########
        #.......#
        #..GGG..#
        #..GEG..#
        #G..G...#
        #......G#
        #.......#
        #.......#
        #########

        >>> g = Game.load(td + 'attack.txt');g.verbosity=1;g.print();g.turn();g.print();g.turn();g.print();g.turn();\
g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();\
g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();\
g.print();g.turn();g.print();g.turn();g.print();g.turn();g.print();g.turn();g.print();g.turn();g.print();g.turn();\
g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();g.turn();\
g.turn();g.turn();g.turn();g.turn();g.turn();g.print()
        Initially:
        #######
        #.G...#   G(200)
        #...EG#   E(200), G(200)
        #.#.#G#   G(200)
        #..G#E#   G(200), E(200)
        #.....#
        #######
        After 1 round:
        #######
        #..G..#   G(200)
        #...EG#   E(197), G(197)
        #.#G#G#   G(200), G(197)
        #...#E#   E(197)
        #.....#
        #######
        After 2 rounds:
        #######
        #...G.#   G(200)
        #..GEG#   G(200), E(188), G(194)
        #.#.#G#   G(194)
        #...#E#   E(194)
        #.....#
        #######
        After 23 rounds:
        #######
        #...G.#   G(200)
        #..G.G#   G(200), G(131)
        #.#.#G#   G(131)
        #...#E#   E(131)
        #.....#
        #######
        After 24 rounds:
        #######
        #..G..#   G(200)
        #...G.#   G(131)
        #.#G#G#   G(200), G(128)
        #...#E#   E(128)
        #.....#
        #######
        After 25 rounds:
        #######
        #.G...#   G(200)
        #..G..#   G(131)
        #.#.#G#   G(125)
        #..G#E#   G(200), E(125)
        #.....#
        #######
        After 26 rounds:
        #######
        #G....#   G(200)
        #.G...#   G(131)
        #.#.#G#   G(122)
        #...#E#   E(122)
        #..G..#   G(200)
        #######
        After 27 rounds:
        #######
        #G....#   G(200)
        #.G...#   G(131)
        #.#.#G#   G(119)
        #...#E#   E(119)
        #...G.#   G(200)
        #######
        After 28 rounds:
        #######
        #G....#   G(200)
        #.G...#   G(131)
        #.#.#G#   G(116)
        #...#E#   E(113)
        #....G#   G(200)
        #######
        After 47 rounds:
        #######
        #G....#   G(200)
        #.G...#   G(131)
        #.#.#G#   G(59)
        #...#.#
        #....G#   G(200)
        #######
        """

        game_over = False
        for unit in self.turn_order():
            if unit.dead():
                continue

            enemies = self.enemies(unit)

            has_target = False
            for enemy in enemies:
                if enemy.square.adjacent(unit.square):
                    has_target = True

            if not has_target:
                adjacent_squares = self.adjacent_squares(enemies)
                target_step = self.closest_target(unit, adjacent_squares)
                if target_step is not None:
                    self.move(unit, target_step)

            target = self.find_enemy_to_attack(unit, enemies)
            if target is not None:
                unit.attack(target)

                # check for wining sides
                elf_hp = sum(elf.hp for elf in self.units if elf.type == ELF and not elf.dead())
                goblin_hp = sum(goblin.hp for goblin in self.units if goblin.type == GOBLIN and not goblin.dead())

                if elf_hp == 0 or goblin_hp == 0:
                    # game should be over. If there is no more units, the round is finished as well.
                    game_over = True
                    continue

            # game is over, but a unit played, do NOT inc turn_no
            if game_over:
                return

        self.turn_no += 1

    def print(self):
        if self.verbosity > 0:
            if self.turn_no == 0:
                print('Initially:')
            else:
                print('After %d round%s:' % (self.turn_no, 's' if self.turn_no > 1 else ''))

        w = len(self.map)
        h = len(self.map[0])

        for y in range(h):
            for x in range(w):
                print(self.map[x][y].as_chr(), end='')

            if self.verbosity > 0:
                strings = []
                for x in range(w):
                    unit = self.map[x][y].unit
                    if unit is not None and not unit.dead():
                        strings.append('%s(%d)' % (unit.type, unit.hp))

                if strings:
                    print('   %s' % ', '.join(strings), end='')

            print()

    def map_iter(self) -> Iterator[Square]:
        for y in range(self.h):
            for x in range(self.w):
                yield self.map[x][y]

    def turn_order(self) -> List[Unit]:
        """
        All units in reading order.

        >>> Game.load(td + 'order.txt').turn_order()
        [G[2-1] 200 hp, E[4-1] 200 hp, E[1-2] 200 hp, G[3-2] 200 hp, E[5-2] 200 hp, G[2-3] 200 hp, E[4-3] 200 hp]

        ~ 9usec
        """
        return sorted(unit for unit in self.units)

    def enemies(self, unit: Unit) -> List[Unit]:
        """
        All enemies of a unit in reading order.

        >>> g = Game.load(td + 'targets.txt');g.enemies(g.units[0])
        [G[4-1] 200 hp, G[2-3] 200 hp, G[5-3] 200 hp]

        ~ 3usec
        """
        return sorted(enemy for enemy in self.units if enemy.type != unit.type and not enemy.dead())

    def adjacent_squares(self, units: List[Unit]) -> List[Square]:
        """
        All squares (as segments) adjacent to given input units.

        >>> g = Game.load(td + 'targets.txt');g.adjacent_squares(g.enemies(g.map[1][1].unit))
        [.[3-1], .[5-1], .[2-2], .[5-2], .[1-3], .[3-3]]

        ~ 33usec
        """
        result = set()
        for unit in units:
            for dx, dy in Game.DX_DY:
                x = unit.square.x + dx
                y = unit.square.y + dy

                if self.is_space(x, y):
                    result.add((x, y,))

        return list(self.map[s[0]][s[1]] for s in sorted(result, key=lambda s: (s[1], s[0])))

    def is_space(self, x: int, y: int):
        return 0 <= x < self.w and 0 <= y < self.h and self.map[x][y].type == SPACE and self.map[x][y].unit is None

    def closest_target(self, unit: Unit, targets: List[Square]) -> Optional[Square]:
        """
        Returns closest square to which to step when a choosing from list of possible targets.

        Targets must be sorted.

        >>> g = Game.load(td + 'targets.txt');g.closest_target(g.units[0], g.adjacent_squares(g.enemies(g.units[0])))
        .[2-1]

        ~ 480usec
        """
        # create dijkstra map
        map = [[None for y in range(self.h)] for x in range(self.w)]  # type: List[List[Optional[Node]]]
        start = Node(unit.square.x, unit.square.y, None, 0)

        # those two are optimization -- once a target had been found, ignore anything further away
        targets_xy = set((target.x, target.y) for target in targets)
        best_distance = None

        map[start.x][start.y] = start
        buffer = [start, ]
        i = 0

        while i < len(buffer):
            node = buffer[i]
            i += 1

            # opt, once best distance is found, ignore anything bigger
            if best_distance is not None and node.distance > best_distance:
                break

            for dx, dy in Game.DX_DY:
                new_x = node.x + dx
                new_y = node.y + dy

                if self.is_space(new_x, new_y):
                    # unvisited
                    if map[new_x][new_y] is None:
                        new_node = Node(new_x, new_y, node, node.distance + 1)
                        map[new_x][new_y] = new_node
                        buffer.append(new_node)

                        if best_distance is None and (new_x, new_y) in targets_xy:
                            best_distance = new_node.distance

        closest = None  # type: Optional[Node]

        for target in targets:
            node = map[target.x][target.y]
            if node is not None:
                if closest is None or closest.distance > node.distance:
                    closest = node

        # no reachable target
        if closest is None:
            return None

        while True:
            if closest.distance == 1:
                return self.map[closest.x][closest.y]

            closest = closest.parent

    def move(self, unit: Unit, square: Square):
        if square is None or square.type != SPACE or square.unit is not None:
            raise Exception('Cannot move to non-empty square %s' % square)

        if not unit.square.adjacent(square):
            raise Exception('Can only move by one square, %s -> %s' % (unit.square, square))

        unit.square.unit = None
        unit.square = square
        square.unit = unit

    def find_enemy_to_attack(self, unit: Unit, enemies: List[Unit]) -> Optional[Unit]:
        weakest = None  # type: Optional[Unit]
        for enemy in enemies:
            if not enemy.square.adjacent(unit.square):
                continue

            if weakest is None or weakest.hp > enemy.hp:
                weakest = enemy

        return weakest


class Node:

    def __init__(self, x: int, y: int, parent: Optional['Node'], distance: int) -> None:
        self.x = x
        self.y = y
        self.parent = parent
        self.distance = distance

    def __str__(self) -> str:
        return '[%d-%d@%d]' % (self.x, self.y, self.distance)


def star_2(fn: str, verbosity: int = 0):
    """
    >>> star_2(td + 'attack.txt')
    (29, 172)

    >>> star_2(td + 'attack-3.txt')
    (33, 948)

    >>> star_2(td + 'attack-4.txt')
    (37, 94)

    >>> star_2(td + 'attack-5.txt')
    (39, 166)

    >>> star_2(td + 'attack-6.txt')
    (30, 38)
    """
    elven_power = 1
    while True:
        game = Game.load(fn, elven_power)
        game.verbosity = verbosity
        result = game.battle(short_circuit_elves=True)
        if result is not None:
            return result

        elven_power += 1


if __name__ == "__main__":
    game = Game.load('data.txt')
    game.verbosity = 1
    rounds, hp = game.battle()
    print('Day 15 star 1: %d' % (rounds * hp))

    rounds, hp = star_2('data.txt', verbosity=1)
    print('Day 15 star 2: %d' % (rounds * hp))
