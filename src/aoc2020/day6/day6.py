from typing import List


def load_data(fn: str) -> List[List[str]]:
    result = []
    group = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            if not line:
                result.append(group)
                group = []

            else:
                group.append(line)

    if group:
        result.append(group)

    return result


def star1(data: List[List[str]]) -> int:
    """
    >>> star1([['abc'], ['a', 'b', 'c'], ['ab', 'ac'], ['a', 'a', 'a', 'a'], ['b']])
    11
    """

    total = 0
    for group in data:
        group_answers = set()
        for answer in group:
            group_answers = group_answers.union(set(answer))

        total += len(group_answers)

    return total


def star2(data: List[List[str]]) -> int:
    """
    >>> star2([['abc'], ['a', 'b', 'c'], ['ab', 'ac'], ['a', 'a', 'a', 'a'], ['b']])
    6
    """

    total = 0
    for group in data:
        group_answers = None
        for answer in group:
            if group_answers is None:
                group_answers = set(answer)
            else:
                group_answers = group_answers.intersection(set(answer))

        total += len(group_answers)

    return total


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 6 Star 1: %d' % star1(my_data))
    print('Day 6 Star 2: %d' % star2(my_data))
