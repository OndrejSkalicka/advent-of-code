from functools import cmp_to_key

from util.data import txt_fn, load_strings_newline, l_split
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["47|53",
    ...        "97|13",
    ...        "97|61",
    ...        "97|47",
    ...        "75|29",
    ...        "61|13",
    ...        "75|53",
    ...        "29|13",
    ...        "97|29",
    ...        "53|29",
    ...        "61|53",
    ...        "97|53",
    ...        "61|29",
    ...        "47|13",
    ...        "75|47",
    ...        "97|75",
    ...        "47|61",
    ...        "75|61",
    ...        "47|29",
    ...        "75|13",
    ...        "53|13",
    ...        "",
    ...        "75,47,61,53,29",
    ...        "97,61,53,29,13",
    ...        "75,29,13",
    ...        "75,97,47,61,53",
    ...        "61,13,29",
    ...        "97,13,75,29,47"])
    143
    """

    rules = [(int(r.split('|')[0]), int(r.split('|')[1])) for r in l_split(data)[0]]
    updates = [[int(p) for p in u.split(',')] for u in l_split(data)[1]]

    result = 0

    for pages in updates:
        if check_rules(pages, rules):
            result += pages[len(pages) // 2]

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["47|53",
    ...        "97|13",
    ...        "97|61",
    ...        "97|47",
    ...        "75|29",
    ...        "61|13",
    ...        "75|53",
    ...        "29|13",
    ...        "97|29",
    ...        "53|29",
    ...        "61|53",
    ...        "97|53",
    ...        "61|29",
    ...        "47|13",
    ...        "75|47",
    ...        "97|75",
    ...        "47|61",
    ...        "75|61",
    ...        "47|29",
    ...        "75|13",
    ...        "53|13",
    ...        "",
    ...        "75,47,61,53,29",
    ...        "97,61,53,29,13",
    ...        "75,29,13",
    ...        "75,97,47,61,53",
    ...        "61,13,29",
    ...        "97,13,75,29,47"])
    123
    """

    rules = [(int(r.split('|')[0]), int(r.split('|')[1])) for r in l_split(data)[0]]
    updates = [[int(p) for p in u.split(',')] for u in l_split(data)[1]]

    result = 0

    for pages in updates:
        if check_rules(pages, rules):
            continue

        pages.sort(key=cmp_to_key(lambda a, b: cmp(a, b, rules)))  # type: ignore
        result += pages[len(pages) // 2]

    return result


def check_rules(pages: list[int], rules: list[tuple[int, int]]) -> bool:
    pages_map = {p: idx for idx, p in enumerate(pages)}

    for left, right in rules:
        if left not in pages_map or right not in pages_map:
            continue

        if pages_map[left] > pages_map[right]:
            return False

    return True


def cmp(a: int, b: int, rules: list[tuple[int, int]]) -> int:
    for rule in rules:
        if rule[0] == a and rule[1] == b:
            return -1
        if rule[0] == b and rule[1] == a:
            return 1

    return 0


if __name__ == "__main__":
    run_day(4,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
