import os
from collections import deque
from typing import Deque, Tuple, Optional


class MutableInt:

    def __init__(self, value: int) -> None:
        self.value = value


def load_data(fn: str) -> Tuple[Deque[int], Deque[int]]:
    result = (
        deque(),
        deque(),
    )
    active = None
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            if line.startswith("Player "):
                active = result[int(line[7:8]) - 1]  # type:Optional[deque]

                continue

            if line == '':
                continue

            active.append(int(line))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(decks: Tuple[Deque[int], Deque[int]], debug=True) -> int:
    """
    >>> star1(test_data(), debug=False)
    306
    """

    round = 0
    while len(decks[0]) and len(decks[1]):
        round += 1
        if debug:
            print("-- Round %d --" % round)
            print("Player 1's deck: %s" % ', '.join((str(x) for x in decks[0])))
            print("Player 2's deck: %s" % ', '.join((str(x) for x in decks[1])))

        cards = (
            decks[0].popleft(),
            decks[1].popleft()
        )

        if debug:
            print("Player 1 plays: %d" % cards[0])
            print("Player 2 plays: %d" % cards[1])

        winner = 0 if cards[0] > cards[1] else 1
        if debug:
            print("Player %d wins the round!" % (winner + 1))
            print()

        decks[winner].append(cards[winner])
        decks[winner].append(cards[1 - winner])

    result = 0
    winning_deck = decks[0] if len(decks[0]) > 0 else decks[1]

    for idx, value in enumerate(reversed(winning_deck)):
        result += (idx + 1) * value
    return result


def star2(decks: Tuple[Deque[int], Deque[int]], debug=True) -> int:
    """
    >>> star2(test_data(), debug=False)
    291
    """

    return star2_game(decks, MutableInt(0), debug)


def star2_game(decks: Tuple[Deque[int], Deque[int]], game: MutableInt, debug=True) -> int:
    current_game_number = game.value
    game.value += 1
    if debug:
        print("=== Game %s ===" % (current_game_number + 1))
        print()

    round = 0

    existing_rounds = set()

    while len(decks[0]) and len(decks[1]):
        round += 1
        if debug:
            print("-- Round %d (Game %s) --" % (round, current_game_number + 1))
            print("Player 1's deck: %s" % ', '.join((str(x) for x in decks[0])))
            print("Player 2's deck: %s" % ', '.join((str(x) for x in decks[1])))

        round_signature = '!'.join(';'.join(list(str(x) for x in deck)) for deck in decks)
        if round_signature in existing_rounds:
            if debug:
                print("Player 1 won on the basis of recursion")

            return 0
        existing_rounds.add(round_signature)

        draws = (
            decks[0].popleft(),
            decks[1].popleft()
        )

        if debug:
            print("Player 1 plays: %d" % draws[0])
            print("Player 2 plays: %d" % draws[1])

        if draws[0] <= len(decks[0]) and draws[1] <= len(decks[1]):
            if debug:
                print("Playing a sub-game to determine the winner...")
                print()

            sub_decks = (
                deque(list(decks[0])[:draws[0]]),
                deque(list(decks[1])[:draws[1]]),
            )

            winner = star2_game(sub_decks, game, debug)
            if debug:
                print("...anyway, back to game %d." % (current_game_number + 1))

        else:
            winner = 0 if draws[0] > draws[1] else 1

        if debug:
            print("Player %d wins round %d of game %s!" % (winner + 1, round, current_game_number + 1))
            print()

        decks[winner].append(draws[winner])
        decks[winner].append(draws[1 - winner])

    game_winner = 0 if len(decks[0]) > 0 else 1
    if debug:
        print("The winner of game %d is player %d!" % (current_game_number + 1, game_winner + 1))
        print()

    if current_game_number == 0:
        if debug:
            print("== Post-game results ==")
            print("Player 1's deck: %s" % ', '.join((str(x) for x in decks[0])))
            print("Player 2's deck: %s" % ', '.join((str(x) for x in decks[1])))

        result = 0
        winning_deck = decks[0] if len(decks[0]) > 0 else decks[1]

        for idx, value in enumerate(reversed(winning_deck)):
            result += (idx + 1) * value
        return result

    return game_winner


if __name__ == "__main__":
    print('Day 22 Star 1: %d' % star1(load_data('data.txt'), debug=False))
    print('Day 22 Star 2: %s' % star2(load_data('data.txt'), debug=False))
