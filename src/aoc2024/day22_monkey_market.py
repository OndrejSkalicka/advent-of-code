from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "1",
    ...        "10",
    ...        "100",
    ...        "2024",
    ...       ])
    37327623
    """

    result = 0
    for row in data:
        secret = int(row)
        for _ in range(2000):
            secret = next_secret(secret)

        result += secret

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "1",
    ...        "2",
    ...        "3",
    ...        "2024",
    ...       ])
    23
    """

    monkeys = []
    all_keys = set()
    for row in data:
        secret = int(row)
        prices = []
        diffs = []
        prev_price = secret % 10
        prices_by_keys = {}
        for _ in range(2000):
            secret = next_secret(secret)
            price = secret % 10
            diff = price - prev_price
            prev_price = price
            prices.append(price)

            diffs.append(diff)

            if len(diffs) >= 4:
                key = (diffs[-4], diffs[-3], diffs[-2], diffs[-1])
                if key not in prices_by_keys:
                    prices_by_keys[key] = price

                all_keys.add(key)

        monkeys.append(prices_by_keys)

    best = 0
    for key in all_keys:
        total = 0
        for monkey in monkeys:
            total += monkey.get(key, 0)

        if total > best:
            best = total

    return best


def next_secret(secret: int) -> int:
    """
    >>> next_secret(123)
    15887950
    >>> next_secret(15887950)
    16495136
    >>> next_secret(16495136)
    527345
    >>> next_secret(527345)
    704524
    >>> next_secret(704524)
    1553684
    >>> next_secret(1553684)
    12683156
    >>> next_secret(12683156)
    11100544
    >>> next_secret(11100544)
    12249484
    >>> next_secret(12249484)
    7753432
    >>> next_secret(7753432)
    5908254
    """

    secret = prune(mix(secret * 64, secret))
    secret = prune(mix(secret // 32, secret))
    secret = prune(mix(secret * 2048, secret))

    return secret


def mix(value: int, secret: int) -> int:
    """
    >>> mix(15, 42)
    37
    """

    return value ^ secret


def prune(secret: int) -> int:
    """
    >>> prune(100000000)
    16113920
    """

    return secret % 16777216


if __name__ == "__main__":
    run_day(22,
            lambda: star1(load_strings_newline(txt_fn(__file__))),  # 128962
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
