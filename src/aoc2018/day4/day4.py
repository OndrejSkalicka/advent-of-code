import os
import re
from typing import List, Mapping


class Entry:
    def __init__(self, year: int, month: int, day: int, hour: int, minute: int) -> None:
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute
        self.sleep = False
        self.wake = False
        self.shift = False
        self.id = 0

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        if self.sleep:
            status = 'falls asleep'
        elif self.wake:
            status = 'wakes up'
        else:
            status = 'Guard #%d begins shift' % self.id

        return '[%04d-%02d-%02d %02d:%02d] %s' % (
            self.year,
            self.month,
            self.day,
            self.hour,
            self.minute,
            status
        )


class Guard:
    def __init__(self, id: int):
        self.id = id
        self.hours = [0] * 60

    def __str__(self) -> str:
        return 'Guard #%d' % self.id


class HhMm:

    def __init__(self, hour: int, minute: int):
        self.hour = hour
        self.minute = minute

    def __str__(self) -> str:
        return '%02d:%02d' % (self.hour, self.minute)


def guards_stats(entries: List[Entry]) -> Mapping[int, Guard]:
    guards = {}
    guard = None  # type: Guard
    sleep_from = None  # type: HhMm
    for entry in entries:
        if guard is None and not entry.shift:
            raise Exception('First entry must be beginning of a shift!')

        if entry.shift:
            if entry.id not in guards:
                guards[entry.id] = Guard(entry.id)
            guard = guards[entry.id]

        if entry.sleep:
            if sleep_from is not None:
                raise Exception('Cannot fall asleep when already sleeping!')
            sleep_from = HhMm(entry.hour, entry.minute)

        if entry.wake:
            for h in range(sleep_from.minute, entry.minute):
                guard.hours[h] += 1
            sleep_from = None

    return guards


def find_sleeper_strategy_1(guards: Mapping[int, Guard]) -> (int, int):
    """
    >>> find_sleeper_strategy_1(guards_stats(load_data('data-test.txt')))
    (10, 24)
    """
    sleeper = None
    sleep_time = None
    for guard in guards.values():
        current_sleep_time = sum(guard.hours)
        if sleeper is None or current_sleep_time > sleep_time:
            sleeper = guard
            sleep_time = current_sleep_time

    max_sleep_value = max(list(h for h in sleeper.hours if h > 0))
    # sanity check
    if sum(1 for h in sleeper.hours if h == max_sleep_value) != 1:
        raise Exception('Sleeper slept maximum number of hours more than once!')

    return sleeper.id, sleeper.hours.index(max_sleep_value)


def find_sleeper_strategy_2(guards: Mapping[int, Guard]) -> (int, int):
    """
    >>> find_sleeper_strategy_2(guards_stats(load_data('data-test.txt')))
    (99, 45)
    """

    sleeper = None
    max_sleep_value = None
    for guard in guards.values():
        current_sleep_time = max(guard.hours)
        if sleeper is None or current_sleep_time > max_sleep_value:
            sleeper = guard
            max_sleep_value = current_sleep_time

    max_sleep_minute = sleeper.hours.index(max_sleep_value)
    # sanity check
    if sum(1 for h in sleeper.hours if h == max_sleep_value) != 1:
        raise Exception('Sleeper slept maximum number of hours more than once!')

    return sleeper.id, max_sleep_minute


def load_data(fn):
    result = []
    matcher = re.compile(r'''
                ^
                \[
                    (?P<year>\d{4}) # year
                    -
                    (?P<month>\d{2}) # month
                    -
                    (?P<day>\d{2}) # day
                    \s
                    (?P<hour>\d{2}) # hours
                    :
                    (?P<minute>\d{2}) # minutes
                \]\s
                (?:
                    (?P<sleep>falls\ asleep) |
                    (?P<wake>wakes\ up) |
                    (?P<shift>Guard\ \#(?P<id>\d+)\ begins\ shift)
                )
                ''', re.X)
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), fn), 'r') as fh:
        lines = fh.readlines()
        lines.sort()
        for line in lines:
            m = matcher.match(line)
            if not m:
                raise Exception('Cannot parse line "%s"' % line)

            entry = Entry(int(m.group('year')),
                          int(m.group('month')),
                          int(m.group('day')),
                          int(m.group('hour')),
                          int(m.group('minute')))

            if m.group('sleep'):
                entry.sleep = True
            elif m.group('wake'):
                entry.wake = True
            else:
                entry.shift = True
                entry.id = int(m.group('id'))

            result.append(entry)

    return result


if __name__ == "__main__":
    data = load_data('data.txt')
    stats = guards_stats(data)
    guard_id, minute = find_sleeper_strategy_1(stats)
    print('Day 4 Star 1: %d = %d * %d' % (guard_id * minute, guard_id, minute))
    guard_id, minute = find_sleeper_strategy_2(stats)
    print('Day 4 Star 2: %d = %d * %d' % (guard_id * minute, guard_id, minute))
