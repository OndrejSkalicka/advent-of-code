import logging
from textwrap import dedent

import numpy as np

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger

logger = logging.getLogger(__name__)

SCAFFOLD = '#'


def star1(memory: list[int]) -> int:
    program = Program(memory)
    program.run()

    image = ""
    for o in program.iter_output():
        image += chr(o)

    rows = image.split("\n")

    map: np.ndarray = np.zeros((len(rows[0]), len(rows)), dtype=str)  # type: ignore

    for y, row in enumerate(rows):
        for x, c in enumerate(row):
            map[x, y] = c

    total = 0
    for x, y in np.argwhere(map == SCAFFOLD):
        if not (0 < x < map.shape[0] - 1):
            continue
        if not (0 < y < map.shape[1] - 1):
            continue

        if map[x + 1, y] == SCAFFOLD and map[x - 1, y] == SCAFFOLD and map[x, y + 1] == SCAFFOLD and map[x, y - 1] == SCAFFOLD:
            total += x * y

    return total


def star2(memory: list[int]) -> int:
    memory = memory[:]
    memory[0] = 2
    program = Program(memory)

    my_input = dedent("""
    C,B,B,C,B,A,C,A,B,A
    R,12,L,6,L,6,L,8
    L,8,R,12,L,12
    L,4,L,6,L,8,L,12
    n
    """).lstrip()

    for c in my_input:
        program.write_input(ord(c))

    program.run()

    o = -1
    for o in program.iter_output():
        print(chr(o), end='')

    return o


if __name__ == "__main__":
    init_logger()

    print('Day 17 Star 1: %d' % star1(load_numbers_csv('day17.txt')))
    print('Day 17 Star 2: %d' % star2(load_numbers_csv('day17.txt')))
