import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
    ...     "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
    ...     "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
    ...     "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
    ...     "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
    ...     "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
    ... ])
    13
    """

    result = 0
    for card in data:
        parts = card.split(': ')[1].split('|')

        have = set(int(c.strip()) for c in re.findall(r'\d+', parts[0]))
        winning = set(int(c.strip()) for c in re.findall(r'\d+', parts[1]))

        wins = len(winning & have)

        if wins > 0:
            result += 2 ** (wins - 1)

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
    ...     "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
    ...     "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
    ...     "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
    ...     "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
    ...     "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
    ... ])
    30
    """

    result = {i + 1: 1 for i in range(len(data))}
    for card in data:
        card_id = int(re.match(r'Card +(\d+):', card).group(1))  # type: ignore
        parts = card.split(': ')[1].split('|')

        have = set(int(c.strip()) for c in re.findall(r'\d+', parts[0]))
        winning = set(int(c.strip()) for c in re.findall(r'\d+', parts[1]))

        wins = len(winning & have)

        for i in range(wins):
            if card_id + i + 1 not in result:
                continue
            result[card_id + i + 1] += result[card_id]

    return sum(result.values())


if __name__ == "__main__":
    run_day(4,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
