def box_id_stats(box_id):
    """
    >>> box_id_stats('abcdef')
    (0, 0)

    >>> box_id_stats('bababc')
    (1, 1)

    >>> box_id_stats('abbcde')
    (1, 0)

    >>> box_id_stats('abcccd')
    (0, 1)

    >>> box_id_stats('aabcdd')
    (2, 0)

    >>> box_id_stats('abcdee')
    (1, 0)

    >>> box_id_stats('ababab')
    (0, 2)
    """

    stats = {}
    for c in box_id:
        if c in stats:
            stats[c] += 1
        else:
            stats[c] = 1

    pairs = 0
    triplets = 0
    for k, v in stats.items():
        if v == 2:
            pairs += 1
        elif v == 3:
            triplets += 1

    return pairs, triplets


def warehouse_checksum(boxes):
    """
    >>> warehouse_checksum(('abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab'))
    12
    """
    with_pair = 0
    with_triplet = 0

    for box in boxes:
        stats = box_id_stats(box)

        if stats[0] > 0:
            with_pair += 1

        if stats[1] > 0:
            with_triplet += 1

    return with_triplet * with_pair


def similar(left, right):
    """
    >>> similar('abcde', 'axcye')
    False

    >>> similar('fghij', 'fguij')
    True
    """
    if len(left) != len(right):
        return False

    diffs = 0
    for i in range(len(left)):
        if left[i] != right[i]:
            diffs += 1
            if diffs > 1:
                return False

    return diffs == 1


def find_similar(boxes):
    """
    >>> find_similar(('abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz'))
    ('fghij', 'fguij')
    """

    for i in range(len(boxes) - 1):
        for j in range(i + 1, len(boxes)):
            left = boxes[i]
            right = boxes[j]
            if similar(left, right):
                return left, right

    return None


def merge_box_ids(boxes):
    """
    >>> merge_box_ids(('fghij', 'fguij'))
    'fgij'
    """
    same = []
    for i in range(len(boxes[0])):
        if boxes[0][i] == boxes[1][i]:
            same.append(boxes[0][i])

    return ''.join(same)


def load_data():
    with open('data.txt', 'r') as fh:
        return fh.readlines()


if __name__ == "__main__":
    data = load_data()
    print('Day 1 Star 1: %d' % warehouse_checksum(data))
    print('Day 1 Star 2: %s' % merge_box_ids(find_similar(data)))
