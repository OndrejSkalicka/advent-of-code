import logging

# See https://stackoverflow.com/a/56944256/600169 and https://en.wikipedia.org/wiki/ANSI_escape_code
import sys


class ColorFormatter(logging.Formatter):
    grey = "\033[37;20m"
    yellow = "\033[33;20m"
    red = "\033[31;20m"
    bold_red = "\033[31;1m"
    reset = "\033[0m"
    format_str = "%(asctime)s - %(levelname)-8s - %(message)s"

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: format_str,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%H:%M:%S')
        return formatter.format(record)


def init_logger(level: int = logging.INFO) -> None:
    console = logging.StreamHandler(stream=sys.stdout)
    console.setLevel(level)
    console.setFormatter(ColorFormatter())
    logging.getLogger('').addHandler(console)
    logging.getLogger('').setLevel(level)
