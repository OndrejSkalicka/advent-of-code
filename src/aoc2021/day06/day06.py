from typing import List


def load_data(fn: str) -> List[int]:
    with open(fn, 'r') as fh:
        line = fh.readline()
        return list(int(x) for x in line.strip().split(","))


def star1(data: List[int], days: int = 80) -> int:
    """
    >>> star1([3, 4, 3, 1, 2], days=18)
    26

    >>> star1([3, 4, 3, 1, 2])
    5934
    """

    fishes = [0] * 9

    for fish in data:
        fishes[fish] += 1

    for day in range(days):
        next_gen = [0] * 9
        for timer, count in enumerate(fishes):
            if timer > 0:
                next_gen[timer - 1] += count

            else:
                next_gen[6] += count
                next_gen[8] += count

        fishes = next_gen

    return sum(fishes)


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 6 Star 1: %d' % star1(data))
    print('Day 6 Star 2: %d' % star1(data, days=256))
