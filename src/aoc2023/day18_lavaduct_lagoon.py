import re

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Xy, EAST, WEST, SOUTH, NORTH

DIRECTIONS = {
    'R': EAST,
    'D': SOUTH,
    'L': WEST,
    'U': NORTH,

}


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "R 6 (#70c710)",
    ...     "D 5 (#0dc571)",
    ...     "L 2 (#5713f0)",
    ...     "D 2 (#d2c081)",
    ...     "R 2 (#59c680)",
    ...     "D 2 (#411b91)",
    ...     "L 5 (#8ceee2)",
    ...     "U 2 (#caa173)",
    ...     "L 1 (#1b58a2)",
    ...     "U 2 (#caa171)",
    ...     "R 2 (#7807d2)",
    ...     "U 3 (#a77fa3)",
    ...     "L 2 (#015232)",
    ...     "U 2 (#7a21e3)",
    ... ])
    62
    """

    steps = []
    for row in data:
        direction, dist, _ = row.split(' ')
        steps.append((DIRECTIONS[direction], int(dist)))

    return _dark_magic(steps)


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "R 6 (#70c710)",
    ...     "D 5 (#0dc571)",
    ...     "L 2 (#5713f0)",
    ...     "D 2 (#d2c081)",
    ...     "R 2 (#59c680)",
    ...     "D 2 (#411b91)",
    ...     "L 5 (#8ceee2)",
    ...     "U 2 (#caa173)",
    ...     "L 1 (#1b58a2)",
    ...     "U 2 (#caa171)",
    ...     "R 2 (#7807d2)",
    ...     "U 3 (#a77fa3)",
    ...     "L 2 (#015232)",
    ...     "U 2 (#7a21e3)",
    ... ])
    952408144115
    """

    steps = []
    for row in data:
        m = re.match(r'.*#([0-9a-f]+)([0-9a-f])\)', row)
        assert m

        distance = int(m.group(1), 16)
        direction = 'RDLU'[int(m.group(2))]

        steps.append((DIRECTIONS[direction], distance))

    return _dark_magic(steps)


def _dark_magic(steps: list[tuple[Xy, int]]) -> int:
    vertical_lines = []
    horizontal_lines = []
    points = []
    x_splits: list[int] = []
    y_splits: list[int] = []

    xy = Xy(0, 0)
    points.append(xy)
    for direction, length in steps:
        xy2 = xy + (direction * length)

        if xy.x == xy2.x:
            vertical_lines.append((xy.x, min(xy.y, xy2.y), max(xy.y, xy2.y)))
            x_splits.append(xy.x)
        else:
            y_splits.append(xy.y)
            horizontal_lines.append((xy.y, min(xy.x, xy2.x), max(xy.x, xy2.x)))

        xy = xy2
        points.append(xy)

    x_splits = sorted(set(x_splits))
    y_splits = sorted(set(y_splits))

    total = 0

    for x1, x2 in zip(x_splits, x_splits[1:]):
        for y1, y2 in zip(y_splits, y_splits[1:]):

            if _ray_cast((x2 + x1) / 2.0, (y2 + y1) / 2.0, vertical_lines):
                total += (x2 - x1) * (y2 - y1)

    for vertical_line in vertical_lines:
        if not _ray_cast(vertical_line[0] + 0.5, (vertical_line[1] + vertical_line[2]) / 2.0, vertical_lines):
            total += vertical_line[2] - vertical_line[1] - 1

    for horizontal_line in horizontal_lines:
        if not _ray_cast((horizontal_line[1] + horizontal_line[2]) / 2.0, horizontal_line[0] + 0.5, vertical_lines):
            total += horizontal_line[2] - horizontal_line[1] - 1

    for point in points:
        if not _ray_cast(point.x + 0.5, point.y + 0.5, vertical_lines):
            total += 1

    return total


def _ray_cast(x: float, y: float, vertical_lines: list[tuple[int, int, int]]) -> bool:
    rays = 0
    for line in vertical_lines:
        if x < line[0] and line[1] < y < line[2]:
            rays += 1

    return rays % 2 == 1


if __name__ == "__main__":
    run_day(18,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
