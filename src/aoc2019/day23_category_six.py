import logging
from collections import defaultdict

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)


# noinspection DuplicatedCode
def star1(memory: list[int]) -> int:
    computers = []
    for i in range(50):
        computer = Program(memory)
        computer.write_input(i)
        computer.run()
        computers.append(computer)

    packets_for_address: dict[int, list[tuple[int, int]]] = defaultdict(list)
    while True:
        # read packets from network
        for i, computer in enumerate(computers):
            if not packets_for_address[i]:
                computer.write_input(-1)
            else:
                for packet in packets_for_address[i]:
                    computer.write_input(packet[0], packet[1])

                packets_for_address[i] = []

            computer.run_or_resume()

        # write packets from computers to network
        for computer in computers:
            while computer.output:
                address, x, y = computer.read_output(), computer.read_output(), computer.read_output()

                packets_for_address[address].append((x, y))

        if 255 in packets_for_address:
            return packets_for_address[255][0][1]


# noinspection DuplicatedCode
def star2(memory: list[int]) -> int:
    computers: list[Program] = []
    for i in range(50):
        computer = Program(memory)
        computer.write_input(i)
        computer.run()
        computers.append(computer)

    packets_for_address: dict[int, list[tuple[int, int]]] = defaultdict(list)

    y_delivered_from_nat_to_zero: set[int] = set()

    while True:
        # read packets from network
        for i, computer in enumerate(computers):
            if not packets_for_address[i]:
                computer.write_input(-1)
            else:
                for packet in packets_for_address[i]:
                    computer.write_input(packet[0], packet[1])

                packets_for_address[i] = []

            computer.run_or_resume()

        # write packets from computers to network
        for computer in computers:
            while computer.output:
                address, x, y = computer.read_output(), computer.read_output(), computer.read_output()

                packets_for_address[address].append((x, y))

        packets_queue_count = sum(len(packets_for_address[x]) for x in range(50))
        if packets_queue_count == 0:
            x, y = packets_for_address[255][-1][0], packets_for_address[255][-1][1]
            computers[0].write_input(x, y)

            if y in y_delivered_from_nat_to_zero:
                return y

            y_delivered_from_nat_to_zero.add(y)


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(23,
            lambda: star1(load_numbers_csv('day23_category_six.txt')),
            lambda: star2(load_numbers_csv('day23_category_six.txt')),
            )
