from util.data import txt_fn, load_as_str
from util.perf import run_day
from util.record import Xy


def star1(data: str) -> int:
    """
    >>> star1('>')
    2
    >>> star1('^>v<')
    4
    >>> star1('^v^v^v^v^v')
    2
    """

    location = Xy(0, 0)
    visited = {location}

    for c in data:
        match c:
            case '>':
                location = location + Xy(1, 0)
            case 'v':
                location = location + Xy(0, -1)
            case '^':
                location = location + Xy(0, 1)
            case '<':
                location = location + Xy(-1, 0)
            case _:
                assert False

        visited.add(location)

    return len(visited)


def star2(data: str) -> int:
    """
    >>> star2('^v')
    3
    >>> star2('^>v<')
    3
    >>> star2('^v^v^v^v^v')
    11
    """

    location_santa = Xy(0, 0)
    location_robo_santa = Xy(0, 0)
    visited_santa = {location_santa}
    visited_robo_santa = {location_robo_santa}
    active_santa = True

    for c in data:
        match c:
            case '>':
                dxy = Xy(1, 0)
            case 'v':
                dxy = Xy(0, -1)
            case '^':
                dxy = Xy(0, 1)
            case '<':
                dxy = Xy(-1, 0)
            case _:
                assert False

        if active_santa:
            location_santa += dxy
            visited_santa.add(location_santa)
        else:
            location_robo_santa += dxy
            visited_robo_santa.add(location_robo_santa)
        active_santa = not active_santa

    return len(visited_santa | visited_robo_santa)


if __name__ == "__main__":
    run_day(3,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
