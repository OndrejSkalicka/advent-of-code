from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str], steps: int = 64) -> int:
    """
    >>> star1([
    ...     "...........",
    ...     ".....###.#.",
    ...     ".###.##..#.",
    ...     "..#.#...#..",
    ...     "....#.#....",
    ...     ".##..S####.",
    ...     ".##..#...#.",
    ...     ".......##..",
    ...     ".##.#.####.",
    ...     ".##..##.##.",
    ...     "...........",
    ... ], steps=6)
    16
    """

    grid = Grid.from_list_str(data)
    start = grid.find_all_xy(lambda cell: cell == 'S')[0].xy
    grid[start] = '.'

    active: list[tuple[Xy, int]] = [(start, 0)]
    visited: set[tuple[Xy, int]] = set()

    for _ in range(steps + 1):
        new_active = []

        for node in active:
            if node in visited:
                continue

            visited.add(node)
            for adj_xy in grid.adjacent(node[0]):
                if grid[adj_xy] == '.':
                    new_node = adj_xy, (node[1] + 1) % 2

                    if new_node not in visited:
                        new_active.append(new_node)
        active = new_active

    return sum(1 for e in visited if e[1] == 0)


if __name__ == "__main__":
    run_day(20,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            # lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
