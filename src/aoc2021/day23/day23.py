import logging
import os
from functools import total_ordering
from queue import PriorityQueue
from typing import List

from util.logger_config import init_logger

logger = logging.getLogger(__name__)

HOME_BASE = {
    'A': 3,
    'B': 5,
    'C': 7,
    'D': 9,
}
COST = {
    'A': 1,
    'B': 10,
    'C': 100,
    'D': 1_000,
}
LEGAL_TOP_ROW_STOP_X = {
    1, 2, 4, 6, 8, 10, 11
}


@total_ordering
class State:
    def __init__(self, grid: str, cost: int) -> None:
        self.grid = grid
        self.cost = cost

    def at(self, x, y) -> str:
        return self.grid[x + 13 * y]

    def set(self, x: int, y: int, value: str):
        index = x + 13 * y
        self.grid = self.grid[:index] + value + self.grid[index + 1:]

    def dump(self) -> str:
        result = 'Cost %d' % self.cost
        for x, y in self.gen_xy():
            if x == 0:
                result += "\n"
            result += self.at(x, y)

        return result

    def gen_xy(self):
        for y in range(self.height()):
            for x in range(13):
                yield x, y

    def height(self) -> int:
        return len(self.grid) // 13

    def print(self):
        print(self.dump())

    def __eq__(self, other):
        return self.cost == other.cost

    def __lt__(self, other):
        return self.cost < other.cost

    def home_free(self, item: str):
        x = HOME_BASE[item]
        ay = self.height() - 2
        while ay >= 2:
            if self.at(x, ay) not in ('.', item):
                return False
            ay -= 1

        return True

    def expand(self) -> List['State']:
        result = []

        for x, y in self.gen_xy():
            item = self.at(x, y)
            if item not in ('A', 'B', 'C', 'D'):
                continue

            # is in top row
            if y == 1:
                # is home free?
                hx = HOME_BASE[item]
                if not self.home_free(item):
                    continue

                assert hx != x
                if hx > x:
                    dx = 1
                else:
                    dx = -1

                # check path
                nx = x
                side_cost = 0

                while nx != hx:
                    nx += dx
                    side_cost += COST[item]

                    if self.at(nx, y) != '.':
                        break

                # way home blocked
                if nx != hx:
                    continue

                ny = 2
                down_cost = COST[item]
                while self.at(nx, ny + 1) == '.':
                    ny += 1
                    down_cost += COST[item]

                new_state = State(self.grid, self.cost + down_cost + side_cost)
                new_state.set(x, y, '.')
                new_state.set(nx, ny, item)

                result.append(new_state)

                # no more options for top row
                continue

            # check home base
            if self.locked_home(item, x, y):
                continue

            # can move up?
            up_cost = 0
            invalid = False
            ny = y
            while ny > 1:
                ny -= 1
                up_cost += COST[item]

                if self.at(x, ny) != '.':
                    # something in the way
                    invalid = True
                    break

            if invalid:
                continue

            # move left/right
            for dx in (-1, 1):
                nx = x
                side_cost = 0

                while True:
                    nx += dx
                    side_cost += COST[item]

                    if self.at(nx, ny) != '.':
                        break

                    if nx in LEGAL_TOP_ROW_STOP_X:
                        logger.debug('legal stop %d/%d at %d' % (nx, ny, up_cost + side_cost))

                        new_state = State(self.grid, self.cost + up_cost + side_cost)
                        new_state.set(x, y, '.')
                        new_state.set(nx, ny, item)

                        result.append(new_state)

        logger.debug("Expanded state into %d new states." % len(result))
        return result

    def locked_home(self, item: str, x: int, y: int) -> bool:
        if x != HOME_BASE[item]:
            return False

        ay = self.height() - 2
        while ay > y:
            if self.at(x, ay) != '.':
                return False
            ay -= 1

        assert ay == y

        return True

    def victory(self) -> bool:
        for item, x in HOME_BASE.items():
            if self.at(x, 2) != item:
                return False

            if self.at(x, 3) != item:
                return False

        return True


def load_data(fn: str = 'data.txt') -> State:
    result = ''

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.rstrip()

            result += "%-13s" % line

    return State(result, 0)


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def star1(initial_state: State) -> int:
    """
    >>> star1(test_data())
    12521
    """

    pq = PriorityQueue()  # type: PriorityQueue[ State]
    processed = set()
    pq.put_nowait(initial_state)

    steps = 0
    while not pq.empty():
        active = pq.get_nowait()

        if active.victory():
            return active.cost

        if active.grid in processed:
            continue

        for e in active.expand():
            if e.grid in processed:
                continue

            pq.put_nowait(e)

        processed.add(active.grid)

        if steps % 5_000 == 0:
            logger.info(
                "%-6d: $%-6d (len(pq) = %-6d | len(processed) %-6d)" % (steps, active.cost, pq.qsize(), len(processed)))
        steps += 1

    return 0


def star2(initial_state: State) -> int:
    grid = initial_state.grid[:39] + "  #D#C#B#A#    #D#B#A#C#  " + initial_state.grid[39:]
    expanded_state = State(grid, 0)

    return star1(expanded_state)


if __name__ == "__main__":
    init_logger()

    print('Day 23 Star 1: %d' % star1(load_data()))
    print('Day 23 Star 2: %d' % star2(load_data()))
