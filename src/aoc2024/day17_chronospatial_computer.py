import re
from dataclasses import dataclass, field

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


@dataclass
class Computer:
    a: int
    b: int
    c: int
    program: list[int]
    output: list[int] = field(default_factory=list)
    pointer: int = 0

    def run(self) -> None:
        """
        >>> c = Computer(0, 0, 9, [2, 6]); c.run(); c.b
        1

        >>> c = Computer(10, 0, 0, [5, 0, 5, 1, 5, 4]); c.run(); c.output
        [0, 1, 2]

        >>> c = Computer(2024, 0, 0, [0, 1, 5, 4, 3, 0]); c.run(); c.output
        [4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0]

        >>> c = Computer(0, 29, 0, [1, 7]); c.run(); c.b
        26

        >>> c = Computer(0, 2024, 43690, [4, 0]); c.run(); c.b
        44354

        """
        while self.pointer < len(self.program):
            self.tick()

    def tick(self) -> None:
        op = self.program[self.pointer]
        match op:
            case 0:
                self.adv()

            case 1:
                self.bxl()

            case 2:
                self.bst()

            case 3:
                self.jnz()

            case 4:
                self.bxc()

            case 5:
                self.out()

            case 6:
                self.bdv()

            case 7:
                self.cdv()

            case _:
                assert False, op

        self.pointer += 2

    def literal(self) -> int:
        return self.program[self.pointer + 1]

    def adv(self) -> None:
        numerator = self.a
        denominator = 2 ** self.combo()
        self.a = numerator // denominator

    def bxl(self) -> None:
        self.b = self.b ^ self.literal()

    def bst(self) -> None:
        self.b = self.combo() % 8

    def jnz(self) -> None:
        if self.a == 0:
            return

        self.pointer = self.literal() - 2

    def bxc(self) -> None:
        self.b = self.b ^ self.c

    def out(self) -> None:
        self.output.append(self.combo() % 8)

    def bdv(self) -> None:
        numerator = self.a
        denominator = 2 ** self.combo()
        self.b = numerator // denominator

    def cdv(self) -> None:
        numerator = self.a
        denominator = 2 ** self.combo()
        self.c = numerator // denominator

    def combo(self) -> int:
        operand = self.literal()
        if operand < 4:
            return operand
        if operand == 4:
            return self.a
        if operand == 5:
            return self.b
        if operand == 6:
            return self.c
        raise Exception(f'Illegal combo {operand}')


def star1(data: list[str]) -> str:
    """
    >>> star1(["Register A: 729",
    ...        "Register B: 0",
    ...        "Register C: 0",
    ...        "",
    ...        "Program: 0,1,5,4,3,0",
    ...       ])
    '4,6,3,5,6,3,5,2,1,0'
    """

    computer = Computer(
        int(re.match(r'Register A: (\d+)', data[0])[1]),  # type: ignore
        int(re.match(r'Register B: (\d+)', data[1])[1]),  # type: ignore
        int(re.match(r'Register C: (\d+)', data[2])[1]),  # type: ignore
        list(int(c) for c in (re.match(r'Program: ([0-9,]+)', data[4])[1]).split(','))  # type: ignore
    )

    computer.run()

    return ','.join(str(i) for i in computer.output)


def star2(data: list[str]) -> int:
    b = int(re.match(r'Register B: (\d+)', data[1])[1])  # type: ignore
    c = int(re.match(r'Register C: (\d+)', data[2])[1])  # type: ignore
    program = list(int(c) for c in (re.match(r'Program: ([0-9,]+)', data[4])[1]).split(','))  # type: ignore

    results = solve_rec(0, b, c, program, 0)

    return sorted(results)[0]


def solve_rec(a: int, b: int, c: int, program: list[int], index: int) -> list[int]:
    if index > 15:
        return [a]

    results = []
    for x in range(8):
        a_mod = a | x << 45 - 3 * index

        computer = Computer(
            a_mod,
            b,
            c,
            program[:]
        )
        computer.run()

        if len(computer.program) == len(computer.output) and computer.program[-(index + 1):] == computer.output[-(index + 1):]:
            results += solve_rec(a_mod, b, c, program, index + 1)

    return results


if __name__ == "__main__":
    run_day(17,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
