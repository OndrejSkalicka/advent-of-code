from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "#####",
    ...        ".####",
    ...        ".####",
    ...        ".####",
    ...        ".#.#.",
    ...        ".#...",
    ...        ".....",
    ...        "",
    ...        "#####",
    ...        "##.##",
    ...        ".#.##",
    ...        "...##",
    ...        "...#.",
    ...        "...#.",
    ...        ".....",
    ...        "",
    ...        ".....",
    ...        "#....",
    ...        "#....",
    ...        "#...#",
    ...        "#.#.#",
    ...        "#.###",
    ...        "#####",
    ...        "",
    ...        ".....",
    ...        ".....",
    ...        "#.#..",
    ...        "###..",
    ...        "###.#",
    ...        "###.#",
    ...        "#####",
    ...        "",
    ...        ".....",
    ...        ".....",
    ...        ".....",
    ...        "#....",
    ...        "#.#..",
    ...        "#.#.#",
    ...        "#####",
    ...       ])
    3
    """

    locks = []
    keys = []
    for item in l_split(data):
        numbers = []
        for i in range(len(item[0])):
            numbers.append(sum(1 for row in item if row[i] == '#') - 1)

        if '.' in item[0]:
            keys.append(numbers)
        else:
            locks.append(numbers)

    result = 0
    for lock in locks:
        for key in keys:
            for i in range(len(lock)):
                if lock[i] + key[i] > 5:
                    break
            else:
                result += 1

    return result


if __name__ == "__main__":
    run_day(25,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            )
