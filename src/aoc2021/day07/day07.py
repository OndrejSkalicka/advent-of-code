from typing import List


def load_data(fn: str) -> List[int]:
    with open(fn, 'r') as fh:
        line = fh.readline()
        return list(int(x) for x in line.strip().split(","))


def star1(data: List[int]) -> int:
    """
    >>> star1([16, 1, 2, 0, 4, 2, 7, 1, 2, 14])
    37
    """

    best_total = None
    best_pos = None
    for pos in range(min(data), max(data) + 1):
        total = sum(abs(pos - x) for x in data)

        if best_pos is None:
            best_pos = pos
            best_total = total
        elif total < best_total:
            best_pos = pos
            best_total = total

    return best_total


def star2(data: List[int]) -> int:
    """
    >>> star2([16, 1, 2, 0, 4, 2, 7, 1, 2, 14])
    168
    """

    best_total = None
    best_pos = None
    for pos in range(min(data), max(data) + 1):
        total = sum(int(abs(pos - x) * (abs(pos - x) + 1) / 2) for x in data)

        if best_pos is None:
            best_pos = pos
            best_total = total
        elif total < best_total:
            best_pos = pos
            best_total = total

    return best_total


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 7 Star 1: %d' % star1(data))
    print('Day 7 Star 2: %d' % star2(data))
