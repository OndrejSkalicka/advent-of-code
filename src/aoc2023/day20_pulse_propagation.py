import abc
import math
from collections import defaultdict
from dataclasses import dataclass, field

from util.data import load_strings_newline, txt_fn
from util.perf import run_day

LOW = False
HIGH = True


@dataclass
class Pulse:
    source: str
    destination: str
    high: bool


@dataclass
class Module:
    name: str
    destinations: list[str]

    @abc.abstractmethod
    def receive(self, pulse: Pulse) -> list[Pulse]:
        pass


@dataclass
class FlipFlop(Module):
    on: bool = False

    def receive(self, pulse: Pulse) -> list[Pulse]:
        if pulse.high:
            return []

        self.on = not self.on

        return list(Pulse(self.name, d, self.on) for d in self.destinations)


@dataclass
class Conjunction(Module):
    sources: dict[str, bool] = field(default_factory=dict)

    def receive(self, pulse: Pulse) -> list[Pulse]:
        self.sources[pulse.source] = pulse.high

        if sum(1 for e in self.sources.values() if e == LOW) == 0:
            return list(Pulse(self.name, d, LOW) for d in self.destinations)
        else:
            return list(Pulse(self.name, d, HIGH) for d in self.destinations)


@dataclass
class Broadcaster(Module):

    def receive(self, pulse: Pulse) -> list[Pulse]:
        return list(Pulse(self.name, dest, pulse.high) for dest in self.destinations)


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "broadcaster -> a, b, c",
    ...     "%a -> b",
    ...     "%a -> b",
    ...     "%a -> b",
    ...     "%a -> b",
    ...     "%b -> c",
    ...     "%c -> inv",
    ...     "&inv -> a",
    ... ])
    32000000

    >>> star1([
    ...     "broadcaster -> a",
    ...     "%a -> inv, con",
    ...     "&inv -> b",
    ...     "%b -> con",
    ...     "&con -> output",
    ... ])
    11687500
    """

    sources: dict[str, set[str]] = defaultdict(set)
    modules = {}
    for row in data:
        module: Module
        name, dest = row.split(' -> ')
        destinations = dest.split(', ')

        if name.startswith('%'):
            module = FlipFlop(name[1:], destinations)
        elif name.startswith('&'):
            module = Conjunction(name[1:], destinations)
        else:
            assert name == 'broadcaster'
            module = Broadcaster(name, destinations)

        modules[module.name] = module

        for d in destinations:
            sources[d].add(module.name)

    for module in modules.values():
        if isinstance(module, Conjunction):
            for source in sources[module.name]:
                module.sources[source] = LOW
    high = 0
    low = 0

    for _ in range(1_000):
        pulses: list[Pulse] = [Pulse('button', 'broadcaster', LOW)]
        while pulses:
            pulse = pulses.pop(0)
            if pulse.high:
                high += 1
            else:
                low += 1

            if pulse.destination not in modules:
                continue

            pulses += modules[pulse.destination].receive(pulse)

    return low * high


def star2(data: list[str]) -> int:
    sources: dict[str, set[str]] = defaultdict(set)
    modules = {}
    for row in data:
        module: Module
        name, dest = row.split(' -> ')
        destinations = dest.split(', ')

        if name.startswith('%'):
            module = FlipFlop(name[1:], destinations)
        elif name.startswith('&'):
            module = Conjunction(name[1:], destinations)
        else:
            assert name == 'broadcaster'
            module = Broadcaster(name, destinations)

        modules[module.name] = module

        for d in destinations:
            sources[d].add(module.name)

    for module in modules.values():
        if isinstance(module, Conjunction):
            for source in sources[module.name]:
                module.sources[source] = LOW

    button = 0
    periods: dict[str, list[int]] = {}

    for source in sources[list(sources['rx'])[0]]:
        periods[source] = []

    while True:
        button += 1
        pulses: list[Pulse] = [Pulse('button', 'broadcaster', LOW)]

        while pulses:
            pulse = pulses.pop(0)

            if not pulse.high and pulse.destination == 'rx':
                return button

            if pulse.destination not in modules:
                continue

            pulses += modules[pulse.destination].receive(pulse)

            if not pulse.high and pulse.destination in periods.keys():
                periods[pulse.destination].append(button)

        if sum(1 for e in periods.values() if not e) == 0:
            return math.lcm(*list(e[0] for e in periods.values()))


if __name__ == "__main__":
    run_day(20,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
