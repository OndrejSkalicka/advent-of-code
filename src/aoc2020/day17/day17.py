from typing import Dict
from typing import List, Tuple
from typing import Set


def load_data(fn: str) -> List[Tuple[int, int]]:
    active_cubes = []

    with open(fn, 'r') as fh:
        y = 0
        for line in fh.readlines():
            line = line.strip()

            for x, val in enumerate(line):
                if val == '#':
                    active_cubes.append((x, y))

            y += 1

    return active_cubes


def star1(data: List[Tuple[int, int]]) -> int:
    """
    >>> star1([(1, 2), (2, 1), (0, 0), (1, 0), (2, 0)])
    112
    """

    cycles = 6
    active_cubes = set((cube[0], cube[1], 0) for cube in data)
    vector = vector_3d()

    for cycle in range(cycles):
        new_active_cubes = set()
        neighbor_count_map = {}  # type: Dict[Tuple[int, int, int], int]

        for active_cube in active_cubes:
            for (dx, dy, dz) in vector:
                cube = (active_cube[0] + dx, active_cube[1] + dy, active_cube[2] + dz)

                neighbor_count_map[cube] = neighbor_count_map.get(cube, 0) + 1

        for cube, neighbor_count in neighbor_count_map.items():
            if cube in active_cubes:
                # active => active
                if neighbor_count in (2, 3):
                    new_active_cubes.add(cube)

                # active => inactive
                pass
            else:
                # inactive => active
                if neighbor_count == 3:
                    new_active_cubes.add(cube)

        active_cubes = new_active_cubes

    return len(active_cubes)


def vector_3d() -> List[Tuple[int, int, int]]:
    result = []
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            for dz in (-1, 0, 1):
                if dx == 0 and dy == 0 and dz == 0:
                    continue

                result.append((dx, dy, dz))

    return result


def star2(data: List[Tuple[int, int]]) -> int:
    """
    >>> star2([(1, 2), (2, 1), (0, 0), (1, 0), (2, 0)])
    848
    """

    cycles = 6
    active_cubes = set((cube[0], cube[1], 0, 0) for cube in data)
    vector = vector_4d()

    for cycle in range(cycles):
        new_active_cubes = set()
        neighbor_count_map = {}  # type: Dict[Tuple[int, int, int, int], int]

        for active_cube in active_cubes:
            for (dx, dy, dz, dt) in vector:
                cube = (active_cube[0] + dx, active_cube[1] + dy, active_cube[2] + dz, active_cube[3] + dt)

                neighbor_count_map[cube] = neighbor_count_map.get(cube, 0) + 1

        for cube, neighbor_count in neighbor_count_map.items():
            if cube in active_cubes:
                # active => active
                if neighbor_count in (2, 3):
                    new_active_cubes.add(cube)

                # active => inactive
                pass
            else:
                # inactive => active
                if neighbor_count == 3:
                    new_active_cubes.add(cube)

        active_cubes = new_active_cubes

    return len(active_cubes)


def vector_4d() -> List[Tuple[int, int, int, int]]:
    result = []
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            for dz in (-1, 0, 1):
                for dt in (-1, 0, 1):
                    if dx == 0 and dy == 0 and dz == 0 and dt == 0:
                        continue

                    result.append((dx, dy, dz, dt))

    return result


def count_neighbors_4d(active_cube: Tuple[int, int, int, int], active_cubes: Set[Tuple[int, int, int, int]]) -> int:
    count = 0
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            for dz in (-1, 0, 1):
                for dt in (-1, 0, 1):
                    if dx == 0 and dy == 0 and dz == 0 and dt == 0:
                        continue

                    if (active_cube[0] + dx, active_cube[1] + dy, active_cube[2] + dz, active_cube[3] + dt) in active_cubes:
                        count += 1

    return count


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 17 Star 1: %d' % star1(my_data))
    print('Day 17 Star 2: %d' % star2(my_data))
