import os
from typing import Optional, TypeVar, Iterable

import numpy as np

from util.dtype import np_array_2d_str, np_array_2d_int

T = TypeVar('T')


def txt_fn(script_path: str, suffix: str = '.txt') -> str:
    return os.path.join(os.path.dirname(os.path.realpath(script_path)), os.path.basename(script_path).split('.')[0] + suffix)


def load_as_str(fn: str) -> str:
    with open(fn, 'r') as fh:
        return ''.join(fh.readlines())


def load_numbers_newline(fn: str) -> list[int]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            if line == '':
                continue
            result.append(int(line))

    return result


def load_strings_newline(fn: str, ignore_blank: bool = False) -> list[str]:
    result: list[str] = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            if line == '':
                continue

            if line == '\n' and ignore_blank:
                continue

            result.append(line.strip("\n"))

    return result


def load_numbers_csv(fn: str) -> list[int]:
    with open(fn, 'r') as fh:
        return list(int(x) for x in fh.readline().split(','))


def load_line(fn: str) -> str:
    with open(fn, 'r') as fh:
        return fh.readline().strip()


def to_2d_np_char(text: str, rpad: Optional[str] = None) -> np_array_2d_str:
    """
    If rpad is not-none, right-pad all strings to same width
    """
    if rpad is None:
        return np.array([list(row) for row in text.split("\n")], dtype=str)

    max_width = max(len(x) for x in text.split("\n"))
    return np.array([list(row.ljust(max_width, rpad)) for row in text.split("\n")], dtype=str)


def to_2d_np_int(data: list[str]) -> np_array_2d_int:
    return np.array([[int(c) for c in row] for row in data], dtype=int)


def l_split(data: list[T], delimiter: Optional[T] = None) -> list[list[T]]:
    result: list[list[T]] = []
    group: list[T] = []

    for item in data:
        if (delimiter is not None and item == delimiter) or (delimiter is None and not item):
            result.append(group)
            group = []

        else:
            group.append(item)

    if group:
        result.append(group)

    return result


def split_chunks(data: Iterable[T], chunk_size: int) -> list[list[T]]:
    result: list[list[T]] = []

    for d in data:
        if not result or len(result[-1]) == chunk_size:
            result.append([])

        result[-1].append(d)

    return result
