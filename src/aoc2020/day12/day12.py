from typing import List, Tuple


def load_data(fn: str) -> List[str]:
    with open(fn, 'r') as fh:
        return list(line.strip() for line in fh.readlines())


def star1(data: List[str]) -> int:
    """
    >>> star1(['F10', 'N3', 'F7', 'R90', 'F11'])
    25
    """

    directions = (
        (0, -1),  # N
        (1, 0),  # E
        (0, 1),  # S
        (-1, 0),  # W
    )
    direction = 1
    x = 0
    y = 0

    for action in data:
        a = action[0]
        v = int(action[1:])

        if a == 'N':
            y -= v

        elif a == 'E':
            x += v

        elif a == 'S':
            y += v

        elif a == 'W':
            x -= v

        elif a == 'F':
            x += directions[direction][0] * v
            y += directions[direction][1] * v

        elif a == 'R':
            direction = (direction + int(v / 90)) % 4

        elif a == 'L':
            direction = (direction - int(v / 90)) % 4

        else:
            raise Exception("Illegal action %s" % action)

    return abs(x) + abs(y)


def star2(data: List[str]) -> int:
    """
    >>> star2(['F10', 'N3', 'F7', 'R90', 'F11'])
    286
    """

    x = 0
    y = 0
    wx = 10
    wy = -1

    for action in data:
        a = action[0]
        v = int(action[1:])

        if a == 'N':
            wy -= v

        elif a == 'E':
            wx += v

        elif a == 'S':
            wy += v

        elif a == 'W':
            wx -= v

        elif a == 'F':
            x += v * wx
            y += v * wy

        elif a == 'R':
            wx, wy = rot(wx, wy, int(v / 90))

        elif a == 'L':
            wx, wy = rot(wx, wy, 4 - int(v / 90))

        else:
            raise Exception("Illegal action %s" % action)

    return abs(x) + abs(y)


def rot(x: int, y: int, d: int) -> Tuple[int, int]:
    for i in range(d):
        bx = x
        x = -y
        y = bx

    return x, y


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 12 Star 1: %d' % star1(my_data))
    print('Day 12 Star 2: %d' % star2(my_data))
