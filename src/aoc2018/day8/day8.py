from typing import Sequence, List  # noqa


def star_1_metadata(numbers: Sequence[int]) -> int:
    """
    >>> star_1_metadata((2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2))
    138
    """
    root = create_tree(numbers)

    total = 0
    for node in root:
        total += sum(node.meta)

    return total


def star_2_values(numbers: Sequence[int]) -> int:
    """
    >>> star_2_values((2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2))
    66
    """
    root = create_tree(numbers)

    return root.value()


class Node:
    def __init__(self) -> None:
        self.parent = None
        self.children = []  # type: List[Node]
        self.children_cnt = None
        self.meta = []
        self.meta_cnt = None

    def value(self) -> int:
        if not self.children:
            return sum(self.meta)

        total = 0

        for meta in self.meta:
            if 0 < meta <= len(self.children):
                total += self.children[meta - 1].value()

        return total

    def __repr__(self) -> str:
        return '{#C %d, #M %d}' % (self.children_cnt, self.meta_cnt)

    def __iter__(self):
        """
        Implement iterator
        """
        yield self

        for child in self.children:
            yield from child


def create_tree(numbers: Sequence[int]) -> Node:
    stack = []
    top = None
    for i, num in enumerate(numbers):
        # root
        if len(stack) == 0:
            node = Node()
            node.children_cnt = num
            stack.append(node)

            continue

        top = stack[-1]

        # second info byte
        if top.meta_cnt is None:
            top.meta_cnt = num
            continue

        # still missing children
        if len(top.children) < top.children_cnt:
            node = Node()
            node.parent = top
            node.children_cnt = num
            top.children.append(node)
            stack.append(node)
            continue

        # still missing metas
        if len(top.meta) < top.meta_cnt:
            top.meta.append(num)

            if len(top.meta) == top.meta_cnt:
                stack.pop()

            continue

        raise Exception("Unexpected argument %d at %d" % (num, i))

    if top is None:
        raise Exception('Root not found')

    return top


def _name_gen():
    i = 0
    while True:
        yield 'N%d' % i
        i += 1


def load_data(fn: str) -> Sequence[int]:
    with open(fn, 'r') as fh:
        return list(int(c) for c in fh.read().split(' '))


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 8 Star 1: %s' % star_1_metadata(data))
    print('Day 8 Star 2: %s' % star_2_values(data))
