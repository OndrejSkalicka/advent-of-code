from dataclasses import dataclass

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy, NORTH, WEST, SOUTH, EAST


@dataclass(frozen=True)
class BeamType:
    mask: int
    dxy_next: Xy

    def __hash__(self) -> int:
        return super().__hash__()


BEAM_EAST = BeamType(0b0001, EAST)
BEAM_SOUTH = BeamType(0b0010, SOUTH)
BEAM_WEST = BeamType(0b0100, WEST)
BEAM_NORTH = BeamType(0b1000, NORTH)

LEFT = {
    BEAM_EAST: BEAM_NORTH,
    BEAM_SOUTH: BEAM_EAST,
    BEAM_WEST: BEAM_SOUTH,
    BEAM_NORTH: BEAM_WEST,
}
RIGHT = {
    BEAM_EAST: BEAM_SOUTH,
    BEAM_SOUTH: BEAM_WEST,
    BEAM_WEST: BEAM_NORTH,
    BEAM_NORTH: BEAM_EAST,
}


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     ".|...\\\\....",
    ...     "|.-.\\\\.....",
    ...     ".....|-...",
    ...     "........|.",
    ...     "..........",
    ...     ".........\\\\",
    ...     "..../.\\\\\\\\..",
    ...     ".-.-/..|..",
    ...     ".|....-|.\\\\",
    ...     "..//.|....",
    ... ])
    46
    """

    grid = Grid.from_list_str(data)

    return _energize(grid, Xy(0, 0), BEAM_EAST)


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     ".|...\\\\....",
    ...     "|.-.\\\\.....",
    ...     ".....|-...",
    ...     "........|.",
    ...     "..........",
    ...     ".........\\\\",
    ...     "..../.\\\\\\\\..",
    ...     ".-.-/..|..",
    ...     ".|....-|.\\\\",
    ...     "..//.|....",
    ... ])
    51
    """

    grid = Grid.from_list_str(data)
    results = []

    for x in range(grid.width):
        results.append(_energize(grid, Xy(x, 0), BEAM_SOUTH))
        results.append(_energize(grid, Xy(x, grid.height - 1), BEAM_NORTH))

    for y in range(grid.height):
        results.append(_energize(grid, Xy(0, y), BEAM_EAST))
        results.append(_energize(grid, Xy(grid.width - 1, y), BEAM_WEST))

    return max(results)


def _next(beam_type: BeamType, field: str) -> list[BeamType]:
    result: list[BeamType] = []

    if field == '.':
        return [beam_type]

    if field == '-':
        if beam_type in (BEAM_EAST, BEAM_WEST):
            return [beam_type]

        return [
            (LEFT[beam_type]),
            (RIGHT[beam_type]),
        ]

    if field == '|':
        if beam_type in (BEAM_NORTH, BEAM_SOUTH):
            return [beam_type]

        return [
            (LEFT[beam_type]),
            (RIGHT[beam_type]),
        ]

    if field == '/':
        if beam_type in (BEAM_EAST, BEAM_WEST):
            return [LEFT[beam_type]]
        else:
            return [RIGHT[beam_type]]

    if field == '\\':
        if beam_type in (BEAM_EAST, BEAM_WEST):
            return [RIGHT[beam_type]]
        else:
            return [LEFT[beam_type]]

    return result


def _energize(grid: Grid[str], start_xy: Xy, start_type: BeamType) -> int:
    energy = Grid.constant(0, grid.width, grid.height)
    buffer: list[tuple[Xy, BeamType]] = [(start_xy, start_type)]

    while buffer:
        xy, beam_type = buffer.pop()

        if xy not in grid:
            continue

        if energy[xy] & beam_type.mask:
            continue

        energy[xy] |= beam_type.mask

        for bt in _next(beam_type, grid[xy]):
            buffer.append((xy + bt.dxy_next, bt))

    return sum(1 for c in energy if c.value > 0)


if __name__ == "__main__":
    run_day(16,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
