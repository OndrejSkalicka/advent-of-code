from dataclasses import dataclass, field
from typing import Optional

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


@dataclass(frozen=True)
class File:
    name: str
    size: int


@dataclass
class Dir:
    name: str
    parent: Optional['Dir'] = None
    size_cache: Optional[int] = None
    files: dict[str, File] = field(default_factory=dict, init=False)
    directories: dict[str, 'Dir'] = field(default_factory=dict, init=False)

    def cd(self, directory: str) -> 'Dir':
        if directory == '..':
            assert self.parent is not None
            return self.parent

        assert directory in self.directories

        return self.directories[directory]

    def add_dir(self, directory: str) -> None:
        if directory not in self.directories:
            self.directories[directory] = Dir(name=directory, parent=self)

    def add_file(self, file: str, size: int) -> None:
        if file not in self.files:
            self.files[file] = File(name=file, size=size)

    def total_size(self) -> int:
        if self.size_cache is not None:
            return self.size_cache

        return sum(file.size for file in self.files.values()) + sum(directory.total_size() for directory in self.directories.values())


def star1(data: list[str]) -> int:
    """
    >>> star1(["$ cd /",
    ...        "$ ls",
    ...        "dir a",
    ...        "14848514 b.txt",
    ...        "8504156 c.dat",
    ...        "dir d",
    ...        "$ cd a",
    ...        "$ ls",
    ...        "dir e",
    ...        "29116 f",
    ...        "2557 g",
    ...        "62596 h.lst",
    ...        "$ cd e",
    ...        "$ ls",
    ...        "584 i",
    ...        "$ cd ..",
    ...        "$ cd ..",
    ...        "$ cd d",
    ...        "$ ls",
    ...        "4060174 j",
    ...        "8033020 d.log",
    ...        "5626152 d.ext",
    ...        "7214296 k",
    ...        ])
    95437
    """

    root = _parse(data)

    result = 0
    buffer = [root]

    while buffer:
        current = buffer.pop()
        buffer += current.directories.values()
        if current.total_size() < 100_000:
            result += current.total_size()

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["$ cd /",
    ...        "$ ls",
    ...        "dir a",
    ...        "14848514 b.txt",
    ...        "8504156 c.dat",
    ...        "dir d",
    ...        "$ cd a",
    ...        "$ ls",
    ...        "dir e",
    ...        "29116 f",
    ...        "2557 g",
    ...        "62596 h.lst",
    ...        "$ cd e",
    ...        "$ ls",
    ...        "584 i",
    ...        "$ cd ..",
    ...        "$ cd ..",
    ...        "$ cd d",
    ...        "$ ls",
    ...        "4060174 j",
    ...        "8033020 d.log",
    ...        "5626152 d.ext",
    ...        "7214296 k",
    ...        ])
    24933642
    """

    root = _parse(data)

    sizes: dict[str, int] = dict()
    buffer = [root]

    while buffer:
        current = buffer.pop()
        buffer += current.directories.values()
        sizes[current.name] = current.total_size()

    total = root.total_size()

    for size in sorted(sizes.values()):
        if total - size < 40_000_000:
            return size

    raise Exception("404")


def _parse(data: list[str]) -> Dir:
    root = Dir('/')
    cwd = root

    for row in data:
        if row == '$ cd /':
            cwd = root
            continue

        if row.startswith('$ cd '):
            cwd = cwd.cd(row[len('$ cd '):])
            continue

        if row.startswith('dir '):
            cwd.add_dir(row[len('dir '):])
            continue

        if row == '$ ls':
            continue

        size, file = row.split(' ')
        cwd.add_file(file, int(size))

    return root


if __name__ == "__main__":
    run_day(6,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
