from util.data import load_numbers_newline


def star1(masses: list[int]) -> int:
    return sum(fuel(mass) for mass in masses)


def star2(masses: list[int]) -> int:
    return sum(rec_fuel(mass) for mass in masses)


def fuel(mass: int) -> int:
    """
    >>> fuel(12)
    2

    >>> fuel(14)
    2

    >>> fuel(1969)
    654

    >>> fuel(100756)
    33583
    """

    return max(0, mass // 3 - 2)


def rec_fuel(mass: int) -> int:
    """
    >>> rec_fuel(14)
    2

    >>> rec_fuel(1969)
    966

    >>> rec_fuel(100756)
    50346
    """

    total = 0

    while mass > 0:
        partial = fuel(mass)
        total += partial
        mass = partial

    return total


if __name__ == "__main__":
    print('Day 1 Star 1: %d' % star1(load_numbers_newline('data.txt')))
    print('Day 1 Star 2: %d' % star2(load_numbers_newline('data.txt')))
