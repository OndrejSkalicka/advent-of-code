from typing import List


def load_data(fn: str) -> List[int]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            result.append(int(line))

    return result


def star1(data: List[int]) -> int:
    """
    >>> star1([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
    7
    """

    last = None
    result = 0
    for x in data:
        if last is not None:
            if x > last:
                result += 1

        last = x

    return result


def star2(data: List[int]) -> int:
    """
    >>> star2([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
    5
    """

    d_1 = None
    d_2 = None
    d_3 = None
    result = 0
    for x in data:
        if d_3 is not None:
            if d_3 + d_2 + d_1 < d_2 + d_1 + x:
                result += 1

        d_3 = d_2
        d_2 = d_1
        d_1 = x

    return result


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 1 Star 1: %d' % star1(data))
    print('Day 1 Star 2: %d' % star2(data))
