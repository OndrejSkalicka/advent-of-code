import numpy as np

from util.data import load_line


def star1(digits: list[int], width: int, height: int) -> int:
    """
    >>> star1([1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2], 3, 2)
    1
    """

    layers = np.zeros((len(digits) // width // height, width, height), dtype=int)

    for i, d in enumerate(digits):
        layers[i // width // height, i % width, i // width % height] = d

    fewest_zeroes_layer = None
    for layer in layers:
        if fewest_zeroes_layer is None or (layer == 0).sum() < (fewest_zeroes_layer == 0).sum():
            fewest_zeroes_layer = layer

    return (fewest_zeroes_layer == 1).sum() * (fewest_zeroes_layer == 2).sum()  # type: ignore


def star2(digits: list[int], width: int, height: int) -> str:
    """
    >>> star2([0,2,2,2,1,1,2,2,2,2,1,2,0,0,0,0], 2, 2)
    '01\\n10'
    """

    layers = np.zeros((len(digits) // width // height, width, height), dtype=int)

    for i, d in enumerate(digits):
        layers[i // width // height, i % width, i // width % height] = d

    final_image = np.zeros((width, height), dtype=int)
    for x in range(width):
        for y in range(height):
            final_image[x, y] = layers[:, x, y][layers[:, x, y] != 2][0]
    result = ""
    for y in range(height):
        for x in range(width):
            result += str(final_image[x, y])
        result += "\n"

    return result.strip()


if __name__ == "__main__":
    print('Day 7 Star 1: %d' % star1([int(x) for x in load_line('data.txt')], width=25, height=6))
    print('Day 7 Star 2:\n%s' % star2([int(x) for x in load_line('data.txt')], width=25, height=6).replace('1', '#').replace('0', ' '))
