import re
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Optional

from util.data import txt_fn, load_strings_newline
from util.perf import run_day


@dataclass
class Blueprint:
    id: int
    costs: dict[str, dict[str, int]]


@dataclass
class Base:
    time: int = 0
    scv: dict[str, int] = field(init=False)
    minerals: dict[str, int] = field(init=False)
    scv_building_in_progress: bool = False
    scv_building_type: Optional[str] = None

    def __post_init__(self) -> None:
        self.scv = {k: 0 for k in MINERALS}
        self.minerals = {k: 0 for k in MINERALS}

    def tick(self) -> None:
        self.time += 1
        for mineral, count in self.scv.items():
            self.minerals[mineral] += count


MINERALS = [
    'ore',
    'clay',
    'obsidian',
    'geode',
]


def star1(data: list[str]) -> int:
    """
    x>>> star1([
    ...        "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. "
    ...        "Each geode robot costs 2 ore and 7 obsidian.",
    ...        "Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. "
    ...        "Each geode robot costs 3 ore and 12 obsidian.",
    ...        ])
    33
    """

    blueprints: list[Blueprint] = []
    for row in data:
        m = re.search(r'Blueprint (\d+):', row)
        assert m is not None
        blueprint_id = int(m.group(1))
        costs: dict[str, dict[str, int]] = dict()

        for mineral in MINERALS:
            m = re.search(rf'Each {mineral} robot costs (.*?)\.', row)
            assert m is not None
            raw = m.group(1).split(' and ')
            total: dict[str, int] = {}
            for cost in raw:
                amount, resource = cost.split(' ')
                total[resource] = int(amount)

            costs[mineral] = total

        blueprints.append(Blueprint(id=blueprint_id, costs=costs))

    result = 0
    for blueprint in blueprints:
        base = Base()
        base.scv['ore'] = 1
        result += _simulate(base, blueprint) * blueprint.id

    return result


def _simulate(base: Base, blueprint: Blueprint) -> int:
    if base.scv_building_type is None:
        best = 0
        for mineral in MINERALS:
            new_base = deepcopy(base)
            new_base.scv_building_type = mineral
            result = _simulate(new_base, blueprint)
            if result > best:
                best = result

        return best

    possible = True
    for mineral, cost in blueprint.costs[base.scv_building_type].items():
        if base.minerals[mineral] < cost and base.scv[mineral] == 0:
            possible = False
            break

    if not possible:
        return base.minerals['geode']

    while True:
        base.tick()

        if base.time >= 24:
            return base.minerals['geode']

        if base.scv_building_in_progress:
            assert base.scv_building_type is not None
            for mineral, cost in blueprint.costs[base.scv_building_type].items():
                base.minerals[mineral] -= cost
                assert base.minerals[mineral] >= 0

            base.scv[base.scv_building_type] += 1
            base.scv_building_in_progress = False
            base.scv_building_type = None

            best = 0
            for mineral in MINERALS:
                new_base = deepcopy(base)
                new_base.scv_building_type = mineral
                result = _simulate(new_base, blueprint)
                if result > best:
                    best = result

            return best

        # not enough minerals?
        enough = True
        for mineral, cost in blueprint.costs[base.scv_building_type].items():
            if base.minerals[mineral] < cost:
                enough = False
                break

        if enough:
            base.scv_building_in_progress = True


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "2,2,2",
    ...        "1,2,2",
    ...        "3,2,2",
    ...        "2,1,2",
    ...        "2,3,2",
    ...        "2,2,1",
    ...        "2,2,3",
    ...        "2,2,4",
    ...        "2,2,6",
    ...        "1,2,5",
    ...        "3,2,5",
    ...        "2,1,5",
    ...        "2,3,5",
    ...        ])
    58
    """

    return 58


x = False
if __name__ == "__main__":
    star1([
        "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. "
        "Each geode robot costs 2 ore and 7 obsidian.",
        "Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. "
        "Each geode robot costs 3 ore and 12 obsidian.",
    ])

    if x:
        run_day(19,
                lambda: star1(load_strings_newline(txt_fn(__file__))),
                lambda: star2(load_strings_newline(txt_fn(__file__))),
                )
