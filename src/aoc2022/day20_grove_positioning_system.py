from typing import Optional

from util.data import txt_fn, load_numbers_newline
from util.perf import run_day


def star1(data: list[int]) -> int:
    """
    >>> star1([
    ...        1,
    ...        2,
    ...        -3,
    ...        3,
    ...        -2,
    ...        0,
    ...        4,
    ...        ])
    3
    """

    decrypted: list[tuple[int, int]] = []
    zero: Optional[tuple[int, int]] = None
    for i, v in enumerate(data):
        decrypted.append((i, v))
        if v == 0:
            assert zero is None
            zero = (i, v)
    assert zero is not None

    for i, v in enumerate(data):
        element = (i, v)

        old_index = decrypted.index(element)
        new_index = (old_index + v) % (len(decrypted) - 1)
        if new_index == 0:
            new_index = len(decrypted)

        if old_index == new_index:
            continue

        # move right
        if old_index < new_index:
            decrypted = decrypted[:old_index] + decrypted[old_index + 1:new_index + 1] + [element] + decrypted[new_index + 1:]
        else:
            decrypted = decrypted[:new_index] + [element] + decrypted[new_index:old_index] + decrypted[old_index + 1:]

    index_zero = decrypted.index(zero)
    return (
            decrypted[(1000 + index_zero) % len(decrypted)][1]
            + decrypted[(2000 + index_zero) % len(decrypted)][1]
            + decrypted[(3000 + index_zero) % len(decrypted)][1]
    )


def star2(data: list[int]) -> int:
    """
    >>> star2([
    ...        1,
    ...        2,
    ...        -3,
    ...        3,
    ...        -2,
    ...        0,
    ...        4,
    ...        ])
    1623178306
    """

    decrypted: list[tuple[int, int]] = []
    zero: Optional[tuple[int, int]] = None
    for i, v in enumerate(data):
        decrypted.append((i, v * 811589153))
        if v == 0:
            assert zero is None
            zero = (i, v)
    assert zero is not None

    original: list[tuple[int, int]] = decrypted[:]
    for _ in range(10):
        for element in original:

            old_index = decrypted.index(element)
            new_index = (old_index + element[1]) % (len(decrypted) - 1)
            if new_index == 0:
                new_index = len(decrypted)

            if old_index == new_index:
                continue

            # move right
            if old_index < new_index:
                decrypted = decrypted[:old_index] + decrypted[old_index + 1:new_index + 1] + [element] + decrypted[new_index + 1:]
            else:
                decrypted = decrypted[:new_index] + [element] + decrypted[new_index:old_index] + decrypted[old_index + 1:]

    index_zero = decrypted.index(zero)
    return (
            decrypted[(1000 + index_zero) % len(decrypted)][1]
            + decrypted[(2000 + index_zero) % len(decrypted)][1]
            + decrypted[(3000 + index_zero) % len(decrypted)][1]
    )


if __name__ == "__main__":
    run_day(20,
            lambda: star1(load_numbers_newline(txt_fn(__file__))),
            lambda: star2(load_numbers_newline(txt_fn(__file__))),
            )
