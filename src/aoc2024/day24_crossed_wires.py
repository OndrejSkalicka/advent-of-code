import itertools
import re
from collections import defaultdict
from dataclasses import field, dataclass

from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day


@dataclass(frozen=True)
class Gate:
    in1: str
    in2: str
    op: str
    out: str

    @staticmethod
    def from_str(gate: str) -> 'Gate':
        m = re.match(r'^(\w+) (AND|OR|XOR) (\w+) -> (\w+)$', gate)
        assert m
        return Gate(m[1], m[3], m[2], m[4])

    def evaluate(self, in1: int, in2: int) -> int:
        match self.op:
            case 'AND':
                return in1 & in2
            case 'OR':
                return in1 | in2
            case 'XOR':
                return in1 ^ in2
            case _:
                assert False


@dataclass
class Calculator:
    gates: list[Gate] = field(repr=False)
    input_bits: int = field(init=False)
    gates_by_input: dict[str, list[Gate]] = field(init=False)

    def __post_init__(self) -> None:
        self.gates_by_input = defaultdict(list)
        all_inputs = set()
        for gate in self.gates:
            self.gates_by_input[gate.in1].append(gate)
            self.gates_by_input[gate.in2].append(gate)
            all_inputs.add(gate.in1)
            all_inputs.add(gate.in2)

        self.input_bits = 0
        while f'x{self.input_bits:02}' in all_inputs:
            self.input_bits += 1

    def evaluate(self, x: int, y: int) -> int:
        wires: dict[str, int] = dict()
        for i in range(self.input_bits):
            wires[f'x{i:02}'] = (x & (1 << i)) >> i
            wires[f'y{i:02}'] = (y & (1 << i)) >> i

        buffer = self.gates[:]

        while buffer:
            new_buffer = []
            for gate in buffer:
                if gate.in1 in wires and gate.in2 in wires:
                    wires[gate.out] = gate.evaluate(wires[gate.in1], wires[gate.in2])
                else:
                    new_buffer.append(gate)

            if buffer == new_buffer:
                return -1

            buffer = new_buffer

        return self.wires_to_int(wires, 'z')

    @staticmethod
    def from_strings(data: list[str]) -> 'Calculator':
        return Calculator(list(Gate.from_str(gate) for gate in data))

    @staticmethod
    def wires_to_int(wires: dict[str, int], prefix: str) -> int:
        i = 0
        res_str = ''
        while f'{prefix}{i:02}' in wires:
            res_str = str(wires[f'{prefix}{i:02}']) + res_str
            i += 1

        return int(res_str, 2)


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "x00: 1",
    ...        "x01: 1",
    ...        "x02: 1",
    ...        "y00: 0",
    ...        "y01: 1",
    ...        "y02: 0",
    ...        "",
    ...        "x00 AND y00 -> z00",
    ...        "x01 XOR y01 -> z01",
    ...        "x02 OR y02 -> z02",
    ...       ])
    4
    >>> star1([
    ...        "x00: 1",
    ...        "x01: 0",
    ...        "x02: 1",
    ...        "x03: 1",
    ...        "x04: 0",
    ...        "y00: 1",
    ...        "y01: 1",
    ...        "y02: 1",
    ...        "y03: 1",
    ...        "y04: 1",
    ...        "",
    ...        "ntg XOR fgs -> mjb",
    ...        "y02 OR x01 -> tnw",
    ...        "kwq OR kpj -> z05",
    ...        "x00 OR x03 -> fst",
    ...        "tgd XOR rvg -> z01",
    ...        "vdt OR tnw -> bfw",
    ...        "bfw AND frj -> z10",
    ...        "ffh OR nrd -> bqk",
    ...        "y00 AND y03 -> djm",
    ...        "y03 OR y00 -> psh",
    ...        "bqk OR frj -> z08",
    ...        "tnw OR fst -> frj",
    ...        "gnj AND tgd -> z11",
    ...        "bfw XOR mjb -> z00",
    ...        "x03 OR x00 -> vdt",
    ...        "gnj AND wpb -> z02",
    ...        "x04 AND y00 -> kjc",
    ...        "djm OR pbm -> qhw",
    ...        "nrd AND vdt -> hwm",
    ...        "kjc AND fst -> rvg",
    ...        "y04 OR y02 -> fgs",
    ...        "y01 AND x02 -> pbm",
    ...        "ntg OR kjc -> kwq",
    ...        "psh XOR fgs -> tgd",
    ...        "qhw XOR tgd -> z09",
    ...        "pbm OR djm -> kpj",
    ...        "x03 XOR y03 -> ffh",
    ...        "x00 XOR y04 -> ntg",
    ...        "bfw OR bqk -> z06",
    ...        "nrd XOR fgs -> wpb",
    ...        "frj XOR qhw -> z04",
    ...        "bqk OR frj -> z07",
    ...        "y03 OR x01 -> nrd",
    ...        "hwm AND bqk -> z03",
    ...        "tgd XOR rvg -> z12",
    ...        "tnw OR pbm -> gnj",
    ...       ])
    2024
    """

    wires: dict[str, int] = {}
    for wire in l_split(data)[0]:
        wires[wire.split(': ')[0]] = int(wire.split(': ')[1])

    calc = Calculator.from_strings(l_split(data)[1])
    return calc.evaluate(Calculator.wires_to_int(wires, 'x'), Calculator.wires_to_int(wires, 'y'))


def star2(data: list[str]) -> str:
    calc = Calculator.from_strings(l_split(data)[1])
    swaps = []

    for bit in range(0, calc.input_bits):
        if not verify_at_bit(calc, bit):
            reachable_gates = set()
            buffer = calc.gates_by_input[f'x{bit:02}'] + calc.gates_by_input[f'x{bit - 1:02}']
            for _ in range(2):
                new_buffer = []
                for active in buffer:
                    if active in reachable_gates:
                        continue

                    reachable_gates.add(active)
                    new_buffer += calc.gates_by_input[active.out]

                buffer = new_buffer

            for swap_a, swap_b in itertools.combinations(reachable_gates, 2):
                gates = []
                for gate in calc.gates:
                    if gate == swap_a:
                        gates.append(Gate(swap_a.in1, swap_a.in2, swap_a.op, swap_b.out))
                    elif gate == swap_b:
                        gates.append(Gate(swap_b.in1, swap_b.in2, swap_b.op, swap_a.out))
                    else:
                        gates.append(gate)

                updated_calc = Calculator(gates)

                if verify_from_to_to_bit(updated_calc, bit - 1, bit + 1):
                    calc = updated_calc
                    swaps.append(swap_a.out)
                    swaps.append(swap_b.out)
                    break
            else:
                assert False, f"Cannot fix gates at bit {bit}"

    return ','.join(sorted(swaps))


def verify_from_to_to_bit(calc: Calculator, min_bit: int, max_bit: int) -> bool:
    for bit in range(min_bit, max_bit + 1):
        if not verify_at_bit(calc, bit):
            return False

    return True


def verify_at_bit(calc: Calculator, bit: int) -> bool:
    test_numbers = [0, 1, 2, 3]

    for x in test_numbers:
        x <<= max(0, (bit - 2))
        for y in test_numbers:
            y <<= max(0, (bit - 2))
            actual = calc.evaluate(x, y)
            expected = x + y
            if actual != expected:
                return False

    return True


if __name__ == "__main__":
    run_day(24,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
