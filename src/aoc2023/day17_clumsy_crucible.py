from dataclasses import dataclass
from functools import total_ordering
from typing import Iterable

from util.data import load_strings_newline, txt_fn
from util.dijkstra import dijkstra
from util.perf import run_day
from util.record import Grid, Xy, EAST


@total_ordering
@dataclass(frozen=True)
class State:
    xy: Xy
    direction: Xy
    straight_distance: int

    def __lt__(self, other: 'State') -> bool:
        return (self.straight_distance, self.direction) < (other.straight_distance, other.direction)


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "2413432311323",
    ...     "3215453535623",
    ...     "3255245654254",
    ...     "3446585845452",
    ...     "4546657867536",
    ...     "1438598798454",
    ...     "4457876987766",
    ...     "3637877979653",
    ...     "4654967986887",
    ...     "4564679986453",
    ...     "1224686865563",
    ...     "2546548887735",
    ...     "4322674655533",
    ... ])
    102
    """

    grid = Grid.from_list_str_to_int(data)

    distances, _ = dijkstra(
        State(Xy(0, 0, ), EAST, 0),
        lambda point: _adj(point, grid)
    )

    return min(distances[d] for d in distances if d.xy == Xy(grid.width - 1, grid.height - 1))


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "2413432311323",
    ...     "3215453535623",
    ...     "3255245654254",
    ...     "3446585845452",
    ...     "4546657867536",
    ...     "1438598798454",
    ...     "4457876987766",
    ...     "3637877979653",
    ...     "4654967986887",
    ...     "4564679986453",
    ...     "1224686865563",
    ...     "2546548887735",
    ...     "4322674655533",
    ... ])
    94

    >>> star2([
    ...     "111111111111",
    ...     "999999999991",
    ...     "999999999991",
    ...     "999999999991",
    ...     "999999999991",
    ... ])
    71
    """

    grid = Grid.from_list_str_to_int(data)

    distances, _ = dijkstra(
        State(Xy(0, 0, ), EAST, 0),
        lambda point: _adj(point, grid, max_straight=10, min_turn=4)
    )

    return min(distances[d] for d in distances if d.xy == Xy(grid.width - 1, grid.height - 1) and d.straight_distance > 3)


def _adj(state: State, grid: Grid[int], max_straight: int = 3, min_turn: int = 0) -> Iterable[tuple[State, int]]:
    result = []

    if state.straight_distance < max_straight:
        xy = state.xy + state.direction
        if xy in grid:
            result.append(
                (
                    State(xy, state.direction, state.straight_distance + 1),
                    grid[xy]
                )
            )

    if state.straight_distance >= min_turn:
        left = state.direction.rot_ccw()
        xy = state.xy + left
        if xy in grid:
            if xy in grid:
                result.append(
                    (
                        State(xy, left, 1),
                        grid[xy]
                    )
                )

        right = state.direction.rot_cw()
        xy = state.xy + right
        if xy in grid:
            if xy in grid:
                result.append(
                    (
                        State(xy, right, 1),
                        grid[xy]
                    )
                )

    return result


if __name__ == "__main__":
    run_day(17,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
