import re

from util.data import txt_fn, load_strings_newline
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(['WORDS:THE,OWE,MES,ROD,HER', 'AWAKEN THE POWER ADORNED WITH THE FLAMES BRIGHT IRE'])
    4
    >>> star1(['WORDS:THE,OWE,MES,ROD,HER', 'THE FLAME SHIELDED THE HEART OF THE KINGS'])
    3
    >>> star1(['WORDS:THE,OWE,MES,ROD,HER', 'POWE PO WER P OWE R'])
    2
    >>> star1(['WORDS:THE,OWE,MES,ROD,HER', 'THERE IS THE END'])
    3
    """

    result = 0
    for word in data[0][len('WORDS:'):].split(','):
        result += len(re.findall(word, data[1]))

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(['WORDS:THE,OWE,MES,ROD,HER,QAQ', 'AWAKEN THE POWE ADORNED WITH THE FLAMES BRIGHT IRE', 'THE FLAME SHIELDED THE HEART OF THE KINGS',\
               'POWE PO WER P OWE R', 'THERE IS THE END', 'QAQAQ'])
    42
    """

    words = data[0][len('WORDS:'):].split(',')

    result = list([False] * len(r) for r in data[1:])

    for word in words + list(w[::-1] for w in words):

        for row_num, row in enumerate(data[1:]):
            index = 0
            while True:
                index = row.find(word, index)
                if index == -1:
                    break

                for i in range(index, index + len(word)):
                    result[row_num][i] = True

                index += 1

    return sum(sum(1 for c in row if c) for row in result)


def star3(data: list[str]) -> int:
    """
    >>> star3(['WORDS:THE,OWE,MES,ROD,RODEO', 'HELWORLT', 'ENIGWDXL', 'TRODEOAL'])
    10
    """

    words = data[0][len('WORDS:'):].split(',')

    text = data[1:]
    result = list([False] * len(r) for r in text)

    for word in words + list(w[::-1] for w in words):
        for row_num, row in enumerate(text):
            index = 0
            while True:
                index = (row + row).find(word, index)
                if index == -1:
                    break

                for i in range(index, index + len(word)):
                    result[row_num][i % len(row)] = True

                index += 1

    # rotate both
    text = list(''.join(text[y][x] for y in range(len(text))) for x in range(len(text[0])))
    result = list(list(result[y][x] for y in range(len(result))) for x in range(len(result[0])))

    for word in words + list(w[::-1] for w in words):
        for row_num, row in enumerate(text):
            index = 0
            while True:
                index = row.find(word, index)
                if index == -1:
                    break

                for i in range(index, index + len(word)):
                    result[row_num][i % len(row)] = True

                index += 1

    return sum(sum(1 for c in row if c) for row in result)


if __name__ == "__main__":
    run_day(2,
            lambda: star1(load_strings_newline(txt_fn(__file__), ignore_blank=True)),
            lambda: star2(load_strings_newline(txt_fn(__file__, '-part-2.txt'), ignore_blank=True)),
            lambda: star3(load_strings_newline(txt_fn(__file__, '-part-3.txt'), ignore_blank=True)),
            )
