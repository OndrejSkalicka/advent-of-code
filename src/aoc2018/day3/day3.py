import re
from typing import Optional


def overlaps(claims) -> int:
    """
    >>> overlaps(((1, 1, 3, 4, 4), (2, 3, 1, 4, 4), (3, 5, 5, 2, 2)))
    4

    :param data: list of quadruplets [id, x, y, width, height]
    :return: number of overlapping points
    """
    computed = {}
    for claim in claims:
        for x in range(claim[1], claim[1] + claim[3]):
            for y in range(claim[2], claim[2] + claim[4]):
                point = (x, y)
                if point in computed:
                    computed[point] += 1
                else:
                    computed[point] = 1

    return sum(1 for x in computed.values() if x > 1)


def find_non_overlapping(claims) -> Optional[int]:
    """
    >>> find_non_overlapping(((1, 1, 3, 4, 4), (2, 3, 1, 4, 4), (3, 5, 5, 2, 2)))
    3
    """
    computed = {}
    for claim in claims:
        for x in range(claim[1], claim[1] + claim[3]):
            for y in range(claim[2], claim[2] + claim[4]):
                point = (x, y)
                if point in computed:
                    computed[point] += 1
                else:
                    computed[point] = 1

    # second pass to find which overlaps, eg. need to find one that has '1' for all its hexes
    for claim in claims:
        if is_non_overlapping(claim, computed):
            return claim[0]

    return None


def is_non_overlapping(claim, computed) -> bool:
    for x in range(claim[1], claim[1] + claim[3]):
        for y in range(claim[2], claim[2] + claim[4]):
            if computed[(x, y)] > 1:
                return False

    return True


def load_data():
    claims = []
    with open('data.txt', 'r') as fh:
        for line in fh.readlines():
            m = re.search('^#(?P<id>\\d+) @ (?P<x>\\d+),(?P<y>\\d+): (?P<w>\\d+)x(?P<h>\\d+)$', line)
            if not m:
                raise Exception('Cannot parse line "%s"' % line)

            claims.append((int(m.group('id')),
                           int(m.group('x')),
                           int(m.group('y')),
                           int(m.group('w')),
                           int(m.group('h'))))

    return claims


if __name__ == "__main__":
    data = load_data()
    print('Day 3 Star 1: %d' % overlaps(data))
    print('Day 3 Star 2: %d' % find_non_overlapping(data))
