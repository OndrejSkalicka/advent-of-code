import hashlib

from util.data import txt_fn, load_as_str
from util.perf import run_day


def star1(data: str) -> int:
    """
    >>> star1('abcdef')
    609043
    >>> star1('pqrstuv')
    1048970
    """

    i = 0
    while True:
        md5hash = hashlib.md5(f'{data}{i}'.encode(), usedforsecurity=False).hexdigest()
        if md5hash.startswith('00000'):
            return i
        i += 1


def star2(data: str) -> int:
    i = 0
    while True:
        md5hash = hashlib.md5(f'{data}{i}'.encode(), usedforsecurity=False).hexdigest()
        if md5hash.startswith('000000'):
            return i
        i += 1


if __name__ == "__main__":
    run_day(4,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
