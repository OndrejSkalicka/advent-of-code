from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy

ARROWS = {
    'v': Xy(0, 1),
    '^': Xy(0, -1),
    '>': Xy(1, 0),
    '<': Xy(-1, 0)
}


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "#.#####################",
    ...     "#.......#########...###",
    ...     "#######.#########.#.###",
    ...     "###.....#.>.>.###.#.###",
    ...     "###v#####.#v#.###.#.###",
    ...     "###.>...#.#.#.....#...#",
    ...     "###v###.#.#.#########.#",
    ...     "###...#.#.#.......#...#",
    ...     "#####.#.#.#######.#.###",
    ...     "#.....#.#.#.......#...#",
    ...     "#.#####.#.#.#########v#",
    ...     "#.#...#...#...###...>.#",
    ...     "#.#.#v#######v###.###v#",
    ...     "#...#.>.#...>.>.#.###.#",
    ...     "#####v#.#.###v#.#.###.#",
    ...     "#.....#...#...#.#.#...#",
    ...     "#.#########.###.#.#.###",
    ...     "#...###...#...#...#.###",
    ...     "###.###.#.###v#####v###",
    ...     "#...#...#.#.>.>.#.>.###",
    ...     "#.###.###.#.###.#.#v###",
    ...     "#.....###...###...#...#",
    ...     "#####################.#",
    ... ])
    94
    """

    grid = Grid.from_list_str(data)
    entrance = grid.find_all_xy(lambda v: v == '.')[0].xy
    destination = grid.find_all_xy(lambda v: v == '.')[-1].xy
    assert grid[entrance] == '.'

    distances = _distances(entrance, grid)

    distances_to: dict[Xy, list[tuple[Xy, int]]] = defaultdict(list)

    for k, v in distances.items():
        distances_to[k[1]].append((k[0], v))

    return _max_distance_to(destination, distances_to, {})


def _distances(entrance: Xy, grid: Grid[str]) -> dict[tuple[Xy, Xy], int]:
    distances: dict[tuple[Xy, Xy], int] = dict()
    buffer: list[tuple[Xy, Xy, int]] = [(entrance, entrance, 0)]
    while buffer:
        start, active, distance = buffer.pop()
        prev = start

        while True:
            all_adj = []

            for adj in grid.adjacent(active):
                if prev == adj:
                    continue

                if grid[adj] != '#':
                    all_adj.append(adj)

            if len(all_adj) == 0:
                assert active.y == grid.height - 1
                distances[start, active] = distance
                break

            if len(all_adj) > 1:
                distances[start, active] = distance

                for adj in all_adj:
                    if adj - ARROWS[grid[adj]] == active:
                        buffer.append((active, adj, 1))

                break

            prev = active
            active = all_adj[0]
            distance += 1
    return distances


def _max_distance_to(xy: Xy, distances_to: dict[Xy, list[tuple[Xy, int]]], cache: dict[Xy, int]) -> int:
    if xy in cache:
        return cache[xy]

    best = 0
    for other_xy, distance in distances_to[xy]:
        total = distance + _max_distance_to(other_xy, distances_to, cache)
        if total > best:
            best = total

    cache[xy] = best
    return best


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "#.#####################",
    ...     "#.......#########...###",
    ...     "#######.#########.#.###",
    ...     "###.....#.>.>.###.#.###",
    ...     "###v#####.#v#.###.#.###",
    ...     "###.>...#.#.#.....#...#",
    ...     "###v###.#.#.#########.#",
    ...     "###...#.#.#.......#...#",
    ...     "#####.#.#.#######.#.###",
    ...     "#.....#.#.#.......#...#",
    ...     "#.#####.#.#.#########v#",
    ...     "#.#...#...#...###...>.#",
    ...     "#.#.#v#######v###.###v#",
    ...     "#...#.>.#...>.>.#.###.#",
    ...     "#####v#.#.###v#.#.###.#",
    ...     "#.....#...#...#.#.#...#",
    ...     "#.#########.###.#.#.###",
    ...     "#...###...#...#...#.###",
    ...     "###.###.#.###v#####v###",
    ...     "#...#...#.#.>.>.#.>.###",
    ...     "#.###.###.#.###.#.#v###",
    ...     "#.....###...###...#...#",
    ...     "#####################.#",
    ... ])
    154
    """

    grid = Grid.from_list_str(data)
    entrance_xy = grid.find_all_xy(lambda v: v == '.')[0].xy
    destination_xy = grid.find_all_xy(lambda v: v == '.')[-1].xy
    assert grid[entrance_xy] == '.'

    xy_forward_distances = _distances(entrance_xy, grid)
    xy_id_map = _xy_id_map(xy_forward_distances)
    destination = xy_id_map[destination_xy]

    distances: dict[int, dict[int, int]] = defaultdict(dict)
    for (xy_from, xy_to), distance in xy_forward_distances.items():
        distances[xy_id_map[xy_from]][xy_id_map[xy_to]] = distance
        distances[xy_id_map[xy_to]][xy_id_map[xy_from]] = distance

    paths: list[list[int]] = [[xy_id_map[entrance_xy]]]
    best = 0

    # just brute force it, using integers instead of slower Xy for vertices*
    while paths:
        path = paths.pop()

        last = path[-1]
        for target in distances[last].keys():
            if target == destination:
                distance = 0
                for step_from, step_to in zip(path + [target], path[1:] + [target]):
                    distance += distances[step_from][step_to]

                best = max(best, distance)
                continue

            if target not in path:
                paths.append(path + [target])

    return best


def _xy_id_map(distances: dict[tuple[Xy, Xy], int]) -> dict[Xy, int]:
    all_xy_nodes = set()
    for k in distances:
        all_xy_nodes.add(k[0])
        all_xy_nodes.add(k[1])

    result = dict()

    for idx, xy in enumerate(sorted(all_xy_nodes)):
        result[xy] = idx

    return result


if __name__ == "__main__":
    run_day(23,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
