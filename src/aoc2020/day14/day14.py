import re
from typing import List, Union, Tuple, Dict


def load_data(fn: str) -> List[Union[Tuple[str, int, str], Tuple[str, int, int]]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            m = re.match(r'^(\w+)(?:\[(\d+)\])? = (\w+)$', line.strip())
            if m[1] == 'mask':
                result.append((m[1], 0, m[3]))
            else:
                result.append((m[1], int(m[2]), int(m[3])))

    return result


def star1(data: List[Union[Tuple[str, int, str], Tuple[str, int, int]]]) -> int:
    """
    >>> star1([('mask', 0, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'), ('mem', 8, 11), ('mem', 7, 101), ('mem', 8, 0)])
    165
    """

    memory = {}
    mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    for op in data:
        if op[0] == 'mask':
            mask = op[2]
            continue

        if op[0] == 'mem':
            value_bin = bin(op[2])[2:].rjust(36, '0')
            for i in range(36):
                if mask[i] != 'X':
                    value_bin = value_bin[:i] + mask[i] + value_bin[i + 1:]

            memory[op[1]] = int(value_bin, 2)

    return sum(memory.values())


def star2(data: List[Union[Tuple[str, int, str], Tuple[str, int, int]]]) -> int:
    """
    >>> star2([('mask', 0, '000000000000000000000000000000X1001X'), ('mem', 42, 100), ('mask', 0, '00000000000000000000000000000000X0XX'), ('mem', 26, 1)])
    208
    """

    memory = {}
    mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    for op in data:
        if op[0] == 'mask':
            mask = op[2]
            continue

        if op[0] == 'mem':
            address = list(bin(op[1])[2:].rjust(36, '0'))
            for i in range(36):
                if mask[i] == '1':
                    address[i] = '1'
                elif mask[i] == 'X':
                    address[i] = 'X'

            write_to_memory(address, 0, op[2], memory)

    return sum(memory.values())


def write_to_memory(address: List[str], index: int, value: int, memory: Dict[int, int]):
    if index >= len(address):
        memory[int(''.join(address), 2)] = value
        return

    if address[index] == 'X':
        address[index] = '0'
        write_to_memory(address[:], index + 1, value, memory)
        address[index] = '1'
        write_to_memory(address[:], index + 1, value, memory)
    else:
        write_to_memory(address[:], index + 1, value, memory)


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 14 Star 1: %d' % star1(my_data))
    print('Day 14 Star 2: %d' % star2(my_data))
