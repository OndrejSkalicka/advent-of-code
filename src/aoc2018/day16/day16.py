import re
from typing import Tuple, List

ALL_OPS = (
    'addr',
    'addi',
    'mulr',
    'muli',
    'banr',
    'bani',
    'borr',
    'bori',
    'setr',
    'seti',
    'gtir',
    'gtri',
    'gtrr',
    'eqir',
    'eqri',
    'eqrr',
)


def star_1(before_op_after) -> int:
    total = 0
    for before, op, after in before_op_after:

        behaves_as = []
        for operation in ALL_OPS:
            if after == exec(operation, before, op[1], op[2], op[3]):
                behaves_as.append(operation)

        if len(behaves_as) > 2:
            total += 1

    return total


def star_2(before_op_after, instr: List[Tuple[int, int, int, int]]):
    op_id_to_operations = [ALL_OPS, ] * 16

    for before, op, after in before_op_after:
        operation_id = op[0]

        result = []
        for operation in op_id_to_operations[operation_id]:
            if after == exec(operation, before, op[1], op[2], op[3]):
                result.append(operation)

        op_id_to_operations[operation_id] = result

    mapping = tuple((op_id, tuple(ops)) for op_id, ops in enumerate(op_id_to_operations))

    result = guess(mapping, 0, ['', ] * 16)
    assert result

    reg = (0,) * 4
    for instruction in instr:
        reg = exec(result[instruction[0]], reg, instruction[1], instruction[2], instruction[3])

    return reg[0]


def guess(mapping: List[Tuple[int, Tuple[str, ...]]], index: int, guessed: List[str]):
    if index == 16:
        return guessed

    for opcode in mapping[index][1]:
        if opcode in guessed:
            continue

        new_guess = guessed.copy()
        new_guess[index] = opcode

        result = guess(mapping, index + 1, new_guess)

        if result and '' not in result:
            return result


def exec(name: str, r: Tuple[int, int, int, int], a: int, b: int, c: int) -> Tuple[int, int, int, int]:
    """
    >>> exec('mulr', (3, 2, 1, 1), 2, 1, 2)
    (3, 2, 2, 1)

    >>> exec('addi', (3, 2, 1, 1), 2, 1, 2)
    (3, 2, 2, 1)

    >>> exec('seti', (3, 2, 1, 1), 2, 1, 2)
    (3, 2, 2, 1)
    """
    result = list(r)
    if name == 'addr':
        result[c] = r[a] + r[b]
    elif name == 'addi':
        result[c] = r[a] + b
    elif name == 'mulr':
        result[c] = r[a] * r[b]
    elif name == 'muli':
        result[c] = r[a] * b
    elif name == 'banr':
        result[c] = r[a] & r[b]
    elif name == 'bani':
        result[c] = r[a] & b
    elif name == 'borr':
        result[c] = r[a] | r[b]
    elif name == 'bori':
        result[c] = r[a] | b
    elif name == 'setr':
        result[c] = r[a]
    elif name == 'seti':
        result[c] = a
    elif name == 'gtir':
        result[c] = 1 if a > r[b] else 0
    elif name == 'gtri':
        result[c] = 1 if r[a] > b else 0
    elif name == 'gtrr':
        result[c] = 1 if r[a] > r[b] else 0
    elif name == 'eqir':
        result[c] = 1 if a == r[b] else 0
    elif name == 'eqri':
        result[c] = 1 if r[a] == b else 0
    elif name == 'eqrr':
        result[c] = 1 if r[a] == r[b] else 0
    else:
        raise Exception('Uknown operation %s' % name)

    # noinspection PyTypeChecker
    return tuple(result)


def load_data(fn):
    before_op_after = []
    instr = []

    before = re.compile(r'^Before: \[(\d+), (\d+), (\d+), (\d+)\]$')
    op = re.compile(r'^(\d+) (\d+) (\d+) (\d+)$')
    after = re.compile(r'^After: +\[(\d+), (\d+), (\d+), (\d+)\]$')

    with open(fn, 'r') as fh:
        while True:
            line = fh.readline().strip()

            if line.startswith('Before:'):
                m_before = before.match(line)
                if not m_before:
                    raise Exception('Cannot parse line "%s" as `before`' % line)

                line = fh.readline().strip()
                m_op = op.match(line)
                if not m_op:
                    raise Exception('Cannot parse line "%s" as `op`' % line)

                line = fh.readline().strip()
                m_after = after.match(line)
                if not m_after:
                    raise Exception('Cannot parse line "%s" as `after`' % line)

                before_op_after.append((
                    tuple(int(x) for x in m_before.groups()),
                    tuple(int(x) for x in m_op.groups()),
                    tuple(int(x) for x in m_after.groups()),
                ))

                assert fh.readline().strip() == ''
                continue

            assert fh.readline().strip() == ''

            while True:
                line = fh.readline().strip()

                if not line:
                    return before_op_after, instr

                m_op = op.match(line)
                assert m_op

                instr.append(tuple(int(x) for x in m_op.groups()))


if __name__ == "__main__":
    data = load_data('data.txt')

    print('Day 16 star 1: %d' % star_1(data[0]))
    print('Day 16 star 2: %d' % star_2(*data))
