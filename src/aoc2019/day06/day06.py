import heapq
from collections import defaultdict


def load_data(fn: str) -> list[tuple[str, str]]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            a, b = line.strip().split(')')
            result.append((a, b))
    return result


def star1(star_map: list[tuple[str, str]]) -> int:
    """
    >>> star1([('COM', 'B'),
    ...        ('B', 'C'),
    ...        ('C', 'D'),
    ...        ('D', 'E'),
    ...        ('E', 'F'),
    ...        ('B', 'G'),
    ...        ('G', 'H'),
    ...        ('D', 'I'),
    ...        ('E', 'J'),
    ...        ('J', 'K'),
    ...        ('K', 'L')])
    42
    """

    parents = dict()

    for orbit in star_map:
        parents[orbit[1]] = orbit[0]

    crc = 0
    for star_object in parents.keys():
        while star_object in parents:
            crc += 1
            star_object = parents[star_object]

    return crc


def star2(star_map: list[tuple[str, str]]) -> int:
    """
    >>> star2([('COM', 'B'),
    ...        ('B', 'C'),
    ...        ('C', 'D'),
    ...        ('D', 'E'),
    ...        ('E', 'F'),
    ...        ('B', 'G'),
    ...        ('G', 'H'),
    ...        ('D', 'I'),
    ...        ('E', 'J'),
    ...        ('J', 'K'),
    ...        ('K', 'L'),
    ...        ('K', 'YOU'),
    ...        ('I', 'SAN')])
    4
    """

    nodes: dict[str, list[str]] = defaultdict(list)

    for orbit in star_map:
        nodes[orbit[1]].append(orbit[0])
        nodes[orbit[0]].append(orbit[1])

    pq: list[tuple[int, str]] = []
    heapq.heappush(pq, (0, 'YOU'))
    visited = set()
    distances = {'YOU': 0}
    parents_map = {}

    while pq:
        _, active = heapq.heappop(pq)

        if active in visited:
            continue
        visited.add(active)

        distance = distances[active]
        for node in nodes[active]:
            if node in visited:
                continue

            parents_map[node] = active
            distances[node] = distance + 1
            heapq.heappush(pq, (distance + 1, node))

    cost = 0
    current = 'SAN'
    while True:
        if current not in parents_map:
            raise Exception(f"Cannot find {current} in Dijkstra map")
        current = parents_map[current]
        cost += 1
        if current == 'YOU':
            return cost - 2


if __name__ == "__main__":
    print('Day 6 Star 1: %d' % star1(load_data('data.txt')))
    print('Day 6 Star 2: %d' % star2(load_data('data.txt')))
