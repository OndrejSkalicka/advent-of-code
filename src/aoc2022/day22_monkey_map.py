import re

from util.data import load_as_str, txt_fn
from util.perf import run_day
from util.record import Xy

DIRECTIONS = [
    Xy(1, 0),
    Xy(0, 1),
    Xy(-1, 0),
    Xy(0, -1),
]


def star1(data: str) -> int:
    """
    >>> star1(
    ...        "        ...#\\n"
    ...        "        .#..\\n"
    ...        "        #...\\n"
    ...        "        ....\\n"
    ...        "...#.......#\\n"
    ...        "........#...\\n"
    ...        "..#....#....\\n"
    ...        "..........#.\\n"
    ...        "        ...#....\\n"
    ...        "        .....#..\\n"
    ...        "        .#......\\n"
    ...        "        ......#.\\n"
    ...        "\\n"
    ...        "10R5L5R10L4R5L5\\n"
    ...        )
    6032
    """

    map, steps = _parse_input(data)
    direction_index = 0
    for x, c in enumerate(map[0]):
        if c == '.':
            pos: Xy = Xy(x, 0)
            break
    else:
        raise Exception("Cannot find starting position")

    for step in steps:
        if step == 'R':
            direction_index = (direction_index + 1) % len(DIRECTIONS)
        elif step == 'L':
            direction_index = (direction_index - 1) % len(DIRECTIONS)
        else:
            direction = DIRECTIONS[direction_index]
            for _ in range(int(step)):
                new_pos = _step_idclip(pos, direction, map)

                if map[new_pos.y][new_pos.x] == '#':
                    break
                pos = new_pos

    return 1000 * (pos.y + 1) + 4 * (pos.x + 1) + direction_index


def _parse_input(data: str) -> tuple[list[str], list[str]]:
    parts: list[str] = data.split("\n\n")

    return parts[0].rstrip("\n").splitlines(), [s for s in re.split(r'(\d+)', parts[1].strip()) if s != '']


def _step_idclip(pos: Xy, direction: Xy, map: list[str]) -> Xy:
    new_pos = pos + direction
    if _out_of_bounds(new_pos, map):
        while True:
            new_pos = pos - direction
            if _out_of_bounds(new_pos, map):
                return pos

            pos = new_pos

    return new_pos


def _out_of_bounds(pos: Xy, map: list[str]) -> bool:
    return pos.x < 0 or pos.y < 0 or pos.y >= len(map) or pos.x >= len(map[pos.y]) or map[pos.y][pos.x] == ' '


def star2(data: str, width: int = 50) -> int:
    """
    >>> star2(
    ...        "        ...#\\n"
    ...        "        .#..\\n"
    ...        "        #...\\n"
    ...        "        ....\\n"
    ...        "...#.......#\\n"
    ...        "........#...\\n"
    ...        "..#....#....\\n"
    ...        "..........#.\\n"
    ...        "        ...#....\\n"
    ...        "        .....#..\\n"
    ...        "        .#......\\n"
    ...        "        ......#.\\n"
    ...        "\\n"
    ...        "10R5L5R10L4R5L5\\n",
    ...        width=4
    ...        )
    5031
    """

    map, steps = _parse_input(data)
    direction_index = 0
    for x, c in enumerate(map[0]):
        if c == '.':
            pos: Xy = Xy(x, 0)
            break
    else:
        raise Exception("Cannot find starting position")

    for step in steps:
        if step == 'R':
            direction_index = (direction_index + 1) % len(DIRECTIONS)
        elif step == 'L':
            direction_index = (direction_index - 1) % len(DIRECTIONS)
        else:
            for _ in range(int(step)):
                new_pos, new_direction = _step_idclip2(pos, direction_index, map, width)

                if map[new_pos.y][new_pos.x] == '#':
                    break
                pos = new_pos
                direction_index = new_direction

    return 1000 * (pos.y + 1) + 4 * (pos.x + 1) + direction_index


def _step_idclip2(pos: Xy, direction_index: int, map: list[str], width: int) -> tuple[Xy, int]:
    direction = DIRECTIONS[direction_index]
    new_pos = pos + direction
    if not _out_of_bounds(new_pos, map):
        return new_pos, direction_index

    if width == 4:
        if new_pos.x == 12 and 3 < new_pos.y < 8:
            dy = -pos.y % 4
            assert direction_index == 0
            return Xy(11 + dy, 8), 1

        if new_pos.y == 12 and 7 < new_pos.x < 12:
            dx = pos.x % 4
            assert direction_index == 1
            return Xy(3 - dx, 7), 3

        if new_pos.y == 3 and 3 < new_pos.x < 8:
            dx = -pos.x % 4
            assert direction_index == 3
            return Xy(8, 4 - dx), 0

    if width == 50:
        # manual configuration just for my solution :D
        sec_from = Xy(pos.x // width, pos.y // width)
        sec_space = Xy(new_pos.x // width, new_pos.y // width)

        if sec_from == Xy(1, 0) and sec_space == Xy(1, -1):
            rot = 1
            sec_dest = Xy(0, 3)
        elif sec_from == Xy(0, 3) and sec_space == Xy(-1, 3):
            rot = 3
            sec_dest = Xy(1, 0)
        elif sec_from == Xy(0, 2) and sec_space == Xy(-1, 2):
            rot = 2
            sec_dest = Xy(1, 0)
        elif sec_from == Xy(1, 0) and sec_space == Xy(0, 0):
            rot = 2
            sec_dest = Xy(0, 2)
        elif sec_from == Xy(1, 2) and sec_space == Xy(1, 3):
            rot = 1
            sec_dest = Xy(0, 3)
        elif sec_from == Xy(2, 0) and sec_space == Xy(2, 1):
            rot = 1
            sec_dest = Xy(1, 1)
        elif sec_from == Xy(1, 1) and sec_space == Xy(2, 1):
            rot = 3
            sec_dest = Xy(2, 0)
        elif sec_from == Xy(0, 2) and sec_space == Xy(0, 1):
            rot = 1
            sec_dest = Xy(1, 1)
        elif sec_from == Xy(1, 1) and sec_space == Xy(0, 1):
            rot = 3
            sec_dest = Xy(0, 2)
        elif sec_from == Xy(2, 0) and sec_space == Xy(2, -1):
            rot = 0
            sec_dest = Xy(0, 3)
        elif sec_from == Xy(2, 0) and sec_space == Xy(3, 0):
            rot = 2
            sec_dest = Xy(1, 2)
        elif sec_from == Xy(1, 2) and sec_space == Xy(2, 2):
            rot = 2
            sec_dest = Xy(2, 0)
        elif sec_from == Xy(0, 3) and sec_space == Xy(1, 3):
            rot = 3
            sec_dest = Xy(1, 2)
        elif sec_from == Xy(0, 3) and sec_space == Xy(0, 4):
            rot = 0
            sec_dest = Xy(2, 0)
        else:
            raise Exception(f"Not implemented {sec_from} {sec_space} // {pos} {new_pos}")

        # leaving offset
        match direction_index:
            case 0:
                offset = (width - pos.y - 1) % width
            case 1:
                offset = pos.x % width
            case 2:
                offset = pos.y % width
            case 3:
                offset = (width - pos.x - 1) % width
            case _:
                assert False

        new_direction = (direction_index + rot) % 4
        min_x = sec_dest.x * width
        max_x = (sec_dest.x + 1) * width - 1
        min_y = sec_dest.y * width
        max_y = (sec_dest.y + 1) * width - 1

        match new_direction:
            case 0:
                return Xy(min_x, max_y - offset), new_direction
            case 1:
                return Xy(min_x + offset, min_y), new_direction
            case 2:
                return Xy(max_x, min_y + offset), new_direction
            case 3:
                return Xy(max_x - offset, max_y), new_direction
            case _:
                assert False

    raise Exception(f"Not implemented {width} {new_pos}")


if __name__ == "__main__":
    run_day(22,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
