from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["A Y",
    ...        "B X",
    ...        "C Z"])
    15
    """

    results = {
        'A X': 3 + 1,
        'B X': 0 + 1,
        'C X': 6 + 1,

        'A Y': 6 + 2,
        'B Y': 3 + 2,
        'C Y': 0 + 2,

        'A Z': 0 + 3,
        'B Z': 6 + 3,
        'C Z': 3 + 3,
    }

    return sum(results[match] for match in data)


def star2(data: list[str]) -> int:
    """
    >>> star2(["A Y",
    ...        "B X",
    ...        "C Z"])
    12
    """
    results = {
        'A X': 0 + 3,
        'B X': 0 + 1,
        'C X': 0 + 2,

        'A Y': 3 + 1,
        'B Y': 3 + 2,
        'C Y': 3 + 3,

        'A Z': 6 + 2,
        'B Z': 6 + 3,
        'C Z': 6 + 1,
    }

    return sum(results[match] for match in data)


if __name__ == "__main__":
    run_day(2,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
