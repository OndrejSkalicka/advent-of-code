from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "kh-tc",
    ...        "qp-kh",
    ...        "de-cg",
    ...        "ka-co",
    ...        "yn-aq",
    ...        "qp-ub",
    ...        "cg-tb",
    ...        "vc-aq",
    ...        "tb-ka",
    ...        "wh-tc",
    ...        "yn-cg",
    ...        "kh-ub",
    ...        "ta-co",
    ...        "de-co",
    ...        "tc-td",
    ...        "tb-wq",
    ...        "wh-td",
    ...        "ta-ka",
    ...        "td-qp",
    ...        "aq-cg",
    ...        "wq-ub",
    ...        "ub-vc",
    ...        "de-ta",
    ...        "wq-aq",
    ...        "wq-vc",
    ...        "wh-yn",
    ...        "ka-de",
    ...        "kh-ta",
    ...        "co-tc",
    ...        "wh-qp",
    ...        "tb-vc",
    ...        "td-yn",
    ...       ])
    7
    """

    connections: dict[str, set[str]] = defaultdict(set)
    for row in data:
        a, b = row.split('-')
        assert b not in connections[a]
        assert a not in connections[b]
        connections[a].add(b)
        connections[b].add(a)

    groups: set[frozenset[str]] = set(frozenset([k]) for k in connections)

    for _ in range(2):
        new_groups: set[frozenset[str]] = set()

        for group in groups:
            for try_to_add in connections[next(iter(group))]:
                if try_to_add in group:
                    continue

                group_copy = set(group)
                group_copy.add(try_to_add)
                if _is_connected(group_copy, connections):
                    new_groups.add(frozenset(group_copy))

        groups = new_groups

    result = 0
    for group in groups:
        for computer in group:
            if computer[0] == 't':
                break
        else:
            continue
        result += 1

    return result


def star2(data: list[str]) -> str:
    """
    >>> star2([
    ...        "kh-tc",
    ...        "qp-kh",
    ...        "de-cg",
    ...        "ka-co",
    ...        "yn-aq",
    ...        "qp-ub",
    ...        "cg-tb",
    ...        "vc-aq",
    ...        "tb-ka",
    ...        "wh-tc",
    ...        "yn-cg",
    ...        "kh-ub",
    ...        "ta-co",
    ...        "de-co",
    ...        "tc-td",
    ...        "tb-wq",
    ...        "wh-td",
    ...        "ta-ka",
    ...        "td-qp",
    ...        "aq-cg",
    ...        "wq-ub",
    ...        "ub-vc",
    ...        "de-ta",
    ...        "wq-aq",
    ...        "wq-vc",
    ...        "wh-yn",
    ...        "ka-de",
    ...        "kh-ta",
    ...        "co-tc",
    ...        "wh-qp",
    ...        "tb-vc",
    ...        "td-yn",
    ...       ])
    'co,de,ka,ta'
    """

    connections: dict[str, set[str]] = defaultdict(set)
    for row in data:
        a, b = row.split('-')
        assert b not in connections[a]
        assert a not in connections[b]
        connections[a].add(b)
        connections[b].add(a)

    groups: set[frozenset[str]] = set(frozenset([k]) for k in connections)
    longest = set()

    while groups:
        new_groups: set[frozenset[str]] = set()

        for group in groups:
            longest = set(group)
            for try_to_add in connections[next(iter(group))]:
                if try_to_add in group:
                    continue

                group_copy = set(group)
                group_copy.add(try_to_add)
                if _is_connected(group_copy, connections):
                    new_groups.add(frozenset(group_copy))

        groups = new_groups

    return ','.join(sorted(longest))


def _is_connected(lan: set[str], connections: dict[str, set[str]]) -> bool:
    for computer in lan:
        for other in lan:
            if computer == other:
                continue

            if other not in connections[computer]:
                return False

    return True


if __name__ == "__main__":
    run_day(23,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
