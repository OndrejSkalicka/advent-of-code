import os
from typing import Dict
from typing import List, Optional, Tuple


class Image:
    def __init__(self, id: int, pixels: List[str]) -> None:
        self.id = id
        self.pixels = pixels

        self.x = None  # type: Optional[int]
        self.y = None  # type: Optional[int]

    def side(self, rotation: int) -> str:
        """
        0 = north, 1 = east, 2 = south, 3 = west

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', '.#.#.#..##',\
                      '..#....#..', '###...#.#.', '..###..###']).side(0)
        '..##.#..#.'

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', '.#.#.#..##',\
                      '..#....#..', '###...#.#.', '..###..###']).side(1)
        '...#.##..#'

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', '.#.#.#..##',\
                      '..#....#..', '###...#.#.', '..###..###']).side(2)
        '###..###..'

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', '.#.#.#..##',\
                      '..#....#..', '###...#.#.', '..###..###']).side(3)
        '.#..#####.'
        """
        if rotation == 0:
            return self.pixels[0]

        if rotation == 1:
            return ''.join(x[-1] for x in self.pixels)

        if rotation == 2:
            return self.pixels[-1][::-1]

        if rotation == 3:
            return ''.join(x[0] for x in self.pixels[::-1])

        raise Exception('Illegal rotation %d' % rotation)

    def rotate_cw(self) -> 'Image':
        """
        Returns a modified self.

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', \
                          '.#.#.#..##', '..#....#..', '###...#.#.', '..###..###']).rotate_cw().pixels
        ['.#..#####.', '.#.####.#.', '###...#..#', '#..#.##..#', '#....#.##.', '...##.##.#', '.#...#....', \
'#.#.##....', '##.###.#.#', '#..##.#...']
        """
        self.pixels = list(''.join(x) for x in zip(*self.pixels[::-1]))

        return self

    def flip(self) -> 'Image':
        """
        Returns a modified self.

        Flip on its back. Like a puzzle piece. Flip over vertical axis.

        >>> Image(0, ['..##.#..#.', '##..#.....', '#...##..#.', '####.#...#', '##.##.###.', '##...#.###', \
                          '.#.#.#..##', '..#....#..', '###...#.#.', '..###..###']).flip().pixels
        ['.#..#.##..', '.....#..##', '.#..##...#', '#...#.####', '.###.##.##', '###.#...##', '##..#.#.#.', \
'..#....#..', '.#.#...###', '###..###..']
        """
        self.pixels = list(x[::-1] for x in self.pixels)
        return self

    def matches(self, other: 'Image') -> bool:
        pass

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return "%d" % self.id


class Puzzle:

    def __init__(self, data: List[Image]) -> None:
        self.fixed_images = []
        self.floating_images = data

        self.grid = {}  # type: Dict[Tuple[int, int], Image]

        self.add_to_grid(data[0], 0, 0)

    def add_to_grid(self, image: Image, x: int, y: int) -> None:
        self.grid[(x, y)] = image
        image.x = x
        image.y = y
        self.fixed_images.append(image)
        self.floating_images.remove(image)

    def min_max_xy(self) -> Tuple[int, int, int, int]:
        return (
            min(k[0] for k in self.grid.keys()),
            max(k[0] for k in self.grid.keys()),
            min(k[1] for k in self.grid.keys()),
            max(k[1] for k in self.grid.keys())
        )

    def solution_as_2d_array(self) -> List[List[Image]]:
        (min_x, max_x, min_y, max_y) = self.min_max_xy()
        result = []

        for y in range(min_y, max_y + 1):
            row = []
            for x in range(min_x, max_x + 1):
                row.append(self.grid.get((x, y)))

            result.append(row)

        return result

    def print_grid(self):
        images = self.solution_as_2d_array()

        print('GRID [Fixed %d, Floating %d]' % (len(self.fixed_images), len(self.floating_images)))
        for row in images:
            for image in row:
                print('[%5s]' % image, end='')

            print()

    def solve_puzzle(self):
        for i in range(len(self.floating_images)):
            self.try_to_add_one()

        if self.floating_images:
            raise Exception('Not all images processed :((')

    @staticmethod
    def direction_to_dx_dy(direction: int) -> Tuple[int, int]:
        return (
            (0, -1),
            (1, 0),
            (0, 1),
            (-1, 0)
        )[direction]

    def try_to_add_one(self) -> bool:
        for floating in self.floating_images:
            for fixed in self.fixed_images:
                for flip in (False, True):
                    if flip:
                        floating.flip()

                    for fixed_direction in range(4):
                        floating_direction = (fixed_direction + 2) % 4
                        (dx, dy) = self.direction_to_dx_dy(fixed_direction)
                        new_x = fixed.x + dx
                        new_y = fixed.y + dy
                        if self.grid.get((new_x, new_y)) is not None:
                            continue

                        for rotate in range(4):
                            fixed_side = fixed.side(fixed_direction)
                            floating_side = floating.side(floating_direction)

                            if floating_side[::-1] != fixed_side:
                                floating.rotate_cw()
                                continue

                            self.add_to_grid(floating, fixed.x + dx, fixed.y + dy)

                            return True

        return False


def load_data(fn: str) -> List[Image]:
    image_id = None
    pixels = []
    result = []

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            if line.startswith("Tile "):
                image_id = int(line[5:-1])
                continue

            if not line:
                result.append(Image(image_id, pixels))
                image_id = None
                pixels = []
                continue

            pixels.append(line)

    if image_id is not None:
        result.append(Image(image_id, pixels))

    return result


def test_data():
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/data-test.txt')


def star1(data: List[Image]) -> int:
    """
    >>> star1(test_data())
    20899048083289
    """

    puzzle = Puzzle(data)
    puzzle.solve_puzzle()
    images = puzzle.solution_as_2d_array()

    return (
            images[0][0].id *
            images[-1][0].id *
            images[0][-1].id *
            images[-1][-1].id
    )


def star2(data: List[Image]) -> int:
    """
    >>> star2(test_data())
    273
    """

    puzzle = Puzzle(data)
    puzzle.solve_puzzle()
    images = puzzle.solution_as_2d_array()

    big_image = []
    for image_row in images:

        batch = []

        for image in image_row:
            for (i, pixel_row) in enumerate(image.pixels[1:-1]):
                if i >= len(batch):
                    batch.append('')

                batch[i] += pixel_row[1:-1]

        for batch_row in batch:
            big_image.append(batch_row)

    monster = [
        '                  # ',
        '#    ##    ##    ###',
        ' #  #  #  #  #  #   ',
    ]

    for flip in (False, True):
        if flip:
            big_image = list(x[::-1] for x in big_image)

        for i in range(4):
            for x in range(len(big_image[0]) - len(monster[0])):
                for y in range(len(big_image) - len(monster)):
                    if not check_monster(big_image, monster, x, y):
                        continue

                    for dx in range(len(monster[0])):
                        for dy in range(len(monster)):
                            if monster[dy][dx] == '#':
                                tmp = list(big_image[y + dy])
                                tmp[x + dx] = 'O'
                                big_image[y + dy] = ''.join(tmp)

            big_image = list(''.join(x) for x in zip(*big_image[::-1]))

    return sum(sum(1 for x in row if x == '#') for row in big_image)


def check_monster(big_image: List[str], monster: List[str], x: int, y: int) -> bool:
    for dx in range(len(monster[0])):
        for dy in range(len(monster)):
            if monster[dy][dx] == '#' and big_image[y + dy][x + dx] not in ('#', 'O'):
                return False

    return True


if __name__ == "__main__":
    print('Day 20 Star 1: %d' % star1(load_data('data.txt')))
    print('Day 20 Star 2: %d' % star2(load_data('data.txt')))
