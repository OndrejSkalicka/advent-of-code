import logging
from typing import Optional

import numpy as np

from aoc2019.intcode import Program
from util.data import load_numbers_csv
from util.logger_config import init_logger

logger = logging.getLogger(__name__)

yx = tuple[int, int]


def star1(memory: list[int]) -> int:
    program = Program(memory)
    program.run()

    triplets = []
    for i in range(len(program.output) // 3):
        triplets.append((program.output[3 * i], program.output[3 * i + 1], program.output[3 * i + 2]))

    w = max(t[0] for t in triplets) + 1
    h = max(t[1] for t in triplets) + 1

    final_image = np.zeros((h, w), dtype=int)

    for x, y, v in triplets:
        final_image[y, x] = v

    return (final_image == 2).sum()  # type: ignore


def star2(memory: list[int]) -> int:
    memory[0] = 2
    program = Program(memory)

    w = 40
    h = 24
    display = np.zeros((h, w), dtype=int)

    prev_ball_pos: Optional[yx] = None
    last_joy_dir: int = 0

    turn = 0
    score = -1

    while True:
        turn += 1
        program.run_or_resume()

        triplets = []
        while program.output:
            x = program.output.pop(0)
            y = program.output.pop(0)
            v = program.output.pop(0)

            if x == -1:
                score = v
            else:
                triplets.append((x, y, v))

        for x, y, v in triplets:
            display[y, x] = v

        # noinspection PyTypeChecker
        paddle_pos_x: int = np.unravel_index((display == 3).argmax(), display.shape)[1]
        ball_pos: yx = np.unravel_index((display == 4).argmax(), display.shape)  # type: ignore

        paddle_desired_x = None

        if prev_ball_pos is not None:
            if ball_pos[0] > prev_ball_pos[0]:
                # going down

                if ball_pos[1] > prev_ball_pos[1]:
                    # going right
                    paddle_desired_x = ball_pos[1] + (22 - ball_pos[0])
                else:
                    # going left
                    paddle_desired_x = ball_pos[1] - (22 - ball_pos[0])
            else:
                # going up

                if ball_pos[1] > prev_ball_pos[1]:
                    # going right
                    paddle_desired_x = ball_pos[1] + 1
                else:
                    # going left
                    paddle_desired_x = ball_pos[1] - 1
        prev_ball_pos = ball_pos

        next_joy_dir = 0
        if paddle_desired_x is not None:
            if paddle_desired_x > paddle_pos_x:
                next_joy_dir = 1
            elif paddle_desired_x < paddle_pos_x:
                next_joy_dir = -1
            else:
                next_joy_dir = -last_joy_dir

        program.input.append(next_joy_dir)
        last_joy_dir = next_joy_dir

        dump = f"Turn: {turn}\n"
        dump += f"Score: {score}\n"
        for y in range(h):
            dump += f'{y:2d} | '
            for x in range(w):
                dump += [' ', '#', 'o', '=', '*'][display[y, x]]
            dump += "\n"
        dump += '               111111111122222222223333333333\n'
        dump += '     0123456789012345678901234567890123456789\n'
        dump += f"Paddle: {paddle_pos_x} -> {program.input[0]}\n"
        dump += f"Ball Pos:      {np.unravel_index((display == 4).argmax(), display.shape)}\n"
        dump += f"Paddle Pos:    {np.unravel_index((display == 3).argmax(), display.shape)}\n"
        dump += f"Paddle desire: {paddle_desired_x}\n"
        dump += f"Joy:    {next_joy_dir}\n"
        dump += "\n"
        logger.debug(f"Game state: \n{dump}")

        if (display == 2).sum() == 0:
            break

    return score


if __name__ == "__main__":
    init_logger()

    print('Day 13 Star 1: %d' % star1(load_numbers_csv('day13.txt')))
    print('Day 13 Star 2: %d' % star2(load_numbers_csv('day13.txt')))
