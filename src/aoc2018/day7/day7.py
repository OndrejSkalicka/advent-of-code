import os  # noqa
import re
from typing import Sequence, Tuple, Set, Dict


def find_path(reqs: Sequence[Tuple[int, int]]) -> str:
    """
    >>> find_path(load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test.txt')))
    'CABDFE'
    """
    reversed = reverse(reqs)

    result = ''
    while len(reversed) > 0:
        found = []
        for key, dependencies in reversed.items():
            if len(dependencies) == 0:
                found.append(key)

        if len(found) == 0:
            raise Exception('Cannot continue: %s' % reversed)

        found.sort()
        found_string = found[0]

        result += found_string
        del reversed[found_string]
        for k, v in reversed.items():
            if found_string in v:
                v.remove(found_string)

    return result


class Worker:
    def __init__(self, id):
        self.id = id
        self.working_on = None
        self.free_since = 0

    def __repr__(self) -> str:
        return 'Worker %d working on %s, free time %d' % (self.id, self.working_on, self.free_since)


def find_path_parallel(reqs: Sequence[Tuple[int, int]], workers: int, time_offset: int, verbose: bool = False) -> int:
    """
    >>> find_path_parallel(load_data(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data-test.txt')), 2, 0)
    15
    """
    workers = list(Worker(i) for i in range(workers))  # type: Sequence[Worker]
    reversed = reverse(reqs)

    time = 0
    blocked = set()
    while len(reversed) > 0:
        # finished workers
        for w in workers:
            if w.working_on is not None and w.free_since == time:
                if verbose:
                    print('Worker %d finished %s at %d' % (w.id, w.working_on, time))
                del reversed[w.working_on]
                blocked.remove(w.working_on)
                for k, v in reversed.items():
                    if w.working_on in v:
                        v.remove(w.working_on)
                w.working_on = None

        # no workers, just increase time
        if sum(1 for w in workers if w.working_on is None) == 0:
            if verbose:
                print('No free workers at %d' % time)
            time += 1
            continue

        found = []
        for key, dependencies in reversed.items():
            if len(dependencies) == 0 and key not in blocked:
                found.append(key)

        found.sort()

        # found work, assign, but keep time
        if len(found) > 0:
            found_string = found[0]
            for w in workers:
                if w.working_on is None:
                    w.working_on = found_string
                    w.free_since = time + time_offset + ord(found_string) - 64
                    blocked.add(found_string)
                    if verbose:
                        print('Found work at %d: %s' % (time, w))
                    break

        else:
            if verbose:
                print('No work found at time %d' % time)
            time += 1

    return time - 1  # -1 because it all ends with 'no work found, time++'


def reverse(reqs: Sequence[Tuple[int, int]]) -> Dict[str, Set[str]]:
    # init reversal
    reversed = {}
    for req in reqs:
        reversed[req[0]] = set()
        reversed[req[1]] = set()

    for req in reqs:
        reversed[req[1]].add(req[0])

    return reversed


def load_data(fn: str) -> Sequence[Tuple[int, int]]:
    matcher = re.compile(r'Step ([A-Z]) must be finished before step ([A-Z]) can begin\.')
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            m = matcher.match(line)
            if not m:
                raise Exception('Cannot parse line "%s"' % line)

            result.append((m.group(1), m.group(2)))

    return result


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 7 Star 1: %s' % find_path(data))
    print('Day 7 Star 2: %d' % find_path_parallel(data, workers=5, time_offset=60, verbose=True))
