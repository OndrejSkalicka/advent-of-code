import re
from dataclasses import dataclass

from util.data import txt_fn, load_as_str
from util.perf import run_day


@dataclass
class Lens:
    focal_length: int
    label: str

    def __repr__(self) -> str:
        return '%s %d' % (self.label, self.focal_length)


def star1(data: str) -> int:
    """
    >>> star1('rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7')
    1320
    """

    return sum(_hash(e) for e in data.split(','))


def star2(data: str) -> int:
    """
    >>> star2('rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7')
    145
    """

    boxes: list[list[Lens]] = list([] for _ in range(256))

    for step in data.split(','):
        m = re.match(r'(\w+)(?:(-)|=(\d))$', step)
        assert m

        label: str = m.group(1)
        box = boxes[_hash(label)]

        if m.group(2) == '-':

            for i, item in enumerate(box):
                if item.label == label:
                    del box[i]

        else:
            focal_length = int(m.group(3))

            lens = Lens(focal_length, label)

            for i, item in enumerate(box):
                if item.label == label:
                    box[i] = lens
                    break

            else:
                box.append(lens)

    result = 0
    for box_id, box in enumerate(boxes):
        for lens_order, lens in enumerate(box):
            result += (box_id + 1) * (lens_order + 1) * lens.focal_length

    return result


def _hash(value: str) -> int:
    """
    >>> _hash('HASH')
    52
    >>> _hash('rn=1')
    30
    >>> _hash('cm-')
    253
    >>> _hash('qp=3')
    97
    >>> _hash('cm=2')
    47
    >>> _hash('qp-')
    14
    >>> _hash('pc=4')
    180
    >>> _hash('ot=9')
    9
    >>> _hash('ab=5')
    197
    >>> _hash('pc-')
    48
    >>> _hash('pc=6')
    214
    >>> _hash('ot=7')
    231
    """

    result = 0
    for c in value:
        result = ((result + ord(c)) * 17) % 256
    return result


if __name__ == "__main__":
    run_day(15,
            lambda: star1(load_as_str(txt_fn(__file__))),
            lambda: star2(load_as_str(txt_fn(__file__))),
            )
