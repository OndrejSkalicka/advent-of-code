from typing import List


def load_data(fn: str) -> List[int]:
    result = []
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            result.append(int(line))

    return result


def star1(data: List[int]) -> int:
    """
    >>> star1([1721, 979, 366, 299, 675, 1456])
    514579
    """

    # nice o(n2) algorithm there :D
    for x in data:
        for y in data:
            if x + y == 2020:
                return x * y

    return 0


def star2(data: List[int]) -> int:
    """
    >>> star2([1721, 979, 366, 299, 675, 1456])
    241861950
    """

    # nice o(n3) algorithm there :D
    for x in data:
        for y in data:
            for z in data:
                if x + y + z == 2020:
                    return x * y * z

    return 0


if __name__ == "__main__":
    data = load_data('data.txt')
    print('Day 1 Star 1: %d' % star1(data))
    print('Day 1 Star 2: %d' % star2(data))
