def star_1(threshold: int) -> str:
    """
    >>> star_1(9)
    '5158916779'

    >>> star_1(5)
    '0124515891'

    >>> star_1(18)
    '9251071085'

    >>> star_1(2018)
    '5941429882'
    """

    recipes = [3, 7]
    elves = [0, 1]

    while True:
        recipe = recipes[elves[0]] + recipes[elves[1]]

        if recipe < 10:
            recipes.append(recipe)
        elif recipe < 20:
            recipes.append(1)
            recipes.append(recipe - 10)
        else:
            raise Exception("Got combined recipe of %s, elves %s, recipes %s" % (recipe, elves, recipes))

        for i, elf in enumerate(elves):
            elves[i] = (elf + 1 + recipes[elf]) % len(recipes)

        if len(recipes) > threshold + 10:
            return ''.join(str(r) for r in recipes[threshold:threshold + 10])


def star_2(expected: str) -> int:
    """
    >>> star_2('51589')
    9

    >>> star_2('01245')
    5

    >>> star_2('92510')
    18

    >>> star_2('59414')
    2018
    """

    recipes = '37'
    elves = [0, 1]
    last = expected[-1]

    while True:
        recipe = int(recipes[elves[0]]) + int(recipes[elves[1]])
        lookie = False

        if recipe < 10:
            recipes += str(recipe)
            if str(recipe) == last:
                lookie = True
        elif recipe < 20:
            recipes += str(1)
            recipes += str(recipe - 10)

            if str(recipe - 10) == last or '1' == last:
                lookie = True
        else:
            raise Exception("Got combined recipe of %s, elves %s, recipes %s" % (recipe, elves, recipes))

        for i, elf in enumerate(elves):
            elves[i] = (elf + 1 + int(recipes[elf])) % len(recipes)

        if lookie and recipes.find(expected, len(recipes) - 7) > 0:
            return recipes.find(expected)


if __name__ == "__main__":
    print(star_1(323081))
    print(star_2('323081'))
