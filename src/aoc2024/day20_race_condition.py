from collections import defaultdict
from typing import Iterable

from util.data import load_strings_newline, txt_fn
from util.dijkstra import dijkstra
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str], min_difference: int = 100) -> int:
    """
    >>> star1([
    ...        "###############",
    ...        "#...#...#.....#",
    ...        "#.#.#.#.#.###.#",
    ...        "#S#...#.#.#...#",
    ...        "#######.#.#.###",
    ...        "#######.#.#...#",
    ...        "#######.#.###.#",
    ...        "###..E#...#...#",
    ...        "###.#######.###",
    ...        "#...###...#...#",
    ...        "#.#####.#.###.#",
    ...        "#.#...#.#.#...#",
    ...        "#.#.#.#.#.#.###",
    ...        "#...#...#...###",
    ...        "###############",
    ...       ], 64)
    1
    >>> star1([
    ...        "###############",
    ...        "#...#...#.....#",
    ...        "#.#.#.#.#.###.#",
    ...        "#S#...#.#.#...#",
    ...        "#######.#.#.###",
    ...        "#######.#.#...#",
    ...        "#######.#.###.#",
    ...        "###..E#...#...#",
    ...        "###.#######.###",
    ...        "#...###...#...#",
    ...        "#.#####.#.###.#",
    ...        "#.#...#.#.#...#",
    ...        "#.#.#.#.#.#.###",
    ...        "#...#...#...###",
    ...        "###############",
    ...       ], 12)
    8
    """

    return _solve(Grid.from_list_str(data), 2, min_difference)


def star2(data: list[str], min_difference: int = 100) -> int:
    """
    >>> star2([
    ...        "###############",
    ...        "#...#...#.....#",
    ...        "#.#.#.#.#.###.#",
    ...        "#S#...#.#.#...#",
    ...        "#######.#.#.###",
    ...        "#######.#.#...#",
    ...        "#######.#.###.#",
    ...        "###..E#...#...#",
    ...        "###.#######.###",
    ...        "#...###...#...#",
    ...        "#.#####.#.###.#",
    ...        "#.#...#.#.#...#",
    ...        "#.#.#.#.#.#.###",
    ...        "#...#...#...###",
    ...        "###############",
    ...       ], 76)
    3
    >>> star2([
    ...        "###############",
    ...        "#...#...#.....#",
    ...        "#.#.#.#.#.###.#",
    ...        "#S#...#.#.#...#",
    ...        "#######.#.#.###",
    ...        "#######.#.#...#",
    ...        "#######.#.###.#",
    ...        "###..E#...#...#",
    ...        "###.#######.###",
    ...        "#...###...#...#",
    ...        "#.#####.#.###.#",
    ...        "#.#...#.#.#...#",
    ...        "#.#.#.#.#.#.###",
    ...        "#...#...#...###",
    ...        "###############",
    ...       ], 66)
    67
    """

    return _solve(Grid.from_list_str(data), 20, min_difference)


def _adj(state: Xy, grid: Grid[str]) -> Iterable[tuple[Xy, int]]:
    result = []
    for xy in grid.adjacent(state):
        if grid[xy] != '#':
            result.append((xy, 1))
    return result


def _solve(grid: Grid[str], max_steps: int, min_difference: int) -> int:
    start = grid.find_all_xy('S')[0].xy
    end = grid.find_all_xy('E')[0].xy
    distances, _ = dijkstra(
        start,
        lambda point: _adj(point, grid)
    )
    rev_dist = dict()
    total_distance = distances[end]
    for xy in distances:
        rev_dist[xy] = total_distance - distances[xy]
    cheats: dict[int, int] = defaultdict(int)
    for a in distances.keys():
        for dx in range(-max_steps, max_steps + 1):
            for dy in range(-max_steps, max_steps + 1):
                if dx == 0 and dy == 0 or abs(dx) + abs(dy) > max_steps:
                    continue

                b = Xy(a.x + dx, a.y + dy)

                if b in distances:
                    saved_steps = total_distance - ((distances[a] + rev_dist[b]) + a.manhattan(b))
                    if saved_steps > 0:
                        cheats[saved_steps] += 1

    result = 0
    for k, v in cheats.items():
        if k >= min_difference:
            result += v
    return result


if __name__ == "__main__":
    run_day(20,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
