from dataclasses import dataclass, field, InitVar
from typing import Generator


@dataclass
class Program:
    memory_list: InitVar[list[int]]
    input: list[int] = field(default_factory=list)
    memory: dict[int, int] = field(default_factory=dict, init=False, repr=False)
    output: list[int] = field(default_factory=list, init=False)
    pointer: int = field(default=0, init=False)
    waiting_for_input: bool = field(default=False, init=False)
    waiting_for_input_address: int = field(default=0, init=False)
    relative_base: int = field(default=0, init=False)

    def __post_init__(self, memory_list: list[int]) -> None:
        self.memory = {k: v for k, v in enumerate(memory_list)}
        self.input = self.input[:]

    def run(self) -> None:
        """
        >>> p = Program([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]); p.run(); p.memory_as_list()
        [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]

        >>> p = Program([1, 0, 0, 0, 99]); p.run(); p.memory_as_list()
        [2, 0, 0, 0, 99]

        >>> p = Program([2, 3, 0, 3, 99]); p.run(); p.memory_as_list()
        [2, 3, 0, 6, 99]

        >>> p = Program([2, 4, 4, 5, 99, 0]); p.run(); p.memory_as_list()
        [2, 4, 4, 5, 99, 9801]

        >>> p = Program([1, 1, 1, 4, 99, 5, 6, 0, 99]); p.run(); p.memory_as_list()
        [30, 1, 1, 4, 2, 5, 6, 0, 99]

        >>> p = Program([3,0,4,0,99], input=[42]); p.run(); p.output
        [42]

        >>> p = Program([3,9,8,9,10,9,4,9,99,-1,8], input=[8]); p.run(); p.output
        [1]

        >>> p = Program([3,9,8,9,10,9,4,9,99,-1,8], input=[9]); p.run(); p.output
        [0]

        >>> p = Program([3,9,7,9,10,9,4,9,99,-1,8], input=[7]); p.run(); p.output
        [1]

        >>> p = Program([3,9,7,9,10,9,4,9,99,-1,8], input=[8]); p.run(); p.output
        [0]

        >>> p = Program([3,3,1108,-1,8,3,4,3,99], input=[8]); p.run(); p.output
        [1]

        >>> p = Program([3,3,1108,-1,8,3,4,3,99], input=[9]); p.run(); p.output
        [0]

        >>> p = Program([3,3,1107,-1,8,3,4,3,99], input=[7]); p.run(); p.output
        [1]

        >>> p = Program([3,3,1107,-1,8,3,4,3,99], input=[8]); p.run(); p.output
        [0]

        >>> p = Program([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input=[0]); p.run(); p.output
        [0]

        >>> p = Program([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input=[42]); p.run(); p.output
        [1]

        >>> p = Program([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input=[0]); p.run(); p.output
        [0]

        >>> p = Program([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input=[42]); p.run(); p.output
        [1]

        >>> p = Program([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,\
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input=[7]); p.run(); p.output
        [999]

        >>> p = Program([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,\
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input=[8]); p.run(); p.output
        [1000]

        >>> p = Program([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,\
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input=[9]); p.run(); p.output
        [1001]

        >>> p = Program([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]); p.run(); p.output
        [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]

        >>> p = Program([1102,34915192,34915192,7,4,7,99,0]); p.run(); p.output
        [1219070632396864]

        >>> p = Program([104,1125899906842624,99]); p.run(); p.output
        [1125899906842624]
        """

        if self.waiting_for_input:
            raise Exception("Cannot re-run program waiting for input. Use resume() instead.")

        while True:
            op_and_params = self.next_opcode()
            op = op_and_params[0]
            params = op_and_params[1:]

            match op:
                case 1:
                    # add
                    self.memory[params[2]] = params[0] + params[1]

                case 2:
                    # mult
                    self.memory[params[2]] = params[0] * params[1]

                case 3:
                    # read-input
                    if not self.input:
                        self.waiting_for_input_address = params[0]
                        self.waiting_for_input = True
                        return

                    self.memory[params[0]] = self.input.pop(0)

                case 4:
                    # write-output
                    self.output.append(params[0])

                case 5:
                    # jump-if-true
                    if params[0] != 0:
                        self.pointer = params[1]

                case 6:
                    # jump-if-false
                    if params[0] == 0:
                        self.pointer = params[1]

                case 7:
                    # less than
                    if params[0] < params[1]:
                        self.memory[params[2]] = 1
                    else:
                        self.memory[params[2]] = 0

                case 8:
                    # equals
                    if params[0] == params[1]:
                        self.memory[params[2]] = 1
                    else:
                        self.memory[params[2]] = 0

                case 9:
                    # adjust-the-relative-base
                    self.relative_base += params[0]

                case 99:
                    # terminate
                    return

                case _:
                    raise Exception(f"Illegal opt {op}")

    def resume(self) -> None:
        if not self.waiting_for_input:
            raise Exception("Program not waiting for input, cannot resume.")

        self.memory[self.waiting_for_input_address] = self.input.pop(0)

        self.waiting_for_input = False
        self.waiting_for_input_address = 0
        self.run()

    def run_or_resume(self) -> None:
        if self.waiting_for_input:
            self.resume()
        else:
            self.run()

    def write_input(self, *args: int) -> None:
        for value in args:
            self.input.append(value)

    def write_ascii_command(self, command: str) -> None:
        for c in command:
            self.write_input(ord(c))

        self.write_input(ord("\n"))

    def read_output(self) -> int:
        return self.output.pop(0)

    def iter_output(self) -> Generator[int, None, None]:
        while self.output:
            yield self.output.pop(0)

    def next_opcode(self) -> list[int]:
        """
        >>> Program([1002,4,3,4,33]).next_opcode()
        [2, 33, 3, 4]
        """

        op_str = str(self._next_mem())

        if len(op_str) == 1:
            op = int(op_str)
            modes = ''

        else:
            op = int(op_str[-2:])
            modes = op_str[:len(op_str) - 2]

        params_count: tuple[int, int] = {
            1: (2, 1),  # add
            2: (2, 1),  # mult
            3: (0, 1),  # read-input
            4: (1, 0),  # write-output
            5: (2, 0),  # jump-if-true
            6: (2, 0),  # jump-if-false
            7: (2, 1),  # less than
            8: (2, 1),  # equals
            9: (1, 0),  # adjust-the-relative-base
            99: (0, 0),  # terminate
        }[op]
        """
        Value -- first number of IN parameters, second number of OUT parameters
        """

        modes = modes.rjust(sum(params_count), '0')[::-1]

        params = []

        for i in range(sum(params_count)):
            mode = int(modes[i])
            raw_value = self._next_mem()

            # in params
            if i < params_count[0]:
                match mode:
                    case 0:
                        # position mode -- value stored at position
                        value = self._mem_at(raw_value)
                    case 1:
                        # immediate mode -- value stored in parameter
                        value = raw_value
                    case 2:
                        # relative base -- value stored at base + position
                        value = self._mem_at(self.relative_base + raw_value)
                    case _:
                        raise Exception(f"Uknown parameter mode {mode}")

                params.append(value)
            else:
                match mode:
                    case 0:
                        # position mode -- value stored at position
                        params.append(raw_value)
                    case 2:
                        # relative base -- value stored at base + position
                        params.append(self.relative_base + raw_value)
                    case _:
                        raise Exception(f"Uknown parameter mode {mode}")

        return [op] + params

    def memory_as_list(self) -> list[int]:
        return list(self._mem_at(addr) for addr in range(max(self.memory.keys()) + 1))

    def _next_mem(self) -> int:
        self.pointer += 1
        return self.memory[self.pointer - 1]

    def _mem_at(self, address: int) -> int:
        return self.memory[address] if address in self.memory else 0
