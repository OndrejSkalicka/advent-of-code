from collections import defaultdict

from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["3   4",
    ...       "4   3",
    ...       "2   5",
    ...       "1   3",
    ...       "3   9",
    ...       "3   3"])
    11
    """

    left = []
    right = []

    for row in data:
        p = row.split("   ")
        left.append(int(p[0]))
        right.append(int(p[1]))

    left.sort()
    right.sort()

    result = 0
    for i in range(len(left)):
        result += abs(left[i] - right[i])

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["3   4",
    ...       "4   3",
    ...       "2   5",
    ...       "1   3",
    ...       "3   9",
    ...       "3   3"])
    31
    """

    left = []
    right: dict[int, int] = defaultdict(int)

    for row in data:
        p = row.split("   ")
        left.append(int(p[0]))
        right[int(p[1])] += 1

    result = 0
    for number in left:
        result += number * right[number]

    return result


if __name__ == "__main__":
    run_day(1,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
