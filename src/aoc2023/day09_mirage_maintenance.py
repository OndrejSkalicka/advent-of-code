from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "0 3 6 9 12 15",
    ...     "1 3 6 10 15 21",
    ...     "10 13 16 21 30 45",
    ... ])
    114
    """

    result = 0
    for row in data:
        numbers = [[int(e) for e in row.split(" ")]]

        while True:
            active_row = numbers[-1]
            new_row = []

            active_number = active_row[0]

            for num in active_row[1:]:
                new_row.append(num - active_number)
                active_number = num

            numbers.append(new_row)
            active_row = new_row

            if len([e for e in active_row if e != 0]) == 0:
                break

        numbers.reverse()
        numbers[0].append(0)

        for i in range(1, len(numbers)):
            numbers[i].append(numbers[i][-1] + numbers[i - 1][-1])

        result += numbers[-1][-1]

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "10 13 16 21 30 45",
    ... ])
    5
    """

    result = 0
    for row in data:
        numbers = [[int(e) for e in row.split(" ")]]

        while True:
            active_row = numbers[-1]
            new_row = []

            active_number = active_row[0]

            for num in active_row[1:]:
                new_row.append(num - active_number)
                active_number = num

            numbers.append(new_row)
            active_row = new_row

            if len([e for e in active_row if e != 0]) == 0:
                break

        numbers.reverse()
        numbers[0].insert(0, 0)

        for i in range(1, len(numbers)):
            numbers[i].insert(0, numbers[i][0] - numbers[i - 1][0])

        result += numbers[-1][0]

    return result


if __name__ == "__main__":
    run_day(9,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
