from util.data import load_strings_newline, txt_fn
from util.perf import run_day
from util.record import Grid, Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["AAAA",
    ...        "BBCD",
    ...        "BBCC",
    ...        "EEEC",])
    140
    >>> star1(["OOOOO",
    ...        "OXOXO",
    ...        "OOOOO",
    ...        "OXOXO",
    ...        "OOOOO",])
    772
    >>> star1(["RRRRIICCFF",
    ...        "RRRRIICCCF",
    ...        "VVRRRCCFFF",
    ...        "VVRCCCJFFF",
    ...        "VVVVCJJCFE",
    ...        "VVIVCCJJEE",
    ...        "VVIIICJJEE",
    ...        "MIIIIIJJEE",
    ...        "MIIISIJEEE",
    ...        "MMMISSJEEE",])
    1930
    """

    grid = Grid.from_list_str(data)
    processed: set[Xy] = set()

    result = 0
    for cell in grid:
        if cell.xy in processed:
            continue

        # floodfill
        color = cell.value
        buffer: set[Xy] = set()
        region: set[Xy] = set()
        buffer.add(cell.xy)

        while buffer:
            xy = buffer.pop()
            region.add(xy)
            processed.add(xy)
            for adj in grid.adjacent(xy):
                if adj in processed or adj in region or adj in buffer or grid[adj] != color:
                    continue

                buffer.add(adj)

        sides = 0
        for xy in region:
            sides += 4
            for adj in grid.adjacent(xy):
                if grid[adj] == color:
                    sides -= 1

        result += sides * len(region)

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["AAAA",
    ...        "BBCD",
    ...        "BBCC",
    ...        "EEEC",])
    80
    >>> star2(["OOOOO",
    ...        "OXOXO",
    ...        "OOOOO",
    ...        "OXOXO",
    ...        "OOOOO",])
    436
    >>> star2(["EEEEE",
    ...        "EXXXX",
    ...        "EEEEE",
    ...        "EXXXX",
    ...        "EEEEE",])
    236
    >>> star2(["AAAAAA",
    ...        "AAABBA",
    ...        "AAABBA",
    ...        "ABBAAA",
    ...        "ABBAAA",
    ...        "AAAAAA",])
    368
    >>> star2(["RRRRIICCFF",
    ...        "RRRRIICCCF",
    ...        "VVRRRCCFFF",
    ...        "VVRCCCJFFF",
    ...        "VVVVCJJCFE",
    ...        "VVIVCCJJEE",
    ...        "VVIIICJJEE",
    ...        "MIIIIIJJEE",
    ...        "MIIISIJEEE",
    ...        "MMMISSJEEE",])
    1206
    """

    grid = Grid.from_list_str(data)
    processed: set[Xy] = set()

    result = 0
    for cell in grid:
        if cell.xy in processed:
            continue

        # floodfill
        color = cell.value
        buffer: set[Xy] = set()
        region: set[Xy] = set()
        buffer.add(cell.xy)

        while buffer:
            xy = buffer.pop()
            region.add(xy)
            processed.add(xy)
            for adj in grid.adjacent(xy):
                if adj in processed or adj in region or adj in buffer or grid[adj] != color:
                    continue

                buffer.add(adj)

        sides = 0
        for dxy in Xy.cardinal_directions():
            processed_sides: set[Xy] = set()

            for xy in region:
                if xy in processed_sides:
                    continue

                processed_sides.add(xy)

                if grid.get_or_default(xy + dxy, '') != color:
                    sides += 1

                    mov_xy = xy
                    for rot_dxy in (Xy(dxy.y, dxy.x), Xy(-dxy.y, -dxy.x)):
                        # expand
                        while True:
                            mov_xy += rot_dxy
                            if grid.get_or_default(mov_xy, '') == color and grid.get_or_default(mov_xy + dxy, '') != color:
                                processed_sides.add(mov_xy)
                            else:
                                break

        result += sides * len(region)

    return result


if __name__ == "__main__":
    run_day(12,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
