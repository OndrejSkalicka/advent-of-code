package aoc2020.day06;

public class Cup {
    private final int value;
    private Cup nextCup;

    public Cup(final int value) {
        this.value = value;
    }

    public void insertAfter(final Cup target) {
        final Cup currentNext = this.nextCup;
        this.nextCup = target;

        Cup active = target;
        while (active.getNextCup() != null) {
            active = active.getNextCup();
        }

        active.setNextCup(currentNext);
    }

    public Cup pluckNext(final int count) {
        final Cup pluckRoot = this.nextCup;
        Cup active = this.nextCup;

        for (int i = 0; i < count - 1; ++i) {
            active = active.getNextCup();
        }

        this.nextCup = active.getNextCup();
        active.setNextCup(null);

        return pluckRoot;
    }

    public int getValue() {
        return value;
    }

    public Cup getNextCup() {
        return nextCup;
    }

    public Cup setNextCup(final Cup nextCup) {
        this.nextCup = nextCup;
        return this;
    }
}
