from aoc2019.intcode import Program
from util.data import load_numbers_csv


def star1(memory: list[int]) -> int:
    program = Program(memory, input=[1])
    program.run()

    return program.output[-1]


def star2(memory: list[int]) -> int:
    program = Program(memory, input=[5])
    program.run()

    return program.output[0]


if __name__ == "__main__":
    print('Day 5 Star 1: %d' % star1(load_numbers_csv('data.txt')))
    print('Day 5 Star 2: %d' % star2(load_numbers_csv('data.txt')))
