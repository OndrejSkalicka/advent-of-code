from collections import deque

from util.data import txt_fn, load_strings_newline
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...        "498,4 -> 498,6 -> 496,6",
    ...        "503,4 -> 502,4 -> 502,9 -> 494,9",
    ...        ])
    24
    """

    cave = _cave(data)

    lowest_y = max(c[1] for c in cave.keys())
    sand = None
    while sand is None or sand[1] <= lowest_y:
        if sand is None:
            sand = [500, 0]

            continue

        for dxy in ((0, 1), (-1, 1), (1, 1)):
            target = sand[0] + dxy[0], sand[1] + dxy[1]
            if target not in cave:
                sand = target
                break
        else:
            cave[sand] = 'o'
            sand = None

    return sum(1 for c in cave.values() if c == 'o')


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...        "498,4 -> 498,6 -> 496,6",
    ...        "503,4 -> 502,4 -> 502,9 -> 494,9",
    ...        ])
    93
    """

    cave = _cave(data)

    floor_y = max(c[1] for c in cave.keys()) + 2

    sand_line = deque([(500, 0)])
    while sand_line:
        sand = sand_line.popleft()

        if sand in cave:
            continue

        cave[sand] = 'o'

        for dx, dy in ((0, 1), (-1, 1), (1, 1)):
            child = sand[0] + dx, sand[1] + dy
            if child[1] != floor_y and child not in cave:
                sand_line.append(child)

    return sum(1 for c in cave.values() if c == 'o')


def _cave(data: list[str]) -> dict[tuple[int, int], str]:
    cave: dict[tuple[int, int], str] = dict()
    for row in data:
        current = None
        for points in (row.split(' -> ')):
            target = int(points.split(',')[0]), int(points.split(',')[1])

            if not current:
                current = target
                continue

            assert current != target
            if target[0] == current[0]:
                dx = 0
                dy = 1 if target[1] > current[1] else -1
            else:
                dx = 1 if target[0] > current[0] else -1
                dy = 0

            while current != target:
                cave[current] = '#'
                current = current[0] + dx, current[1] + dy
            cave[current] = '#'
    return cave


if __name__ == "__main__":
    run_day(14,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
