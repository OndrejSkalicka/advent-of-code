import heapq
from typing import Callable, TypeVar, Iterable, Optional

T = TypeVar('T')


def dijkstra(start: T,
             adjacent: Callable[[T], Iterable[tuple[T, int]]],
             destination: Optional[Callable[[T], bool]] = None
             ) -> tuple[dict[T, int], dict[T, T]]:
    pq: list[tuple[int, T]] = []
    active: T
    heapq.heappush(pq, (0, start))
    visited = set()
    distances: dict[T, int] = {start: 0}
    parents_map: dict[T, T] = {}

    while pq:
        active_distance, active = heapq.heappop(pq)

        if active in visited:
            continue

        visited.add(active)
        if destination is not None and destination(active):
            break

        for node, node_distance in adjacent(active):
            if node in visited:
                continue

            if node in distances and distances[node] <= active_distance + node_distance:
                continue

            parents_map[node] = active
            distances[node] = active_distance + node_distance

            heapq.heappush(pq, (active_distance + node_distance, node))

    return distances, parents_map
