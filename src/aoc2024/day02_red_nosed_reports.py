from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["7 6 4 2 1",
    ...       "1 2 7 8 9",
    ...       "9 7 6 2 1",
    ...       "1 3 2 4 5",
    ...       "8 6 4 4 1",
    ...       "1 3 6 7 9"])
    2
    """

    result = 0
    for row in data:
        if is_safe([int(p) for p in row.split(' ')]):
            result += 1

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["7 6 4 2 1",
    ...       "1 2 7 8 9",
    ...       "9 7 6 2 1",
    ...       "1 3 2 4 5",
    ...       "8 6 4 4 1",
    ...       "1 3 6 7 9"])
    4
    """

    result = 0
    for row in data:
        levels = [int(p) for p in row.split(' ')]

        if is_safe(levels):
            result += 1
            continue

        for i in range(len(levels)):
            sub_levels = levels[:i] + levels[i + 1:]
            if is_safe(sub_levels):
                result += 1
                break

    return result


def is_safe(levels: list[int]) -> bool:
    report_inc = levels[0] < levels[1]
    for i in range(len(levels) - 1):
        inc = levels[i] < levels[i + 1]

        if report_inc != inc:
            return False

        if not 1 <= abs(levels[i] - levels[i + 1]) <= 3:
            return False

    return True


if __name__ == "__main__":
    run_day(2,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
