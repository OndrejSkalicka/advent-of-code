import numpy as np

from util.data import load_strings_newline, txt_fn, to_2d_np_int
from util.dtype import np_array_2d_int
from util.perf import run_day
from util.record import Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["30373",
    ...        "25512",
    ...        "65332",
    ...        "33549",
    ...        "35390",
    ...        ])
    21
    """

    forest = to_2d_np_int(data)
    visibility = np.zeros(forest.shape, dtype=int)

    for y in range(forest.shape[0]):
        _view_from_edge(Xy(0, y),
                        Xy(1, 0),
                        forest, visibility)
        _view_from_edge(Xy(forest.shape[1] - 1, y),
                        Xy(-1, 0),
                        forest, visibility)

    for x in range(forest.shape[1]):
        _view_from_edge(Xy(x, 0),
                        Xy(0, 1),
                        forest, visibility)
        _view_from_edge(Xy(x, forest.shape[0] - 1),
                        Xy(0, -1),
                        forest, visibility)

    return int(np.sum(visibility))


def _view_from_edge(xy: Xy, dxy: Xy, forest: np_array_2d_int, visibility: np_array_2d_int) -> None:
    tallest = -1
    while xy.in_np_shape(forest.shape):
        if forest[xy.np_yx()] > tallest:
            tallest = forest[xy.np_yx()]
            visibility[xy.np_yx()] = 1

        xy += dxy


def star2(data: list[str]) -> int:
    """
    >>> star2(["30373",
    ...        "25512",
    ...        "65332",
    ...        "33549",
    ...        "35390",
    ...        ])
    8
    """

    forest = np.array([[int(c) for c in row] for row in data], dtype=int)

    best = 0

    for xy in Xy.iter_np_shape(forest.shape):
        visibility = 1
        for dxy in Xy.cardinal_directions():
            visibility *= _view_from_tree(xy, dxy, forest)

        if visibility > best:
            best = visibility

    return best


def _view_from_tree(xy: Xy, dxy: Xy, forest: np_array_2d_int) -> int:
    height = forest[xy.np_yx()]
    view = 0
    while True:
        xy += dxy

        if not xy.in_np_shape(forest.shape):
            return view

        view += 1

        if forest[xy.np_yx()] >= height:
            return view


if __name__ == "__main__":
    run_day(8,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
