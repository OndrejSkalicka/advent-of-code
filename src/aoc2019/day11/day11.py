from dataclasses import dataclass

from aoc2019.intcode import Program
from util.data import load_numbers_csv

XY = tuple[int, int]

DIRECTIONS: tuple[XY, XY, XY, XY] = (
    (0, -1),  # UP
    (1, 0),  # RIGHT
    (0, 1),  # DOWN
    (-1, 0),  # LEFT
)


@dataclass
class Robot:
    pos: XY = 0, 0
    direction: int = 0


def star1(memory: list[int]) -> int:
    robot = Robot()
    whites: set[XY] = set()
    painted: set[XY] = set()

    program = Program(memory)
    program.run()

    while True:
        if not program.waiting_for_input:
            return len(painted)

        program.input.append(1 if robot.pos in whites else 0)
        program.resume()

        color = program.output.pop(0)
        direction = program.output.pop(0)

        if color == 1:
            whites.add(robot.pos)
        elif robot.pos in whites:
            whites.remove(robot.pos)

        painted.add(robot.pos)

        if direction == 0:
            robot.direction = (robot.direction - 1) % 4
        else:
            robot.direction = (robot.direction + 1) % 4

        robot.pos = robot.pos[0] + DIRECTIONS[robot.direction][0], robot.pos[1] + DIRECTIONS[robot.direction][1]


def star2(memory: list[int]) -> str:
    robot = Robot()
    whites: set[XY] = set()
    painted: set[XY] = set()
    whites.add((0, 0))

    program = Program(memory)
    program.run()

    while True:
        if not program.waiting_for_input:
            break

        program.input.append(1 if robot.pos in whites else 0)
        program.resume()

        color = program.output.pop(0)
        direction = program.output.pop(0)

        if color == 1:
            whites.add(robot.pos)
        elif robot.pos in whites:
            whites.remove(robot.pos)

        painted.add(robot.pos)

        if direction == 0:
            robot.direction = (robot.direction - 1) % 4
        else:
            robot.direction = (robot.direction + 1) % 4

        robot.pos = robot.pos[0] + DIRECTIONS[robot.direction][0], robot.pos[1] + DIRECTIONS[robot.direction][1]

    result = ''

    for y in range(min(xy[1] for xy in painted), max(xy[1] for xy in painted) + 1):
        for x in range(min(xy[0] for xy in painted), max(xy[0] for xy in painted) + 1):
            if (x, y) in whites:
                result += '#'
            else:
                result += '.'

        result += "\n"

    return result


if __name__ == "__main__":
    print('Day 11 Star 1: %d' % star1(load_numbers_csv('data.txt')))
    print('Day 11 Star 2:\n%s' % star2(load_numbers_csv('data.txt')))
