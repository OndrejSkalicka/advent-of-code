set N=100
python -m timeit -n %N% -s "from day15 import Game;g = Game.load('data.txt')" "g.turn_order()"
python -m timeit -n %N% -s "from day15 import Game;g = Game.load('data.txt');tu=g.turn_order()" "g.enemies(tu[0])"
python -m timeit -n %N% -s "from day15 import Game;g = Game.load('data.txt');u=g.units[0];e=g.enemies(u)" "g.adjacent_squares(e)"
python -m timeit -n %N% -s "from day15 import Game;g = Game.load('data.txt');u=g.units[0];e=g.enemies(u);s=g.adjacent_squares(e)" "g.closest_target(u,s)"
python -m timeit -n %N% -s "from day15 import Game;" "Game.load('data.txt').turn()"