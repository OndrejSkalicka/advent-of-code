from util.data import load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> str:
    """
    >>> star1([
    ...        "1=-0-2",
    ...        "12111",
    ...        "2=0=",
    ...        "21",
    ...        "2=01",
    ...        "111",
    ...        "20012",
    ...        "112",
    ...        "1=-1=",
    ...        "1-12",
    ...        "12",
    ...        "1=",
    ...        "122",
    ...        ])
    '2=-1=0'
    """

    return to_snafu(sum(from_snafu(c) for c in data))


def from_snafu(snafu: str) -> int:
    """
    >>> from_snafu('1=-0-2')
    1747
    >>> from_snafu('12111')
    906
    >>> from_snafu('2=0=')
    198
    >>> from_snafu('21')
    11
    >>> from_snafu('2=01')
    201
    >>> from_snafu('111')
    31
    >>> from_snafu('20012')
    1257
    >>> from_snafu('112')
    32
    >>> from_snafu('1=-1=')
    353
    >>> from_snafu('1-12')
    107
    >>> from_snafu('12')
    7
    >>> from_snafu('1=')
    3
    >>> from_snafu('122')
    37
    """
    result = 0
    for order, c in enumerate(reversed(snafu)):
        result += 5 ** order * {'2': 2, '1': 1, '0': 0, '-': -1, '=': -2}[c]

    return result


def to_snafu(target: int) -> str:
    """
    >>> to_snafu(4890)
    '2=-1=0'
    """
    # find first higher
    snafu = '2'
    while True:
        if from_snafu(snafu) >= target:
            break

        snafu += '2'

    index = 0
    snafu_as_int = from_snafu(snafu)
    while snafu_as_int != target:
        if snafu[index] == '=':
            index += 1

        assert index < len(snafu)
        candidate = snafu[:index] + '10-='['210-'.index(snafu[index])] + snafu[index + 1:]

        candidate_as_int = from_snafu(candidate)
        if candidate_as_int >= target:
            snafu = candidate
            snafu_as_int = candidate_as_int
        else:
            # too much of a decrease
            index += 1
            assert index < len(snafu)

    return snafu


if __name__ == "__main__":
    run_day(25,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            )
