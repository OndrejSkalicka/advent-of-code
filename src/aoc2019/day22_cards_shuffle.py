import logging
from typing import Callable, Any

from util.data import load_as_str
from util.logger_config import init_logger
from util.perf import run_day

logger = logging.getLogger(__name__)

t_deck = list[int]


def star1(instructions: str) -> int:
    return power_shuffle(instructions, deck_size=10_007, card=2019)


def star2(instructions: str) -> int:
    deck_size = 119_315_717_514_047
    full_iterations = 101_741_582_076_661

    card_pos = 2020
    iteration_by_position = dict()

    parsed: list[tuple[Callable[[int, int], int], Any]] = []
    for instruction in instructions.split("\n"):
        if instruction.startswith("deal with increment "):
            parsed.append((lambda c, d: (c * d) % deck_size, int(instruction[len("deal with increment "):])))

        elif instruction == 'deal into new stack':
            parsed.append((lambda c, d: deck_size - c - 1, None))

        elif instruction.startswith('cut '):
            parsed.append((lambda c, d: (c - d) % deck_size, int(instruction[len("cut "):])))

        else:
            raise Exception(f"Illegal instruction '{instruction}'")

    for i in range(full_iterations):
        for pi, par in parsed:
            card_pos = pi(card_pos, par)

        if card_pos in iteration_by_position:
            logger.info(f"Have a cycle at {i:,}!")

        else:
            iteration_by_position[card_pos] = i

        if i and i % 1_000_000 == 0:
            logger.info(f"Processed {i:,} iterations...")

    return len(iteration_by_position)


def test_power_shuffle(instructions: str, deck_size: int) -> list[int]:
    """
    >>> test_power_shuffle("deal with increment 7\\n"
    ...                    "deal into new stack\\n"
    ...                    "deal into new stack", deck_size=10)
    [0, 3, 6, 9, 2, 5, 8, 1, 4, 7]

    >>> test_power_shuffle("cut 6\\n"
    ...                    "deal with increment 7\\n"
    ...                    "deal into new stack", deck_size=10)
    [3, 0, 7, 4, 1, 8, 5, 2, 9, 6]

    >>> test_power_shuffle("deal with increment 7\\n"
    ...                    "deal with increment 9\\n"
    ...                    "cut -2", deck_size=10)
    [6, 3, 0, 7, 4, 1, 8, 5, 2, 9]

    >>> test_power_shuffle("deal into new stack\\n"
    ...                    "cut -2\\n"
    ...                    "deal with increment 7\\n"
    ...                    "cut 8\\n"
    ...                    "cut -4\\n"
    ...                    "deal with increment 7\\n"
    ...                    "cut 3\\n"
    ...                    "deal with increment 9\\n"
    ...                    "deal with increment 3\\n"
    ...                    "cut -1", deck_size=10)
    [9, 2, 5, 8, 1, 4, 7, 0, 3, 6]
    """

    result = [-1] * deck_size

    for c in range(deck_size):
        destination = power_shuffle(instructions, deck_size, c)
        result[destination] = c

    return result


def power_shuffle(instructions: str, deck_size: int, card: int) -> int:
    card_pos = card

    for instruction in instructions.split("\n"):
        if instruction.startswith("deal with increment "):
            increment = int(instruction[len("deal with increment "):])
            card_pos = (card_pos * increment) % deck_size

        elif instruction == 'deal into new stack':
            card_pos = deck_size - card_pos - 1

        elif instruction.startswith('cut '):
            n = int(instruction[len("cut "):])

            card_pos = (card_pos - n) % deck_size

        else:
            raise Exception(f"Illegal instruction '{instruction}'")

    return card_pos


def shuffle(instructions: str, deck_size: int) -> t_deck:
    """
    >>> shuffle("deal with increment 7\\n"
    ...         "deal into new stack\\n"
    ...         "deal into new stack", deck_size=10)
    [0, 3, 6, 9, 2, 5, 8, 1, 4, 7]

    >>> shuffle("cut 6\\n"
    ...         "deal with increment 7\\n"
    ...         "deal into new stack", deck_size=10)
    [3, 0, 7, 4, 1, 8, 5, 2, 9, 6]

    >>> shuffle("deal with increment 7\\n"
    ...         "deal with increment 9\\n"
    ...         "cut -2", deck_size=10)
    [6, 3, 0, 7, 4, 1, 8, 5, 2, 9]

    >>> shuffle("deal into new stack\\n"
    ...         "cut -2\\n"
    ...         "deal with increment 7\\n"
    ...         "cut 8\\n"
    ...         "cut -4\\n"
    ...         "deal with increment 7\\n"
    ...         "cut 3\\n"
    ...         "deal with increment 9\\n"
    ...         "deal with increment 3\\n"
    ...         "cut -1", deck_size=10)
    [9, 2, 5, 8, 1, 4, 7, 0, 3, 6]
    """

    deck: t_deck = list(range(deck_size))

    for instruction in instructions.split("\n"):
        if instruction.startswith("deal with increment "):
            deck = deal_with_increment(deck, int(instruction[len("deal with increment "):]))

        elif instruction == 'deal into new stack':
            deck = deal_into_new_stack(deck)

        elif instruction.startswith('cut '):
            deck = cut_n(deck, int(instruction[len("cut "):]))

        else:
            raise Exception(f"Illegal instruction '{instruction}'")

    return deck


def deal_into_new_stack(deck: t_deck) -> t_deck:
    """
    >>> deal_into_new_stack(list(range(10)))
    [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    """
    return deck[::-1]


def cut_n(deck: t_deck, n: int) -> t_deck:
    """
    >>> cut_n(list(range(10)), 3)
    [3, 4, 5, 6, 7, 8, 9, 0, 1, 2]

    >>> cut_n(list(range(10)), -4)
    [6, 7, 8, 9, 0, 1, 2, 3, 4, 5]
    """
    return deck[n:] + deck[:n]


def deal_with_increment(deck: t_deck, increment: int) -> t_deck:
    """
    >>> deal_with_increment(list(range(10)), 3)
    [0, 7, 4, 1, 8, 5, 2, 9, 6, 3]
    """
    new_deck = [0] * len(deck)

    for i, card in enumerate(deck):
        assert new_deck[(i * increment) % len(deck)] == 0

        new_deck[(i * increment) % len(deck)] = card

    return new_deck


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(22,
            lambda: star1(load_as_str('day22.txt')),
            # lambda: star2(load_as_str('day22.txt')),
            )
