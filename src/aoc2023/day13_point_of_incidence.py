from typing import Optional

from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day
from util.record import Grid


def star1(data: list[str]) -> int:
    """
    >>> star1([
    ...     "#.##..##.",
    ...     "..#.##.#.",
    ...     "##......#",
    ...     "##......#",
    ...     "..#.##.#.",
    ...     "..##..##.",
    ...     "#.#.##.#.",
    ...     "",
    ...     "#...##..#",
    ...     "#....#..#",
    ...     "..##..###",
    ...     "#####.##.",
    ...     "#####.##.",
    ...     "..##..###",
    ...     "#....#..#",
    ... ])
    405
    """

    result = 0
    for mirror in l_split(data, ''):
        split = _double_split(mirror)
        assert split
        result += split

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2([
    ...     "#.##..##.",
    ...     "..#.##.#.",
    ...     "##......#",
    ...     "##......#",
    ...     "..#.##.#.",
    ...     "..##..##.",
    ...     "#.#.##.#.",
    ...     "",
    ...     "#...##..#",
    ...     "#....#..#",
    ...     "..##..###",
    ...     "#####.##.",
    ...     "#####.##.",
    ...     "..##..###",
    ...     "#....#..#",
    ... ])
    400
    """

    result = 0
    for base_mirror in l_split(data, ''):
        grid = Grid.from_list_str(base_mirror)
        original = _double_split(base_mirror)
        for smudge in grid:
            mirror = base_mirror[:]

            x = smudge.xy.x
            y = smudge.xy.y

            mirror[y] = base_mirror[y][:x] + ('.' if base_mirror[y][x] == '#' else '#') + base_mirror[y][x + 1:]

            split = _double_split(mirror, ignore=original)
            if split is not None:
                result += split
                break
        else:
            assert False

    return result


def _double_split(mirror: list[str], ignore: Optional[int] = None) -> Optional[int]:
    split = _split(mirror, ignore=ignore // 100 if ignore is not None and ignore >= 100 else None)
    if split is not None:
        return split * 100

    else:
        new_mirror = []
        for i in range(len(mirror[0])):
            new_mirror.append(''.join(e[i] for e in mirror))

        split = _split(new_mirror, ignore=ignore)
        if split is not None:
            return split

    return None


def _split(mirror: list[str], ignore: Optional[int] = None) -> Optional[int]:
    for split_line in range(1, len(mirror)):
        if ignore is not None and split_line == ignore:
            continue
        a = mirror[:split_line][::-1]
        b = mirror[split_line:]

        match = True
        for i in range(min(len(a), len(b))):
            if a[i] != b[i]:
                match = False
                break

        if match:
            return split_line

    return None


if __name__ == "__main__":
    run_day(13,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
