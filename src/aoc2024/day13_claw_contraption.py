import re

from util.data import load_strings_newline, txt_fn, l_split
from util.perf import run_day
from util.record import Xy


def star1(data: list[str]) -> int:
    """
    >>> star1(["Button A: X+94, Y+34",
    ...        "Button B: X+22, Y+67",
    ...        "Prize: X=8400, Y=5400",
    ...        "",
    ...        "Button A: X+26, Y+66",
    ...        "Button B: X+67, Y+21",
    ...        "Prize: X=12748, Y=12176",
    ...        "",
    ...        "Button A: X+17, Y+86",
    ...        "Button B: X+84, Y+37",
    ...        "Prize: X=7870, Y=6450",
    ...        "",
    ...        "Button A: X+69, Y+23",
    ...        "Button B: X+27, Y+71",
    ...        "Prize: X=18641, Y=10279",])
    480
    """

    result = 0

    for rows in l_split(data):
        m = re.match(r'^Button A: X\+(\d+), Y\+(\d+)$', rows[0])
        assert m
        button_a = Xy(int(m[1]), int(m[2]))
        m = re.match(r'^Button B: X\+(\d+), Y\+(\d+)$', rows[1])
        assert m
        button_b = Xy(int(m[1]), int(m[2]))
        m = re.match(r'^Prize: X=(\d+), Y=(\d+)$', rows[2])
        assert m
        prize = Xy(int(m[1]), int(m[2]))

        b = (button_a.x * prize.y - button_a.y * prize.x) // (button_a.x * button_b.y - button_a.y * button_b.x)
        a = (button_b.x * prize.y - button_b.y * prize.x) // (button_b.x * button_a.y - button_b.y * button_a.x)

        if a * button_a.x + b * button_b.x == prize.x:
            result += 3 * a + b

    return result


def star2(data: list[str]) -> int:
    """
    >>> star2(["Button A: X+94, Y+34",
    ...        "Button B: X+22, Y+67",
    ...        "Prize: X=8400, Y=5400",
    ...        "",
    ...        "Button A: X+26, Y+66",
    ...        "Button B: X+67, Y+21",
    ...        "Prize: X=12748, Y=12176",
    ...        "",
    ...        "Button A: X+17, Y+86",
    ...        "Button B: X+84, Y+37",
    ...        "Prize: X=7870, Y=6450",
    ...        "",
    ...        "Button A: X+69, Y+23",
    ...        "Button B: X+27, Y+71",
    ...        "Prize: X=18641, Y=10279",])
    875318608908
    """

    result = 0

    for rows in l_split(data):
        m = re.match(r'^Button A: X\+(\d+), Y\+(\d+)$', rows[0])
        assert m
        button_a = Xy(int(m[1]), int(m[2]))
        m = re.match(r'^Button B: X\+(\d+), Y\+(\d+)$', rows[1])
        assert m
        button_b = Xy(int(m[1]), int(m[2]))
        m = re.match(r'^Prize: X=(\d+), Y=(\d+)$', rows[2])
        assert m
        prize = Xy(int(m[1]) + 10000000000000, int(m[2]) + 10000000000000)

        b = (button_a.x * prize.y - button_a.y * prize.x) // (button_a.x * button_b.y - button_a.y * button_b.x)
        a = (button_b.x * prize.y - button_b.y * prize.x) // (button_b.x * button_a.y - button_b.y * button_a.x)

        if a * button_a.x + b * button_b.x == prize.x:
            result += 3 * a + b

    return result


if __name__ == "__main__":
    run_day(13,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )
