package aoc2020.day06;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Day23 {
    private static final int STAR_2_MAX_CUP_VALUE = 1_000_000;

    public String star1(final String cupsDefinition, final int iterations) {
        final Cup rootCup = parseCups(cupsDefinition);
        crabGame(rootCup, iterations, 9);
        final Cup[] cupMap = createCupMap(rootCup, 9);

        Cup active = cupMap[1].getNextCup();
        StringBuilder result = new StringBuilder();
        while (active.getValue() != 1) {
            result.append(active.getValue());
            active = active.getNextCup();
        }

        return result.toString();
    }

    public long star2(final String cupsDefinition) {
        return star2(cupsDefinition, 10_000_000);
    }

    public long star2(final String cupsDefinition, final int iterations) {
        final Cup rootCup = parseCups(cupsDefinition);
        final Set<Integer> initialValues = new HashSet<>();
        Cup lastCup = rootCup;
        Cup currentCup = rootCup;

        while (!initialValues.contains(currentCup.getValue())) {

            initialValues.add(currentCup.getValue());
            lastCup = currentCup;
            currentCup = currentCup.getNextCup();
        }

        final int maxInitialValue = Collections.max(initialValues);
        final Cup[] initialCrabMap = createCupMap(rootCup, maxInitialValue);
        int extrasCounter = maxInitialValue + 1;
        Cup extrasRoot = null;
        Cup extrasLast = null;
        while (extrasCounter <= STAR_2_MAX_CUP_VALUE) {
            Cup newCup = new Cup(extrasCounter);
            extrasCounter++;

            if (extrasRoot == null) {
                extrasRoot = newCup;
            }

            if (extrasLast != null) {
                extrasLast.setNextCup(newCup);
            }

            extrasLast = newCup;
        }

        lastCup.insertAfter(extrasRoot);

        crabGame(rootCup, iterations, STAR_2_MAX_CUP_VALUE);

        Cup cup1 = initialCrabMap[1];

        return (long) cup1.getNextCup().getValue() * cup1.getNextCup().getNextCup().getValue();
    }

    public Cup parseCups(final String cupsDefinition) {
        Cup previous = null;
        Cup first = null;

        for (int i = 0; i < cupsDefinition.length(); ++i) {
            int value = Integer.parseInt(String.valueOf(cupsDefinition.charAt(i)));

            Cup current = new Cup(value);
            if (previous != null) {
                previous.setNextCup(current);
            }

            previous = current;
            if (first == null) {
                first = current;
            }
        }

        //noinspection ConstantConditions
        previous.setNextCup(first);

        return first;
    }

    public void crabGame(final Cup rootCup, final int iterations, final int maxValue) {
        final Cup[] cupMap = createCupMap(rootCup, maxValue);

        Cup currentCup = rootCup;
        for (int i = 0; i < iterations; ++i) {
            final Cup pluck3 = currentCup.pluckNext(3);

            // faster than set
            final int value1 = pluck3.getValue();
            final int value2 = pluck3.getNextCup().getValue();
            final int value3 = pluck3.getNextCup().getNextCup().getValue();

            int destination = dec(currentCup.getValue(), maxValue);
            while (destination == value1 || destination == value2 || destination == value3) {
                destination = dec(destination, maxValue);
            }

            cupMap[destination].insertAfter(pluck3);
            currentCup = currentCup.getNextCup();
        }

    }

    public int dec(final int val, final int cupCount) {
        final int result = val - 1;
        if (result > 0) {
            return result;
        }

        return result + cupCount;
    }

    public Cup[] createCupMap(final Cup rootCup, final int maxValue) {
        Cup[] result = new Cup[maxValue + 1];

        Cup currentCup = rootCup;
        while (result[currentCup.getValue()] == null) {
            result[currentCup.getValue()] = currentCup;
            currentCup = currentCup.getNextCup();
        }

        return result;
    }
}