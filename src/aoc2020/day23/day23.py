import sys
from datetime import datetime
from typing import Optional, List


class Cup:
    def __init__(self, value: int) -> None:
        self.value = value
        self.next_cup = None  # type: Optional[Cup]

    def insert_after(self, target: 'Cup') -> None:
        current_next = self.next_cup
        self.next_cup = target

        active = target
        while active.next_cup is not None:
            active = active.next_cup

        active.next_cup = current_next

    def pluck_next(self, count: int) -> 'Cup':
        pluck_root = self.next_cup
        active = self.next_cup

        for i in range(count - 1):
            active = active.next_cup

        self.next_cup = active.next_cup
        active.next_cup = None

        return pluck_root

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return "%d" % self.value


def parse_cups(cups_definition: str) -> Cup:
    previous = None  # type: Optional[Cup]
    first = None  # type: Optional[Cup]
    for cup_id in list(cups_definition):
        cup_id = int(cup_id)

        current = Cup(cup_id)
        if previous:
            previous.next_cup = current

        previous = current
        if first is None:
            first = current

    previous.next_cup = first

    return first


def crab_game(root_cup: Cup, iterations: int, debug: bool, max_value: int) -> Cup:
    cup_map = create_cup_map(root_cup, max_value)
    current_cup = root_cup

    for i in range(iterations):
        if debug:
            dump(current_cup)

        pluck_3 = current_cup.pluck_next(3)
        pick_up_values = (
            pluck_3.value,
            pluck_3.next_cup.value,
            pluck_3.next_cup.next_cup.value,
        )
        if debug:
            print("pick up: %d, %d, %d" % pick_up_values)

        destination = dec(current_cup.value, max_value)
        while destination in pick_up_values:
            destination = dec(destination, max_value)

        if debug:
            print("destination: %d" % destination)
            print()

        cup_map[destination].insert_after(pluck_3)
        current_cup = current_cup.next_cup

    return current_cup


def create_cup_map(root_cup: Cup, max_value: int) -> List[Cup]:
    # noinspection PyTypeChecker
    cup_map = [None] * (max_value + 1)  # type: List[Cup]

    current_cup = root_cup
    while cup_map[current_cup.value] is None:
        cup_map[current_cup.value] = current_cup
        current_cup = current_cup.next_cup

    return cup_map


def star1(cups_definition: str, iterations: int, debug=False) -> str:
    """
    >>> star1('389125467', 10)
    '92658374'

    >>> star1('389125467', 100)
    '67384529'
    """

    root_cup = parse_cups(cups_definition)
    current_cup = crab_game(root_cup, iterations, debug, 9)
    cup_map = create_cup_map(root_cup, 9)

    if debug:
        print("-- final --")
        dump(current_cup)

    result = ''

    active = cup_map[1].next_cup
    while active.value != 1:
        result += str(active.value)
        active = active.next_cup
    return result


def dec(val: int, cup_count: int) -> int:
    return (val - 2) % cup_count + 1


def dump(current_cup: Cup):
    processed = set()
    active = current_cup
    first = True
    print("cups: ", end="")
    while active.value not in processed:
        if first:
            print("(%d)" % active.value, end='')

        else:
            print(" %d " % active.value, end='')

        processed.add(active.value)
        first = False
        active = active.next_cup

    print()


def star2(cups_definition: str, iterations=10_000_000) -> int:
    """
    >>> star2('389125467')
    149245887792
    """

    root_cup = parse_cups(cups_definition)
    initial_crab_map = create_cup_map(root_cup, 9)

    initial_values = set()
    last_cup = current_cup = root_cup

    while True:
        if current_cup.value in initial_values:
            break

        initial_values.add(current_cup.value)
        last_cup = current_cup
        current_cup = current_cup.next_cup

    max_initial_value = max(initial_values)
    extras_counter = max_initial_value + 1
    extras_root = None  # type: Optional[Cup]
    extras_last = None  # type: Optional[Cup]
    while extras_counter <= 1_000_000:
        new_cup = Cup(extras_counter)
        extras_counter += 1

        if extras_root is None:
            extras_root = new_cup

        if extras_last is not None:
            extras_last.next_cup = new_cup

        extras_last = new_cup

    last_cup.insert_after(extras_root)

    crab_game(root_cup, iterations, False, 1_000_000)
    cup_1 = initial_crab_map[1]

    return cup_1.next_cup.value * cup_1.next_cup.next_cup.value


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'perftest':
        print('PERFTEST using interpreter "%s"' % sys.executable)
        print('Version %s' % sys.version)

        for iterations in (1, 5, 10, 20, 30, 50, 75, 100):
            start = datetime.now()
            result = star2('853192647', iterations=iterations * 1_000_000)
            print('Finished %3dM in %s [result=%d]' % (iterations, datetime.now() - start, result))

    else:

        print('Day 23 Star 1: %s' % star1('853192647', 100, debug=False))
        print('Day 23 Star 2: %s' % star2('853192647'))
