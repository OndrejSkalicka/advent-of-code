from util.data import load_as_str, load_strings_newline, txt_fn
from util.perf import run_day


def star1(data: list[str]) -> int:
    """
    >>> star1(["1000",
    ...       "2000",
    ...       "3000",
    ...       "",
    ...       "4000",
    ...       "",
    ...       "5000",
    ...       "6000",
    ...       "",
    ...       "7000",
    ...       "8000",
    ...       "9000",
    ...       "",
    ...       "10000"])
    24000
    """

    elves = []
    current = 0
    data = data + ['']
    for calorie in data:
        if calorie == '':
            elves.append(current)
            current = 0
            continue

        current += int(calorie)

    return max(elves)


def star2(data: list[str]) -> int:
    """
    >>> star2(["1000",
    ...       "2000",
    ...       "3000",
    ...       "",
    ...       "4000",
    ...       "",
    ...       "5000",
    ...       "6000",
    ...       "",
    ...       "7000",
    ...       "8000",
    ...       "9000",
    ...       "",
    ...       "10000"])
    45000
    """

    elves = []
    current = 0
    data = data + ['']
    for calorie in data:
        if calorie == '':
            elves.append(current)
            current = 0
            continue

        current += int(calorie)

    elves.sort(reverse=True)

    return sum(elves[:3])


def star1_oneliner(data: str) -> int:
    """
    >>> star1_oneliner("1000\\n"
    ...                "2000\\n"
    ...                "3000\\n"
    ...                "\\n"
    ...                "4000\\n"
    ...                "\\n"
    ...                "5000\\n"
    ...                "6000\\n"
    ...                "\\n"
    ...                "7000\\n"
    ...                "8000\\n"
    ...                "9000\\n"
    ...                "\\n"
    ...                "10000")
    24000
    """

    return max(sum(int(calorie) for calorie in snack_list) for snack_list in (snack_str.split("\n") for snack_str in data.split("\n\n")))


def star2_oneliner(data: str) -> int:
    """
    >>> star2_oneliner("1000\\n"
    ...                "2000\\n"
    ...                "3000\\n"
    ...                "\\n"
    ...                "4000\\n"
    ...                "\\n"
    ...                "5000\\n"
    ...                "6000\\n"
    ...                "\\n"
    ...                "7000\\n"
    ...                "8000\\n"
    ...                "9000\\n"
    ...                "\\n"
    ...                "10000")
    45000
    """

    return sum(
        sorted(
            [sum(int(calorie) for calorie in snack_list) for snack_list in (snack_str.split("\n") for snack_str in data.split("\n\n"))],
            reverse=True)[:3])


if __name__ == "__main__":
    run_day(1,
            lambda: star1(load_strings_newline(txt_fn(__file__))),
            lambda: star2(load_strings_newline(txt_fn(__file__))),
            )

    run_day(1,
            lambda: star1_oneliner(load_as_str(txt_fn(__file__))),
            lambda: star2_oneliner(load_as_str(txt_fn(__file__))),
            )
