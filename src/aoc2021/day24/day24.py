import logging
import os
import random
from typing import List
from typing import Union

from util.logger_config import init_logger

CHECK_ITERS = 1_000

logger = logging.getLogger(__name__)


class Memory:

    def __init__(self) -> None:
        self.w = 0
        self.x = 0
        self.y = 0
        self.z = 0

    def set(self, key: str, value: int):
        if key == 'w':
            self.w = value
        elif key == 'x':
            self.x = value
        elif key == 'y':
            self.y = value
        elif key == 'z':
            self.z = value
        else:
            raise KeyError("Wrong key %s" % key)

    def get(self, key: Union[str, int]) -> int:
        if isinstance(key, int):
            return key

        if key == 'w':
            return self.w
        elif key == 'x':
            return self.x
        elif key == 'y':
            return self.y
        elif key == 'z':
            return self.z
        else:
            raise KeyError("Wrong key %s" % key)

    def __repr__(self) -> str:
        return 'x=%-4d y=%-4d z=%-4d w=%-4d' % (self.x, self.y, self.z, self.w)


class Input:

    def __init__(self, data: List[int]) -> None:
        self.data = data
        self.index = 0

    def next(self):
        value = self.data[self.index]
        self.index += 1
        return value


class Instruction:

    def __init__(self, command: str, a: str, b: Union[str, int] = None) -> None:
        self.command = command
        self.a = a
        self.b = b

        if self.b is not None and self.b not in ('x', 'y', 'z', 'w'):
            self.b = int(self.b)

    def exec(self, memory: Memory, input: Input):
        if self.command == 'inp':
            memory.set(self.a, input.next())

        elif self.command == 'add':
            memory.set(self.a, memory.get(self.a) + memory.get(self.b))

        elif self.command == 'mul':
            memory.set(self.a, memory.get(self.a) * memory.get(self.b))

        elif self.command == 'div':
            memory.set(self.a, memory.get(self.a) // memory.get(self.b))

        elif self.command == 'mod':
            memory.set(self.a, memory.get(self.a) % memory.get(self.b))

        elif self.command == 'eql':
            memory.set(self.a, 1 if memory.get(self.a) == memory.get(self.b) else 0)

        elif self.command == 'SET':
            memory.set(self.a, memory.get(self.b))

        else:
            raise KeyError("Uknown instruction %s" % self.command)

    def __repr__(self) -> str:
        return '%s %s %s' % (self.command, self.a, '' if self.b is None else self.b)


def load_data(fn: str = 'data.txt') -> List[Instruction]:
    result = []

    with open(fn, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()

            if '#' in line:
                idx = line.index('#')
                line = line[:idx].strip()

            if line:
                result.append(Instruction(*(line.split())))

    return result


def test_data(fn='data-test.txt'):
    return load_data(os.path.dirname(os.path.realpath(__file__)) + '/' + fn)


def alu(instructions: List[Instruction], memory: Memory, input: Input):
    """
    >>> m = Memory(); alu(test_data('data-test-negate-write.txt'), m, Input([5])); m.x
    -5
    >>> m = Memory(); alu(test_data('data-test-three-times-larger.txt'), m, Input([2, 6])); m.z
    1
    >>> m = Memory(); alu(test_data('data-test-three-times-larger.txt'), m, Input([2, 7])); m.z
    0
    >>> m = Memory(); alu(test_data('data-test-binary.txt'), m, Input([5])); m.z; m.y; m.x; m.w
    1
    0
    1
    0
    """
    logger.debug("Initial  : %s" % memory)
    for instruction in instructions:
        instruction.exec(memory, input)
        logger.debug("%-9s: %s" % (instruction, memory))


def star1(instructions: List[Instruction]) -> int:
    instruction_blocks = []
    active_block = []

    for instruction in instructions:

        if instruction.command == 'inp':
            active_block = []
            instruction_blocks.append(active_block)

        active_block.append(instruction)

    # max_setup = [
    #     9,  # 0: must be #1 + 1
    #     4,  # 1: must be #12 - 5
    #     9,  # 2: must be #11
    #     9,  # 3: must be #8 + 8
    #     2,  # 4: must be #5 - 7
    #     9,  # 5: must be #4 + 7
    #     9,  # 6: must be #7 + 5
    #     4,  # 7: must be #6 - 5
    #     1,  # 8: must be #3 - 8
    #     9,  # 9: must be #10 + 4
    #     5,  # 10: must be #9 - 4
    #     9,  # 11: must be #2
    #     9,  # 12: must be #1 + 5
    #     8,  # 13: must be #0 - 1
    # ]
    min_setup = [
        2,  # 0: must be #13 + 1
        1,  # 1: must be #12 - 5
        1,  # 2: must be #11
        9,  # 3: must be #8 + 8
        1,  # 4: must be #5 - 7
        8,  # 5: must be #4 + 7
        6,  # 6: must be #7 + 5
        1,  # 7: must be #6 - 5
        1,  # 8: must be #3 - 8
        5,  # 9: must be #10 + 4
        1,  # 10: must be #9 - 4
        1,  # 11: must be #2
        6,  # 12: must be #1 + 5
        1,  # 13: must be #0 - 1
    ]

    manual_data = min_setup

    mem = Memory()
    input = Input(manual_data)

    for block_id in range(len(manual_data)):
        alu(instruction_blocks[block_id], mem, input)

        logger.debug("")
        logger.debug("STEP %d FINISHED" % (block_id + 1))
        logger.debug("")

    logger.debug("Input: %s" % (''.join(str(s) for s in manual_data)))

    # verify manual vs original
    all_ok = True
    manual_mode = False
    fail_fast = True
    for _ in range(CHECK_ITERS):
        data = []
        for _ in range(len(manual_data)):
            data.append(random.randint(1, 9))

        if manual_mode:
            data = manual_data

        reduced_instructions = []
        for i in range(len(data)):
            reduced_instructions += instruction_blocks[i]

        mem = Memory()
        alu(reduced_instructions, mem, Input(data))

        # s1
        z = data[0]

        # s2
        z = 26 * z + 6 + data[1]

        # s3
        z = 26 * z + 4 + data[2]

        # s4
        z = 26 * z + 2 + data[3]

        # s5
        z = 26 * z + 9 + data[4]

        # s6
        z = z // 26
        if data[4] + 7 != data[5]:
            z = z * 26 + 1 + data[5]

        # s7
        z = 26 * z + 10 + data[6]

        # s8
        z = z // 26
        if data[6] - 5 != data[7]:
            z = z * 26 + 6 + data[7]

        # s9
        tmp_z = z
        z = z // 26
        # note that this is also data[3] - 8 != data[8]
        if tmp_z % 26 - 10 != data[8]:
            z = z * 26 + 4 + data[8]

        # s10
        z = z * 26 + 6 + data[9]

        # s11
        z = z // 26
        if data[9] - 4 != data[10]:
            z = z * 26 + 3 + data[10]

        # s12
        tmp_z = z
        z = z // 26
        # note that this is also data[2] != data[11]
        if tmp_z % 26 - 4 != data[11]:
            z = z * 26 + 9 + data[11]

        # s13
        tmp_z = z
        z = z // 26
        # note that this is also data[2] != data[11]
        if tmp_z % 26 - 1 != data[12]:
            z = z * 26 + 15 + data[12]

        # s14
        tmp_z = z
        z = z // 26
        # note that this is also data[2] != data[11]
        if tmp_z % 26 - 1 != data[13]:
            z = z * 26 + 5 + data[13]

        if mem.z != z:
            logger.error("FAIL comparison: %d != %s" % (z, mem))
            all_ok = False
            if fail_fast:
                break

        if manual_mode:
            break

    if all_ok:
        logger.info("All checks ok :-)")

    return 0


if __name__ == "__main__":
    init_logger()

    star1(load_data())
