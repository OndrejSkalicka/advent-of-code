from typing import List, Dict


def load_data(fn: str) -> List[int]:
    with open(fn, 'r') as fh:
        return list(int(line.strip()) for line in fh.readlines())


def star1(data: List[int]) -> int:
    """
    >>> star1([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    35

    >>> star1([28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3])
    220
    """

    data.sort()
    joltage = 0
    distribution = {
        1: 0,
        2: 0,
        3: 0
    }
    for adapter in data:
        dj = adapter - joltage
        joltage = adapter
        distribution[dj] += 1

    return distribution[1] * (1 + distribution[3])


def star2(data: List[int]) -> int:
    """
    >>> star2([1, 4, 5, 6])
    2

    >>> star2([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    8

    >>> star2([28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3])
    19208
    """

    data.sort()
    cache = {}

    return no_combinations(0, 0, max(data) + 3, data, cache)


def no_combinations(joltage: int, index: int, device_joltage: int, data: List[int], cache: Dict[str, int]) -> int:
    key = '%d-%d' % (joltage, index)
    if key in cache:
        return cache[key]

    result = 0

    while index < len(data) and data[index] <= joltage + 3:
        result += no_combinations(data[index], index + 1, device_joltage, data, cache)
        index += 1

    if joltage + 3 >= device_joltage:
        result += 1

    cache[key] = result
    return result


if __name__ == "__main__":
    my_data = load_data('data.txt')
    print('Day 10 Star 1: %d' % star1(my_data))
    print('Day 10 Star 2: %d' % star2(my_data))
