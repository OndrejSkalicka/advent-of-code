import re
from typing import List, Tuple


def load_data(fn: str) -> Tuple[List[Tuple[str, List[Tuple[int, int]]]], List[int], List[List[int]]]:
    rules = []
    my_ticket = []
    nearby_tickets = []

    with open(fn, 'r') as fh:
        phase = 0
        for line in fh.readlines():
            line = line.strip()

            if not line:
                continue

            if line == 'your ticket:':
                phase = 1
                continue

            if line == 'nearby tickets:':
                phase = 2
                continue

            if phase == 0:
                m = re.match(r'^([\w ]+): (\d+)-(\d+) or (\d+)-(\d+)$', line)
                rules.append((m[1], [(int(m[2]), int(m[3])), (int(m[4]), int(m[5]))]))

                continue

            if phase == 1:
                my_ticket = list(int(x) for x in line.split(','))

            if phase == 2:
                nearby_tickets.append(list(int(x) for x in line.split(',')))

    return rules, my_ticket, nearby_tickets


def star1(rules: List[Tuple[str, List[Tuple[int, int]]]], my_ticket: List[int], nearby_tickets: List[List[int]]) -> int:
    """
    >>> star1([('class', [(1, 3), (5, 7)]), ('row', [(6, 11), (33, 44)]), ('seat', [(13, 40), (45, 50)]), ],
    ...       [7, 1, 14],
    ...       [[7, 3, 47], [40, 4, 50], [55, 2, 20], [38, 6, 12]], )
    71
    """

    invalid = []
    for ticket in nearby_tickets:
        for number in ticket:
            if not number_valid(number, rules):
                invalid.append(number)
    return sum(invalid)


def star2(rules: List[Tuple[str, List[Tuple[int, int]]]], my_ticket: List[int], nearby_tickets: List[List[int]]) -> int:
    valid_tickets = []
    for ticket in nearby_tickets:
        valid = True
        for number in ticket:
            if not number_valid(number, rules):
                valid = False
                break

        if valid:
            valid_tickets.append(ticket)

    valid_tickets.append(my_ticket)

    rules_valid_for = []
    for rule in rules:
        valid_for = []
        for i in range(len(my_ticket)):
            valid = True
            for ticket in valid_tickets:
                if not number_valid_single(ticket[i], rule):
                    valid = False
                    break

            if valid:
                valid_for.append(i)

        rules_valid_for.append((rule[0], valid_for))

    rules_valid_for.sort(key=lambda x: len(x[1]))
    rules_mapping = {}
    for rule_valid_for in rules_valid_for:
        value = rule_valid_for[1][0]
        rules_mapping[rule_valid_for[0]] = value
        for rvf in rules_valid_for:
            if value in rvf[1]:
                rvf[1].remove(value)

    result = 1
    for rule, mapping in rules_mapping.items():
        if rule.startswith('departure '):
            result *= my_ticket[mapping]

    return result


def number_valid(number, rules: List[Tuple[str, List[Tuple[int, int]]]]) -> bool:
    for rule in rules:
        for num_range in rule[1]:
            if num_range[0] <= number <= num_range[1]:
                return True

    return False


def number_valid_single(number, rule: Tuple[str, List[Tuple[int, int]]]) -> bool:
    for num_range in rule[1]:
        if num_range[0] <= number <= num_range[1]:
            return True

    return False


if __name__ == "__main__":
    rules, my_ticket, nearby_tickets = load_data('data.txt')
    print('Day 16 Star 1: %d' % star1(rules, my_ticket, nearby_tickets))
    print('Day 16 Star 2: %d' % star2(rules, my_ticket, nearby_tickets))
