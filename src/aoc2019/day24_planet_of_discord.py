import logging
from collections import defaultdict
from typing import Iterable

import numpy as np

from util.data import to_2d_np_char, load_as_str
from util.dtype import np_array_2d_str
from util.logger_config import init_logger
from util.perf import run_day
from util.record import Xy

logger = logging.getLogger(__name__)

DIRECTIONS: list[Xy] = [
    Xy(0, -1),
    Xy(0, 1),
    Xy(-1, 0),
    Xy(1, 0),
]

DIMENSION = 5


def star1(initial: str) -> int:
    """
    >>> star1("....#\\n"
    ...       "#..#.\\n"
    ...       "#..##\\n"
    ...       "..#..\\n"
    ...       "#....")
    2129920

    """
    current: np_array_2d_str = to_2d_np_char(initial)

    seen_layouts: set[str] = set()

    while True:
        current_as_str = _as_str(current)
        if current_as_str in seen_layouts:
            result = 0
            # noinspection PyTypeChecker
            for y, x in np.ndindex(current.shape):
                if current[y, x] == '#':
                    result += 2 ** (y * current.shape[0] + x)

            return result
        seen_layouts.add(current_as_str)

        future = current.copy()
        # noinspection PyTypeChecker
        for y, x in np.ndindex(current.shape):
            xy = Xy(x, y)
            adjacent_bugs = _adj_bugs(current, xy)
            if future[xy.np_yx()] == '#' and adjacent_bugs != 1:
                # bug dies
                future[xy.np_yx()] = '.'

            elif future[xy.np_yx()] == '.' and adjacent_bugs in (1, 2):
                # bug rises
                future[xy.np_yx()] = '#'

        current = future


def _as_str(area: np_array_2d_str) -> str:
    result = ""
    for y in range(DIMENSION):
        for x in range(DIMENSION):
            result += area[y, x]
        result += "\n"

    return result


def _adj_bugs(area: np_array_2d_str, xy: Xy) -> int:
    result = 0

    for dx in DIRECTIONS:
        nxy = xy + dx
        if nxy.x < 0 or nxy.x >= DIMENSION:
            continue
        if nxy.y < 0 or nxy.y >= DIMENSION:
            continue

        if area[nxy.np_yx()] == '#':
            result += 1

    return result


def star2(initial: str, iterations: int = 200) -> int:
    """
    >>> star2("....#\\n"
    ...       "#..#.\\n"
    ...       "#..##\\n"
    ...       "..#..\\n"
    ...       "#....", iterations=10)
    99
    """
    multiverse: dict[int, np_array_2d_str] = defaultdict(lambda: np.full([DIMENSION, DIMENSION], '.'))
    multiverse[0] = to_2d_np_char(initial)

    for _ in range(iterations):
        new_multiverse: dict[int, np_array_2d_str] = defaultdict(lambda: np.full([DIMENSION, DIMENSION], '.'))

        for level in range(min(multiverse.keys()) - 1, max(multiverse.keys()) + 2):
            # noinspection PyTypeChecker
            for y in range(DIMENSION):
                for x in range(DIMENSION):
                    if x == y == 2:
                        continue

                    xy = Xy(x, y)

                    adjacent_bugs = 0
                    for tile_xy, tile_level in _adjacent_multi_tiles(xy, level):
                        if multiverse[tile_level][tile_xy.np_yx()] == '#':
                            adjacent_bugs += 1

                    if multiverse[level][xy.np_yx()] == '#' and adjacent_bugs != 1:
                        # bug dies
                        new_multiverse[level][xy.np_yx()] = '.'

                    elif multiverse[level][xy.np_yx()] == '.' and adjacent_bugs in (1, 2):
                        # bug rises
                        new_multiverse[level][xy.np_yx()] = '#'
                    else:
                        new_multiverse[level][xy.np_yx()] = multiverse[level][xy.np_yx()]

        for level in list(new_multiverse.keys()):
            if np.sum(new_multiverse[level] == '#') == 0:
                del new_multiverse[level]

        multiverse = new_multiverse

    return sum(np.sum(multiverse[level] == '#') for level in multiverse.keys())


def _adjacent_multi_tiles(src_xy: Xy, level: int) -> Iterable[tuple[Xy, int]]:
    """
    >>> list(_adjacent_multi_tiles(Xy(3, 3), 0))
    [(Xy(x=3, y=2), 0), (Xy(x=3, y=4), 0), (Xy(x=2, y=3), 0), (Xy(x=4, y=3), 0)]

    >>> list(_adjacent_multi_tiles(Xy(1, 1), 0))
    [(Xy(x=1, y=0), 0), (Xy(x=1, y=2), 0), (Xy(x=0, y=1), 0), (Xy(x=2, y=1), 0)]

    >>> list(_adjacent_multi_tiles(Xy(3, 2), 0))
    [(Xy(x=3, y=1), 0), (Xy(x=3, y=3), 0), (Xy(x=4, y=0), 1), (Xy(x=4, y=1), 1), (Xy(x=4, y=2), 1), (Xy(x=4, y=3), 1), (Xy(x=4, y=4), 1), (Xy(x=4, y=2), 0)]


    """
    for dx in DIRECTIONS:
        nxy = src_xy + dx

        # center underflow
        if nxy == Xy(2, 2):
            match src_xy:
                # underflow from left
                case Xy(1, 2):
                    for y in range(DIMENSION):
                        yield Xy(0, y), level + 1

                # underflow from right
                case Xy(3, 2):
                    for y in range(DIMENSION):
                        yield Xy(4, y), level + 1

                # underflow from top
                case Xy(2, 1):
                    for x in range(DIMENSION):
                        yield Xy(x, 0), level + 1

                # underflow from bottom
                case Xy(2, 3):
                    for x in range(DIMENSION):
                        yield Xy(x, 4), level + 1

                case _:
                    raise Exception(f"Cannot reach {nxy} from {src_xy}")

        # overflow left
        elif nxy.x == -1:
            yield Xy(1, 2), level - 1

        # overflow right
        elif nxy.x == DIMENSION:
            yield Xy(3, 2), level - 1

        # overflow top
        elif nxy.y == -1:
            yield Xy(2, 1), level - 1

        # overflow bottom
        elif nxy.y == DIMENSION:
            yield Xy(2, 3), level - 1

        else:
            yield nxy, level


if __name__ == "__main__":
    init_logger(logging.DEBUG)

    run_day(23,
            lambda: star1(load_as_str('day24_planet_of_discord.txt')),
            lambda: star2(load_as_str('day24_planet_of_discord.txt')),
            )
